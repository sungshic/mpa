__author__ = 'spark'

import numpy as np

#import scikits.cuda.cula as cula
from scikits.cuda.linalg import cula as cula
import pycuda.gpuarray as gpuarray
import pycuda.autoinit
import scipy
import scipy.sparse

from scipy.linalg import lu_factor
from scipy.linalg import lu

def get_perm_matrix(pivotList, rdim):
    pLen = len(pivotList)
    P = np.empty((rdim,rdim))
    I = np.eye(rdim)
    permList = np.arange(0,rdim)

    # convert pivot exchange list (in CULA convention) into a row idx permutation list
    for i in range(0,pLen):
        temp = permList[i]
        permList[i] = permList[pivotList[i]-1]
        permList[pivotList[i]-1] = temp

    for i in range(0,rdim):
        P[i,:] = I[permList[i],:]

    # now P is in column major as the input 'pivotList' is from LAPACK, and we need to go back to row major order by returning P.T
    return P.T

def truncate_matrix_to_dim(a, rowdim, coldim):
    [n,m] = a.shape

    if (rowdim <= n and coldim < m) or (rowdim < n and coldim <= m):
        trunc_a = a[:rowdim, :coldim]
        return trunc_a
    else:
        return a

def pad_matrix_to_dim(a, rowdim, coldim):
    [n,m] = a.shape

    if (rowdim >= n and coldim > m) or (rowdim > n or coldim >= m):
        padded_a = np.zeros((rowdim, coldim))
        padded_a[:n, :m] = a
        return padded_a
    else:
        return a

def pad_matrix_to_nearest_256s(a):
    [n,m] = a.shape
    n_padded = (n/256 + 1)*256
    m_padded = (m/256 + 1)*256

    padded_a = np.zeros((n_padded,m_padded))
    padded_a[:n,:m] = a

    return padded_a

#a=np.array([[1,2,3,4],[6,7,8,9],[7,2,3,5],[2,4,5,6]], dtype=np.float32)
#a=np.array([[3,6,7,5],[3,5,6,2],[9,1,2,7],[0,9,3,6]], dtype=np.float32)
#a = np.array([[3,3,0],[0,2,2],[1,0,1]], dtype=np.float32)
N = 6000
#a = np.random.random_floats(-100,100,size=(N,N),dtype=np.float32)
#### dense random matrix
#a = np.float32(np.random.randn(1391,1707))
#a = np.float32(np.random.randn(2047,2048))
#### sparse random matrix
'''
N = 10 ** 3

density = 1e-3
ij = np.random.randint(N, size=(2, N * N * density))
data = np.random.rand(N * N * density)
a = scipy.sparse.coo.coo_matrix((data, ij), (N, N), dtype=np.float32)
a = a.toarray()
'''


#### loading a saved matrix
a = np.load('testmat.npy')
#a = pad_matrix_to_nearest_256s(a)

#a = pad_matrix_to_dim(a, 2048, 2048)
#m=n=2048

[m,n]=a.shape

'''
if n > m:
    a = pad_matrix_to_dim(a, n, n)
    m = n
'''

a = np.float32(a)
lda=m

ipiv=np.empty(min(m,n),dtype=np.int32)
ipiv_gpu=gpuarray.to_gpu(ipiv)

a_gpu = gpuarray.to_gpu(a.flatten('F')) # flatten 'a' matrix into Fortran's column major order

#a2_cpu = lu_factor(a)[0]

[P,L,U] = lu(a)

cula.culaInitialize()
cula.culaDeviceSgetrf(m,n,a_gpu.gpudata,lda,ipiv_gpu.gpudata)
#cula.culaDeviceSgeTransposeInplace(n,a_gpu.gpudata,)
#cula.culaDeviceSgeTransposeInplace(n,a_gpu.gpudata,n)
cula.culaShutdown()
a2 = a_gpu.get().reshape(n,-1).T
ipiv_cpu = ipiv_gpu.get()


p1 = get_perm_matrix(ipiv_cpu, m)
#l1_mask = np.array([[0,0,0,0],[1,0,0,0],[1,1,0,0],[1,1,1,0]])
#u1_mask = np.array([[1,1,1,1],[0,1,1,1],[0,0,1,1],[0,0,0,1]])
#l1 = l1_mask*a2+np.eye(len(ipiv_cpu))
#u1 = u1_mask*a2

if m > n:
    l1 = scipy.linalg.tril(a2,-1) + pad_matrix_to_dim(np.eye(len(ipiv_cpu)), m, n)
else:
    l1 = scipy.linalg.tril(a2,-1)+np.eye(len(ipiv_cpu)) # unverified code

if m > n:
    u1 = scipy.linalg.triu(truncate_matrix_to_dim(a2,n,n))
else:
    u1 = scipy.linalg.triu(a2) # unverified code


print np.dot(np.dot(p1,l1),u1)
print a2




__author__ = 'spark'

import numpy
from sparseNull import sparseNullCPU
from cobra.io.sbml import *
#from cobra import Reaction, Metabolite

def null(A, eps=1e-15):
    """
    http://mail.scipy.org/pipermail/scipy-user/2005-June/004650.html
    """
    u, s, vh = numpy.linalg.svd(A)
    n = A.shape[1]   # the number of columns of A
    if len(s)<n:
        expanded_s = numpy.zeros(n, dtype = s.dtype)
        expanded_s[:len(s)] = s
        s = expanded_s
    null_mask = (s <= eps)
    null_space = numpy.compress(null_mask, vh, axis=0)
    return numpy.transpose(null_space)


# python port of the matlab function addLoopLawConstraints() by Jan Schellenberger
# utilizing cobraToolbox data format
#INPUT
# LPproblem Structure containing the following fields
#  A      LHS matrix
#  b      RHS vector
#  c      Objective coeff vector
#  lb     Lower bound vector
#  ub     Upper bound vector
#  osense Objective sense (-1 max, +1 min)
#  csense Constraint senses, a string containting the constraint sense for
#         each row in A ('E', equality, 'G' greater than, 'L' less than).
#  F (optional)  If *QP problem
#  vartype (optional) if MI*P problem
# model     The cobraToolbox model for which the loops should be removed
#
#OPTIONAL INPUT
# rxnIndex The index of variables in LPproblem corresponding to fluxes.
#     default = [1:n]
#
#
#OUTPUT
# Problem structure containing the following fields describing an MILP problem
# A, b, c, lb, ub - same as before but longer
# vartype - variable type of the MILP problem ('C', and 'B')
# x0 = [] Needed for solveMILPproblem
#
def addLoopLawConstraintsCOBRA(model, rxnIndex=None):
    #method = 2 # methd = 1 - separete af,ar;  method = 2 - only af;  method 3 - same as method 2 except use b_L, b_U instad of b and csense;
    reduce_vars = 0 # eliminates additional integer variables.  Should be faster in all cases but in practice may not be for some weird reason.
    combine_vars = 0 # combines flux coupled reactions into one variable.  Should be faster in all cases but in practice may not be.

    if rxnIndex == None:
        rxnIndex = numpy.array(range(0, model.S.shape[1]))
    else:
        rxnIndex = numpy.array(rxnIndex)

    S = model.S.toarray()
    [m,n] = S.shape # LPproblem.A.shape

    nontransport = numpy.array([sum(S != 0) > 1]) #reactions which are not transport reactions.
    nontransport = (nontransport | ((model.lower_bounds ==0) & (model.upper_bounds == 0)))

    reverse_reactions = [x for x in model.reactions
                         if x.reflection is not None and
                         x.id.endswith('_reverse')]
    rxn_indicator_dict = {}

    if reduce_vars == 1:
        active1 = ~((model.lower_bounds ==0) & (model.upper_bounds == 0))

        S2 = S[:,active1] # exclude rxns with both ub and lb equal to 0
        N2 = sparseNullCPU(S2, tol=1e-15) #, tol=1e-15)

        if N2.ndim == 1:
            N2_col_len = 1
        else:
            N2_col_len = N2.shape[1]


        N = numpy.zeros((len(active1), N2_col_len))
        N[active1,:] = N2

        active = numpy.any(abs(N) > 1e-15, axis=1) # exclude rxns not in null space

        nontransport = nontransport & active

    Sn = S[:,nontransport[0]]

    Ninternal = sparseNullCPU(Sn) #, tol=1e-15)
    #Ninternal = null(Sn,'r')
    linternal = Ninternal.shape[1]

    nint = len(numpy.where(nontransport)[0])

    # model already has colnames for internal rxns, so update only what's being newly added here
    # col names for ccol_a_0, ccol_a_1, ... ccol_a_n
    # col names for ccol_G_0, ccol_G_1, ... ccol_G_n

    the_constraint_mets = []
    the_constraint_rxns = []
    the_metabolic_rxns_internal = []
    the_G_rxns = []
    for idx in range(0,nint):
        # get a reference to the current metabolic reaction of interest (i.e. v_i)
        cur_met_rxn = model.reactions[rxnIndex[idx]]
        the_metabolic_rxns_internal.append(cur_met_rxn)
        # new reactions needed for the constraints
        constraint_rxn_a = Reaction(name='ccol_a_'+str(idx))
        rxn_indicator_dict[cur_met_rxn] = constraint_rxn_a # build a lookup for the binary constraint for post-processing

        # a_i lower_bound at 0 and upper_bound at 1
        constraint_rxn_a.lower_bound = 0
        constraint_rxn_a.upper_bound = 1
        constraint_rxn_a.variable_kind = 'integer' # essentially a binary variable in this case
        the_constraint_rxns.append(constraint_rxn_a)
        constraint_rxn_G = Reaction(name='ccol_G_'+str(idx))
        # G_i lower_bound at -1000 and upper_bound at 1000
        constraint_rxn_G.lower_bound = -1000
        constraint_rxn_G.upper_bound = 1000
        constraint_rxn_G.variable_kind = 'continuous' # default value, explicitly written for clarity
        the_constraint_rxns.append(constraint_rxn_G)
        the_G_rxns.append(constraint_rxn_G)

        # construct for v - 1000*a_i < 0
        constraint_id = 'cvar_a_ul_constraint_' + str(idx)
        constraint_met = Metabolite(id=constraint_id)
        constraint_met._constraint_sense = 'L' # <
        constraint_met._bound = 0
        the_constraint_mets.append(constraint_met)
        # add the constraint coeffs to the relevant reactions
        cur_met_rxn.add_metabolites({constraint_met: 1}) # v
        constraint_rxn_a.add_metabolites({constraint_met:-1000}) # - 1000*a_i

        # construct for v - 1000*a_i > -1000
        constraint_id = 'cvar_a_ll_constraint_' + str(idx)
        constraint_met = Metabolite(id=constraint_id)
        constraint_met._constraint_sense = 'G' # >
        constraint_met._bound = -1000
        the_constraint_mets.append(constraint_met)
        # add the constraint coeffs to the relevant reactions
        cur_met_rxn.add_metabolites({constraint_met: 1}) # v
        constraint_rxn_a.add_metabolites({constraint_met:-1000}) # - 1000*a_i

        # construct for G + 1001*a_i < 1000
        constraint_id = 'cvar_G_ul_constraint_' + str(idx)
        constraint_met = Metabolite(id=constraint_id)
        constraint_met._constraint_sense = 'L' # <
        constraint_met._bound = 1000
        the_constraint_mets.append(constraint_met)
        # add the constraint coeffs to the relevant reactions
        constraint_rxn_G.add_metabolites({constraint_met: 1})
        constraint_rxn_a.add_metabolites({constraint_met: 1001})

        # construct for G + 1001*a_i > 1
        constraint_id = 'cvar_G_ll_constraint_' + str(idx)
        constraint_met = Metabolite(id=constraint_id)
        constraint_met._constraint_sense = 'G' # <
        constraint_met._bound = 1
        the_constraint_mets.append(constraint_met)
        # add the constraint coeffs to the relevant reactions
        constraint_rxn_G.add_metabolites({constraint_met: 1})
        constraint_rxn_a.add_metabolites({constraint_met: 1001})

    # constructs for N*G = 0
    the_NdotG_vars = []
    for N_idx in range(0, linternal):
        constraint_id = 'cvar_NdotG_constraint_' + str(N_idx)
        constraint_met = Metabolite(id=constraint_id)
        constraint_met._constraint_sense = 'E' # =
        constraint_met._bound = 0
        the_constraint_mets.append(constraint_met)
        the_NdotG_vars.append(constraint_met)
    for G_idx in range(0,nint):
            constraint_rxn_G = the_G_rxns[G_idx]
            # add the constraint coeffs to the relevant column (reaction) in the matrix
            for N_idx in range(0, linternal):
                constraint_met = the_NdotG_vars[N_idx]
                coeff_val = Ninternal[G_idx, N_idx]
                constraint_rxn_G.add_metabolites({constraint_met: coeff_val})

    # constraints to couple forward & reverse metabolic reaction pairs #####
    # each metabolic reaction would have a corresponding binary indicator rxn
    # for a pair of reactions for_rxn and rev_rxn, there exist a corresponding pair of binary indicators b_for and b_rev respectively.
    # this block of code adds constraints to the binary rxn indicators such that b_for + b_rev = 1
    # this constraint only allows one reaction out of the pair [for_rxn, rev_rxn] to be active at a time.
    for rev_rxn in reverse_reactions:
        for_rxn = rev_rxn.reflection
        constraint_id = 'or_constraint_' + str(for_rxn)
        or_constraint = Metabolite(id=constraint_id)
        or_constraint._bound = 1
        the_constraint_mets.append(or_constraint)
        for_rxn_indicator = rxn_indicator_dict[for_rxn]
        rev_rxn_indicator = rxn_indicator_dict[rev_rxn]
        for_rxn_indicator.add_metabolites({or_constraint: 1})
        rev_rxn_indicator.add_metabolites({or_constraint: 1})

    model.add_reactions(the_constraint_rxns) # this will automatically add all of the relevant constraint metabolites
    model.update()

    return model, [the_metabolic_rxns_internal, the_constraint_rxns, the_constraint_mets]


def removeLoopLawConstraints(model):
    removed_model = model.deepcopy()



# python port of the matlab function addLoopLawConstraints() by Jan Schellenberger
#INPUT
# LPproblem Structure containing the following fields
#  A      LHS matrix
#  b      RHS vector
#  c      Objective coeff vector
#  lb     Lower bound vector
#  ub     Upper bound vector
#  osense Objective sense (-1 max, +1 min)
#  csense Constraint senses, a string containting the constraint sense for
#         each row in A ('E', equality, 'G' greater than, 'L' less than).
#  F (optional)  If *QP problem
#  vartype (optional) if MI*P problem
# model     The model for which the loops should be removed
#
#OPTIONAL INPUT
# rxnIndex The index of variables in LPproblem corresponding to fluxes.
#     default = [1:n]
#
#
#OUTPUT
# Problem structure containing the following fields describing an MILP problem
# A, b, c, lb, ub - same as before but longer
# vartype - variable type of the MILP problem ('C', and 'B')
# x0 = [] Needed for solveMILPproblem
#
def addLoopLawConstraints(LPproblem, model, rxnIndex=None):
    #method = 2 # methd = 1 - separete af,ar;  method = 2 - only af;  method 3 - same as method 2 except use b_L, b_U instad of b and csense;
    reduce_vars = 1 # eliminates additional integer variables.  Should be faster in all cases but in practice may not be for some weird reason.
    combine_vars = 0 # combines flux coupled reactions into one variable.  Should be faster in all cases but in practice may not be.

    if rxnIndex == None:
        if LPproblem.A.shape[1] == model.S.shape[1]: # if the number of variables matches the number of model reactions
            rxnIndex = numpy.array([range(0, model.S.shape[1])])
        elif LPproblem.A.shape[1] > mode.S.shape[1]:
            print('warning:  extra variables in LPproblem.  will assume first n correspond to v')
            rxnIndex = numpy.array([range(0, model.S.shape[1])])
        else:
            print('LPproblem must have at least as many variables as model has reactions')
            return
    elif len(rxnIndex) != model.S.shape[1]:
        print('rxnIndex must contain exactly n entries')
        return
    else:
        rxnIndex = numpy.array([rxnIndex])

    '''
    if any(rxnIndex > size(LPproblem.A,2))
        display('rxnIndex out of bounds');
        return;
    end
    '''

    MILPproblem = LPproblem

    S = model.S.toarray()
    [m,n] = LPproblem.A.shape

    nontransport = numpy.array([sum(S != 0) > 1]) #reactions which are not transport reactions.
    nontransport = (nontransport | ((model.lower_bounds ==0) & (model.upper_bounds == 0)))

    if reduce_vars == 1:
        active1 = ~((model.lower_bounds ==0) & (model.upper_bounds == 0))

        S2 = S[:,active1] # exclude rxns with both ub and lb equal to 0
        N2 = sparseNullCPU(S2)

        N = numpy.zeros((len(active1), N2.shape[1]));
        N[active1,:] = N2

        active = numpy.any(abs(N) > 1e-6, axis=1); # exclude rxns not in null space

        nontransport = nontransport & active

    Sn = S[:,nontransport[0]]

    Ninternal = sparseNullCPU(Sn)
    linternal = Ninternal.shape[1]

    nint = len(numpy.where(nontransport)[0])
    temp = numpy.zeros((nint, n))
    temp[:, rxnIndex[nontransport]] = numpy.eye(nint)

    # update colnames in LPproblem
    # LPproblem already has colnames for internal rxns, so update only what's being newly added here
    # col names for a_0, a_1, ... a_n
    colnames = []
    for idx in range(0,nint):
        colname = 'a_' + str(idx)
        colnames.append(colname)
    # col names for G_0, G_1, ... G_n
    for idx in range(0,nint):
        colname = 'G_' + str(idx)
        colnames.append(colname)

    LPproblem.colnames = numpy.append(LPproblem.colnames, numpy.array([colnames]).T, axis=0)

    #method == 2 % One variables (a)
    block_Ax_eq_b_vars = numpy.hstack((LPproblem.A.toarray(), numpy.zeros((m, 2*nint))))
    #block_Ax_eq_b_op = 'E' <Already in LPproblem.csense>
    block_Ax_eq_b_bound = LPproblem.b

    # matrix construct for v - 1000*a_i < 0
    temp2 = numpy.hstack((temp, -1000*numpy.eye(nint)))
    block_v_upper_op = numpy.chararray((nint, 1))
    block_v_upper_vars = numpy.hstack((temp2, numpy.zeros((nint, nint))))  # v - 1000*a_i
    block_v_upper_op[:] = 'L'                                                   # <
    block_v_upper_bound = numpy.zeros((nint,1))                                 # 0

    # matrix construct for v - 1000*a_i > -1000
    block_v_lower_op = numpy.chararray((nint, 1))
    block_v_lower_vars = numpy.hstack((temp2, numpy.zeros((nint, nint))))   # v - 1000*a_i
    block_v_lower_op[:] = 'G'                                                   # >
    block_v_lower_bound = -1000*numpy.ones((nint,1))                            # -1000

    # matrix construct for G + 1001*a_i < 1000
    temp3 = numpy.hstack((numpy.zeros((nint, n)), 1001*numpy.eye(nint)))
    block_G_upper_op = numpy.chararray((nint, 1))
    block_G_upper_vars = numpy.hstack((temp3, numpy.eye(nint)))             # G + 1001*a_i
    block_G_upper_op[:] = 'L'                                                   # <
    block_G_upper_bound = 1000*numpy.ones((nint, 1))                            # 1000

    # matrix construct for G + 1001*a_i > 1
    block_G_lower_op = numpy.chararray((nint, 1))
    block_G_lower_vars = numpy.hstack((temp3, numpy.eye(nint)))             # G + 1001*a_i
    block_G_lower_op[:] = 'G'                                                   # >
    block_G_lower_bound = numpy.ones((nint, 1))                                 # 1

    # matrix construct for N*G = 0
    block_NxG_op = numpy.chararray((linternal, 1))
    block_NxG_vars = numpy.hstack((numpy.zeros((linternal, n+nint)), Ninternal.T))  # N*G
    block_NxG_op[:] = 'E'                                                               # =
    block_NxG_bound = numpy.zeros((linternal,1))                                        # 0

    # stack the blocks up row-wise
    block_all_vars = numpy.vstack((block_Ax_eq_b_vars, block_v_upper_vars))
    block_all_vars = numpy.vstack((block_all_vars, block_v_lower_vars))
    block_all_vars = numpy.vstack((block_all_vars, block_G_upper_vars))
    block_all_vars = numpy.vstack((block_all_vars, block_G_lower_vars))
    block_all_vars = numpy.vstack((block_all_vars, block_NxG_vars))

    block_all_bounds = numpy.vstack((block_Ax_eq_b_bound, block_v_upper_bound))
    block_all_bounds = numpy.vstack((block_all_bounds, block_v_lower_bound))
    block_all_bounds = numpy.vstack((block_all_bounds, block_G_upper_bound))
    block_all_bounds = numpy.vstack((block_all_bounds, block_G_lower_bound))
    block_all_bounds = numpy.vstack((block_all_bounds, block_NxG_bound))

    block_all_csense = numpy.vstack((LPproblem.csense, block_v_upper_op))
    block_all_csense = numpy.vstack((block_all_csense, block_v_lower_op))
    block_all_csense = numpy.vstack((block_all_csense, block_G_upper_op))
    block_all_csense = numpy.vstack((block_all_csense, block_G_lower_op))
    block_all_csense = numpy.vstack((block_all_csense, block_NxG_op))

    MILPproblem.A = block_all_vars
    MILPproblem.b = block_all_bounds
    MILPproblem.c = numpy.vstack((LPproblem.c, numpy.zeros((2*nint, 1))))
    MILPproblem.csense = block_all_csense

    #MILPproblem.vartype = ''
    if hasattr(LPproblem, 'vartype'):
        MILPproblem.vartype = LPproblem.vartype # keep variables same as previously
    else:
        MILPproblem.vartype = numpy.chararray((n,1))
        MILPproblem.vartype[:] = 'C'

    var_types_a = numpy.chararray((nint,1))
    var_types_a[:] = 'B'

    var_types_G = numpy.chararray((nint,1))
    var_types_G[:] = 'C'

    var_types_all = numpy.vstack((var_types_a, var_types_G))
    MILPproblem.vartype = numpy.vstack((MILPproblem.vartype, var_types_all))

    block_all_lb = numpy.vstack((LPproblem.lb, numpy.zeros((nint, 1))))         # lower_bound at 0 for a_i
    block_all_lb = numpy.vstack((block_all_lb, -1000*numpy.ones((nint, 1))))    # lower_bound at -1000 for G_i

    block_all_ub = numpy.vstack((LPproblem.ub, numpy.ones((nint, 1))))          # upper_bound at 1 for a_i
    block_all_ub = numpy.vstack((block_all_ub, 1000*numpy.ones((nint, 1))))     # upper_bound at 1000 for G_i

    MILPproblem.lb = block_all_lb
    MILPproblem.ub = block_all_ub

    MILPproblem.x0 = []

    return MILPproblem




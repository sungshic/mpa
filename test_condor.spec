# -*- mode: python -*-

block_cipher = None


a = Analysis(['test_condor.py'],
             pathex=['/vagrant/Documents/workspace/CrodaMPA',
'/home/vagrant/.virtualenv/pycobra/lib/python2.7/site-packages',
'/home/vagrant/.virtualenv/pycobra/src/pyinstaller',
'/usr/bin',
'/usr/lib/python2.7/dist-packages',
'/usr/lib/python2.7',
'/usr/lib/python2.7/plat-i386-linux-gnu',
'/usr/lib/python2.7/lib-tk',
'/usr/lib/python2.7/lib-dynload',
'/usr/local/lib/python2.7/dist-packages',
'/usr/lib/python2.7/dist-packages/PILcompat',
'/usr/lib/python2.7/dist-packages/gtk-2.0',
'/usr/lib/pymodules/python2.7',
'/usr/lib/python2.7/dist-packages/ubuntu-sso-client',
'/usr/lib/python2.7/dist-packages/IPython/extensions'
],
             hiddenimports=[],
             hookspath=['hooks'],
             runtime_hooks=None,
             cipher=block_cipher)
pyz = PYZ(a.pure,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
	  a.binaries,
	  a.zipfiles,
	  a.datas,
          name='test_condor',
          debug=True,
          strip=None,
          upx=True,
          console=True )

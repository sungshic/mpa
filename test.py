__author__ = 'spark'

import numpy

def pad_to_square(a, pad_value=0):
    m = a.reshape((a.shape[0], -1))
    padded = pad_value * numpy.ones(2 * [max(m.shape)], dtype=m.dtype)
    padded[0:m.shape[0], 0:m.shape[1]] = m
    return padded

from scipy.sparse import *

A = numpy.array([[1,1,0,1,5],[1,0,0,2,2],[0,0,1,4,-1],[0,0,0,0,0]])

#A = pad_to_square(A)

#A1 = csc_matrix(A)

from scipy.sparse.linalg import *

#splu(A1)


_options = dict(DiagPivotThresh=None, ColPerm=None,
                    PanelSize=None, Relax=None)


'''
import pysparse

L = pysparse.spmatrix.ll_mat(5,5)

for m in range(5):
    for n in range(5):
        L[m,n] = A[m,n]
'''

[n,m] = A.shape

from sparseNull import *
#[L,U,Q] = luq(A, 1e-9)
#N = sparseNull(A)

from setShinorineReactions import setToyModel
from addLoopLawConstraints import addLoopLawConstraints

tm = setToyModel()
tm.to_array_based_model()

'''
class Bunch:
    def __init__(self, **kwds):
        self.__dict__.update(kwds)

LPproblem = Bunch()

[nMets, nRxns] = tm.S.shape

LPproblem.osense = -1 # maximize
LPproblem.csense = numpy.chararray((nMets,1))
LPproblem.csense[:] = 'E'

if hasattr(tm, 'b'):
    LPproblem.b = numpy.array([tm.b]).T
else:
    LPproblem.b = numpy.zeros((tm.shape[0],1))

# rest of the LP problem setup
LPproblem.A = tm.S
LPproblem.c = numpy.array([tm.objective_coefficients]).T
LPproblem.lb = numpy.array([tm.lower_bounds]).T
LPproblem.ub = numpy.array([tm.upper_bounds]).T
LPproblem.colnames = numpy.array([tm.reactions]).T
LPproblem.rownames = numpy.array([tm.metabolites]).T

MILPproblem = addLoopLawConstraints(LPproblem, tm, range(0,nRxns))
'''

'''
import glpk
import cobra

def format_lp_problem(cobra_model, add_looplaw=False):
    class Bunch:
        def __init__(self, **kwds):
            self.__dict__.update(kwds)

    lp_problem = Bunch()

    if not hasattr(cobra_model, 'S'):
       cobra_model.to_array_based_model()

    [nMets, nRxns] = cobra_model.S.shape

    lp_problem.osense = -1 # maximize
    lp_problem.csense = numpy.chararray((nMets,1))
    lp_problem.csense[:] = 'E'

    if hasattr(cobra_model, 'b'):
        lp_problem.b = numpy.array([cobra_model.b]).T
    else:
        lp_problem.b = numpy.zeros((cobra_model.shape[0],1))

    # rest of the LP problem setup
    lp_problem.A = cobra_model.S
    lp_problem.c = numpy.array([cobra_model.objective_coefficients]).T
    lp_problem.lb = numpy.array([cobra_model.lower_bounds]).T
    lp_problem.ub = numpy.array([cobra_model.upper_bounds]).T
    lp_problem.colnames = numpy.array([[rxn.id for rxn in cobra_model.reactions]]).T
    lp_problem.rownames = numpy.array([[met.id for met in cobra_model.metabolites]]).T

    if add_looplaw:
        milp_problem = addLoopLawConstraints(lp_problem, cobra_model, range(0,nRxns))
        return milp_problem
    else:
        return lp_problem

def format_cobra_solution(lp):
    if (lp.status == 'opt'):
        lp_x = [col.value for col in lp.cols]
        f_val = sum(numpy.array(lp.obj[:])*numpy.array(lp_x))
        cobra_sol = cobra.core.Solution(f_val)
        cobra_sol.status = 'optimal'

        cobra_sol.x = lp_x
        cobra_sol.x_dict = {}
        for idx, col in enumerate(lp.cols):
            cobra_sol.x_dict[col.name] = col.value
    else:
        cobra_sol = cobra.core.Solution(0)
        cobra_sol.status = lp.status

    return cobra_sol

def solve_glpk(lp_problem, is_milp=False):
    lp = glpk.LPX()             # empty LP instance
    glpk.env.term_on = False    # stop annoying messages
    [m,n] = lp_problem.A.shape
    lp.cols.add(n)              # add total num of rxns
    lp.rows.add(m)              # add total num of vars

    for idx, row in enumerate(lp.rows):
        csense = lp_problem.csense[idx]
        limit_val = lp_problem.b[idx]
        #row.name = lp_problem.rownames[idx,0].id
        if csense == 'L':
            row.bounds = None, limit_val            # -inf < var <= limit_val
        elif csense == 'G':
            row.bounds = limit_val, None            # limit_val <= var < inf
        else: # default csense of 'E'
            row.bounds = limit_val                  # var == limit_val

    mat = []

    for idx, col in enumerate(lp.cols):
        col.bounds = lp_problem.lb[idx], lp_problem.ub[idx]         # lower and upper bounds for the rxn
        col.name = lp_problem.colnames[idx,0]
        if hasattr(lp_problem, 'vartype'):
            if lp_problem.vartype[idx] == 'C':
                col.kind = float
                #col.kind = int
            else:
                col.kind = bool
        else:
            col.kind = float
            #col.kind = int

        lp.obj[idx] = lp_problem.c[idx]                               # set the objective coeff

    linear_constrains = []
    for m_idx in range(0,m):
        for n_idx in range(0,n):
            coeff_val = lp_problem.A[m_idx, n_idx]
            linear_constrains.append((m_idx, n_idx, coeff_val))

    lp.matrix = linear_constrains
    lp.obj.maximize = True

    lp.simplex()
    if is_milp:
        lp.integer()

    #lp.obj

    print 'done'
    return lp
'''

from ll_cobra import *

'''
lp_problem = format_lp_problem(tm, True)
lp = solve_glpk(lp_problem, True)
cobra_sol = format_cobra_solution(lp)
#solve_milp(LPproblem)
'''


from setMediaV2 import *
from setShinorineReactions import *

sbml_file = "data/iBsu1103V2/nar-01731-m-2012-File013.xml"
sb_model = create_cobra_model_from_sbml_file(sbml_file)

setShinorineReactions(sb_model)
#setLBmedia(sb_model, -10)
setMediaV2(sb_model, -10)

biomassRxnIdx = sb_model.reactions.index('bio00127')
srcRxnIdx = sb_model.reactions.index('EX_cpd00027_e') #D-Glu uptake
tgtRxnIdx = sb_model.reactions.index('rxn_S00150') #Shinorine secretion
#tgtRxnIdx = sb_model.reactions.index('rxn01343') #sh7p
srcCompoundIdx = sb_model.metabolites.index('cpd00027_e')
#tgtCompoundIdx = sb_model.metabolites.index('cpd00238_c')
#tgtCompoundIdx = sb_model.metabolites.index('cpdADDED_Shinorine_e')
#tgtCompoundIdx = sb_model.metabolites.index('cpd00238_c') #cytosolic sedoheptulose-7-phosphate
#tgtCompoundIdx = sb_model.metabolites.index('cpdADDED_DDG_c') #cytosolic 2-demethyl-4-deoxygadusol
tgtCompoundIdx = sb_model.metabolites.index('cpd00699_c') #cytosolic 2-demethyl-4-deoxygadusol

# setting a dual objective
sb_model.reactions.bio00127.objective_coefficient = 1
sb_model.reactions.rxn_S00150.objective_coefficient = 1
sb_model.reactions.bio00127.lower_bound = 0.5
#sb_model.genes.
sb_model_mat = sb_model.to_array_based_model()

'''
lp_problem2 = format_lp_problem(sb_model, True)
lp2 = solve_glpk(lp_problem2, True)
cobra_sol2 = format_cobra_solution(lp2)
'''
lp_problem2 = format_lp_problem(sb_model, True)
lp2 = solve_gurobi(lp_problem2)
cobra_sol2 = format_cobra_solution_from_GUROBIresult(lp2)

print 'done!'

'''
tol = 1e-9

#if not issparse(A):     # if A is not in sparse-matrix format
#    A = csc_matrix(A)   # construct a sparse matrix format of A
##----------
## SPECIAL exit conditions for recursive calls
##----------
if n == 0 or m == 0: #A.shape[0] == 0:
    L = eye(n)
    U = A
    Q = eye(m)
    return L,U,Q

##----------
## LU Decomposition
##----------
[P,L,U] = lu(A)
Q = eye(m)

p = A.shape[0] - L.shape[1]
print 'n:' + str(n) + ' p:' + str(p) + ' n-p:' + str(n-p)

LL = numpy.append(numpy.zeros((n-p,p)),numpy.eye(p),axis=0) # append two matrices row-wise

L = numpy.append(numpy.dot(P.T, L), P[range(n-p,n),:].T, axis=1) #append two matrices column-wise
if p > 0 and m > 0:
    U = numpy.append(U, numpy.zeros(p,m), axis=0) # append two matrices row-wise

##-----------
## Find rows with zero and nonzero elements on the diagonal
##-----------
if U.shape[0] == 1 or U.shape[1] == 1:
    S = U[0,0]
else:
    S = numpy.diag(U)

I = numpy.where(abs(S)>tol)[0] # find the column indices of the elements in S greater than the tolerance value
Jl = numpy.array(range(0,n)).T
Jl[I] = []
Jq = numpy.array(range(0,m)).T
Jq[I] = []

Ubar1 = U[I,I]
Ubar2 = U[Jl, Jq]
Qbar1 = Q[I,:]
Lbar1 = L[:,I]

##---------
## Eliminates nonzero elements below and on the right of the invertible block of the matrix U
##
## Updates matrices L & Q
##---------
if len(I) > 0:
    Utmp = U[I,Jq]
    X = numpy.linalg.lstsq(Ubar1.T,U(Jl,I).T)[0] #Left matrix division: x,resid,rank,s = np.linalg.lstsq(B,b)
    Ubar2 = Ubar2 - numpy.dot(X.T, Utmp)
    Lbar1 = Lbar1 + numpy.dot(L[:,Jl], X.T)

    X = numpy.linalg.lstsq(Ubar1, Utmp) #Left matrix division
    Qbar1 = Qbar1 + numpy.dot(X, Q[Jq, :])
    Utmp = []
    X = []

##---------
## Finds rows and columns with only zero elements
##---------
I2 = numpy.where(numpy.amax(abs(Ubar2), axis=1) > tol)[0] # find indices of the row max greater than the tol value
I5 = numpy.where(numpy.amax(abs(Ubar2), axis=0) > tol)[0] # find indices of the column max greater than tol

I3 = Jl(I2)
I4 = Jq(I5)
Jq[I5] = []
Jl[I2] = []
U = []


##--------
## Finds a part of the matrix U which is not in the required form
##--------
A = Ubar2(I2,I5)

##---------
## Performs recursive LUQ decomposition of the matrix A
##---------
[L1, U1, Q1] = luq(A, tol)

##---------
## Updates matrices L, U, Q
##---------
Lbar2 = numpy.dot(L[:,I3], L1)
Qbar2 = numpy.dot(Q1, Q[I4,:])
L = numpy.append(numpy.append(Lbar1, Lbar2, axis=1), L[:,Jl], axis=1) # L = [Lbar1, Lbar2, L[:,Jl]]
Q = numpy.append(numpy.append(Qbar1, Qbar2, axis=0), Q[Jq,:], axis=0) # Q = [Qbar1; Qbar2; Q[Jq,:]]

n1 = len(I)
n2 = len(I3)
m2 = len(I4)

# U = [Ubar1 sparse(n1,m-n1);sparse(n2,n1) U1 sparse(n2,m-n1-m2);sparse(n-n1-n2,m)];
U = numpy.append(numpy.append(numpy.append(Ubar1, numpy.zeros(n1, m-n1), axis=1),
                              numpy.append(numpy.append(numpy.zeros(n2,n1), U1, axis=1), numpy.zeros(n2, m-n1-m2), axis=1),
                              axis=0),
                 numpy.zeros(n-n1-n2,m),
                 axis=0)

return L,U,Q
'''
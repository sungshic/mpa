__author__ = 'spark'

import numpy
import scipy
from scipy.sparse import *
from scipy.linalg import lu
#from scikits.cuda.linalg import cula
#from scikits.cuda.linalg.cula import culaInitialize, culaShutdown
#import pycuda.gpuarray as gpuarray
#import pycuda.autoinit

def get_perm_matrix(pivotList):
    pLen = len(pivotList)
    P = numpy.empty((pLen,pLen))
    I = numpy.eye(pLen)
    permList = numpy.arange(0,pLen)

    # convert pivot exchange list (in CULA convention) into a row idx permutation list
    for i in range(0,pLen):
        temp = permList[i]
        permList[i] = permList[pivotList[i]-1]
        permList[pivotList[i]-1] = temp

    for i in range(0,pLen):
        P[i,:] = I[permList[i],:]

    # now P is in column major as the input 'pivotList' is from LAPACK, and we need to go back to row major order by returning P.T
    return P.T

#
#
# python port of the matlab routines by Jan Schellenberger and Pawel Kowal
#
# sparseNull returns computes the sparse Null basis of a matrix
#
# N = sparseNull(S, tol)
#
# Computes a basis of the null space for a sparse matrix.  For sparse
# matrices this is much faster than using null.  It does however have lower
# numerical accuracy.  N is itself sparse and not orthonormal.  So in this
# way it is like using N = null(S, 'r'), except of course much faster.
#
# Jan Schellenberger 10/20/2009
# based on this:
# http://www.mathworks.com/matlabcentral/fileexchange/11120-null-space-of-a-sparse-matrix
def sparseNullCPU(S, tol=None):
    N = None
    ##### debug code
    ##### S = numpy.float32(numpy.random.randn(1391,1707))

    [SpLeft, SpRight] = spspaces(S, 2, tol)
    if SpRight != None:
        N = SpRight[0][:,SpRight[2][0]]
        N[abs(N) < tol] = 0

    return N

#
# python port of the matlab routines by Jan Schellenberger and Pawel Kowal
#
#  Function luq
#  calculates the following decomposition
#
#       A = L |Ubar  0 | Q
#             |0     0 |
#
#       where Ubar is a square invertible matrix
#       and matrices L, Q are invertible.
#
# ---------------------------------------------------
#  USAGE: [L,U,Q] = luq(A,do_pivot,tol)
#  INPUT:
#         A             a sparse matrix
#         tol           uses the tolerance tol in separating zero and
#                       nonzero values
#
#   OUTPUT:
#         L,U,Q          matrices
#
#   COMMENTS:
#         based on lu decomposition
#
# Copyright  (c) Pawel Kowal (2006)
# All rights reserved
def luq(A, tol):
    [n,m] = A.shape

    #if not issparse(A):     # if A is not in sparse-matrix format
    #    A = csc_matrix(A)   # construct a sparse matrix format of A
    ##----------
    ## SPECIAL exit conditions for recursive calls
    ##----------
    if n == 0 or m == 0: #A.shape[0] == 0:
        L = numpy.eye(n)
        U = A
        Q = numpy.eye(m)
        return L,U,Q

    ##----------
    ## LU Decomposition
    ##----------
    [P,L,U] = lu(A)
    Q = numpy.eye(m)

    p = A.shape[0] - L.shape[1]
    print 'n:' + str(n) + ' p:' + str(p) + ' n-p:' + str(n-p)

    LL = numpy.append(numpy.zeros((n-p,p)),numpy.eye(p),axis=0) # append two matrices row-wise

    L = numpy.append(numpy.dot(P.T, L), P[range(n-p,n),:].T, axis=1) #append two matrices column-wise
    U = numpy.append(U, numpy.zeros((p,m)), axis=0) # append two matrices row-wise

    ##-----------
    ## Find rows with zero and nonzero elements on the diagonal
    ##-----------
    if U.shape[0] == 1 or U.shape[1] == 1:
        S = numpy.array([[U[0,0]]])
    else:
        S = numpy.array([numpy.diag(U)])

    I = numpy.array([numpy.where(abs(S)>tol)[1]]) # find the column indices of the elements in S greater than the tolerance value
    Jl = numpy.array([range(0,n)])
    Jl = numpy.delete(Jl.T, I, axis=0).T #Jl[I] = []

    Jq = numpy.array([range(0,m)])
    Jq = numpy.delete(Jq.T, I, axis=0).T #Jq[I] = []

    Ubar1 = U[I.T,I] #[:,I]
    Ubar2 = U[Jl.T,Jq] #[:,Jq]
    Qbar1 = Q[I[0],:]
    Lbar1 = L[:,I[0]]

    ##---------
    ## Eliminates nonzero elements below and on the right of the invertible block of the matrix U
    ##
    ## Updates matrices L & Q
    ##---------
    if len(I) > 0:
        Utmp1 = U[Jl.T,I].T
        Utmp2 = U[I.T,Jq] #[:,Jq]

        #X = numpy.linalg.lstsq(Ubar1.T,(U[Jl.T,I].T))[0] #Left matrix division: x,resid,rank,s = np.linalg.lstsq(B,b)
        if Utmp1.shape[1] != 0:
            X = numpy.linalg.lstsq(Ubar1.T,Utmp1)[0] #Left matrix division: x,resid,rank,s = np.linalg.lstsq(B,b)
            Ubar2 = Ubar2 - numpy.dot(X.T, Utmp2)
            Lbar1 = Lbar1 + numpy.dot(L[:,Jl[0]], X.T)

        if Utmp2.shape[1] != 0:
            X = numpy.linalg.lstsq(Ubar1, Utmp2)[0] #Left matrix division
            Qbar1 = Qbar1 + numpy.dot(X, Q[Jq[0], :])
        Utmp1 = []
        Utmp2 = []
        X = []

    ##---------
    ## Finds rows and columns with only zero elements
    ##---------
    if Ubar2.shape[1] > 0:
        I2 = numpy.array([numpy.where([numpy.amax(abs(Ubar2), axis=1) > tol])[1]]) # find indices of the row max greater than the tol value
    else:
        I2 = numpy.array([[]], dtype=int)

    if Ubar2.shape[0] > 0:
        I5 = numpy.array([numpy.where([numpy.amax(abs(Ubar2), axis=0) > tol])[1]]) # find indices of the column max greater than tol
    else:
        I5 = numpy.array([[]], dtype=int)

    if len(I2[0]) > 0: # and len(Jl[:]) > 0:
        I3 = Jl[:,I2[0]]
        Jl = numpy.delete(Jl.T, I2, axis=0).T # Jl[I2] = []
    else:
        I3 = numpy.array([[]],dtype=numpy.int32)

    if len(I5[0]) > 0: # and len(Jq[:]) > 0:
        I4 = Jq[:,I5[0]]
        Jq = numpy.delete(Jq.T, I5, axis=0).T # Jq[I5] = []
    else:
        I4 = numpy.array([[]], dtype=numpy.int32)

    U = []


    ##--------
    ## Finds a part of the matrix U which is not in the required form
    ##--------
    A = Ubar2[I2.T,I5]#[:,I5]

    ##---------
    ## Performs recursive LUQ decomposition of the matrix A
    ##---------
    [L1, U1, Q1] = luq(A, tol)

    ##---------
    ## Updates matrices L, U, Q
    ##---------
    Lbar2 = numpy.dot(L[:,I3[0]], L1)
    Qbar2 = numpy.dot(Q1, Q[I4[0],:])
    L = numpy.append(numpy.append(Lbar1, Lbar2, axis=1), L[:,Jl[0]], axis=1) # L = [Lbar1, Lbar2, L[:,Jl]]
    Q = numpy.append(numpy.append(Qbar1, Qbar2, axis=0), Q[Jq[0],:], axis=0) # Q = [Qbar1; Qbar2; Q[Jq,:]]

    n1 = len(I[0])
    n2 = len(I3[0])
    m2 = len(I4[0])

    # U = [Ubar1 sparse(n1,m-n1);sparse(n2,n1) U1 sparse(n2,m-n1-m2);sparse(n-n1-n2,m)];
    U = numpy.append(numpy.append(numpy.append(Ubar1, numpy.zeros((n1, m-n1)), axis=1),
                                  numpy.append(numpy.append(numpy.zeros((n2,n1)), U1, axis=1), numpy.zeros((n2, m-n1-m2)), axis=1),
                                  axis=0),
                     numpy.zeros((n-n1-n2,m)),
                     axis=0)

    return L,U,Q


#
# python port of the matlab routines by Jan Schellenberger and Pawel Kowal
#
#  PURPOSE: finds left and right null and range space of a sparse matrix A
#
# ---------------------------------------------------
#  USAGE: [SpLeft, SpRight] = spspaces(A,opt,tol)
#
#  INPUT:
#       A                           a sparse matrix
#       opt                         spaces to calculate
#                                   = 1: left null and range space
#                                   = 2: right null and range space
#                                   = 3: both left and right spaces
#       tol                         uses the tolerance tol when calculating
#                                   null subspaces (optional)
#
#   OUTPUT:
#       SpLeft                      1x4 cell. SpLeft = {} if opt =2.
#           SpLeft{1}               an invertible matrix Q
#           SpLeft{2}               indices, I, of rows of the matrix Q that
#                                   span the left range of the matrix A
#           SpLeft{3}               indices, J, of rows of the matrix Q that
#                                   span the left null space of the matrix A
#                                   Q(J,:)A = 0
#           SpLeft{4}               inverse of the matrix Q
#       SpRight                     1x4 cell. SpRight = {} if opt =1.
#           SpLeft{1}               an invertible matrix Q
#           SpLeft{2}               indices, I, of rows of the matrix Q that
#                                   span the right range of the matrix A
#           SpLeft{3}               indices, J, of rows of the matrix Q that
#                                   span the right null space of the matrix A
#                                   AQ(:,J) = 0
#           SpLeft{4}               inverse of the matrix Q
#
#   COMMENTS:
#       uses luq routine, that finds matrices L, U, Q such that
#
#           A = L | U 0 | Q
#                 | 0 0 |
#
#       where L, Q, U are invertible matrices, U is upper triangular. This
#       decomposition is calculated using lu decomposition.
#
#    This routine is fast, but can deliver inaccurate null and range
#       spaces if zero and nonzero singular values of the matrix A are not
#       well separated.
#
#   WARNING:
#       right null and rang space may be very inaccurate
#
# Copyright  (c) Pawel Kowal (2006)
def spspaces(A,opt,tol=None):
    if tol==None:
        tol =  max(max(A.shape) * numpy.linalg.norm(A,1) * numpy.spacing(1), 100*numpy.spacing(1))

    if opt == 1:
        calc_left = True
        calc_right = False
    elif opt == 2:
        calc_left = False
        calc_right = True
    else:
        calc_left = True
        calc_right = True

    #[L,U,Q] = luq(A, tol)
    [L,U,Q] = luq(A, tol)

    if calc_left:
        if len(L) > 0:
            LL = numpy.linalg.inv(L)
        else:
            LL = L
        #S = [numpy.amax(abs(U), axis=1)]
        #I = numpy.array([numpy.where(S>tol)[1]])
        S = numpy.amax(abs(U), axis=1)
        I = numpy.array([numpy.where(S>tol)[0]])

        if len(S) > 0:
            #J = numpy.array([numpy.where(S <= tol)[1]])
            J = numpy.array([numpy.where(S <= tol)[0]])
        else:
            #J = numpy.array([range(0,S.shape[0])]).T
            J = numpy.array([range(0,S.shape[0])]).T

        SpLeft = [LL, I, J, L]
    else:
        SpLeft = []

    if calc_right:
        if len(Q) > 0:
            QQ = numpy.linalg.inv(Q)
        else:
            QQ = Q

        #S = [numpy.amax(abs(U), axis=0)]
        #I = numpy.array([numpy.where(S>tol)[1]])
        S = numpy.amax(abs(U), axis=0)
        I = numpy.array([numpy.where(S>tol)[0]])

        if len(S) > 0:
            #J = numpy.array([numpy.where(S<=tol)[1]])
            J = numpy.array([numpy.where(S<=tol)[0]])
        else:
            #J = numpy.array([range(0,S.shape[1])]).T
            J = numpy.array([range(0,S.shape[0])]).T

        SpRight = [QQ, I, J, Q]
    else:
        SpRight = []


    return SpLeft, SpRight



def pad_to_square(a, pad_value=0):
    m = a.reshape((a.shape[0], -1))
    padded = pad_value * numpy.ones(2 * [max(m.shape)], dtype=m.dtype)
    padded[0:m.shape[0], 0:m.shape[1]] = m
    return padded

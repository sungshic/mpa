include apt

class init {

	group { "puppet":
		ensure => "present",
	}

	exec { "install hiera dependencies":
		command => "sudo gem install hiera hiera-puppet",
	}

	# update the apt-get repositories
	exec { "update-apt":
		command => "sudo apt-get update",
	}


	# install apt-get module dependencies
	package {
		["picard-tools", "samtools", "mummer", "ncbi-blast+", "blast2", "tigr-glimmer"]:
		ensure => installed,
		install_options => ['--force-yes'],
		require => Exec['update-apt'], # the system update needs to run first
	}

	# install "pip install" dependencies
	package {
		["argparse", "ez_setup", "cobra", "matplotlib", "numpy", "ply", "scipy", "wsgiref", "biopython", "pysam", "bcbio-gff", "pyvcf", "pyPDF2", "reportlab", "pdfrw"]:
		provider => pip,
		ensure => installed,
		require => [Package['python-pip', 'python-dev']],
	}

	
}

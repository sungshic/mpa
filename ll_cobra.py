__author__ = 'spark'
import numpy
import glpk
from gurobipy import *
import cobra
from addLoopLawConstraints import addLoopLawConstraints

def format_lp_problem(cobra_model, add_looplaw=False):
    class Bunch:
        def __init__(self, **kwds):
            self.__dict__.update(kwds)

    lp_problem = Bunch()

    if not hasattr(cobra_model, 'S'):
       cobra_model.to_array_based_model()

    [nMets, nRxns] = cobra_model.S.shape

    lp_problem.osense = -1 # maximize
    lp_problem.csense = numpy.chararray((nMets,1))
    lp_problem.csense[:] = 'E'

    if hasattr(cobra_model, 'b'):
        lp_problem.b = numpy.array([cobra_model.b]).T
    else:
        lp_problem.b = numpy.zeros((cobra_model.shape[0],1))

    # rest of the LP problem setup
    lp_problem.A = cobra_model.S
    lp_problem.c = numpy.array([cobra_model.objective_coefficients]).T
    lp_problem.lb = numpy.array([cobra_model.lower_bounds]).T
    lp_problem.ub = numpy.array([cobra_model.upper_bounds]).T
    lp_problem.colnames = numpy.array([[rxn.id for rxn in cobra_model.reactions]]).T
    lp_problem.rownames = numpy.array([[met.id for met in cobra_model.metabolites]]).T

    if add_looplaw:
        milp_problem = addLoopLawConstraints(lp_problem, cobra_model, range(0,nRxns))
        return milp_problem
    else:
        return lp_problem


def format_cobra_solution_from_GUROBIresult(gurobiout):
    if (gurobiout.status == GRB.status.OPTIMAL):
        colvars = gurobiout.getVars()
        lp_x = [v.x for v in colvars] #[col.value for col in lp.cols]
        f_val = gurobiout.objVal #sum(numpy.array(lp.obj[:])*numpy.array(lp_x))
        cobra_sol = cobra.core.Solution(f_val)
        cobra_sol.status = 'optimal'

        cobra_sol.x = lp_x
        cobra_sol.x_dict = {}
        for idx, col in enumerate(colvars):
            cobra_sol.x_dict[col.varName] = col.x
    else:
        cobra_sol = cobra.core.Solution(0)
        cobra_sol.status = gurobiout.status

    return cobra_sol

def format_cobra_solution_from_CPLEXresult(cplexsol, pool_idx, rxn_list):
    if cplexsol.solution.pool.get_num() > pool_idx:
        lp_x = cplexsol.solution.pool.get_values(pool_idx)
        f_val = cplexsol.solution.pool.get_objective_value(pool_idx)
        cobra_sol = cobra.core.Solution(f_val)
        cobra_sol.status = 'optimal'

        cobra_sol.x = lp_x
        cobra_sol.x_dict = {}
        for idx, rxn in enumerate(rxn_list):
            cobra_sol.x_dict[rxn.id] = lp_x[idx]
    else:
        cobra_sol = cobra.core.Solution(0)

    return cobra_sol

def format_cobra_solution_from_GLPKresult(lp):
    if (lp.status == 'opt'):
        lp_x = [col.value for col in lp.cols]
        f_val = sum(numpy.array(lp.obj[:])*numpy.array(lp_x))
        cobra_sol = cobra.core.Solution(f_val)
        cobra_sol.status = 'optimal'

        cobra_sol.x = lp_x
        cobra_sol.x_dict = {}
        for idx, col in enumerate(lp.cols):
            cobra_sol.x_dict[col.name] = col.value
    else:
        cobra_sol = cobra.core.Solution(0)
        cobra_sol.status = lp.status

    return cobra_sol

def solve_gurobi(lp_problem):
    try:
        # create a new gurobi model
        model = gurobipy.Model("ll_cobra_MILP")

        vartypes = numpy.array([[idx,val[0]] for idx,val in enumerate(lp_problem.vartype)])

        # add x variables to gurobi model and set their objective coefficients, types [B or C], and lower and upper bounds.
        numcols = len(lp_problem.vartype)
        colvars = numpy.empty(numcols, dtype=gurobipy.Var)
        for cidx in range(0,numcols):
            #varname = 'colvar_' + str(vartypes[idx, 0])
            varname = lp_problem.colnames[cidx, 0]
            vartype = vartypes[cidx, 1]
            obj_coeff = lp_problem.c[cidx, 0]
            rxn_ub = lp_problem.ub[cidx, 0]
            rxn_lb = lp_problem.lb[cidx, 0]

            if vartype == 'B':
                colvars[cidx] = model.addVar(lb=rxn_lb, ub=rxn_ub, vtype=GRB.BINARY, obj=obj_coeff, name=varname)
            elif vartype == 'C':
                colvars[cidx] = model.addVar(lb=rxn_lb, ub=rxn_ub, vtype=GRB.CONTINUOUS, obj=obj_coeff, name=varname)
        # end for

        model.modelSense = GRB.MAXIMIZE
        # update the gurobi model with new variables
        model.update()

        rowlimits = lp_problem.b
        optypes = lp_problem.csense
        # set constraint conditions: eg. 'A', 'b' and '=' in Ax = b
        for ridx, constraint in enumerate(lp_problem.A):
            constraint_construct = gurobipy.quicksum(constraint[nzridx] * colvars[nzridx] for nzridx in numpy.where(constraint)[0])
            rowname_base = 'constraint_' + str(ridx) #+ str(optypes[ridx, 0])
            optype = optypes[ridx, 0]
            limit_val = rowlimits[ridx, 0]
            if optype == 'L':
                model.addConstr(constraint_construct <= limit_val, rowname_base)
            elif optype == 'G':
                model.addConstr(constraint_construct >= limit_val, rowname_base)
            else: # default csense of 'E'
                model.addConstr(constraint_construct == limit_val, rowname_base)
        # end for

        model.update()
        model.optimize()

        return model

    except GurobiError:
        print('Gurobi error')

        return None
# end def



def solve_glpk(lp_problem, is_milp=False):
    lp = glpk.LPX()             # empty LP instance
    glpk.env.term_on = False    # stop annoying messages
    [m,n] = lp_problem.A.shape
    lp.cols.add(n)              # add total num of rxns
    lp.rows.add(m)              # add total num of vars

    for idx, row in enumerate(lp.rows):
        csense = lp_problem.csense[idx]
        limit_val = lp_problem.b[idx]
        #row.name = lp_problem.rownames[idx,0].id
        if csense == 'L':
            row.bounds = None, limit_val            # -inf < var <= limit_val
        elif csense == 'G':
            row.bounds = limit_val, None            # limit_val <= var < inf
        else: # default csense of 'E'
            row.bounds = limit_val                  # var == limit_val

    mat = []

    for idx, col in enumerate(lp.cols):
        col.bounds = lp_problem.lb[idx], lp_problem.ub[idx]         # lower and upper bounds for the rxn
        col.name = lp_problem.colnames[idx,0]
        if hasattr(lp_problem, 'vartype'):
            if lp_problem.vartype[idx] == 'C':
                col.kind = float
                #col.kind = int
            else:
                col.kind = bool
        else:
            col.kind = float
            #col.kind = int

        lp.obj[idx] = lp_problem.c[idx]                               # set the objective coeff

    linear_constrains = []
    rowrange = range(0,m)
    colrange = range(0,n)

    for lprow, matrow in zip(lp.rows, lp_problem.A):
        lprow.matrix = [(cidx, val) for cidx, val in enumerate(matrow)]
    '''
    for m_idx in rowrange:
        for n_idx in colrange:
            coeff_val = lp_problem.A[m_idx, n_idx]
            linear_constrains.append((m_idx, n_idx, coeff_val))

    lp.matrix = linear_constrains
    '''

    lp.obj.maximize = True

    lp.simplex()
    if is_milp:
        lp.integer()

    #lp.obj

    print 'done'
    return lp
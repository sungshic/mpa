# -*- coding: utf-8 -*-
"""

@author: Jurijs Meitalovs
"""
from _libsbml import Species_setBoundaryCondition

import cobra
import re 

from libsbml import *
from cobra.io.sbml import *
from cobra.io import read_sbml_model, write_sbml_model

sbml_file = "data/gb-2009-10-6-r69-s4.xml"

#SBML part

reader = SBMLReader()
document = reader.readSBML(sbml_file)
document.getNumErrors()
model = document.getModel()

sp = model.createSpecies()
sp.setId('cpd00238_e')
sp.name = 'Sedoheptulose_7-phosphate_C7H14O10P'
sp.compartment = 'Extracellular'
sp.setBoundaryCondition(False)
sp.charge = -1

spb = model.createSpecies()
spb.setId('cpd00238_b')
spb.name = 'Sedoheptulose_7-phosphate_C7H14O10P'
spb.compartment = 'Extracellular'
spb.setBoundaryCondition(True)
spb.charge = -1

rxnE = model.createReaction()
rxnE.setId('rxn00000')
rxnE.name = 'Sedoheptulose_7-phosphate_EX_transport'
rxnE.setReversible(True)
reactant = rxnE.createReactant()
reactant.setSpecies('cpd00238_c')
reactant.setStoichiometry(1.0)
product = rxnE.createProduct()
product.setSpecies('cpd00238_e')
product.setStoichiometry(1.0)

kl = rxnE.createKineticLaw()
mathXMLString = '<math xmlns=\"http://www.w3.org/1998/Math/MathML\"> <math xmlns="http://www.w3.org/1998/Math/MathML"> <ci> FLUX_VALUE </ci> </math>'
astMath = readMathMLFromString(mathXMLString)
kl.setMath(astMath)
#how to remove astMath?

paral = kl.createParameter()
paral.setId('LOWER_BOUND')
paral.setValue(-1000)
paral.setUnits('mmol_per_gDW_per_hr')

parau = kl.createParameter()
parau.setId('UPPER_BOUND')
parau.setValue(1000)
parau.setUnits('mmol_per_gDW_per_hr')

parao = kl.createParameter()
parao.setId('OBJECTIVE_COEFFICIENT')
parao.setValue(1)

parfl = kl.createParameter()
parfl.setId('FLUX_VALUE')
parfl.setValue(0)
parfl.setUnits('mmol_per_gDW_per_hr')




rxnEx = model.createReaction()
rxnEx.setId('EX_rxn000001')
rxnEx.name = 'Sedoheptulose_7-phosphate_EX_b_transport'
rxnEx.setReversible(True)
reactant = rxnEx.createReactant()
reactant.setSpecies('cpd00238_e')
reactant.setStoichiometry(1.0)
product = rxnEx.createProduct()
product.setSpecies('cpd00238_b')
product.setStoichiometry(1.0)

kl2 = rxnEx.createKineticLaw()
mathXMLString = '<math xmlns=\"http://www.w3.org/1998/Math/MathML\"> <math xmlns="http://www.w3.org/1998/Math/MathML"> <ci> FLUX_VALUE </ci> </math>'
astMath = readMathMLFromString(mathXMLString)
kl2.setMath(astMath)
#how to remove astMath?

paral2 = kl2.createParameter()
paral2.setId('LOWER_BOUND')
paral2.setValue(-1000)
paral2.setUnits('mmol_per_gDW_per_hr')

parau2 = kl2.createParameter()
parau2.setId('UPPER_BOUND')
parau2.setValue(1000)
parau2.setUnits('mmol_per_gDW_per_hr')

parao2 = kl2.createParameter()
parao2.setId('OBJECTIVE_COEFFICIENT')
parao2.setValue(0)

parfl2 = kl2.createParameter()
parfl2.setId('FLUX_VALUE')
parfl2.setValue(0)
parfl2.setUnits('mmol_per_gDW_per_hr')


writeSBMLToFile(document, 'data/model_for_cobra.xml')


#COBRA part

#convert from sbml

sb_model = create_cobra_model_from_sbml_file('data/model_for_cobra.xml')
sb_model.reactions.bio00006.objective_coefficient = 0
sb_model_mat = sb_model.to_array_based_model()
sb_model_mat.optimize()
print sb_model_mat.solution

write_cobra_model_to_sbml_file(sb_model, 'data/test_cobra.xml', 2,  1, print_time=True)

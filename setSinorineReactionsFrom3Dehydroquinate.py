__author__ = 'Jurijs MEitalovs'


from cobra.io.sbml import *

def set3Dehydroquinate4DeoxygadusolToShinorine(sb_model):


 ##############################################################################################################
        #UKNOWN GENE - transformation from 3-dehydoquinate to 4-deoxygadusol
        #C7H9O6+CO2+2H2O -> C8H12O5 + 2O2 + H+
        rxn_S00160 = Reaction('rxnS00160')
        rxn_S00160.name = '3-dehydoquinate_4-deoxygadusol'
        rxn_S00160.subsystem = 'c'
        rxn_S00160.lower_bound = -1000. #This is the default
        rxn_S00160.upper_bound = 1000. #This is the default
        rxn_S00160.reversibility = 1 #This is the default
        rxn_S00160.objective_coefficient = 0 #this is the default

        cpd00699_c = sb_model.metabolites.get_by_id('cpd00699_c') #3-Dehydroquinate
        cpd00011_c = sb_model.metabolites.get_by_id('cpd00011_c') #CO2
        cpd00001_c = sb_model.metabolites.get_by_id('cpd00001_c') #H20
        cpd_S00110_c = sb_model.metabolites.get_by_id('cpdS00110_c')
        if not cpd_S00110_c: # if not seen before
            # define the metabolite in the model
            cpd_S00110_c = Metabolite('cpdS00110_c', formula='C8H12O5', name='4-deoxygadusol', compartment='c') #4DG

        cpd00007_c = sb_model.metabolites.get_by_id('cpd00007_c') #O2
        cpd00067_c = sb_model.metabolites.get_by_id('cpd00067_c') #H+

        #add the metabolites to the reaction:
        rxn_S00160.add_metabolites({cpd00699_c: -1.0,
                                  cpd00011_c: -1.0,
                                  cpd00001_c: -2.0,
                                  cpd_S00110_c: 1.0,
                                  cpd00007_c: 2.0,
                                  cpd00067_c: 1.0})

        sb_model.add_reaction(rxn_S00160)


        ##############################################################################################################
        #AVA_3856
        #C8H12O5 + C10H16N5O13P3 (C10H13N5O13P3 in the model)-> C8H12O8P + C10H15N5O10P2 (C10H13N5O10P2 in the model)
        #4-DG + ATP -> 4-DG_ATP + ADP
        rxn_S00120 = Reaction('rxn_S00120')
        rxn_S00120.name = '4-deoxygadusol_4-deoxygadusol-ATP'
        rxn_S00120.subsystem = 'c'
        rxn_S00120.lower_bound = -1000. #This is the default
        rxn_S00120.upper_bound = 1000. #This is the default
        rxn_S00120.reversibility = 1 #This is the default
        rxn_S00120.objective_coefficient = 0 #this is the default

        #Create the metabolites
        cpd_S00110_c = sb_model.metabolites.get_by_id('cpd_S00110_c') #4-DG
        cpd00002_c = sb_model.metabolites.get_by_id('cpd00002_c') #ATP
        cpd00008_c = sb_model.metabolites.get_by_id('cpd00008_c') #ADP
        cpd_S00120_c = Metabolite('cpd_S00120_c', formula='C8H12O8P', name='4-deoxygadusol-phosphate', compartment='c') #4-DG-ATP

        #add the metabolites to the reaction:
        rxn_S00120.add_metabolites({cpd_S00110_c: -1.0,
                                  cpd00002_c: -1.0,
                                  cpd00008_c: 1.0,
                                  cpd_S00120_c: 1.0})

        sb_model.add_reaction(rxn_S00120)

        ##############################################################################################################
        #AVA_3856
        #C8H12O8P + C2H5NO2 -> C10H15O6N + HPO4 + H+
        #4-DG-ATP + Glycine -> Mycosporine-Gly + Pi + H+
        rxn_S00130 = Reaction('rxn_S00130')
        rxn_S00130.name = '4-deoxygadusol-ATP_Mycosporine-Gly'
        rxn_S00130.subsystem = 'c'
        rxn_S00130.lower_bound = -1000. #This is the default
        rxn_S00130.upper_bound = 1000. #This is the default
        rxn_S00130.reversibility = 1 #This is the default
        rxn_S00130.objective_coefficient = 0 #this is the default

        #Create the metabolites
        cpd_S00120_c = sb_model.metabolites.get_by_id('cpd_S00120_c') #4-DG-ATP
        cpd00033_c = sb_model.metabolites.get_by_id('cpd00033_c') #Glycine
        cpd00009_c = sb_model.metabolites.get_by_id('cpd00009_c') #Phosphate (Pi)
        cpd00067_c = sb_model.metabolites.get_by_id('cpd00067_c') #H+
        cpd_S00130_c = Metabolite('cpd_S00130_c', formula='C10H15O6N', name='Mycosporine-Gly', compartment='c') #Mycosporine-Gly

        #add the metabolites to the reaction:
        rxn_S00130.add_metabolites({cpd_S00120_c: -1.0,
                                  cpd00033_c: -1.0,
                                  cpd00009_c: 1.0,
                                  cpd00067_c: 1.0,
                                  cpd_S00130_c: 1.0})

        sb_model.add_reaction(rxn_S00130)

        ##############################################################################################################
        #AVA_3855
        #C10H15O6N + C3H7NO3 ->C13O8H20N2 + H2O
        #Mycosporine-Gly + Serine -> Shinorine + H2O
        rxn_S00140 = Reaction('rxn_S00140')
        rxn_S00140.name = 'Mycosporine-Gly_Shinorine'
        rxn_S00140.subsystem = 'c'
        rxn_S00140.lower_bound = -1000. #This is the default
        rxn_S00140.upper_bound = 1000. #This is the default
        rxn_S00140.reversibility = 1 #This is the default
        rxn_S00140.objective_coefficient = 0 #this is the default

        #Create the metabolites
        cpd_S00130_c = sb_model.metabolites.get_by_id('cpd_S00130_c') #Mycosporine-Gly
        cpd00054_c = sb_model.metabolites.get_by_id('cpd00054_c') #Serine
        cpd00001_c = sb_model.metabolites.get_by_id('cpd00001_c') #H20
        cpd_S00140_c = Metabolite('cpd_S00140_c', formula='C13O8H20N2', name='Shinorine', compartment='c') #Shinorine

        #add the metabolites to the reaction:
        rxn_S00140.add_metabolites({cpd_S00130_c: -1.0,
                                  cpd00054_c: -1.0,
                                  cpd00001_c: 1.0,
                                  cpd_S00140_c: 1.0})

        sb_model.add_reaction(rxn_S00140)

        ##############################################################################################################
        #Shinorine_C <=> Shinorine_E exchange reaction
        rxn_S00150 = Reaction('rxn_S00150')
        rxn_S00150.name = 'Shinorine_Transport'
        rxn_S00150.subsystem = 'Extracellular'
        rxn_S00150.lower_bound = -1000. #This is the default
        rxn_S00150.upper_bound = 1000. #This is the default
        rxn_S00150.reversibility = 1 #This is the default
        rxn_S00150.objective_coefficient = 0 #this is the default

        cpd_S00140_c = sb_model.metabolites.get_by_id('cpd_S00140_c') #Shinorine C
        cpd_S00140_e = Metabolite('cpd_S00140_e', formula='C13O8H20N2', name='Shinorine', compartment='e') #Shinorine E

        rxn_S00150.add_metabolites({cpd_S00140_c: -1.0, cpd_S00140_e: 1.0})
        sb_model.add_reaction(rxn_S00150)

        ##############################################################################################################
        #Shinorine_E <=> Shinorine_B boundary reaction
        rxn_S00150_EX = Reaction('EX_rxn_S00150_e')
        rxn_S00150_EX.name = 'Shinorine_Transport_Boundary'
        rxn_S00150_EX.subsystem = 'Extracellular'
        rxn_S00150_EX.lower_bound = -1000. #This is the default
        rxn_S00150_EX.upper_bound = 1000. #This is the default
        rxn_S00150_EX.reversibility = 1 #This is the default
        rxn_S00150_EX.objective_coefficient = 0 #this is the default

        cpd_S00140_e = sb_model.metabolites.get_by_id('cpd_S00140_e') #Shinorine E

        rxn_S00150_EX.add_metabolites({cpd_S00140_e: -1.0})
        sb_model.add_reaction(rxn_S00150_EX)

        return sb_model

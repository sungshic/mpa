function model = croda_analysis(model)

%AVA_3858 - from 3dehydroquinate
model = addReaction(model,{'rxn_S00160', ...
            '3-dehydoquinate_4-deoxygadusol'}', ...
                'cpd00699[c] + cpd00011[c] + 2 cpd00001[c] <=> cpd_S00110[c] + 2 cpd00007[c] + cpd00067[c]');

index = find(strcmp('cpd_S00110[c]', model.mets),1);
model.metFormulas{index}='C8H12O5';
                       

% %AVA_3858
% model = addReaction(model,{'rxn_S00170', ...
%             'Sedoheptulose7-phosphate_2-epi-5-epi-valiolone'}', ...
%                 'cpd00238[c] <=> cpd00009[c] + 2 cpd00067[c] + cpd_S00150[c]');

%index = find(strcmp('cpd_S00150[c]', model.mets),1);
%model.metFormulas{index}='C7H12O6';


% %ava_3857            
% model = addReaction(model,{'rxn_S00180', ...
%             '2-epi-5-epi-valiolone_4-deoxygadusol'}', ...
%                 'cpd_S00150[c]  + cpd00001[c] + cpd00011[c] <=> 2 cpd00007[c] + 2 cpd00067[c] + cpd_S00110[c]');
%  
% index = find(strcmp('cpd_S00110[c]', model.mets),1);
% model.metFormulas{index}='C8H12O5';            
            
%ava_3856_1        
model = addReaction(model,{'rxn_S00120', ...
            '4-deoxygadusol_4-deoxygadusol-ATP'}', ...
                'cpd_S00110[c] + cpd00002[c] <=> cpd00008[c] + cpd_S00120[c]');            


index = find(strcmp('cpd_S00120[c]', model.mets),1);
model.metFormulas{index}='C8H12O5';            
            
%ava_3856_2           
model = addReaction(model,{'rxn_S00130', ...
            '4-deoxygadusol-ATP_Mycosporine-Gly'}', ...
                'cpd_S00120[c] + cpd00033[c] <=> cpd00009[c] + cpd00067[c] + cpd_S00130[c]');
            

index = find(strcmp('cpd_S00130[c]', model.mets),1);
model.metFormulas{index}='C8H12O8P';
            
%ava_3855           
model = addReaction(model,{'rxn_S00140', ...
            'Mycosporine-Gly_Shinorine'}', ...
                'cpd_S00130[c] + cpd00054[c] <=> cpd00001[c] + cpd_S00140[c]');


index = find(strcmp('cpd_S00140[c]', model.mets),1);
model.metFormulas{index}='C10H15O6N';            
            
            
%shinorine transport         
model = addReaction(model,{'rxn_S00150', ...
            'shinorine_Transport'}', ...
                'cpd_S00140[c] <=> cpd_S00140[e]');   

index = find(strcmp('cpd_S00140[e]', model.mets),1);
model.metFormulas{index}='C13O8H20N2';
            
%shinorine boundary transport         
model = addReaction(model,{'EX_S00140(e)', ...
            'Shinorine_Transport_Boundary'}', ...
                'cpd_S00140[e] <=> ');  
                   
            
 model = changeObjective(model,{'bio00127'},1);
 model = changeObjective(model,{'rxn_S00150'},1); 
  
 model = changeRxnBounds(model,'bio00127',5,'l');
 
 model=setLBMedia(model,-10);
 
 biomassRxnIdx = find(strcmp(model.rxns, 'bio00127')==1);
 targetRxnIdx = find(strcmp(model.rxns, 'EX_S00140(e)')==1);

 solution = optimizeCbModel(model)
disp('biomass reaction:');
solution.x(biomassRxnIdx)
disp('target reaction:');
solution.x(targetRxnIdx)

%assignin('base', 'croda_model', model);
%no cplex
 %[xOptGene, populationOptGene, scoresOptGene, optGeneSolOptGene] = optGene(model_s, 'EX_S00140(e)', 'EX_cpd00027(e)', selectedRxns, 10);

            
function [selectedRxns,selectedGenes] = get_list_of_reactions(model)

%metabilotes with [e] to get transport reactions
listRxn=[];
transpMetabolites = regexp(model.mets, '[e]');
for i=1:length(transpMetabolites)
    if transpMetabolites{i}>0
       listRxn = [listRxn; i];
    end 
end

%get transport and exchange reactions from S matrix
listOfExcludetRxns=[];
for i=1:length(listRxn)
    rxnMatrix = model.S(listRxn(i),:);
    rxnArray = find(rxnMatrix);
    
    for y=1:length(rxnArray)
        listOfExcludetRxns=[listOfExcludetRxns;rxnArray(y)];
    end
end
%biomass
listOfExcludetRxns=[listOfExcludetRxns;1451];
%biomass exchange
listOfExcludetRxns=[listOfExcludetRxns;1554];
%ava reactions
listOfExcludetRxns=[listOfExcludetRxns;1701];
listOfExcludetRxns=[listOfExcludetRxns;1702];
listOfExcludetRxns=[listOfExcludetRxns;1703];
listOfExcludetRxns=[listOfExcludetRxns;1704];
listOfExcludetRxns=[listOfExcludetRxns;1705];
listOfExcludetRxns=[listOfExcludetRxns;1706];

uniqueRxns = unique(listOfExcludetRxns);

%get non exchange reactions
deSelectedRxns = {model.rxns{[uniqueRxns]}};
selectedRxns=setdiff(model.rxns,deSelectedRxns);



selectedGenes=[];
for i=1:length(model.genes)
    selectedGenes=[selectedGenes, model.genes(i)];
end

%[wtResSOK_R,delResSOK_R] = simpleOptKnock(model_s,'rxn_S00150',selectedGenes,true,0.05,false);
% [grRatioDble,grRateKO,grRateWT] =  doubleGeneDeletion(model,method,geneList1,geneList2,verbFlag)
% [wtRes,delRes] = simpleOptKnock(model,targetRxn,deletions,geneDelFlag,minGrowth,doubleDelFlag)
%[grRatio,grRateKO,grRateWT,hasEffect,delRxn,fluxSolution] = singleRxnDeletion(model,method,rxnList,verbFlag)
%[grRatioSGD,grRateKOSGD,grRateWTSGD,delRxnsSGD,hasEffectSGD] = singleGeneDeletion(model_s);
%[xOptGene, populationOptGene, scoresOptGene, optGeneSolOptGene] = optGene(model_s, 'rxn_S00150', '', selectedGenes, 10);
function rxns = getReactions(model)

exchangeFuncList = regexp(model.rxns, 'EX_');

exrxns = [];
for i=1:length(exchangeFuncList)
    if exchangeFuncList{i}==1
        exrxns = [exrxns, model.rxns(i)];
    end
end
%biomass, dna, rna, lipids
exrxns= [exrxns, {'bio00127','rxn05294','rxn05295','rxn11921'}];
rxns=setdiff(model.rxns,exrxns);


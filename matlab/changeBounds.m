function model = changeBounds(model, rxns)
% model = changeRxnBounds(model,rxnNameList,value,boundType)
rxnNameList=[];
for i=1:length(rxns)
    rxn_name1 = strcat(rxns{i},'_r');
    rxn_name2 = strcat(rxns{i},'_l');
    rxnNameList=[rxnNameList, {rxn_name1,rxn_name2}];
end
    
model = changeRxnBounds(model,rxnNameList,1,'l');    
model = changeRxnBounds(model,rxnNameList,1000,'u');

model = changeRxnBounds(model,rxns,-1000,'u');    
model = changeRxnBounds(model,rxns,-1,'l');     
    
    
    
    
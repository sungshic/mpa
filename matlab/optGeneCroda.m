function optGeneCroda(model)

%add shinorine pathway
%add medium reactions
%selectedRxns = get_list_of_reactions(model);
%[x, population, scores, optGeneSol] = optGene(model, targetRxn, substrateRxn, generxnList, MaxKOs, population)
%
[x, population, scores, optGeneSol] = optGene(model, 'rxn_S00150','EX_cpd00027_e',selectedRxns);

 [gdlsSolution, bilevelMILPproblem, gdlsSolutionStructs] = GDLS(model_s, {'rxn_S00150', 'bio00127'}, 'minGrowth', 65, 'selectedRxns', selectedRxns, 'maxKO', 50, 'nbhdsz', 3);


assignin('base', 'optGeneX', x);
assignin('base', 'optGenePopulation', population);
assignin('base', 'optGeneScores', scores);
assignin('base', 'optGeneOptGeneSol', optGeneSol);

% %where: targetRxn specifies the reaction to optimize; substrateRxn 
% specifies the exchange reaction for the growth; generxnList is a
% cell array of strings that specifies which genes or reactions are
% allowed to be deleted; and maxKOs sets the maximum number of 
% knockouts; x is the best scoring set as determined by the functions
% optGeneFitness or optGeneFitnessTilt; population is the binary 
% matrix representing the knockout sets; and optGeneSol is the 
% structure summarizing the results. If resuming a previous 
% simulation, the binary matrix (population) can be specified.
function model = splitReactions(model, rxns)
reaction1='';
reaction2='';
for i=1:length(rxns)
    %index of rxn in model.rxns
    index = find(strcmp(model.rxns,rxns{i}));
    if  index>0
        %if it is reversible
        if model.rev(index)==1
            %getting sparse matrix of metabolites presented in reaction
            mets = model.S(:,index);
            %getting metabolite indexes
            metArray = find(mets);
            %foreach metabolite in sparse matrix get it side
            equation_left_parts = [];
            equation_right_parts = [];
            for y=1:length(metArray)
                if mets(metArray(y),1) <0
                    %left side of equation
                    equation_left_parts= [equation_left_parts; ...
                        {sprintf('%s %s',num2str(abs(mets(metArray(y),1))),model.mets{metArray(y)})}];
                else
                    %right side of equation
                    equation_right_parts
                    abs(mets(metArray(y),1))
                    metArray(y)
                    rxns{i}
                    equation_right_parts= [equation_right_parts; ...
                        {sprintf('%s %s',num2str(abs(mets(metArray(y),1))),model.mets{metArray(y)})}];
                end    
            end
            equation1='';
            equation2='';
            for y=1:length(equation_left_parts(:,1))
                if ~isequal(y,length(equation_left_parts(:,1)))
                    equation1 = sprintf('%s %s %s ',equation1, equation_left_parts{y,:}, '+');
                else
                     equation1 = sprintf('%s %s ',equation1, equation_left_parts{y,:});
                end
            end
            for y=1:length(equation_right_parts(:,1))
                if ~isequal(y,length(equation_right_parts(:,1)))
                    equation2 = sprintf('%s %s %s',equation2, equation_right_parts{y,:}, '+');
                else
                     equation2 = sprintf('%s %s',equation2, equation_right_parts{y,:});
                end
            end
       
            reaction1 = sprintf('%s %s %s',equation1,'->',equation2);
            reaction2 = sprintf('%s %s %s',equation2,'->',equation1);
           %remove original, add splitted
   
           model = removeRxns(model,rxns(i),false,false);
           model = addReaction(model, strcat(rxns{i},'_r'), reaction1);
           model = addReaction(model, strcat(rxns{i},'_l'), reaction2);
           
           %get rxn indexes and change bounds
           
        end
    end
end
            
            
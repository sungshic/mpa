function optKnockSol = optKnockCroda(model,selectedRxns)

%[selectedRxns,selectedGenes] = get_list_of_reactions(model);

%assignin('base', 'selectedRxns2', deSelectedRxns);

options.targetRxn = 'rxn_S00150';
options.vMax = 1000;
options.numDel = 10;
options.numDelSense = 'L'; % 5 or less
constrOpt.rxnList = {'bio00127'};
constrOpt.values = [5];
constrOpt.sense = 'GE';
optKnockSol = OptKnock(model, selectedRxns, options, constrOpt);

assignin('base', 'optKnockSol', optKnockSol);


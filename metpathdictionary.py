__author__ = 'spark'

import csv
import re
import numpy as np

class MetPathDict():
    def __init__(self, cmplist_csvfilepath, rxnlist_csvfilepath):
        try:
            f_cmplist = open(cmplist_csvfilepath, 'rU')
            f_rxnlist = open(rxnlist_csvfilepath, 'rU')
            self._f_cmplist = f_cmplist
            self._f_rxnlist = f_rxnlist
            self._cmpDict = {}
            self._rxnDict = {}
            self.buildCmpLookupTable()
            self.buildRxnLookupTable()

        except Exception as e:
            print e

    def buildCmpLookupTable(self):
        try:
            reader = csv.reader(self._f_cmplist)
            for row in reader:
                self._cmpDict[row[0]] = self.parseAliases(row[1])

        except Exception as e:
            print e

    def buildRxnLookupTable(self):
        try:
            reader = csv.reader(self._f_rxnlist)
            for row in reader:
                #print row
                if row[1].strip(): # if aliases string not empty
                    self._rxnDict[row[0]] = {'aliases':self.parseAliases(row[1]), 'definition':row[2]}
                else: # if aliases string empty, use id as the alias
                    self._rxnDict[row[0]] = {'aliases':row[0], 'definition':row[2]}
        except Exception as e:
            print e

    def mergeRxnLookupTable(self, rxnDict2):
        for key in rxnDict2.keys():
            if not self._rxnDict.has_key(key):
                self._rxnDict[key] = rxnDict2[key] #{'aliases':rxnDict2[key]['aliases'], 'definition':rxnDict2[key]['definition']}
            else: # there is a clash
                definition2 = rxnDict2[key]['definition']
                if definition2.strip(): # if definition2 not empty
                    # replace the definition
                    self._rxnDict[key]['definition'] = definition2

    def parseAliases(self, aliases):
        match_list = re.findall('\|?([^\|]+)\|?', aliases) # match anything in between '|' that are not '|'
        return match_list

    # splits cmp id with trailing _e or _c that respectively denotes extracellular or cytosolic compounds.
    # for example, 'cpd00001_e' will return ['cpd00001', 'e']
    def parseCmpID(self, cmpID):
        match_list = re.findall('([^_]+)', cmpID)
        if len(match_list) == 3: # case of boundary species of form EX_cpdxxxx_e
            match_list[0] = match_list[1]
            match_list[1] = match_list[2] + '(EX)'

        return match_list

    def parseIDType(self, vID):
        match_list = re.findall('(rxn|cpd|bio)+((.*)_reverse|.*)', vID)
        if match_list:
            return list(match_list[0])
        else:
            return match_list # return an empty list []

    def lookupAliasesByID(self, vID):
        idType = self.parseIDType(vID)
        if len(idType) >= 1: # we have a match
            if idType[0] == 'rxn' or idType[0] == 'bio':
                if idType[2] != '': # this is True of the case with any rxns having the '_reverse' postfix
                    # then lookup the aliase for its counterpart rxn, as the lookup dict would not have an entry for the '_reverse' rxns.
                    return self.lookupRxnAliasesByID(idType[0]+idType[2]), None
                else:
                    return self.lookupRxnAliasesByID(vID), None
            elif idType[0] == 'cpd':
                cmpID = self.parseCmpID(vID)
                aliases = self.lookupCmpByID(cmpID[0])
                #aliases[0] = aliases[0] + '[' + cmpID[1] + ']'
                return aliases, cmpID[1]
        # no match
        return None

    def lookupCmpByID(self, cmpID):
        try:
            return self._cmpDict[cmpID]
        except Exception as e:
            print e

    def lookupRxnAliasesByID(self, rxnID):
        try:
            return self._rxnDict[rxnID]['aliases']
        except Exception as e:
            print e

    def lookupRxnDefinitionByID(self, rxnID):
        try:
            return self._rxnDict[rxnID]['definition']
        except Exception as e:
            print e

class GeneDict():
    def __init__(self, genename_csvfilepath, cb_rxn_lookup):
        try:
            f_genelist = open(genename_csvfilepath, 'rU')
            self._f_genelist = f_genelist
            self._genedict = {}
            self._rxndict = {}
            self._rxntogenedict = {}
            self.buildGeneNameLookupTable()
            self.buildRxnIDLookupTable(cb_rxn_lookup)
            self.buildRxnIDToGeneNameLookup(cb_rxn_lookup)

        except Exception as e:
            print e
            raise e


    def buildGeneNameLookupTable(self):
        try:
            reader = csv.reader(self._f_genelist)
            genelist = list(reader)
            for row in genelist[1:]:
                #print row
                if row[1].strip(): # if aliases string not empty
                    self._genedict[row[0]] = {'aliases':self.parseAliases(row[1]), 'definition':row[4]}
                else: # if aliases string empty, use id as the alias
                    self._genedict[row[0]] = {'aliases':row[0], 'definition':row[4]}
        except Exception as e:
            print e
            raise e

    def buildRxnIDLookupTable(self, cb_rxn_lookup):
        try:
            for rxn in cb_rxn_lookup:
                peg_id_list = rxn.get_gene()
                if len(peg_id_list) > 0:
                    gene_name_list = [gene['aliases'][0] for gene in self.lookupGeneNameByPEGId(peg_id_list)]
                    for gene_name in gene_name_list:
                        if gene_name in self._rxndict:
                            cur_rxn_list = self._rxndict[gene_name]
                            if not cur_rxn_list:
                                self._rxndict[gene_name] = [rxn.id]
                            elif rxn not in cur_rxn_list:
                                cur_rxn_list.append(rxn.id)
                                self._rxndict[gene_name] = cur_rxn_list
                        else:
                            self._rxndict[gene_name] = [rxn.id]

        except Exception as e:
            print e
            raise e

    # convert iBsu1103 specific rxn ids (e.g. rxn00740) to their corresponding B. subtilis common gene names (e.g. yjcI)
    # rxn to gene relationships could be 1-to-1, 1-to-many, many-to-1 or many-to-many
    # input arguments
    ## rxn_id_list: list of iBSU1103 rxn ids in the form of 'rxn%5d', where %5d is a five digit number,
    ## cb_rxn_lookup: list of cobra reactions (of class cobra.core.DictList.DictList) constituting a genome-scale model
    # output: list of common gene names (e.g. trpC, metE)
    def buildRxnIDToGeneNameLookup(self, cb_rxn_lookup):
        for rxn in cb_rxn_lookup:
            peg_id_list = rxn.get_gene()
            gene_names = [gene['aliases'][0] for gene in self.lookupGeneNameByPEGId(peg_id_list)]
            self._rxntogenedict[rxn.id] = gene_names

    def parseAliases(self, aliases):
        match_list = re.findall('^(.+?),', aliases) # match anything before the first comma
        return match_list

    # a predicate to test if the str contains a peg id starting with "peg." followed by some number
    def isPEGId(self, gene_id_str):
        match_list = re.findall('^(peg\.\d+)', gene_id_str)
        if len(match_list) > 0:
            return True
        else:
            return False

    # lookup common gene names by peg ids
    def lookupGeneNameByPEGId(self, cb_gene_list):
        gene_name_list = []
        for cb_gene in cb_gene_list:
            if self.isPEGId(cb_gene.id):
                gene_name_list.append(self._genedict[cb_gene.id])

        return gene_name_list


    # convert iBsu1103 specific rxn ids (e.g. rxn00740) to their corresponding B. subtilis common gene names (e.g. yjcI)
    # rxn to gene relationships could be 1-to-1, 1-to-many, many-to-1 or many-to-many
    # input arguments
    ## rxn_id_list: list of iBSU1103 rxn ids in the form of 'rxn%5d', where %5d is a five digit number,
    # output: list of common gene names (e.g. trpC, metE)
    def convertRxnIDsToGeneNames(self, rxn_id_list):
        gene_name_list = []
        for rxn_id in rxn_id_list:
            if rxn_id in self._rxntogenedict:
                corresponding_genes = self._rxntogenedict[rxn_id]
                if len(corresponding_genes) > 0:
                    gene_name_list = list(np.unique(gene_name_list + corresponding_genes))

        return gene_name_list


    def convertGeneNamesToRxnIDs(self, gene_name_list):
        rxn_id_list = []
        for gene_name in gene_name_list:
            if gene_name in self._rxndict:
                rxns = self._rxndict[gene_name]
                rxn_id_list = list(np.unique(rxn_id_list + rxns))

        return rxn_id_list


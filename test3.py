__author__ = 'spark'

from setShinorineReactions import setToyModel
from addLoopLawConstraints import addLoopLawConstraints

tm = setToyModel()
tm.to_array_based_model()


from ll_cobra import *

lp_problem = format_lp_problem(tm, True)
gurobiout = solve_gurobi(lp_problem)

cobra_sol = format_cobra_solution_from_GUROBIresult(gurobiout)


tm2 = setToyModel()
tm2_arr = tm2.to_array_based_model()

from addLoopLawConstraints import addLoopLawConstraintsCOBRA

tm2_arr, intmet = addLoopLawConstraintsCOBRA(tm2_arr)
cbsol1 = tm2.optimize(solver='gurobi', the_problem=None)
print 'optimization done.'
#solve_milp(LPproblem)
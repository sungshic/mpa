# -*- coding: utf-8 -*-
"""

"""

#TODO delete aroA, tkt, ywjH genes


import cobra
import re 
from cobra.io.sbml import *

sbml_file = "data/iBsu1103V2/nar-01731-m-2012-File013.xml"
sb_model = create_cobra_model_from_sbml_file(sbml_file)

from setLBmediaV2 import *
from setShinorineReactions import *
from set3Dehydrouinate4deoxygadusol import *
from setSinorineReactionsFrom3Dehydroquinate import *

#add Shinorine transformation pathway from Sedoheptulose7-phosphate to Shinorine
#setShinorineReactions(sb_model)

#add reaction of transformation 3-dehydoquinate to 4-deoxygadusol to Shinorine pathway
#set3Dehydroquinate4Deoxygadusol(sb_model)

#add Shinorine transformation pathway from 3-dehydoquinate to shinorine
set3Dehydroquinate4DeoxygadusolToShinorine(sb_model)


setLBmedia(sb_model, -10)
biomassRxnIdx = sb_model.reactions.index('bio00127')
targetRxnIdx = sb_model.reactions.index('rxn_S00150') #shinorine

#targetRxnIdx = sb_model.reactions.index('rxn02213') #phenylalanin


sb_model_mat = sb_model.to_array_based_model()


# dual objective
sb_model.reactions.rxn_S00150.objective_coefficient = 1

#sb_model.reactions.rxn02213.objective_coefficient = 1
sb_model.reactions.bio00127.objective_coefficient = 1

# dual objective with minimum biomass requirement
print ' dual objective with minimum biomass requirement'
sb_model.reactions.bio00127.lower_bound = 5

sb_model_mat.update()
sb_model_mat.optimize()

solution = sb_model_mat.solution
print solution
print 'biomass flux: ' + str(sb_model.solution.x[biomassRxnIdx])
print 'target flux: ' + str(sb_model.solution.x[targetRxnIdx])

#print '######### aroA tkt ywjh deletion ##########'


aroA1 = sb_model.reactions.index('rxn01332')
aroA2 = sb_model.reactions.index('rxn01256')

tkt1 = sb_model.reactions.index('rxn00785')
tkt2 = sb_model.reactions.index('rxn01200')

ywjh = sb_model.reactions.index('rxn01333')

#aroA
#sb_model.reactions.rxn01332.lower_bound = 0
#sb_model.reactions.rxn01332.upper_bound = 0
#sb_model.reactions.rxn01256.lower_bound = 0
#sb_model.reactions.rxn01256.upper_bound = 0

#tkt
#sb_model.reactions.rxn00785.lower_bound = 0
#sb_model.reactions.rxn00785.upper_bound = 0
#sb_model.reactions.rxn01200.lower_bound = 0
#sb_model.reactions.rxn01200.upper_bound = 0
''''
#ywjh
sb_model.reactions.rxn01333.lower_bound = 0
sb_model.reactions.rxn01333.upper_bound = 0

#pfk

sb_model.reactions.rxn01343.lower_bound = 0
sb_model.reactions.rxn01343.upper_bound = 0
sb_model.reactions.rxn02314.lower_bound = 0
sb_model.reactions.rxn02314.upper_bound = 0
sb_model.reactions.rxn00545.lower_bound = 0
sb_model.reactions.rxn00545.upper_bound = 0
'''''

''''
#optKnock
sb_model.reactions.rxn09997.lower_bound = 0
sb_model.reactions.rxn09997.upper_bound = 0

sb_model.reactions.rxn05040.lower_bound = 0
sb_model.reactions.rxn05040.upper_bound = 0

sb_model.reactions.rxn01509.lower_bound = 0
sb_model.reactions.rxn01509.upper_bound = 0

sb_model.reactions.rxn00836.lower_bound = 0
sb_model.reactions.rxn00836.upper_bound = 0

sb_model.reactions.rxn00139.lower_bound = 0
sb_model.reactions.rxn00139.upper_bound = 0

'''''

sb_model_mat.update()
sb_model_mat.optimize()

solution = sb_model_mat.solution
print solution
print 'biomass flux 3: ' + str(sb_model.solution.x[biomassRxnIdx])
print 'target flux 3: ' + str(sb_model.solution.x[targetRxnIdx])




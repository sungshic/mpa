# -*- coding: utf-8 -*-
"""
Created on Wed Oct 03 18:53:34 2012

@author: Sungshic Park (b0928115@ncl.ac.uk)
"""

#TODO delete aroA, tkt, ywjH genes


import cobra
import re 
from cobra.io.sbml import *

sbml_file = "data/iBsu1103V2/nar-01731-m-2012-File013.xml"
sb_model = create_cobra_model_from_sbml_file(sbml_file)

from setLBmediaV2 import *
from setShinorineReactions import *
from set3Dehydrouinate4deoxygadusol import *
from setSinorineReactionsFrom3Dehydroquinate import *

#add Shinorine transformation pathway from Sedoheptulose7-phosphate to Shinorine
setShinorineReactions(sb_model)

#add reaction of transformation 3-dehydoquinate to 4-deoxygadusol to Shinorine pathway
set3Dehydroquinate4Deoxygadusol(sb_model)

#add Shinorine transformation pathway from 3-dehydoquinate to shinorine
#set3Dehydroquinate4DeoxygadusolToShinorine(sb_model)


setLBmedia(sb_model, -10)
biomassRxnIdx = sb_model.reactions.index('bio00127')
targetRxnIdx = sb_model.reactions.index('rxn_S00150')


print 'objective function is set to biomass'

sb_model.reactions.bio00127.objective_coefficient = 1

sb_model_mat = sb_model.to_array_based_model()
sb_model_mat.optimize()
print sb_model_mat.solution

print 'biomass flux 1: ' + str(sb_model.solution.x[biomassRxnIdx])
print 'target flux 1: ' + str(sb_model.solution.x[targetRxnIdx])


print ' dual objective'
# dual objective
sb_model.reactions.rxn_S00150.objective_coefficient = 1
sb_model.reactions.bio00127.objective_coefficient = 1


sb_model_mat.update()
sb_model_mat.optimize()

print sb_model_mat.solution
print 'biomass flux 2: ' + str(sb_model.solution.x[biomassRxnIdx])
print 'target flux 2: ' + str(sb_model.solution.x[targetRxnIdx])


# dual objective with minimum biomass requirement
print ' dual objective with minimum biomass requirement'
sb_model.reactions.bio00127.lower_bound = 5

sb_model_mat.update()
sb_model_mat.optimize()

solution = sb_model_mat.solution
print solution
print 'biomass flux 3: ' + str(sb_model.solution.x[biomassRxnIdx])
print 'target flux 3: ' + str(sb_model.solution.x[targetRxnIdx])

print 'single gene deletion:'

from single_gene_deletion import *
singleGeneDeletion(sb_model)


print '######### aroA tkt ywjh deletion ##########'


aroA1 = sb_model.reactions.index('rxn01332')
aroA2 = sb_model.reactions.index('rxn01256')

tkt1 = sb_model.reactions.index('rxn00785')
tkt2 = sb_model.reactions.index('rxn01200')

ywjh = sb_model.reactions.index('rxn01333')


#sb_model.reactions.rxn01332.lower_bound = 0
#sb_model.reactions.rxn01332.upper_bound = 0

#sb_model.reactions.rxn01256.lower_bound = 0
#sb_model.reactions.rxn01256.upper_bound = 0

#sb_model.reactions.rxn00785.lower_bound = 0
#sb_model.reactions.rxn00785.upper_bound = 0

#b_model.reactions.rxn01200.lower_bound = 0
#sb_model.reactions.rxn01200.upper_bound = 0

#sb_model.reactions.rxn01333.lower_bound = 0
#sb_model.reactions.rxn01333.upper_bound = 0

sb_model_mat.update()
sb_model_mat.optimize()

solution = sb_model_mat.solution
print solution
print 'biomass flux 3: ' + str(sb_model.solution.x[biomassRxnIdx])
print 'target flux 3: ' + str(sb_model.solution.x[targetRxnIdx])


print 'double deletion: '
from double_gene_deletion import *
#doubleGeneDeletion(sb_model)

''''
from cPickle import load
import numpy
import pickle, pprint

print 'open results file'

pkl_file = open('double_deletion_results.pickle')

data1 = pickle.load(pkl_file)

arr = data1['data']

for x in range(0, len(data1['data'][0])-1):
    for y in range(0, len(data1['data'][0])-1):
        if float(data1['data'][x][y]) > solution.f:
            if float(data1['data'][x][y]) - solution.f > 1:
                print 'geneX: ' + str(data1['x'][x]) + ' geneY: ' + str(data1['y'][y]) + ' biomass flux: ' + str(float(data1['data'][x][y])) + ' change: ' + str(float(data1['data'][x][y]) - solution.f)

#pprint.pprint(data1['data'])

#pprint.pprint(data1)

pkl_file.close()

'''''




#print 'knock-out analysis'

#sb_model_knockout = sb_model.copy()
#sb_model_ko_mat = sb_model_knockout.to_array_based_model()

''''
#setLBmedia(sb_model_knockout, -1*uptakeRate)
for uptakeRate in range(10, 15):
	print sb_model_ko_mat.reactions[substrateRxnIdx].name + ' is ' + str(uptakeRate)
	sb_model_ko_mat.reactions[substrateRxnIdx].lower_bound = -1*uptakeRate

	sb_model_ko_mat.update()
	sb_model_ko_mat.optimize()
	if sb_model_ko_mat.solution != None:
		print sb_model_ko_mat.solution
		print 'biomass flux: ' + str(sb_model_ko_mat.solution.x[biomassRxnIdx])
		print 'target flux: ' + str(sb_model_ko_mat.solution.x[targetRxnIdx])
	else:
		print 'fatal knock-out'
	

for rxnidx, rxn in enumerate(sb_model_knockout.reactions):
	m = re.search('(rxn|EX\_cpd)\w+', rxn.id)
	if m and rxn.id not in ['rxn05146', 'bio00006']:
		print '## ' + rxn.id
		#print sb_model_knockout.reactions[rxnidx].id
	
		rxn.lower_bound = 0
		rxn.upper_bound = 0
		sb_model_ko_mat.update()
		sb_model_ko_mat.optimize()
		if sb_model_ko_mat.solution != None:
			print sb_model_ko_mat.solution
			print 'biomass flux: ' + str(sb_model_ko_mat.solution.x[biomassRxnIdx])
			print 'target flux: ' + str(sb_model_ko_mat.solution.x[targetRxnIdx])
		else:
			print 'fatal knock-out'


		# restore
		rxn.lower_bound = sb_model.reactions[rxnidx].lower_bound
		rxn.upper_bound = sb_model.reactions[rxnidx].upper_bound



'''''


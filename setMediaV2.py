# -*- coding: utf-8 -*-
"""
Created on Wed Oct 03 17:08:37 2012

@author: Sungshic Park (b0928115@ncl.ac.uk)
"""

def setMediaV2(model, lb):
    exchangeFuncList = model.reactions.query(search_function='EX_')



    rxnList =   [
                'EX_cpd00027_e',   	#D-Glucose
                #'EX_cpd00130_e',    #L-Malate
                'EX_cpd00013_e',	#NH3
                'EX_cpd00048_e',	#Sulfate
                'EX_cpd00205_e',	#K+
                'EX_cpd00067_e',	#H+
                'EX_cpd00009_e',	#Phosphate
                'EX_cpd00254_e',	#Mg
                'EX_cpd00063_e',    #Ca2+
                'EX_cpd00011_e',    #CO2
                #'EX_cpd00099_e',	#Cl-

                'EX_cpd00149_e',	#Co2+
                'EX_cpd00058_e',	#Cu2+
                'EX_cpd10516_e',	#fe3
                'EX_cpd00001_e',	#H2O
                'EX_cpd00030_e',	#Mn2+
                #'EX_cpd00065_e',	#L-Tryptophan
                'EX_cpd00063_e',	#Ca2+
                'EX_cpd00007_e',	#O2
                'EX_cpd00034_e',	#Zn2+
                'EX_cpd10515_e',	#Fe2+
                'EX_cpd00971_e'	    #Na+
                ]

    for rxn in exchangeFuncList:
        rxn.lower_bound = 0
        rxn.upper_bound = 1000
        
    for rxnID in rxnList:
        rxn = model.reactions.get_by_id(rxnID)
        rxn.lower_bound = lb
        rxn.upper_bound = 1000

    print rxn.name + ', '
    return model



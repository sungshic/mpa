import sys
from cx_Freeze import setup, Executable

# Dependencies are automatically detected, but it might need fine tuning.
build_exe_options = {"packages": ["os"], "excludes": ["tkinter"]}

# GUI applications require a different base on Windows (the default is for a
# console application).

if sys.platform == "win32":
    base = "Win32GUI"

exedef = Executable(
	script="test_graphdraw.py",
	base=None
)

setup(  name = "sparkfreezetest",
        version = "0.1",
        description = "My frozen app!",
        options = {"build_exe": build_exe_options},
        executables = [exedef])

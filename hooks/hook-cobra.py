__author__ = 'spark'

import sys, DLFCN
sys.setdlopenflags(DLFCN.RTLD_NOW | DLFCN.RTLD_GLOBAL)

from hookutils import collect_submodules

hiddenimports = (collect_submodules('cplex') +
		collect_submodules('cobra') + 
		collect_submodules('cobra.core') + 
		collect_submodules('cobra.solvers') + 
		collect_submodules('cobra.io') + 
		collect_submodules('numpy')) 

datas = [
	('/home/spark/pyvirtualenv/pycobra/lib/python2.7/site-packages/cobra/*', 'cobra'),
	('/home/spark/workspace/CrodaMPA/data', 'data')
	]

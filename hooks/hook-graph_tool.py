__author__ = 'spark'

from hookutils import collect_submodules

import DLFCN, sys
sys.setdlopenflags(DLFCN.RTLD_NOW | DLFCN.RTLD_GLOBAL)

hiddenimports = (collect_submodules('graph_tool') +
		collect_submodules('graph_tool.draw') +
		collect_submodules('graph_tool.flow') +
		collect_submodules('graph_tool.run_action') +
		collect_submodules('graph_tool.centrality') +
		collect_submodules('graph_tool.collection') +
		collect_submodules('graph_tool.community') +
		collect_submodules('graph_tool.topology') +
		collect_submodules('graph_tool.util') +
		collect_submodules('graph_tool.stats') +
		collect_submodules('numpy')) 

datas = [
	('/usr/lib/python2.7/dist-packages/graph_tool/*', 'graph_tool')
	]

__author__ = 'Jurijs Meitalovs'

from cobra.io.sbml import *

def set3Dehydroquinate4Deoxygadusol(sb_model):


 ##############################################################################################################
        #UKNOWN GENE - transformation from 3-dehydoquinate to 4-deoxygadusol
        #C7H9O6+CO2+2H2O -> C8H12O5 + 2O2 + H+
        rxn_S00160 = Reaction('rxn_S00160')
        rxn_S00160.name = '3-dehydoquinate_4-deoxygadusol'
        rxn_S00160.subsystem = 'c'
        rxn_S00160.lower_bound = -1000. #This is the default
        rxn_S00160.upper_bound = 1000. #This is the default
        rxn_S00160.reversibility = 1 #This is the default
        rxn_S00160.objective_coefficient = 0 #this is the default

        gene_reaction_rule = '( UKNOWN_GENE_3DQ_4DG )'
        rxn_S00160.add_gene_reaction_rule(gene_reaction_rule)

        cpd00699_c = sb_model.metabolites.get_by_id('cpd00699_c') #3-Dehydroquinate
        cpd00011_c = sb_model.metabolites.get_by_id('cpd00011_c') #CO2
        cpd00001_c = sb_model.metabolites.get_by_id('cpd00001_c') #H20
        cpd_S00110_c = sb_model.metabolites.get_by_id('cpd_S00110_c') #4-DG
        cpd00007_c = sb_model.metabolites.get_by_id('cpd00007_c') #O2
        cpd00067_c = sb_model.metabolites.get_by_id('cpd00067_c') #H+

        #add the metabolites to the reaction:
        rxn_S00160.add_metabolites({cpd00699_c: -1.0,
                                  cpd00011_c: -1.0,
                                  cpd00001_c: -2.0,
                                  cpd_S00110_c: 1.0,
                                  cpd00007_c: 2.0,
                                  cpd00067_c: 1.0})

        sb_model.add_reaction(rxn_S00160)

        return sb_model
__author__ = 'Jurijs Meitalovs'

#cobra/examples/07_double_deletion_parallel.py
#
#Advanced user example illustrating how to perform double deletion studies on a multicore system
#

#TODO install Pyton Parallel to launch it

def doubleGeneDeletion(sb_model):

        from cobra.flux_analysis.double_deletion import double_deletion
        from cPickle import load, dump
        from time import time
        from cobra.io.sbml import *


        print 'dual objective for double gene deletion'
        sb_model.reactions.bio00127.objective_coefficient = 1
        sb_model.reactions.rxn_S00150.objective_coefficient = 1
        sb_model.reactions.bio00127.lower_bound = 5

        number_of_processes = 2 #Number of parallel processes to start
        number_of_genes = len(sb_model.genes) #Total number of genes to perform double deletion on
        out_filename = 'double_deletion_results.pickle'


        #When specifying the genes to delete use the locus ids because cobra.Gene objects
        #are not thread safe and copy times might be excessive.
        gene_list = [x.id for x in sb_model.genes[:number_of_genes]]

        start_time = time()
        print 'running double deletion for %i genes on %i cores'%(len(gene_list), number_of_processes)

        the_results = double_deletion(sb_model, element_list_1=gene_list, number_of_processes=number_of_processes)

        print 'took %1.2f seconds to do double deletion for %i genes'%(time() - start_time, len(gene_list))

        with open(out_filename, 'w') as out_file:
            dump(the_results, out_file)
            print 'saved the results to %s'%out_filename


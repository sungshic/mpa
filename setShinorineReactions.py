__author__ = 'spark'

import cobra
import re

from cobra.io.sbml import *

def setToyModel():
    toymodel = Model()

    met_A = Metabolite('metA')
    met_B = Metabolite('metB')
    met_C = Metabolite('metC')
    toymodel.add_metabolites([met_A, met_B, met_C])

    rxn_v1 = Reaction('v1')
    rxn_v1.lower_bound = -10
    rxn_v1.upper_bound = 10
    rxn_v1.reversibility = 0
    rxn_v1.objective_coefficient = 0
    rxn_v1.add_metabolites({met_A: -1.0, met_B: 1.0})

    toymodel.add_reaction(rxn_v1)

    rxn_v2 = Reaction('v2')
    rxn_v2.lower_bound = -10
    rxn_v2.upper_bound = 10
    rxn_v2.reversibility = 1
    rxn_v2.objective_coefficient = 0
    rxn_v2.add_metabolites({met_B: -1.0, met_C: 1.0})

    toymodel.add_reaction(rxn_v2)

    rxn_v3 = Reaction('v3')
    rxn_v3.lower_bound = -10
    rxn_v3.upper_bound = 10
    rxn_v3.reversibility = 1
    rxn_v3.objective_coefficient = 1
    rxn_v3.add_metabolites({met_A: -1.0, met_C: 1.0})

    toymodel.add_reaction(rxn_v3)

    rxn_vA = Reaction('vA')
    rxn_vA.lower_bound = 0
    rxn_vA.upper_bound = 1
    rxn_vA.reversibility = 0
    rxn_vA.objective_coefficient = 0
    rxn_vA.add_metabolites({met_A: 1.0})

    toymodel.add_reaction(rxn_vA)

    rxn_vB = Reaction('vB')
    rxn_vB.lower_bound = 0
    rxn_vB.upper_bound = 1
    rxn_vB.reversibility = 0
    rxn_vB.objective_coefficient = 0
    rxn_vB.add_metabolites({met_C: -1.0})

    toymodel.add_reaction(rxn_vB)
    #toymodel = toymodel.to_array_based_model()
    #toymodel.update()

    return toymodel




def setShinorineReactions(model_manager, isShikimateActive=True):

        ##############################################################################################################
       #AVA_3858
        #C7H15O10P -> C7H12O5 + HPO4 + 2H+
        #SHP -> 2-epi-5-epi-valiolone+ Pi + 2H+
	print 'adding rxnS00170'
        rxn_S00170 = Reaction('rxnS00170')
        #rxn_S00170.name = 'Sedoheptulose7-phosphate_2-epi-5-epi-valiolone'
        rxn_S00170.name = 'AVA_3858'
        rxn_S00170.subsystem = 'c'
        rxn_S00170.lower_bound = -1000. #This is the default
        rxn_S00170.upper_bound = 1000. #This is the default
        rxn_S00170.reversibility = 1 #This is the default
        rxn_S00170.objective_coefficient = 0 #this is the default

        gene_reaction_rule = '( AVA_3858 )'
        rxn_S00170.add_gene_reaction_rule(gene_reaction_rule)

        cpd00238_c = model_manager._cb_model.metabolites.get_by_id('cpd00238_c') #Sedoheptulose7-phosphate
        cpd00009_c = model_manager._cb_model.metabolites.get_by_id('cpd00009_c') #Phosphate (Pi)
        cpd00067_c = model_manager._cb_model.metabolites.get_by_id('cpd00067_c') #H+
        cpd_S00150_c = Metabolite('cpdS00150_c', formula='C7H12O6', name='2-epi-5-epi-valiolone', compartment='c') #2-epi-5-epi-valiolone

        #add the metabolites to the reaction:
	print 'adding metabolites to rxn_S00170'
        rxn_S00170.add_metabolites({cpd00238_c: -1.0,
                                  cpd00009_c: 1.0,
                                  cpd00067_c: 2.0,
                                  cpd_S00150_c : 1.0})

	print 'adding rxn to model manager'
        model_manager.cobra_lazy_add_nontransport_rxn(rxn_S00170) #sb_model.add_reaction(rxn_S00170)
	print 'adding met to model manager'
        model_manager.cobra_lazy_add_new_met_by_rxn(rxn_S00170)

        ''''
        rxn_S00100 = Reaction('rxn_S00100')
        rxn_S00100.name = 'Sedoheptulose7-phosphate_2-demethyl-4-deoxygadusol'
        rxn_S00100.subsystem = 'c'
        rxn_S00100.lower_bound = -1000. #This is the default
        rxn_S00100.upper_bound = 1000. #This is the default
        rxn_S00100.reversibility = 1 #This is the default
        rxn_S00100.objective_coefficient = 0 #this is the default

        gene_reaction_rule = '( AVA_3858 )'
        rxn_S00100.add_gene_reaction_rule(gene_reaction_rule)

        cpd00238_c = sb_model.metabolites.get_by_id('cpd00238_c') #Sedoheptulose7-phosphate
        cpd00009_c = sb_model.metabolites.get_by_id('cpd00009_c') #Phosphate (Pi)
        cpd00001_c = sb_model.metabolites.get_by_id('cpd00001_c') #H20
        cpd00067_c = sb_model.metabolites.get_by_id('cpd00067_c') #H+
        cpd_S00100_c = Metabolite('cpd_S00100_c', formula='C7H10O5', name='2-demethyl-4-deoxygadusol', compartment='c') #DDG

        #add the metabolites to the reaction:
        rxn_S00100.add_metabolites({cpd00238_c: -1.0,
                                  cpd00009_c: 1.0,
                                  cpd00001_c: 1.0,
                                  cpd00067_c: 2.0,
                                  cpd_S00100_c : 1.0})

        sb_model.add_reaction(rxn_S00100)
        '''''

        ##############################################################################################################
        #AVA_3857
        #C7H12O6 + CO2 + H2O -> C8H12O5 + 2O2 +2H+
        #2E5EV + CO2 + H2O -> 4-DG + 2O2 + 2H+
	print 'adding rxnS00180'
        rxn_S00180 = Reaction('rxnS00180')
        #rxn_S00180.name = '2-epi-5-epi-valiolone_4-deoxygadusol'
        rxn_S00180.name = 'AVA_3857'
        rxn_S00180.subsystem = 'c'
        rxn_S00180.lower_bound = -1000. #This is the default
        rxn_S00180.upper_bound = 1000. #This is the default
        rxn_S00180.reversibility = 1 #This is the default
        rxn_S00180.objective_coefficient = 0 #this is the default

        gene_reaction_rule = '( AVA_3857 )'
        rxn_S00180.add_gene_reaction_rule(gene_reaction_rule)

        #Create the metabolites
        cpd_S00150_c = model_manager._cb_model.metabolites.get_by_id('cpdS00150_c') #2-epi-5-epi-valiolone
        cpd00001_c = model_manager._cb_model.metabolites.get_by_id('cpd00001_c') #H20
        cpd00011_c = model_manager._cb_model.metabolites.get_by_id('cpd00011_c') #CO2
        cpd00007_c = model_manager._cb_model.metabolites.get_by_id('cpd00007_c') #O2
        cpd00067_c = model_manager._cb_model.metabolites.get_by_id('cpd00067_c') #H+
        cpd_S00110_c = Metabolite('cpdS00110_c', formula='C8H12O5', name='4-deoxygadusol', compartment='c') #4DG

        #add the metabolites to the reaction:
        rxn_S00180.add_metabolites({cpd_S00150_c: -1.0,
                                  cpd00001_c: -1.0,
                                  cpd00011_c: -1.0,
                                  cpd00007_c: 2.0,
                                  cpd00067_c: 2.0,
                                  cpd_S00110_c: 1.0})

        model_manager.cobra_lazy_add_nontransport_rxn(rxn_S00180)
        model_manager.cobra_lazy_add_new_met_by_rxn(rxn_S00180)

        '''''
         ##############################################################################################################
        #AVA_3857
        #C7H10O5 + CO2 + 2H2O -> C8H12O5 + 2O2 +2H+
        #DDG + CO2 + 2H2O -> 4-DG + 2O2 + 2H+
        rxn_S00110 = Reaction('rxn_S00110')
        rxn_S00110.name = '2-demethyl-4-deoxygadusol_4-deoxygadusol'
        rxn_S00110.subsystem = 'c'
        rxn_S00110.lower_bound = -1000. #This is the default
        rxn_S00110.upper_bound = 1000. #This is the default
        rxn_S00110.reversibility = 1 #This is the default
        rxn_S00110.objective_coefficient = 0 #this is the default

        gene_reaction_rule = '( AVA_3857 )'
        rxn_S00110.add_gene_reaction_rule(gene_reaction_rule)

        #Create the metabolites
        cpd_S00100_c = sb_model.metabolites.get_by_id('cpd_S00100_c') #DDG
        cpd00001_c = sb_model.metabolites.get_by_id('cpd00001_c') #H20
        cpd00011_c = sb_model.metabolites.get_by_id('cpd00011_c') #CO2
        cpd00007_c = sb_model.metabolites.get_by_id('cpd00007_c') #O2
        cpd00067_c = sb_model.metabolites.get_by_id('cpd00067_c') #H+
        cpd_S00110_c = Metabolite('cpd_S00110_c', formula='C8H12O5', name='4-deoxygadusol', compartment='c') #4DG

        #add the metabolites to the reaction:
        rxn_S00110.add_metabolites({cpd_S00100_c : -1.0,
                                  cpd00001_c: -2.0,
                                  cpd00011_c: -1.0,
                                  cpd00007_c: 2.0,
                                  cpd00067_c: 2.0,
                                  cpd_S00110_c: 1.0})

        sb_model.add_reaction(rxn_S00110)

        '''''

        ##############################################################################################################
        #AVA_3856
        #C8H12O5 + C10H16N5O13P3 (C10H13N5O13P3 in the model)-> C8H12O8P + C10H15N5O10P2 (C10H13N5O10P2 in the model)
        #4-DG + ATP -> 4-DG_ATP + ADP
	print 'adding rxnS00120'
        rxn_S00120 = Reaction('rxnS00120')
        #rxn_S00120.name = '4-deoxygadusol_4-deoxygadusol-ATP'
        rxn_S00120.name = 'AVA_3856'
        rxn_S00120.subsystem = 'c'
        rxn_S00120.lower_bound = -1000. #This is the default
        rxn_S00120.upper_bound = 1000. #This is the default
        rxn_S00120.reversibility = 1 #This is the default
        rxn_S00120.objective_coefficient = 0 #this is the default


        gene_reaction_rule = '( AVA_3856 )'
        rxn_S00120.add_gene_reaction_rule(gene_reaction_rule)

        #Create the metabolites
        cpd_S00110_c = model_manager._cb_model.metabolites.get_by_id('cpdS00110_c') #4-DG
        cpd00002_c = model_manager._cb_model.metabolites.get_by_id('cpd00002_c') #ATP
        cpd00008_c = model_manager._cb_model.metabolites.get_by_id('cpd00008_c') #ADP
        cpd_S00120_c = Metabolite('cpdS00120_c', formula='C8H12O8P', name='4-deoxygadusol-phosphate', compartment='c') #4-DG-ATP

        #add the metabolites to the reaction:
        rxn_S00120.add_metabolites({cpd_S00110_c: -1.0,
                                  cpd00002_c: -1.0,
                                  cpd00008_c: 1.0,
                                  cpd_S00120_c: 1.0})

        model_manager.cobra_lazy_add_nontransport_rxn(rxn_S00120)
        model_manager.cobra_lazy_add_new_met_by_rxn(rxn_S00120)

        ##############################################################################################################
        #AVA_3856
        #C8H12O8P + C2H5NO2 -> C10H15O6N + HPO4 + H+
        #4-DG-ATP + Glycine -> Mycosporine-Gly + Pi + H+
	print 'adding rxnS00130'
        rxn_S00130 = Reaction('rxnS00130')
        #rxn_S00130.name = '4-deoxygadusol-ATP_Mycosporine-Gly'
        rxn_S00130.name = 'AVA_3856'
        rxn_S00130.subsystem = 'c'
        rxn_S00130.lower_bound = -1000. #This is the default
        rxn_S00130.upper_bound = 1000. #This is the default
        rxn_S00130.reversibility = 1 #This is the default
        rxn_S00130.objective_coefficient = 0 #this is the default

        gene_reaction_rule = '( AVA_3856 )'
        rxn_S00130.add_gene_reaction_rule(gene_reaction_rule)

        #Create the metabolites
        cpd_S00120_c = model_manager._cb_model.metabolites.get_by_id('cpdS00120_c') #4-DG-ATP
        cpd00033_c = model_manager._cb_model.metabolites.get_by_id('cpd00033_c') #Glycine
        cpd00009_c = model_manager._cb_model.metabolites.get_by_id('cpd00009_c') #Phosphate (Pi)
        cpd00067_c = model_manager._cb_model.metabolites.get_by_id('cpd00067_c') #H+
        cpd_S00130_c = Metabolite('cpdS00130_c', formula='C10H15O6N', name='Mycosporine-Gly', compartment='c') #Mycosporine-Gly

        #add the metabolites to the reaction:
        rxn_S00130.add_metabolites({cpd_S00120_c: -1.0,
                                  cpd00033_c: -1.0,
                                  cpd00009_c: 1.0,
                                  cpd00067_c: 1.0,
                                  cpd_S00130_c: 1.0})

        model_manager.cobra_lazy_add_nontransport_rxn(rxn_S00130)
        model_manager.cobra_lazy_add_new_met_by_rxn(rxn_S00130)

        ##############################################################################################################
        #AVA_3855
        #C10H15O6N + C3H7NO3 ->C13O8H20N2 + H2O
        #Mycosporine-Gly + Serine -> Shinorine + H2O
	print 'adding rxnS00140'
        rxn_S00140 = Reaction('rxnS00140')
        #rxn_S00140.name = 'Mycosporine-Gly_Shinorine'
        rxn_S00140.name = 'AVA_3855'
        rxn_S00140.subsystem = 'c'
        rxn_S00140.lower_bound = -1000. #This is the default
        rxn_S00140.upper_bound = 1000. #This is the default
        rxn_S00140.reversibility = 1 #This is the default
        rxn_S00140.objective_coefficient = 0 #this is the default

        gene_reaction_rule = '( AVA_3855 )'
        rxn_S00140.add_gene_reaction_rule(gene_reaction_rule)

        #Create the metabolites
        cpd_S00130_c = model_manager._cb_model.metabolites.get_by_id('cpdS00130_c') #Mycosporine-Gly
        cpd00054_c = model_manager._cb_model.metabolites.get_by_id('cpd00054_c') #Serine
        cpd00001_c = model_manager._cb_model.metabolites.get_by_id('cpd00001_c') #H20
        cpd_S00140_c = Metabolite('cpdS00140_c', formula='C13O8H20N2', name='Shinorine', compartment='c') #Shinorine

        #add the metabolites to the reaction:
        rxn_S00140.add_metabolites({cpd_S00130_c: -1.0,
                                  cpd00054_c: -1.0,
                                  cpd00001_c: 1.0,
                                  cpd_S00140_c: 1.0})

        model_manager.cobra_lazy_add_nontransport_rxn(rxn_S00140)
        model_manager.cobra_lazy_add_new_met_by_rxn(rxn_S00140)

        ##############################################################################################################
        #Shinorine_C <=> Shinorine_E exchange reaction
	print 'adding rxnS00150'
        rxn_S00150 = Reaction('rxnS00150')
        rxn_S00150.name = 'Shinorine_Transport'
        rxn_S00150.subsystem = 'Extracellular'
        rxn_S00150.lower_bound = -1000. #This is the default
        rxn_S00150.upper_bound = 1000. #This is the default
        rxn_S00150.reversibility = 1 #This is the default
        rxn_S00150.objective_coefficient = 0 #this is the default

        cpd_S00140_c = model_manager._cb_model.metabolites.get_by_id('cpdS00140_c') #Shinorine C
        cpd_S00140_e = Metabolite('cpdS00140_e', formula='C13O8H20N2', name='Shinorine', compartment='e') #Shinorine E

        rxn_S00150.add_metabolites({cpd_S00140_c: -1.0, cpd_S00140_e: 1.0})
        model_manager.cobra_lazy_add_nontransport_rxn(rxn_S00150)
        model_manager.cobra_lazy_add_new_met_by_rxn(rxn_S00150)

        ##############################################################################################################
        #Shinorine_E <=> Shinorine_B boundary reaction
	print 'adding EX_cpdS00140_e'
        rxn_S00150_EX = Reaction('EX_cpdS00140_e')
        rxn_S00150_EX.name = 'Shinorine_Transport_Boundary'
        rxn_S00150_EX.subsystem = 'Extracellular'
        rxn_S00150_EX.lower_bound = -1000. #This is the default
        rxn_S00150_EX.upper_bound = 1000. #This is the default
        rxn_S00150_EX.reversibility = 1 #This is the default
        rxn_S00150_EX.objective_coefficient = 0 #this is the default

        cpd_S00140_e = model_manager._cb_model.metabolites.get_by_id('cpdS00140_e') #Shinorine E

        rxn_S00150_EX.add_metabolites({cpd_S00140_e: -1.0})
        model_manager.cobra_lazy_add_transport_rxn(rxn_S00150_EX)
        model_manager.cobra_lazy_add_new_met_by_rxn(rxn_S00150_EX)

        if isShikimateActive:
            #UKNOWN GENE - transformation from 3-dehydoquinate to 4-deoxygadusol
            #C7H9O6+CO2+2H2O -> C8H12O5 + 2O2 + H+
	    print 'adding rrxnS00160'
            rxn_S00160 = Reaction('rxnS00160')
            rxn_S00160.name = '3-dehydoquinate_4-deoxygadusol'
            rxn_S00160.subsystem = 'c'
            rxn_S00160.lower_bound = -1000. #This is the default
            rxn_S00160.upper_bound = 1000. #This is the default
            rxn_S00160.reversibility = 1 #This is the default
            rxn_S00160.objective_coefficient = 0 #this is the default

            cpd00699_c = model_manager._cb_model.metabolites.get_by_id('cpd00699_c') #3-Dehydroquinate
            cpd00011_c = model_manager._cb_model.metabolites.get_by_id('cpd00011_c') #CO2
            cpd00001_c = model_manager._cb_model.metabolites.get_by_id('cpd00001_c') #H20
            cpd_S00110_c = model_manager._cb_model.metabolites.get_by_id('cpdS00110_c')
            #if not cpd_S00110_c: # if not seen before
                # define the metabolite in the model
            #    cpd_S00110_c = Metabolite('cpdS00110_c', formula='C8H12O5', name='4-deoxygadusol', compartment='c') #4DG

            cpd00007_c = model_manager._cb_model.metabolites.get_by_id('cpd00007_c') #O2
            cpd00067_c = model_manager._cb_model.metabolites.get_by_id('cpd00067_c') #H+

            #add the metabolites to the reaction:
            rxn_S00160.add_metabolites({cpd00699_c: -1.0,
                                      cpd00011_c: -1.0,
                                      cpd00001_c: -2.0,
                                      cpd00067_c: -2.0, # added to balance the reaction
                                      cpd_S00110_c: 1.0,
                                      cpd00007_c: 2.0,
                                      cpd00001_c: 1.0, # added to balance the reaction
                                      cpd00067_c: 1.0})

            model_manager.cobra_lazy_add_nontransport_rxn(rxn_S00160)
            model_manager.cobra_lazy_add_new_met_by_rxn(rxn_S00160)

        model_manager.cobra_model_rebuild_rxn_dict()
        return model_manager._cb_model

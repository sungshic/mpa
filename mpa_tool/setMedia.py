__author__ = 'spark'

import csv

# -*- coding: utf-8 -*-
"""
Created on Wed Oct 03 17:08:37 2012

@author: Sungshic Park (b0928115@ncl.ac.uk)
"""

oxygenExBoundary = 'EX_cpd00007_e'

cmpList = [
            'cpd00027_e',
            'cpd00130_e',
            'cpd00027_e',
            'cpd00027_e',
            'cpd00130_e',
            'cpd00130_e',
            'cpd00027_e',
            'cpd00027_e',
            'cpd00027_e',
            'cpd00027_e',
            'cpd00027_e',
            'cpd00130_e',
            'cpd00027_e',
            'cpd00027_e',
            'cpd00130_e',
            'cpd00130_e',
            'cpd00027_e'
]

'''{   # idx 0: Glucose + Ammonium Sulfate
                    'metadata':{'CarbonSrcRxnKeys':['EX_cpd00027_e'],
                              'CarbonSrcCmpIds':['cpd00027_e']},
                    'recipe':{
                    'EX_cpd00027_e':1.0,   	#D-Glucose
                    #'EX_cpd00130_e',    #L-Malate
                    'EX_cpd00013_e':1.0,	#NH3
                    'EX_cpd00048_e':1.0,	#Sulfate
                    'EX_cpd00205_e':1.0,	#K+
                    'EX_cpd00067_e':100.0,	#H+
                    'EX_cpd00009_e':5.0,	#Phosphate
                    'EX_cpd00254_e':1.0,	#Mg
                    'EX_cpd00063_e':1.0,    #Ca2+
                    'EX_cpd00011_e':100.0,    #CO2
                    #'EX_cpd00099_e',	#Cl-

                    'EX_cpd00149_e':1.0,	#Co2+
                    'EX_cpd00058_e':1.0,	#Cu2+
                    'EX_cpd10516_e':1.0,	#fe3
                    'EX_cpd00001_e':100.0,	#H2O
                    'EX_cpd00030_e':1.0,	#Mn2+
                    #'EX_cpd00065_e',	#L-Tryptophan
                    'EX_cpd00063_e':1.0,	#Ca2+
                    'EX_cpd00007_e':100.0,	#O2
                    'EX_cpd00034_e':1.0,	#Zn2+
                    'EX_cpd10515_e':1.0,	#Fe2+
                    'EX_cpd00971_e':5.0	    #Na+
                    }
                 },
                 {   # idx 0: Glucose + Ammonium Sulfate
                    'metadata':{'CarbonSrcRxnKeys':['EX_cpd00027_e'],
                                'CarbonSrcCmpIds':['cpd00027_e']},
                    'recipe':{
                        'EX_cpd00027_e':1.0,   	#D-Glucose
                        #'EX_cpd00130_e',    #L-Malate
                        'EX_cpd00013_e':1.0,	#NH3
                        'EX_cpd00048_e':1.0,	#Sulfate
                        'EX_cpd00205_e':1.0,	#K+
                        'EX_cpd00067_e':10.0,	#H+
                        'EX_cpd00009_e':5.0,	#Phosphate
                        'EX_cpd00254_e':1.0,	#Mg
                        'EX_cpd00063_e':1.0,    #Ca2+
                        'EX_cpd00011_e':1.0,    #CO2
                        #'EX_cpd00099_e',	#Cl-

                        'EX_cpd00149_e':1.0,	#Co2+
                        'EX_cpd00058_e':1.0,	#Cu2+
                        'EX_cpd10516_e':1.0,	#fe3
                        'EX_cpd00001_e':10.0,	#H2O
                        'EX_cpd00030_e':1.0,	#Mn2+
                        #'EX_cpd00065_e',	#L-Tryptophan
                        'EX_cpd00063_e':1.0,	#Ca2+
                        'EX_cpd00007_e':2.0,	#O2
                        'EX_cpd00034_e':1.0,	#Zn2+
                        'EX_cpd10515_e':1.0,	#Fe2+
                        'EX_cpd00971_e':1.0	    #Na+
                    }
                },
                '''

mediaList = [
                {   # idx 0: Glucose + Ammonium Sulfate
                    'metadata':{'CarbonSrcRxnKeys':['EX_cpd00027_e'],
                              'CarbonSrcCmpIds':['cpd00027_e']},
                    'recipe':{
                    'EX_cpd00027_e':1.0,   	#D-Glucose
                    #'EX_cpd00130_e',    #L-Malate
                    'EX_cpd00013_e':1.0,	#NH3
                    'EX_cpd00048_e':1.0,	#Sulfate
                    'EX_cpd00205_e':1.0,	#K+
                    'EX_cpd00067_e':100.0,	#H+
                    'EX_cpd00009_e':5.0,	#Phosphate
                    'EX_cpd00254_e':1.0,	#Mg
                    'EX_cpd00063_e':1.0,    #Ca2+
                    'EX_cpd00011_e':100.0,    #CO2
                    #'EX_cpd00099_e',	#Cl-

                    'EX_cpd00149_e':1.0,	#Co2+
                    'EX_cpd00058_e':1.0,	#Cu2+
                    'EX_cpd10516_e':1.0,	#fe3
                    'EX_cpd00001_e':100.0,	#H2O
                    'EX_cpd00030_e':1.0,	#Mn2+
                    #'EX_cpd00065_e',	#L-Tryptophan
                    'EX_cpd00063_e':1.0,	#Ca2+
                    'EX_cpd00007_e':100.0,	#O2
                    'EX_cpd00034_e':1.0,	#Zn2+
                    'EX_cpd10515_e':1.0,	#Fe2+
                    'EX_cpd00971_e':5.0	    #Na+
                    }
                 },
                {   # idx 1: Malate + Ammonium Sulfate
                    'metadata':{'CarbonSrcRxnKeys':['EX_cpd00130_e'],
                              'CarbonSrcCmpIds':['cpd00130_e']},
                    'recipe':{
                    #'EX_cpd00027_e',   	#D-Glucose
                    'EX_cpd00130_e':1.0,    #L-Malate
                    'EX_cpd00013_e':1.0,	#NH3
                    'EX_cpd00048_e':1.0,	#Sulfate
                    'EX_cpd00205_e':1.0,	#K+
                    'EX_cpd00067_e':10.0,	#H+
                    'EX_cpd00009_e':2.0,	#Phosphate
                    'EX_cpd00254_e':1.0,	#Mg
                    'EX_cpd00063_e':1.0,    #Ca2+
                    'EX_cpd00011_e':1.0,    #CO2
                    #'EX_cpd00099_e',	#Cl-

                    'EX_cpd00149_e':1.0,	#Co2+
                    'EX_cpd00058_e':1.0,	#Cu2+
                    'EX_cpd10516_e':1.0,	#fe3
                    'EX_cpd00001_e':31.0,	#H2O
                    'EX_cpd00030_e':1.0,	#Mn2+
                    #'EX_cpd00065_e',	#L-Tryptophan
                    'EX_cpd00063_e':1.0,	#Ca2+
                    'EX_cpd00007_e':1.5,	#O2
                    'EX_cpd00034_e':1.0,	#Zn2+
                    'EX_cpd10515_e':1.0,	#Fe2+
                    'EX_cpd00971_e':5.0	    #Na+
                    }
                 },
                {   # idx 2: Glucose + Glutamate
                    'metadata':{'CarbonSrcRxnKeys':['EX_cpd00027_e'],
                              'CarbonSrcCmpIds':['cpd00027_e']},
                    'recipe':{
                    'EX_cpd00027_e':1.0,   	#D-Glucose
                    #'EX_cpd00130_e',    #L-Malate
                    #'EX_cpd00013_e',	#NH3
                    #'EX_cpd00048_e',	#Sulfate
                    'EX_cpd00023_e':10.0,    #L-Glutamate
                    #'EX_cpd00053_e',    #L-Glutamine
                    'EX_cpd00205_e':1.0,	#K+
                    'EX_cpd00067_e':1.0,	#H+
                    'EX_cpd00009_e':1.0,	#Phosphate
                    'EX_cpd00254_e':1.0,	#Mg
                    'EX_cpd00063_e':1.0,    #Ca2+
                    'EX_cpd00011_e':1.0,    #CO2
                    #'EX_cpd00099_e',	#Cl-

                    'EX_cpd00149_e':1.0,	#Co2+
                    'EX_cpd00058_e':1.0,	#Cu2+
                    'EX_cpd10516_e':1.0,	#fe3
                    'EX_cpd00001_e':1.0,	#H2O
                    'EX_cpd00030_e':1.0,	#Mn2+
                    #'EX_cpd00065_e',	#L-Tryptophan
                    'EX_cpd00063_e':1.0,	#Ca2+
                    'EX_cpd00007_e':1.5,	#O2
                    'EX_cpd00034_e':1.0,	#Zn2+
                    'EX_cpd10515_e':1.0,	#Fe2+
                    'EX_cpd00971_e':1.0	    #Na+
                    }
                 },
                {   # idx 3: Glucose + Glutamine
                    'metadata':{'CarbonSrcRxnKeys':['EX_cpd00027_e'],
                              'CarbonSrcCmpIds':['cpd00027_e']},
                    'recipe':{
                    'EX_cpd00027_e':1.0,   	#D-Glucose
                    #'EX_cpd00130_e',    #L-Malate
                    #'EX_cpd00013_e',	#NH3
                    #'EX_cpd00048_e',	#Sulfate
                    #'EX_cpd00023_e',    #L-Glutamate
                    'EX_cpd00053_e':10.0,    #L-Glutamine
                    'EX_cpd00205_e':1.0,	#K+
                    'EX_cpd00067_e':1.0,	#H+
                    'EX_cpd00009_e':1.0,	#Phosphate
                    'EX_cpd00254_e':1.0,	#Mg
                    'EX_cpd00063_e':1.0,    #Ca2+
                    'EX_cpd00011_e':1.0,    #CO2
                    #'EX_cpd00099_e',	#Cl-

                    'EX_cpd00149_e':1.0,	#Co2+
                    'EX_cpd00058_e':1.0,	#Cu2+
                    'EX_cpd10516_e':1.0,	#fe3
                    'EX_cpd00001_e':1.0,	#H2O
                    'EX_cpd00030_e':1.0,	#Mn2+
                    #'EX_cpd00065_e',	#L-Tryptophan
                    'EX_cpd00063_e':1.0,	#Ca2+
                    'EX_cpd00007_e':1.5,	#O2
                    'EX_cpd00034_e':1.0,	#Zn2+
                    'EX_cpd10515_e':1.0,	#Fe2+
                    'EX_cpd00971_e':1.0	    #Na+
                    }
                 },
                {   # idx 4: Malate + Glutamate
                    'metadata':{'CarbonSrcRxnKeys':['EX_cpd00130_e'],
                              'CarbonSrcCmpIds':['cpd00130_e']},
                    'recipe':{
                    #'EX_cpd00027_e',   	#D-Glucose
                    'EX_cpd00130_e':1.0,    #L-Malate
                    #'EX_cpd00013_e',	#NH3
                    #'EX_cpd00048_e',	#Sulfate
                    'EX_cpd00023_e':10.0,    #L-Glutamate
                    #'EX_cpd00053_e',    #L-Glutamine
                    'EX_cpd00205_e':1.0,	#K+
                    'EX_cpd00067_e':1.0,	#H+
                    'EX_cpd00009_e':1.0,	#Phosphate
                    'EX_cpd00254_e':1.0,	#Mg
                    'EX_cpd00063_e':1.0,    #Ca2+
                    'EX_cpd00011_e':1.0,    #CO2
                    #'EX_cpd00099_e',	#Cl-

                    'EX_cpd00149_e':1.0,	#Co2+
                    'EX_cpd00058_e':1.0,	#Cu2+
                    'EX_cpd10516_e':1.0,	#fe3
                    'EX_cpd00001_e':1.0,	#H2O
                    'EX_cpd00030_e':1.0,	#Mn2+
                    #'EX_cpd00065_e',	#L-Tryptophan
                    'EX_cpd00063_e':1.0,	#Ca2+
                    'EX_cpd00007_e':1.5,	#O2
                    'EX_cpd00034_e':1.0,	#Zn2+
                    'EX_cpd10515_e':1.0,	#Fe2+
                    'EX_cpd00971_e':1.0	    #Na+
                    }
                 },
                {   # idx 5: Malate + Glutamine
                    'metadata':{'CarbonSrcRxnKeys':['EX_cpd00130_e'],
                              'CarbonSrcCmpIds':['cpd00130_e']},
                    'recipe':{
                    #'EX_cpd00027_e',   	#D-Glucose
                    'EX_cpd00130_e':1.0,    #L-Malate
                    #'EX_cpd00013_e',	#NH3
                    #'EX_cpd00048_e',	#Sulfate
                    #'EX_cpd00023_e',    #L-Glutamate
                    'EX_cpd00053_e':10.0,    #L-Glutamine
                    'EX_cpd00205_e':1.0,	#K+
                    'EX_cpd00067_e':1.0,	#H+
                    'EX_cpd00009_e':1.0,	#Phosphate
                    'EX_cpd00254_e':1.0,	#Mg
                    'EX_cpd00063_e':1.0,    #Ca2+
                    'EX_cpd00011_e':1.0,    #CO2
                    #'EX_cpd00099_e',	#Cl-

                    'EX_cpd00149_e':1.0,	#Co2+
                    'EX_cpd00058_e':1.0,	#Cu2+
                    'EX_cpd10516_e':1.0,	#fe3
                    'EX_cpd00001_e':1.0,	#H2O
                    'EX_cpd00030_e':1.0,	#Mn2+
                    #'EX_cpd00065_e',	#L-Tryptophan
                    'EX_cpd00063_e':1.0,	#Ca2+
                    'EX_cpd00007_e':1.5,	#O2
                    'EX_cpd00034_e':1.0,	#Zn2+
                    'EX_cpd10515_e':1.0,	#Fe2+
                    'EX_cpd00971_e':1.0	    #Na+
                    }
                 },
                {
                    'metadata':{'CarbonSrcRxnKeys':['EX_cpd00027_e','EX_cpd00130_e'],
                              'CarbonSrcCmpIds':['cpd00027_e','cpd00130_e']},
                    'recipe':{
                    'EX_cpd00027_e':1.0,   	#D-Glucose
                    'EX_cpd00130_e':1.0,    #L-Malate
                    'EX_cpd00013_e':1.0,	#NH3
                    'EX_cpd00048_e':1.0,	#Sulfate
                    #'EX_cpd00023_e',    #L-Glutamate
                    #'EX_cpd00053_e',    #L-Glutamine
                    'EX_cpd00205_e':1.0,	#K+
                    'EX_cpd00067_e':1.0,	#H+
                    'EX_cpd00009_e':1.0,	#Phosphate
                    'EX_cpd00254_e':1.0,	#Mg
                    'EX_cpd00063_e':1.0,    #Ca2+
                    'EX_cpd00011_e':1.0,    #CO2
                    #'EX_cpd00099_e',	#Cl-

                    'EX_cpd00149_e':1.0,	#Co2+
                    'EX_cpd00058_e':1.0,	#Cu2+
                    'EX_cpd10516_e':1.0,	#fe3
                    'EX_cpd00001_e':1.0,	#H2O
                    'EX_cpd00030_e':1.0,	#Mn2+
                    #'EX_cpd00065_e',	#L-Tryptophan
                    'EX_cpd00063_e':1.0,	#Ca2+
                    'EX_cpd00007_e':1.5,	#O2
                    'EX_cpd00034_e':1.0,	#Zn2+
                    'EX_cpd10515_e':1.0,	#Fe2+
                    'EX_cpd00971_e':1.0	    #Na+
                    }
                 },
                {
                    'metadata':{'CarbonSrcRxnKeys':['EX_cpd00027_e','EX_cpd00130_e'],
                              'CarbonSrcCmpIds':['cpd00027_e','cpd00130_e']},
                    'recipe':{
                    'EX_cpd00027_e':1.0,   	#D-Glucose
                    'EX_cpd00130_e':1.0,    #L-Malate
                    #'EX_cpd00013_e',	#NH3
                    #'EX_cpd00048_e',	#Sulfate
                    'EX_cpd00023_e':10.0,    #L-Glutamate
                    #'EX_cpd00053_e',    #L-Glutamine
                    'EX_cpd00205_e':1.0,	#K+
                    'EX_cpd00067_e':1.0,	#H+
                    'EX_cpd00009_e':1.0,	#Phosphate
                    'EX_cpd00254_e':1.0,	#Mg
                    'EX_cpd00063_e':1.0,    #Ca2+
                    'EX_cpd00011_e':1.0,    #CO2
                    #'EX_cpd00099_e',	#Cl-

                    'EX_cpd00149_e':1.0,	#Co2+
                    'EX_cpd00058_e':1.0,	#Cu2+
                    'EX_cpd10516_e':1.0,	#fe3
                    'EX_cpd00001_e':1.0,	#H2O
                    'EX_cpd00030_e':1.0,	#Mn2+
                    #'EX_cpd00065_e',	#L-Tryptophan
                    'EX_cpd00063_e':1.0,	#Ca2+
                    'EX_cpd00007_e':1.5,	#O2
                    'EX_cpd00034_e':1.0,	#Zn2+
                    'EX_cpd10515_e':1.0,	#Fe2+
                    'EX_cpd00971_e':1.0	    #Na+
                    }
                 },
                {
                    'metadata':{'CarbonSrcRxnKeys':['EX_cpd00027_e','EX_cpd00130_e'],
                              'CarbonSrcCmpIds':['cpd00027_e','cpd00130_e']},
                    'recipe':{
                    'EX_cpd00027_e':1.0,   	#D-Glucose
                    'EX_cpd00130_e':1.0,    #L-Malate
                    #'EX_cpd00013_e',	#NH3
                    #'EX_cpd00048_e',	#Sulfate
                    #'EX_cpd00023_e',    #L-Glutamate
                    'EX_cpd00053_e':10.0,    #L-Glutamine
                    'EX_cpd00205_e':1.0,	#K+
                    'EX_cpd00067_e':1.0,	#H+
                    'EX_cpd00009_e':1.0,	#Phosphate
                    'EX_cpd00254_e':1.0,	#Mg
                    'EX_cpd00063_e':1.0,    #Ca2+
                    'EX_cpd00011_e':1.0,    #CO2
                    #'EX_cpd00099_e',	#Cl-

                    'EX_cpd00149_e':1.0,	#Co2+
                    'EX_cpd00058_e':1.0,	#Cu2+
                    'EX_cpd10516_e':1.0,	#fe3
                    'EX_cpd00001_e':1.0,	#H2O
                    'EX_cpd00030_e':1.0,	#Mn2+
                    #'EX_cpd00065_e',	#L-Tryptophan
                    'EX_cpd00063_e':1.0,	#Ca2+
                    'EX_cpd00007_e':1.5,	#O2
                    'EX_cpd00034_e':1.0,	#Zn2+
                    'EX_cpd10515_e':1.0,	#Fe2+
                    'EX_cpd00971_e':1.0	    #Na+
                    }
                 },
                {   # idx 9: Glucose + L- and D-Glutamate
                    'metadata':{'CarbonSrcRxnKeys':['EX_cpd00027_e'],
                              'CarbonSrcCmpIds':['cpd00027_e']},
                    'recipe':{
                    'EX_cpd00027_e':1.0,   	#D-Glucose
                    #'EX_cpd00130_e',    #L-Malate
                    #'EX_cpd00013_e',	#NH3
                    #'EX_cpd00048_e',	#Sulfate
                    'EX_cpd00023_e':10.0,    #L-Glutamate
                    'EX_cpd00186_e':10.0,    #D-Glutamate
                    #'EX_cpd00053_e',    #L-Glutamine
                    'EX_cpd00205_e':1.0,	#K+
                    'EX_cpd00067_e':1.0,	#H+
                    'EX_cpd00009_e':1.0,	#Phosphate
                    'EX_cpd00254_e':1.0,	#Mg
                    'EX_cpd00063_e':1.0,    #Ca2+
                    'EX_cpd00011_e':1.0,    #CO2
                    #'EX_cpd00099_e',	#Cl-

                    'EX_cpd00149_e':1.0,	#Co2+
                    'EX_cpd00058_e':1.0,	#Cu2+
                    'EX_cpd10516_e':1.0,	#fe3
                    'EX_cpd00001_e':1.0,	#H2O
                    'EX_cpd00030_e':1.0,	#Mn2+
                    #'EX_cpd00065_e',	#L-Tryptophan
                    'EX_cpd00063_e':1.0,	#Ca2+
                    'EX_cpd00007_e':1.5,	#O2
                    'EX_cpd00034_e':1.0,	#Zn2+
                    'EX_cpd10515_e':1.0,	#Fe2+
                    'EX_cpd00971_e':1.0	    #Na+
                    }
                 },
                {   # idx 10: Glucose + L-Glutamate + NH3
                    'metadata':{'CarbonSrcRxnKeys':['EX_cpd00027_e'],
                              'CarbonSrcCmpIds':['cpd00027_e']},
                    'recipe':{
                    'EX_cpd00027_e':1.0,   	#D-Glucose
                    #'EX_cpd00130_e',    #L-Malate
                    'EX_cpd00013_e':1.0,	#NH3
                    'EX_cpd00048_e':1.0,	#Sulfate
                    'EX_cpd00023_e':1.0,    #L-Glutamate
                    #'EX_cpd00186_e',    #D-Glutamate
                    'EX_cpd00053_e':1.0,    #L-Glutamine
                    'EX_cpd00205_e':1.0,	#K+
                    'EX_cpd00067_e':1.0,	#H+
                    'EX_cpd00009_e':1.0,	#Phosphate
                    'EX_cpd00254_e':1.0,	#Mg
                    'EX_cpd00063_e':1.0,    #Ca2+
                    'EX_cpd00011_e':1.0,    #CO2
                    #'EX_cpd00099_e',	#Cl-

                    'EX_cpd00149_e':1.0,	#Co2+
                    'EX_cpd00058_e':1.0,	#Cu2+
                    'EX_cpd10516_e':1.0,	#fe3
                    'EX_cpd00001_e':1.0,	#H2O
                    'EX_cpd00030_e':1.0,	#Mn2+
                    #'EX_cpd00065_e',	#L-Tryptophan
                    'EX_cpd00063_e':1.0,	#Ca2+
                    'EX_cpd00007_e':1.5,	#O2
                    'EX_cpd00034_e':1.0,	#Zn2+
                    'EX_cpd10515_e':1.0,	#Fe2+
                    'EX_cpd00971_e':1.0,    #Na+
                    'EX_cpd00216_e':1.0     #Chorismate
                    }
                 },
                {   # idx 11: Malate + L-Glutamate
                    'metadata':{'CarbonSrcRxnKeys':['EX_cpd00130_e'],
                              'CarbonSrcCmpIds':['cpd00130_e']},
                    'recipe':{
                     #'EX_cpd00027_e':1.0,   	#D-Glucose
                     'EX_cpd00130_e':1.0,    #L-Malate
                     'EX_cpd00013_e':1.0,	#NH3
                     'EX_cpd00048_e':1.0,	#Sulfate
                     'EX_cpd00023_e':1.0,    #L-Glutamate
                     #'EX_cpd00186_e',    #D-Glutamate
                     'EX_cpd00053_e':1.0,    #L-Glutamine
                     'EX_cpd00205_e':1.0,	#K+
                     'EX_cpd00067_e':1.0,	#H+
                     'EX_cpd00009_e':1.0,	#Phosphate
                     'EX_cpd00254_e':1.0,	#Mg
                     'EX_cpd00063_e':1.0,    #Ca2+
                     'EX_cpd00011_e':1.0,    #CO2
                     #'EX_cpd00099_e',	#Cl-

                     'EX_cpd00149_e':1.0,	#Co2+
                     'EX_cpd00058_e':1.0,	#Cu2+
                     'EX_cpd10516_e':1.0,	#fe3
                     'EX_cpd00001_e':1.0,	#H2O
                     'EX_cpd00030_e':1.0,	#Mn2+
                     #'EX_cpd00065_e',	#L-Tryptophan
                     'EX_cpd00063_e':1.0,	#Ca2+
                     'EX_cpd00007_e':1.5,	#O2
                     'EX_cpd00034_e':1.0,	#Zn2+
                     'EX_cpd10515_e':1.0,	#Fe2+
                     'EX_cpd00971_e':1.0,    #Na+
                     'EX_cpd00216_e':1.0     #Chorismate
                    }
                 },
                {   # idx 12: Glucose + L-Glutamate + Ammonium sulfate
                    'metadata':{'CarbonSrcRxnKeys':['EX_cpd00027_e'],
                              'CarbonSrcCmpIds':['cpd00027_e']},
                    'recipe':{
                     'EX_cpd00027_e':1.0,   	#D-Glucose
                     #'EX_cpd00130_e':1.0,    #L-Malate
                     'EX_cpd00013_e':1.0,	#NH3
                     'EX_cpd00048_e':1.0,	#Sulfate
                     'EX_cpd00023_e':1.0,    #L-Glutamate
                     #'EX_cpd00186_e',    #D-Glutamate
                     #'EX_cpd00053_e':1.0,    #L-Glutamine
                     'EX_cpd00205_e':1.0,	#K+
                     'EX_cpd00067_e':1.0,	#H+
                     'EX_cpd00009_e':1.0,	#Phosphate
                     'EX_cpd00254_e':1.0,	#Mg
                     'EX_cpd00063_e':1.0,    #Ca2+
                     'EX_cpd00011_e':1.0,    #CO2
                     #'EX_cpd00099_e',	#Cl-

                     'EX_cpd00149_e':1.0,	#Co2+
                     'EX_cpd00058_e':1.0,	#Cu2+
                     'EX_cpd10516_e':1.0,	#fe3
                     'EX_cpd00001_e':1.0,	#H2O
                     'EX_cpd00030_e':1.0,	#Mn2+
                     #'EX_cpd00065_e',	#L-Tryptophan
                     'EX_cpd00063_e':1.0,	#Ca2+
                     'EX_cpd00007_e':1.5,	#O2
                     'EX_cpd00034_e':1.0,	#Zn2+
                     'EX_cpd10515_e':1.0,	#Fe2+
                     'EX_cpd00971_e':1.0,    #Na+
                     'EX_cpd00216_e':1.0     #Chorismate
                    }
                 },
                {   # idx 13: Glucose + L-Glutamine + Ammonium sulfate
                    'metadata':{'CarbonSrcRxnKeys':['EX_cpd00027_e'],
                              'CarbonSrcCmpIds':['cpd00027_e']},
                    'recipe':{
                     'EX_cpd00027_e':1.0,   	#D-Glucose
                     #'EX_cpd00130_e':1.0,    #L-Malate
                     'EX_cpd00013_e':1.0,	#NH3
                     'EX_cpd00048_e':1.0,	#Sulfate
                     #'EX_cpd00023_e':1.0,    #L-Glutamate
                     #'EX_cpd00186_e',    #D-Glutamate
                     'EX_cpd00053_e':1.0,    #L-Glutamine
                     'EX_cpd00205_e':1.0,	#K+
                     'EX_cpd00067_e':1.0,	#H+
                     'EX_cpd00009_e':1.0,	#Phosphate
                     'EX_cpd00254_e':1.0,	#Mg
                     'EX_cpd00063_e':1.0,    #Ca2+
                     'EX_cpd00011_e':1.0,    #CO2
                     #'EX_cpd00099_e',	#Cl-

                     'EX_cpd00149_e':1.0,	#Co2+
                     'EX_cpd00058_e':1.0,	#Cu2+
                     'EX_cpd10516_e':1.0,	#fe3
                     'EX_cpd00001_e':1.0,	#H2O
                     'EX_cpd00030_e':1.0,	#Mn2+
                     #'EX_cpd00065_e',	#L-Tryptophan
                     'EX_cpd00063_e':1.0,	#Ca2+
                     'EX_cpd00007_e':1.5,	#O2
                     'EX_cpd00034_e':1.0,	#Zn2+
                     'EX_cpd10515_e':1.0,	#Fe2+
                     'EX_cpd00971_e':1.0,    #Na+
                     'EX_cpd00216_e':1.0     #Chorismate
                    }
                 },
                {   # idx 14: Malate + L-Glutamate + Ammonium sulfate
                    'metadata':{'CarbonSrcRxnKeys':['EX_cpd00130_e'],
                              'CarbonSrcCmpIds':['cpd00130_e']},
                    'recipe':{
                     #'EX_cpd00027_e':1.0,   	#D-Glucose
                     'EX_cpd00130_e':1.0,    #L-Malate
                     'EX_cpd00013_e':1.0,	#NH3
                     'EX_cpd00048_e':1.0,	#Sulfate
                     'EX_cpd00023_e':1.0,    #L-Glutamate
                     #'EX_cpd00186_e',    #D-Glutamate
                     #'EX_cpd00053_e':1.0,    #L-Glutamine
                     'EX_cpd00205_e':1.0,	#K+
                     'EX_cpd00067_e':1.0,	#H+
                     'EX_cpd00009_e':1.0,	#Phosphate
                     'EX_cpd00254_e':1.0,	#Mg
                     'EX_cpd00063_e':1.0,    #Ca2+
                     'EX_cpd00011_e':1.0,    #CO2
                     #'EX_cpd00099_e',	#Cl-

                     'EX_cpd00149_e':1.0,	#Co2+
                     'EX_cpd00058_e':1.0,	#Cu2+
                     'EX_cpd10516_e':1.0,	#fe3
                     'EX_cpd00001_e':1.0,	#H2O
                     'EX_cpd00030_e':1.0,	#Mn2+
                     #'EX_cpd00065_e',	#L-Tryptophan
                     'EX_cpd00063_e':1.0,	#Ca2+
                     'EX_cpd00007_e':1.5,	#O2
                     'EX_cpd00034_e':1.0,	#Zn2+
                     'EX_cpd10515_e':1.0,	#Fe2+
                     'EX_cpd00971_e':1.0,    #Na+
                     'EX_cpd00216_e':1.0     #Chorismate
                    }
                 },
                {   # idx 15: Malate + L-Glutamine + Ammonium sulfate
                    'metadata':{'CarbonSrcRxnKeys':['EX_cpd00130_e'],
                              'CarbonSrcCmpIds':['cpd00130_e']},
                    'recipe':{
                     #'EX_cpd00027_e':1.0,   	#D-Glucose
                     'EX_cpd00130_e':1.0,    #L-Malate
                     'EX_cpd00013_e':1.0,	#NH3
                     'EX_cpd00048_e':1.0,	#Sulfate
                     #'EX_cpd00023_e':1.0,    #L-Glutamate
                     #'EX_cpd00186_e',    #D-Glutamate
                     'EX_cpd00053_e':1.0,    #L-Glutamine
                     'EX_cpd00205_e':1.0,	#K+
                     'EX_cpd00067_e':1.0,	#H+
                     'EX_cpd00009_e':1.0,	#Phosphate
                     'EX_cpd00254_e':1.0,	#Mg
                     'EX_cpd00063_e':1.0,    #Ca2+
                     'EX_cpd00011_e':1.0,    #CO2
                     #'EX_cpd00099_e',	#Cl-

                     'EX_cpd00149_e':1.0,	#Co2+
                     'EX_cpd00058_e':1.0,	#Cu2+
                     'EX_cpd10516_e':1.0,	#fe3
                     'EX_cpd00001_e':1.0,	#H2O
                     'EX_cpd00030_e':1.0,	#Mn2+
                     #'EX_cpd00065_e',	#L-Tryptophan
                     'EX_cpd00063_e':1.0,	#Ca2+
                     'EX_cpd00007_e':1.5,	#O2
                     'EX_cpd00034_e':1.0,	#Zn2+
                     'EX_cpd10515_e':1.0,	#Fe2+
                     'EX_cpd00971_e':1.0,    #Na+
                     'EX_cpd00216_e':1.0     #Chorismate
                    }
                 },
                {   # idx 16: Glucose + L-Glutamate + Ammonium sulfate + anaerobic condition
                     'metadata':{'CarbonSrcRxnKeys':['EX_cpd00027_e'],
                                'CarbonSrcCmpIds':['cpd00027_e']},
                     'recipe':{
                         'EX_cpd00027_e':1.0,   	#D-Glucose
                         # #'EX_cpd00130_e':1.0,    #L-Malate
                         'EX_cpd00013_e':1.0,	#NH3
                         'EX_cpd00048_e':1.0,	#Sulfate
                         'EX_cpd00023_e':1.0,    #L-Glutamate
                         #'EX_cpd00186_e',    #D-Glutamate
                         #'EX_cpd00053_e':1.0,    #L-Glutamine
                         'EX_cpd00205_e':1.0,	#K+
                         'EX_cpd00067_e':1.0,	#H+
                         'EX_cpd00009_e':1.0,	#Phosphate
                         'EX_cpd00254_e':1.0,	#Mg
                         'EX_cpd00063_e':1.0,    #Ca2+
                         'EX_cpd00011_e':1.5,    #CO2
                         #'EX_cpd00099_e',	#Cl-

                         'EX_cpd00149_e':1.0,	#Co2+
                         'EX_cpd00058_e':1.0,	#Cu2+
                         'EX_cpd10516_e':1.0,	#fe3
                         'EX_cpd00001_e':1.0,	#H2O
                         'EX_cpd00030_e':1.0,	#Mn2+
                         #'EX_cpd00065_e',	#L-Tryptophan
                         'EX_cpd00063_e':1.0,	#Ca2+
                         #'EX_cpd00007_e':10.0,	#O2
                         'EX_cpd00034_e':1.0,	#Zn2+
                         'EX_cpd10515_e':1.0,	#Fe2+
                         'EX_cpd00971_e':1.0,    #Na+
                         'EX_cpd00216_e':1.0     #Chorismate
                     }
                 },
                {   # idx 17: Glucose + L-arginine + Ammonium sulfate + aerobic condition
                     'metadata':{'CarbonSrcRxnKeys':['EX_cpd00027_e'],
                                'CarbonSrcCmpIds':['cpd00027_e']},
                     'recipe':{
                         'EX_cpd00027_e':1.0,   	#D-Glucose
                         # #'EX_cpd00130_e':1.0,    #L-Malate
                         'EX_cpd00013_e':1.0,	#NH3
                         'EX_cpd00048_e':1.0,	#Sulfate
                         #'EX_cpd00023_e':1.0,    #L-Glutamate
                         #'EX_cpd00186_e',    #D-Glutamate
                         #'EX_cpd00053_e':1.0,    #L-Glutamine
                         'EX_cpd00051_e':1.0,   # L-arginine
                         'EX_cpd00205_e':1.0,	#K+
                         'EX_cpd00067_e':1.0,	#H+
                         'EX_cpd00009_e':1.0,	#Phosphate
                         'EX_cpd00254_e':1.0,	#Mg
                         'EX_cpd00063_e':1.0,    #Ca2+
                         'EX_cpd00011_e':1.0,    #CO2
                         #'EX_cpd00099_e',	#Cl-

                         'EX_cpd00149_e':1.0,	#Co2+
                         'EX_cpd00058_e':1.0,	#Cu2+
                         'EX_cpd10516_e':1.0,	#fe3
                         'EX_cpd00001_e':1.0,	#H2O
                         'EX_cpd00030_e':1.0,	#Mn2+
                         #'EX_cpd00065_e',	#L-Tryptophan
                         'EX_cpd00063_e':1.0,	#Ca2+
                         'EX_cpd00007_e':1.5,	#O2
                         'EX_cpd00034_e':1.0,	#Zn2+
                         'EX_cpd10515_e':1.0,	#Fe2+
                         'EX_cpd00971_e':1.0,    #Na+
                         'EX_cpd00216_e':1.0     #Chorismate
                     }
                 },
                {   # idx 18: malate + L-arginine + Ammonium sulfate + aerobic condition
                     'metadata':{'CarbonSrcRxnKeys':['EX_cpd00027_e'],
                                'CarbonSrcCmpIds':['cpd00027_e']},
                     'recipe':{
                         #'EX_cpd00027_e':1.0,   	#D-Glucose
                         'EX_cpd00130_e':1.0,    #L-Malate
                         'EX_cpd00013_e':1.0,	#NH3
                         'EX_cpd00048_e':1.0,	#Sulfate
                         #'EX_cpd00023_e':1.0,    #L-Glutamate
                         #'EX_cpd00186_e',    #D-Glutamate
                         #'EX_cpd00053_e':1.0,    #L-Glutamine
                         'EX_cpd00051_e':1.0,   # L-arginine
                         'EX_cpd00205_e':1.0,	#K+
                         'EX_cpd00067_e':1.0,	#H+
                         'EX_cpd00009_e':1.0,	#Phosphate
                         'EX_cpd00254_e':1.0,	#Mg
                         'EX_cpd00063_e':1.0,    #Ca2+
                         'EX_cpd00011_e':1.0,    #CO2
                         #'EX_cpd00099_e',	#Cl-

                         'EX_cpd00149_e':1.0,	#Co2+
                         'EX_cpd00058_e':1.0,	#Cu2+
                         'EX_cpd10516_e':1.0,	#fe3
                         'EX_cpd00001_e':1.0,	#H2O
                         'EX_cpd00030_e':1.0,	#Mn2+
                         #'EX_cpd00065_e',	#L-Tryptophan
                         'EX_cpd00063_e':1.0,	#Ca2+
                         'EX_cpd00007_e':1.5,	#O2
                         'EX_cpd00034_e':1.0,	#Zn2+
                         'EX_cpd10515_e':1.0,	#Fe2+
                         'EX_cpd00971_e':1.0,    #Na+
                         'EX_cpd00216_e':1.0     #Chorismate
                     }
                 },
                {   # idx 19: malate + L-glutamine + L-arginine + Ammonium sulfate + aerobic condition
                     'metadata':{'CarbonSrcRxnKeys':['EX_cpd00027_e'],
                                'CarbonSrcCmpIds':['cpd00027_e']},
                     'recipe':{
                         #'EX_cpd00027_e':1.0,   	#D-Glucose
                         'EX_cpd00130_e':1.0,    #L-Malate
                         'EX_cpd00013_e':1.0,	#NH3
                         'EX_cpd00048_e':1.0,	#Sulfate
                         #'EX_cpd00023_e':1.0,    #L-Glutamate
                         #'EX_cpd00186_e',    #D-Glutamate
                         'EX_cpd00053_e':1.0,    #L-Glutamine
                         'EX_cpd00051_e':1.0,   # L-arginine
                         'EX_cpd00205_e':1.0,	#K+
                         'EX_cpd00067_e':1.0,	#H+
                         'EX_cpd00009_e':1.0,	#Phosphate
                         'EX_cpd00254_e':1.0,	#Mg
                         'EX_cpd00063_e':1.0,    #Ca2+
                         'EX_cpd00011_e':1.0,    #CO2
                         #'EX_cpd00099_e',	#Cl-

                         'EX_cpd00149_e':1.0,	#Co2+
                         'EX_cpd00058_e':1.0,	#Cu2+
                         'EX_cpd10516_e':1.0,	#fe3
                         'EX_cpd00001_e':1.0,	#H2O
                         'EX_cpd00030_e':1.0,	#Mn2+
                         #'EX_cpd00065_e',	#L-Tryptophan
                         'EX_cpd00063_e':1.0,	#Ca2+
                         'EX_cpd00007_e':1.5,	#O2
                         'EX_cpd00034_e':1.0,	#Zn2+
                         'EX_cpd10515_e':1.0,	#Fe2+
                         'EX_cpd00971_e':1.0,    #Na+
                         'EX_cpd00216_e':1.0     #Chorismate
                     }
                },
                {   # idx 20: malate + L-glutamate + L-arginine + Ammonium sulfate + aerobic condition
                     'metadata':{'CarbonSrcRxnKeys':['EX_cpd00027_e'],
                                'CarbonSrcCmpIds':['cpd00027_e']},
                     'recipe':{
                         #'EX_cpd00027_e':1.0,   	#D-Glucose
                         'EX_cpd00130_e':1.0,    #L-Malate
                         'EX_cpd00013_e':1.0,	#NH3
                         'EX_cpd00048_e':1.0,	#Sulfate
                         'EX_cpd00023_e':1.0,    #L-Glutamate
                         #'EX_cpd00186_e',    #D-Glutamate
                         #'EX_cpd00053_e':1.0,    #L-Glutamine
                         'EX_cpd00051_e':1.0,   # L-arginine
                         'EX_cpd00205_e':1.0,	#K+
                         'EX_cpd00067_e':1.0,	#H+
                         'EX_cpd00009_e':1.0,	#Phosphate
                         'EX_cpd00254_e':1.0,	#Mg
                         'EX_cpd00063_e':1.0,    #Ca2+
                         'EX_cpd00011_e':1.0,    #CO2
                         #'EX_cpd00099_e',	#Cl-

                         'EX_cpd00149_e':1.0,	#Co2+
                         'EX_cpd00058_e':1.0,	#Cu2+
                         'EX_cpd10516_e':1.0,	#fe3
                         'EX_cpd00001_e':1.0,	#H2O
                         'EX_cpd00030_e':1.0,	#Mn2+
                         #'EX_cpd00065_e',	#L-Tryptophan
                         'EX_cpd00063_e':1.0,	#Ca2+
                         'EX_cpd00007_e':1.5,	#O2
                         'EX_cpd00034_e':1.0,	#Zn2+
                         'EX_cpd10515_e':1.0,	#Fe2+
                         'EX_cpd00971_e':1.0,    #Na+
                         'EX_cpd00216_e':1.0     #Chorismate
                     }
                },
                {   # idx 21: glucose + L-glutamine + L-arginine + Ammonium sulfate + aerobic condition
                     'metadata':{'CarbonSrcRxnKeys':['EX_cpd00027_e'],
                                'CarbonSrcCmpIds':['cpd00027_e']},
                     'recipe':{
                         'EX_cpd00027_e':1.0,   	#D-Glucose
                         #'EX_cpd00130_e':1.0,    #L-Malate
                         'EX_cpd00013_e':1.0,	#NH3
                         'EX_cpd00048_e':1.0,	#Sulfate
                         #'EX_cpd00023_e':1.0,    #L-Glutamate
                         #'EX_cpd00186_e',    #D-Glutamate
                         'EX_cpd00053_e':1.0,    #L-Glutamine
                         'EX_cpd00051_e':1.0,   # L-arginine
                         'EX_cpd00205_e':1.0,	#K+
                         'EX_cpd00067_e':1.0,	#H+
                         'EX_cpd00009_e':1.0,	#Phosphate
                         'EX_cpd00254_e':1.0,	#Mg
                         'EX_cpd00063_e':1.0,    #Ca2+
                         'EX_cpd00011_e':1.0,    #CO2
                         #'EX_cpd00099_e',	#Cl-

                         'EX_cpd00149_e':1.0,	#Co2+
                         'EX_cpd00058_e':1.0,	#Cu2+
                         'EX_cpd10516_e':1.0,	#fe3
                         'EX_cpd00001_e':1.0,	#H2O
                         'EX_cpd00030_e':1.0,	#Mn2+
                         #'EX_cpd00065_e',	#L-Tryptophan
                         'EX_cpd00063_e':1.0,	#Ca2+
                         'EX_cpd00007_e':1.5,	#O2
                         'EX_cpd00034_e':1.0,	#Zn2+
                         'EX_cpd10515_e':1.0,	#Fe2+
                         'EX_cpd00971_e':1.0,    #Na+
                         'EX_cpd00216_e':1.0     #Chorismate
                     }
                },
                {   # idx 22: glucose + L-glutamate + L-arginine + Ammonium sulfate + aerobic condition
                     'metadata':{'CarbonSrcRxnKeys':['EX_cpd00027_e'],
                                'CarbonSrcCmpIds':['cpd00027_e']},
                     'recipe':{
                         'EX_cpd00027_e':1.0,   	#D-Glucose
                         #'EX_cpd00130_e':1.0,    #L-Malate
                         'EX_cpd00013_e':1.0,	#NH3
                         'EX_cpd00048_e':1.0,	#Sulfate
                         'EX_cpd00023_e':1.0,    #L-Glutamate
                         #'EX_cpd00186_e',    #D-Glutamate
                         #'EX_cpd00053_e':1.0,    #L-Glutamine
                         'EX_cpd00051_e':1.0,   # L-arginine
                         'EX_cpd00205_e':1.0,	#K+
                         'EX_cpd00067_e':1.0,	#H+
                         'EX_cpd00009_e':1.0,	#Phosphate
                         'EX_cpd00254_e':1.0,	#Mg
                         'EX_cpd00063_e':1.0,    #Ca2+
                         'EX_cpd00011_e':1.0,    #CO2
                         #'EX_cpd00099_e',	#Cl-

                         'EX_cpd00149_e':1.0,	#Co2+
                         'EX_cpd00058_e':1.0,	#Cu2+
                         'EX_cpd10516_e':1.0,	#fe3
                         'EX_cpd00001_e':1.0,	#H2O
                         'EX_cpd00030_e':1.0,	#Mn2+
                         #'EX_cpd00065_e',	#L-Tryptophan
                         'EX_cpd00063_e':1.0,	#Ca2+
                         'EX_cpd00007_e':1.5,	#O2
                         'EX_cpd00034_e':1.0,	#Zn2+
                         'EX_cpd10515_e':1.0,	#Fe2+
                         'EX_cpd00971_e':1.0,    #Na+
                         'EX_cpd00216_e':1.0     #Chorismate
                     }
                }


            ]

def getMediaSrcRxn(mediaIdx):
    return mediaList[mediaIdx]['metadata']['CarbonSrcRxnKeys'][0]

def getMediaSrcCmp(mediaIdx):
    return mediaList[mediaIdx]['metadata']['CarbonSrcCmpIds'][0]
    #return cmpList[mediaIdx]

# following index is for reading in the data from
# https://basysbio.ethz.ch/openbis/?viewMode=simple#entity=DATA_SET&permId=20091221155335289-2412
# for G->M https://basysbio.ethz.ch/openbis/index.html?viewMode=SIMPLE#action=DOWNLOAD_ATTACHMENT&file=all_rates_GM.tsv&version=1&entity=EXPERIMENT&permId=20100112110615997-3904
# for M->G https://basysbio.ethz.ch/openbis/index.html?viewMode=SIMPLE#action=DOWNLOAD_ATTACHMENT&file=all_rates_MG.tsv&version=1&entity=EXPERIMENT&permId=20100112110748487-3906
dataIdxLookup_BASYSBIO_BIG_metabolomicFlux = {
    'time': 0,
    'mu_est': 1,
    'mu_lb': 2,
    'mu_ub': 3,
    # indices for estimate, lowerbound and upperbound triples, all in the unit of mmol/g/h
    'EX_cpd00027_e': [4, 5, 6], # D-Glucose (CHEBI:17634) uptake estimate [mmol/g/h]
    'EX_cpd00130_e': [7, 8, 9], # L-Malate, malic acid (CHEBI:6650)
    'EX_cpd00020_e': [10, 11, 12], # Pyruvate, pyruvic acid (CHEBI:32816)
    'EX_cpd00029_e': [13, 14, 15], # Acetate, acetic acid (CHEBI:15366)
    'EX_cpd00361_e': [16, 17, 18], # Acetoin (CHEBI:15688)
    'EX_cpd00036_e': [19, 20, 21], # Succinic acid (CHEBI:15741)
    'EX_cpd00106_e': [22, 23, 24] # Fumaric acid (CHEBI:18012)
}

calibration_list_BASYSBIO_BIG_external_met_flux_list =  \
[
    'EX_cpd00027_e', # D-Glucose (CHEBI:17634) uptake estimate [mmol/g/h]
    'EX_cpd00130_e', # L-Malate, malic acid (CHEBI:6650)
    'EX_cpd00020_e', # Pyruvate, pyruvic acid (CHEBI:32816)
    'EX_cpd00029_e', # Acetate, acetic acid (CHEBI:15366)
    #'EX_cpd00361_e', # Acetoin (CHEBI:15688) currently not supported by the model as an external metabolite
    'EX_cpd00036_e', # Succinic acid (CHEBI:15741)
    'EX_cpd00106_e'  # Fumaric acid (CHEBI:18012)
]

def calibrateUptakeRates_BASISBIO_BIG(fluxdatafile="data/BASYSBIO_BIG/metabolic_flux/all_rates_GM.tsv.txt"):
    cur_rates = None

    with open(fluxdatafile) as f:
        reader = csv.reader(f, delimiter='\t')
        uptake_rate_list = list(reader)

        # pick time point at -1.01 h
        cur_rates = uptake_rate_list[100]

    return cur_rates



def setMedia(model, lb, mediaIdx, is_calibrated=0, is_aerobic_growth=True):

    exchangeFuncList = model.reactions.query(search_function='EX_')

    rxnList = mediaList[mediaIdx]['recipe'].keys()
    coeffList = mediaList[mediaIdx]['recipe'].values()

    for rxn in exchangeFuncList:
        rxn.lower_bound = 0
        rxn.upper_bound = 1000

    for rxnID, lb_coeff in zip(rxnList, coeffList):
        rxn = model.reactions.get_by_id(rxnID)
        rxn.lower_bound = lb*lb_coeff
        rxn.upper_bound = 1000

    if is_calibrated:
        calibration_rates = calibrateUptakeRates_BASISBIO_BIG("data/BASYSBIO_BIG/metabolic_flux/all_rates_MG.tsv.txt")
        for rxnID in calibration_list_BASYSBIO_BIG_external_met_flux_list:
            rxn = model.reactions.get_by_id(rxnID)
            [flux_est_idx, flux_lb_idx, flux_ub_idx] = dataIdxLookup_BASYSBIO_BIG_metabolomicFlux[rxnID]
            flux_est = float(calibration_rates[flux_est_idx])
            flux_lb = float(calibration_rates[flux_lb_idx]) # positive flux value from BASYSBIO data means uptake and vice versa
            flux_ub = float(calibration_rates[flux_ub_idx])
            if is_calibrated > 1: # fixed rate calibrated
                rxn.lower_bound = rxn.upper_bound = -1 * flux_est
            else: # range calibrated
                rxn.lower_bound = -1 * flux_ub # negative flux value in the cobra model means uptake and vice versa
                rxn.upper_bound = -1 * flux_lb

    #print rxn.name + ', '

    return model
'''
    if is_aerobic_growth:
        rxn = model.reactions.get_by_id(oxygenExBoundary)
        rxn.lower_bound = -1000
        rxn.upper_bound = 1000
    else:
        rxn = model.reactions.get_by_id(oxygenExBoundary)
        rxn.lower_bound = 0
        rxn.upper_bound = 1000
'''









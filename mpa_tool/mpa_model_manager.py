#coding: utf-8

__author__ = 'spark'

from ..mpa_tool.setMedia import *
from ..sparseNull import sparseNullCPU
import numpy

from cobra.core import DictList
from ..setShinorineReactions import setShinorineReactions

is_shiki_on = True

class MPAModelManager():
    def __init__(self, cb_model, gene_dict, var_idx_limit=0):
        try:
            self._cb_model = cb_model
            self._gene_dict = gene_dict
            self._var_idx_limit = var_idx_limit
	    print 'setting shinorine rxns'
            setShinorineReactions(self, is_shiki_on)
	    print 'finding met set'
            self.findMetSet()
	    print 'finding rxn set'
            self.findRxnSet()
            self._boundary_rxn_set = None
            self._transport_rxn_set = None
            self._curated_currency_cmp_set =  set(
                ['cpd00001_c', 'cpd00001_e', 'cpd00002_c', 'cpd00003_c', 'cpd00004_c',
                 'cpd00005_c', 'cpd00006_c', 'cpd00007_c', 'cpd00007_e', 'cpd00008_c',
                 'cpd00009_c', 'cpd00009_e', 'cpd00010_c', 'cpd00011_c', 'cpd00011_e',
                 'cpd00012_c', 'cpd00012_e', 'cpd00067_c', 'cpd00067_e', 'cpd00971_e',
                 'cpd00971_c'])
            self._curated_mass_rxn_set = set(
                ['rxn11921', 'rxn10198', 'rxn10199', 'rxn10200', 'rxn10201', 'rxn05296', 'rxn05295', 'rxn05294', 'bio00127', 'rxn13568']
            )

            print 'Finding the boundary rxn set...'
            self.findBoundaryRxnSet()
            print 'Finding the transport rxn set...'
            self.findTransportRxnSet()

            print 'MPA Model manager is ready.'

        except Exception as e:
            print e

    # predicates
    def isExtracellularMet(self, m_id):
        m = self._cb_model.metabolites.get_by_id(m_id)
        if m.compartment == 'e':
            return True
        else:
            return False


    # Met = {m | m is an element of V, m is a metabolite}
    def findMetSet(self):
        #met_vertices = [self._graph.vp['v_id'][v] for v in self._graph.vertices() if self._graph.vp['v_type'][v] == 'met']
        met_list = [m.id for m in self._cb_model.metabolites]

        self._met_set = set(met_list) # assign the met list into a set
        return self._met_set

    # Rxn = {r | r is an element of V, r is a metabolic reaction}
    def findRxnSet(self):
        #rxn_vertices = [self._graph.vp['v_id'][v] for v in self._graph.vertices() if self._graph.vp['v_type'][v] == 'rxn']
        rxn_list = [r.id for r in self._cb_model.reactions]

        self._rxn_set = set(rxn_list) # assign the rxn list into a set
        return self._rxn_set

    # BoundaryRxnSet = {r | r ∈ Rxn, Reactants(r) = ∅ ∨ Products(r) = ∅}
    def findBoundaryRxnSet(self):
        boundary_rxn_list = [r_id for r_id in self._rxn_set if len(self.getReactantsSet(r_id)) == 0 or len(self.getProductsSet(r_id)) == 0]
        self._boundary_rxn_set = set(boundary_rxn_list)
        return self._boundary_rxn_set

    # TransportRxnSet = {r | r ∈ Rxn, r ∉ BoundaryRxnSet,  ∃ m ∈ (Reactants(r) ∪ Products(r)) : m is an extracellular species}
    def findTransportRxnSet(self):
        if self._boundary_rxn_set is None:
            self.findBoundaryRxnSet()
        transport_rxn_list = [r_id for r_id in self._rxn_set if r_id not in self._boundary_rxn_set and self.hasExtracellularSpecies(self.getRxnParticipantSet(r_id))]
        self._transport_rxn_set = set(transport_rxn_list)
        return self._transport_rxn_set

    # Reactants(r) = {m | m ∈ Met is a reactant to r ∈ Rxn}
    def getReactantsSet(self, r_id):
        r = self._cb_model.reactions.get_by_id(r_id)
        reactants = [m.id for m in r.get_reactants()]
        reactant_set = set(reactants)
        return reactant_set

    # Products(r) = {m | m ∈ Met is a product of r ∈ Rxn}
    def getProductsSet(self, r_id):
        r = self._cb_model.reactions.get_by_id(r_id)
        products = [m.id for m in r.get_products()]
        product_set = set(products)
        return product_set

    # Reactants(r) ∪ Products(r))
    def getRxnParticipantSet(self, r_id):
        participant_set = set()
        participant_set = participant_set.union(self.getReactantsSet(r_id))
        participant_set = participant_set.union(self.getProductsSet(r_id))
        return participant_set

    def hasExtracellularSpecies(self, met_set):
        extracellular_met_list = [m_id for m_id in met_set if self.isExtracellularMet(m_id)]
        if len(extracellular_met_list) > 0:
            return True
        else:
            return False


    def setMedia(self, lb, mediaIdx, is_calibrated=0, is_aerobic_growth=True):
        setMedia(self._cb_model, lb, mediaIdx, is_calibrated, is_aerobic_growth)

    # this function adds a reaction to model.reactions of type cobra.core.DictList at a specified index
    # without rebuilding the dictionary (hence the name 'lazy')
    def cobra_lazy_add_reaction(self, rxn, idx):
        self._cb_model.reactions._check(rxn.id) # assert non-existent id
        super(DictList, self._cb_model.reactions).insert(idx, rxn) # access the super class of DictList: of type list

    def cobra_lazy_add_nontransport_rxn(self, rxn):
        self.cobra_lazy_add_reaction(rxn, 0) # add to the beginning of the list

    def cobra_lazy_add_transport_rxn(self, rxn):
        idx = len(self._cb_model.reactions)
        self.cobra_lazy_add_reaction(rxn, idx) # add to the end of the list, essentially the same as 'appending' but being lazy

    # this function identifies metabolites of type Metabolite participating in the given rxn of type Reaction
    # and finds out if any of them are new to the cobra model, and add the new metabolites to the model
    # it also looks out for any new gene definitions to the model, and add them accordingly
    # this function is called as part of the call to cobra_lazy_add_reaction
    def cobra_lazy_add_new_met_by_rxn(self, rxn):
        metabolite_dict = {}
        gene_dict = {}
        metabolite_dict.update(dict([(y.id, y) for y in rxn._metabolites]))
        new_metabolites = [metabolite_dict[x]
                           for x in set(metabolite_dict).difference(self._cb_model.metabolites._dict)]
        if new_metabolites:
	    print 'debug: adding met'
            self._cb_model.add_metabolites(new_metabolites)
	    print 'debug: added met'

        gene_dict.update(dict([(y.id, y) for y in rxn._genes]))

        new_genes = [gene_dict[x]
                           for x in set(gene_dict).difference(self._cb_model.genes._dict)]
        if new_genes:
            self._cb_model.genes += DictList(new_genes)
            [setattr(x, '_model', self._cb_model)
             for x in new_genes]

        #This might slow down performance
        #Make sure each reaction knows that it is now part of a Model and uses
        #metabolites in the Model and genes in the Model
        rxn._model = self
        rxn._metabolites = dict([(self._cb_model.metabolites.get_by_id(k.id), v)
                                         for k, v in rxn._metabolites.items()])
        rxn._genes = dict([(self._cb_model.genes.get_by_id(k.id), v)
                                         for k, v in rxn._genes.items()])
        #Make sure the metabolites and genes are aware of the reaction
        rxn._update_awareness()


    # this rebuild function is called once all of the cobra_lazy_add_reaction calls are made
    # in order to rebuild the dictionary of reactions
    def cobra_model_rebuild_rxn_dict(self):
        self._cb_model.reactions._generate_index()

    # copied in from cobrapy ver 0.3.0b3
    # modified to work with the class instance data in this project
    def convert_to_irreversible(self):
        """Will break all of the reversible reactions into two separate irreversible reactions with
        different directions.  Useful for some modeling problems.

        cobra_model: A Model object which will be modified in place.


        TODO: Can we just use a -1*guided_copy or something else?
        """
        #S = cobra_model.S.toarray()
        #[m,n] = S.shape # LPproblem.A.shape
        #nontransport = numpy.array([sum(S != 0) > 1]) #reactions which are not transport reactions.
        cobra_model = self._cb_model

        reactions_to_add = []
        for rxn_idx, reaction in enumerate(cobra_model.reactions):
            #Potential bug because a reaction might run backwards naturally
            #and this would result in adding an empty reaction to the
            #model in addition to the reverse reaction.
            if reaction.id not in self._boundary_rxn_set:
                if reaction.lower_bound < 0:
                    reverse_reaction = Reaction(reaction.id + "_reverse")
                    reverse_reaction.lower_bound = 0
                    reverse_reaction.upper_bound = reaction.lower_bound * -1
                    reaction.lower_bound = 0
                    #Make the directions aware of each other
                    reaction.reflection = reverse_reaction
                    reverse_reaction.reflection = reaction
                    reaction_dict = dict([(k, v*-1)
                                          for k, v in reaction._metabolites.items()])
                    reverse_reaction.add_metabolites(reaction_dict)
                    reverse_reaction._model = reaction._model
                    reverse_reaction._genes = reaction._genes
                    # make the gene and met aware of the reverse reaction
                    for gene in reaction._genes:
                        gene._reaction.add(reverse_reaction)
                    for met in reaction._metabolites:
                        met._reaction.add(reverse_reaction)
                    if hasattr(reaction, '_gene_reaction_rule'):
                        reverse_reaction._gene_reaction_rule = reaction._gene_reaction_rule
                    #reactions_to_add.append(reverse_reaction)
                    self.cobra_lazy_add_reaction(reverse_reaction, rxn_idx+1)
            else:
                print 'skipping a boundary rxn: ' + reaction.id
        #cobra_model.add_reactions(reactions_to_add)
        self.cobra_model_rebuild_rxn_dict()


    # copied in from cobrapy ver 0.3.0b3
    # modified to work with the class instance data in this project
    def revert_to_reversible(self, cobra_model, update_solution=True):
        """This function will convert a reversible model made by convert_to_irreversible
        into a reversible model.

        cobra_model:  A cobra.Model object which will be modified in place.

        NOTE: It might just be easiest to include this function in the Reaction class

        """
        reverse_reactions = [x for x in cobra_model.reactions
                             if x.reflection is not None and
                             x.id.endswith('_reverse')]

        for reverse in reverse_reactions:
            forward = reverse.reflection
            forward.lower_bound = -reverse.upper_bound
            forward.reflection = None
        #Since the metabolites and genes are all still in
        #use we can do this faster removal step.  We can
        #probably speed things up here.
        cobra_model.remove_reactions(reverse_reactions)
        # fix the solution
        if update_solution and cobra_model.solution is not None:
            x_dict = cobra_model.solution.x_dict
            for reverse in reverse_reactions:
                forward = reverse.reflection
                x_dict[forward.id] -= x_dict.pop(reverse.id)
            cobra_model.solution.x = [x_dict[r.id] for r in cobra_model.reactions]


    # copied in from cobrapy ver 0.3.x beta
    # modified to work with the class instance data in this project
    def changeObjective(self, objectives):
        """Change the objective in the cobrapy model.

        objectives: A list or a dictionary.  If a list then
        a list of reactions for which the coefficient in the
        linear objective is set as 1.  If a dictionary then the
        key is the reaction and the value is the linear coefficient
        for the respective reaction.

        """
        # I did not want to refactor code just to rename the variable, but this
        # way the API uses the variable "objectives"
        the_objectives = objectives
        # set all objective coefficients to 0 initially
        for x in self._cb_model.reactions:
            x.objective_coefficient = 0.
        # update the objective coefficients if a dict is passed in
        if hasattr(the_objectives, "items"):
            for the_reaction, the_coefficient in the_objectives.iteritems(): # iteritems(the_objectives):
                if isinstance(the_reaction, int):
                    the_reaction = self._cb_model.reactions[the_reaction]
                else:
                    if hasattr(the_reaction, 'id'):
                        the_reaction = the_reaction.id
                    the_reaction = self._cb_model.reactions.get_by_id(the_reaction)
                the_reaction.objective_coefficient = the_coefficient
        # If a list (or a single reaction is passed in), each reaction gets
        # 1 for the objective coefficent.
        else:
            # Allow for objectives to be constructed from multiple reactions
            if not hasattr(the_objectives, "__iter__") or \
                    isinstance(the_objectives, str):
                the_objectives = [the_objectives]
            for the_reaction in the_objectives:
                if isinstance(the_reaction, int):
                    the_reaction = self._cb_model.reactions[the_reaction]
                else:
                    if hasattr(the_reaction, 'id'):
                        the_reaction = the_reaction.id
                    the_reaction = self._cb_model.reactions.get_by_id(the_reaction)
                the_reaction.objective_coefficient = 1.


    # an objPol of value 1 is to maximize, of value -1 is to minimize the corresponding objective type determined by objType
    # objType 1: max biomass
    # objType 2: min non-biomass ATP maintenance
    #
    def setObjective(self, objType, objPol, isReset=True):

        if isReset:
            # reset all objective coeffs
            for rxn in self._cb_model.reactions:
                rxn.objective_coefficient = 0

        if objType == 1: # max biomass
            self._cb_model.reactions.bio00127.objective_coefficient = objPol
        elif objType == 2: # max min non-biomass ATP maintenance
            self._cb_model.reactions.rxn00062.objective_coefficient = objPol


    def null(self, A, eps=1e-15):
        """
        http://mail.scipy.org/pipermail/scipy-user/2005-June/004650.html
        """
        u, s, vh = numpy.linalg.svd(A)
        n = A.shape[1]   # the number of columns of A
        if len(s)<n:
            expanded_s = numpy.zeros(n, dtype = s.dtype)
            expanded_s[:len(s)] = s
            s = expanded_s
        null_mask = (s <= eps)
        null_space = numpy.compress(null_mask, vh, axis=0)
        return numpy.transpose(null_space)


    # python port of the matlab function addLoopLawConstraints() by Jan Schellenberger
    # utilizing cobraToolbox data format
    #INPUT
    # LPproblem Structure containing the following fields
    #  A      LHS matrix
    #  b      RHS vector
    #  c      Objective coeff vector
    #  lb     Lower bound vector
    #  ub     Upper bound vector
    #  osense Objective sense (-1 max, +1 min)
    #  csense Constraint senses, a string containting the constraint sense for
    #         each row in A ('E', equality, 'G' greater than, 'L' less than).
    #  F (optional)  If *QP problem
    #  vartype (optional) if MI*P problem
    # model     The cobraToolbox model for which the loops should be removed
    #
    #OPTIONAL INPUT
    # rxnIndex The index of variables in LPproblem corresponding to fluxes.
    #     default = [1:n]
    #
    #
    #OUTPUT
    # Problem structure containing the following fields describing an MILP problem
    # A, b, c, lb, ub - same as before but longer
    # vartype - variable type of the MILP problem ('C', and 'B')
    # x0 = [] Needed for solveMILPproblem
    #
    def addLoopLawConstraintsCOBRA(self, rxnIndex=None):
        model = self._cb_model
        #method = 2 # methd = 1 - separete af,ar;  method = 2 - only af;  method 3 - same as method 2 except use b_L, b_U instad of b and csense;
        reduce_vars = 0 # eliminates additional integer variables.  Should be faster in all cases but in practice may not be for some weird reason.
        combine_vars = 0 # combines flux coupled reactions into one variable.  Should be faster in all cases but in practice may not be.

        if rxnIndex == None:
            rxnIndex = numpy.array(range(0, model.S.shape[1]))
        else:
            rxnIndex = numpy.array(rxnIndex)

        S = model.S.toarray()
        [m,n] = S.shape # LPproblem.A.shape

        nontransport = numpy.array([sum(S != 0) > 1]) #reactions which are not transport reactions.
        nontransport = (nontransport | ((model.lower_bounds ==0) & (model.upper_bounds == 0)))

        reverse_reactions = [x for x in model.reactions
                             if x.reflection is not None and
                             x.id.endswith('_reverse')]
        rxn_indicator_dict = {}

        if reduce_vars == 1:
            active1 = ~((model.lower_bounds ==0) & (model.upper_bounds == 0))

            S2 = S[:,active1] # exclude rxns with both ub and lb equal to 0
            N2 = sparseNullCPU(S2, tol=1e-15) #, tol=1e-15)

            if N2.ndim == 1:
                N2_col_len = 1
            else:
                N2_col_len = N2.shape[1]


            N = numpy.zeros((len(active1), N2_col_len))
            N[active1,:] = N2

            active = numpy.any(abs(N) > 1e-15, axis=1) # exclude rxns not in null space

            nontransport = nontransport & active

        Sn = S[:,nontransport[0]]

        Ninternal = sparseNullCPU(Sn) #, tol=1e-15)
        #Ninternal = null(Sn,'r')
        linternal = Ninternal.shape[1]

        nint = len(numpy.where(nontransport)[0])

        # model already has colnames for internal rxns, so update only what's being newly added here
        # col names for ccol_a_0, ccol_a_1, ... ccol_a_n
        # col names for ccol_G_0, ccol_G_1, ... ccol_G_n

        the_constraint_mets = []
        the_constraint_rxns = []
        the_metabolic_rxns_internal = []
        the_G_rxns = []
        transport_rxn_set = set(numpy.where(nontransport[0] == False)[0])
        rxn_idx_set = set(rxnIndex)
        rxnIndex = list(rxn_idx_set.difference(transport_rxn_set))
        for idx in rxnIndex: #range(0,nint):
            # get a reference to the current metabolic reaction of interest (i.e. v_i)
            cur_met_rxn = model.reactions[idx]
            the_metabolic_rxns_internal.append(cur_met_rxn)
            # new reactions needed for the constraints
            constraint_rxn_a = Reaction(name='ccol_a_'+str(idx))
            rxn_indicator_dict[cur_met_rxn] = constraint_rxn_a # build a lookup for the binary constraint for post-processing

            # a_i lower_bound at 0 and upper_bound at 1
            constraint_rxn_a.lower_bound = 0
            constraint_rxn_a.upper_bound = 1
            constraint_rxn_a.variable_kind = 'integer' # essentially a binary variable in this case
            the_constraint_rxns.append(constraint_rxn_a)
            constraint_rxn_G = Reaction(name='ccol_G_'+str(idx))
            # G_i lower_bound at -1000 and upper_bound at 1000
            constraint_rxn_G.lower_bound = -1000
            constraint_rxn_G.upper_bound = 1000
            constraint_rxn_G.variable_kind = 'continuous' # default value, explicitly written for clarity
            the_constraint_rxns.append(constraint_rxn_G)
            the_G_rxns.append(constraint_rxn_G)

            # construct for v - 1000*a_i < 0
            constraint_id = 'cvar_a_ul_constraint_' + str(idx)
            constraint_met = Metabolite(id=constraint_id)
            constraint_met._constraint_sense = 'L' # <
            constraint_met._bound = 0
            the_constraint_mets.append(constraint_met)
            # add the constraint coeffs to the relevant reactions
            cur_met_rxn.add_metabolites({constraint_met: 1}) # v
            constraint_rxn_a.add_metabolites({constraint_met:-1000}) # - 1000*a_i

            # construct for v - 1000*a_i > -1000
            constraint_id = 'cvar_a_ll_constraint_' + str(idx)
            constraint_met = Metabolite(id=constraint_id)
            constraint_met._constraint_sense = 'G' # >
            constraint_met._bound = -1000
            the_constraint_mets.append(constraint_met)
            # add the constraint coeffs to the relevant reactions
            cur_met_rxn.add_metabolites({constraint_met: 1}) # v
            constraint_rxn_a.add_metabolites({constraint_met:-1000}) # - 1000*a_i

            # construct for G + 1001*a_i < 1000
            constraint_id = 'cvar_G_ul_constraint_' + str(idx)
            constraint_met = Metabolite(id=constraint_id)
            constraint_met._constraint_sense = 'L' # <
            constraint_met._bound = 1000
            the_constraint_mets.append(constraint_met)
            # add the constraint coeffs to the relevant reactions
            constraint_rxn_G.add_metabolites({constraint_met: 1})
            constraint_rxn_a.add_metabolites({constraint_met: 1001})

            # construct for G + 1001*a_i > 1
            constraint_id = 'cvar_G_ll_constraint_' + str(idx)
            constraint_met = Metabolite(id=constraint_id)
            constraint_met._constraint_sense = 'G' # <
            constraint_met._bound = 1
            the_constraint_mets.append(constraint_met)
            # add the constraint coeffs to the relevant reactions
            constraint_rxn_G.add_metabolites({constraint_met: 1})
            constraint_rxn_a.add_metabolites({constraint_met: 1001})

        # constructs for N*G = 0
        the_NdotG_vars = []
        for N_idx in range(0, linternal):
            constraint_id = 'cvar_NdotG_constraint_' + str(N_idx)
            constraint_met = Metabolite(id=constraint_id)
            constraint_met._constraint_sense = 'E' # =
            constraint_met._bound = 0
            the_constraint_mets.append(constraint_met)
            the_NdotG_vars.append(constraint_met)
        for G_idx in range(0,nint):
            constraint_rxn_G = the_G_rxns[G_idx]
            # add the constraint coeffs to the relevant column (reaction) in the matrix
            the_mets = {}
            for N_idx in range(0, linternal):
                constraint_met = the_NdotG_vars[N_idx]
                coeff_val = Ninternal[G_idx, N_idx]
                the_mets[constraint_met] = coeff_val

            constraint_rxn_G.add_metabolites(the_mets) #{constraint_met: coeff_val})

        # constraints to couple forward & reverse metabolic reaction pairs #####
        # each metabolic reaction would have a corresponding binary indicator rxn
        # for a pair of reactions for_rxn and rev_rxn, there exist a corresponding pair of binary indicators b_for and b_rev respectively.
        # this block of code adds constraints to the binary rxn indicators such that b_for + b_rev = 1
        # this constraint only allows one reaction out of the pair [for_rxn, rev_rxn] to be active at a time.
        for rev_rxn in reverse_reactions:
            for_rxn = rev_rxn.reflection
            constraint_id = 'or_constraint_' + str(for_rxn)
            or_constraint = Metabolite(id=constraint_id)
            or_constraint._bound = 1
            the_constraint_mets.append(or_constraint)
            for_rxn_indicator = rxn_indicator_dict[for_rxn]
            rev_rxn_indicator = rxn_indicator_dict[rev_rxn]
            for_rxn_indicator.add_metabolites({or_constraint: 1})
            rev_rxn_indicator.add_metabolites({or_constraint: 1})

        model.add_reactions(the_constraint_rxns) # this will automatically add all of the relevant constraint metabolites
        model.update()

        return model, [the_metabolic_rxns_internal, the_constraint_rxns, the_constraint_mets]

# coding: utf-8

__author__ = 'spark'

from graph_tool.centrality import betweenness
from graph_tool.all import Graph, GraphView
import cobra
#import gurobipy
from cobra.core import Solution as CBSolution
#from gurobipy import Model as GUROBISolution
#from gurobipy import GRB

import re

import rdflib


class MPACOBRASol():
    def __init__(self, sol, rxnlist):
        self._cobrasol = sol
        self._rxnlist = rxnlist

    def getFluxByRxnID(self, rxn_id, sol_idx=0):
        return self._cobrasol.x_dict[rxn_id]

    def getFluxByIdx(self, var_idx, sol_idx=0):
        return self._cobrasol.x[var_idx]

    def getFluxesByRxnIDList(self, rxn_id_list, sol_idx=0):
        return [self._cobrasol.x_dict[rxn_id] for rxn_id in rxn_id_list]

    def getZeroFluxRxnIdxList(self, var_idx_limit=0, sol_idx=0):
        if var_idx_limit > 0:
            return [x_idx for x_idx, x in enumerate(self._cobrasol.x[:var_idx_limit]) if x == 0]
        else:
            return [x_idx for x_idx, x in enumerate(self._cobrasol.x) if x == 0]

    def getZeroFluxRxnIDSet(self, var_idx_limit=0, sol_idx=0):
        return set([self._rxnlist[var_idx] for var_idx in self.getZeroFluxRxnIdxList(var_idx_limit, sol_idx)])

    def getNonzeroFluxRxnIdxList(self, var_idx_limit=0, sol_idx=0):
        if var_idx_limit > 0:
            return [x_idx for x_idx, x in enumerate(self._cobrasol.x[:var_idx_limit]) if x != 0]
        else:
            return [x_idx for x_idx, x in enumerate(self._cobrasol.x) if x != 0]

    def getNonzeroFluxRxnIDSet(self, var_idx_limit=0, sol_idx=0):
        idx_list = self.getNonzeroFluxRxnIdxList(var_idx_limit, sol_idx)
        return set([self._rxnlist[x_idx].id for x_idx in idx_list])

class MPAGUROBISol():
    def __init__(self, sol):
        self._gurobisol = sol
        self._rxnlist = None

    def getFluxByRxnID(self, rxn_id, sol_idx=0):
        return [var.getAttr(GRB.attr.X) for var in self._gurobisol.getVars() if var.getAttr(GRB.attr.VarName) == rxn_id][0]

    def getFluxByIdx(self, var_idx, sol_idx=0):
        return self._gurobisol.getVars()[var_idx].getAttr(GRB.attr.X)

    def getFluxesByRxnIDList(self, rxn_id_list, sol_idx=0):
        return [var.getAttr(GRB.attr.X) for var in self._gurobisol.getVars() if var.getAttr(GRB.attr.VarName) in rxn_id_list]

    def getZeroFluxRxnIdxList(self, var_idx_limit=0, sol_idx=0):
        if var_idx_limit > 0:
            return [var.__colno__() for var in self._gurobisol.getVars() if var.getAttr(GRB.attr.X) == 0 and var.__colno__() < var_idx_limit]
        else:
            return [var.__colno__() for var in self._gurobisol.getVars() if var.getAttr(GRB.attr.X) == 0]


    def getZeroFluxRxnIDSet(self, var_idx_limit=0, sol_idx=0):
        if var_idx_limit > 0:
            return set([var.getAttr(GRB.attr.VarName) for var in self._gurobisol.getVars() if var.getAttr(GRB.attr.X) == 0 and var.__colno__() < var_idx_limit])
        else:
            return set([var.getAttr(GRB.attr.VarName) for var in self._gurobisol.getVars() if var.getAttr(GRB.attr.X) == 0])

    def getNonzeroFluxRxnIdxList(self, var_idx_limit=0, sol_idx=0):
        if var_idx_limit > 0:
            return [var.__colno__() for var in self._gurobisol.getVars() if var.getAttr(GRB.attr.X) != 0 and var.__colno__() < var_idx_limit]
        else:
            return [var.__colno__() for var in self._gurobisol.getVars() if var.getAttr(GRB.attr.X) != 0]

    def getNonzeroFluxRxnIDSet(self, var_idx_limit=0, sol_idx=0):
        if var_idx_limit > 0:
            return set([var.getAttr(GRB.attr.VarName) for var in self._gurobisol.getVars() if var.getAttr(GRB.attr.X) != 0 and var.__colno__() < var_idx_limit])
        else:
            return set([var.getAttr(GRB.attr.VarName) for var in self._gurobisol.getVars() if var.getAttr(GRB.attr.X) != 0])

class MPACPLEXSol():
    def __init__(self, sol):
        self._cplexsol = sol
        self._solnum = sol.solution.pool.get_num()
        self._rxnlist = None

    def getFluxByRxnID(self, rxn_id, sol_idx=0):
        return self._cplexsol.solution.pool.get_values(sol_idx)[self._cplexsol.variables.get_indices(rxn_id)]

    def getFluxByIdx(self, var_idx, sol_idx=0):
        return self._cplexsol.solution.pool.get_values(sol_idx)[var_idx]

    def getFluxesByRxnIDList(self, rxn_id_list, sol_idx=0):
        sol_vals = self._cplexsol.solution.pool.get_values(sol_idx)
        return [sol_vals[self._cplexsol.variables.get_indices(rxn_id)] for rxn_id in rxn_id_list]

    def getZeroFluxRxnIdxList(self, var_idx_limit=0, sol_idx=0):
        if var_idx_limit > 0:
            var_idx_list = self._cplexsol.solution.pool.get_values(sol_idx)[:var_idx_limit]
        else:
            var_idx_list = self._cplexsol.solution.pool.get_values(sol_idx)

        return [flux_idx for flux_idx, flux in enumerate(var_idx_list) if flux == 0]

    def getZeroFluxRxnIDSet(self, var_idx_limit=0, sol_idx=0):
        if var_idx_limit > 0:
            var_name_list = self._cplexsol.variables.get_names()[:var_idx_limit]
        else:
            var_name_list = self._cplexsol.variables.get_names()

        return set([var_name_list[var_idx] for var_idx in self.getZeroFluxRxnIdxList(sol_idx, var_idx_limit)])

    def getNonzeroFluxRxnIdxList(self, var_idx_limit=0, sol_idx=0):
        if var_idx_limit > 0:
            var_idx_list = self._cplexsol.solution.pool.get_values(sol_idx)[:var_idx_limit]
        else:
            var_idx_list = self._cplexsol.solution.pool.get_values(sol_idx)

        return [flux_idx for flux_idx, flux in enumerate(var_idx_list) if flux != 0]

    def getNonzeroFluxRxnIDSet(self, var_idx_limit=0, sol_idx=0):
        if var_idx_limit > 0:
            var_name_list = self._cplexsol.variables.get_names()[:var_idx_limit]
        else:
            var_name_list = self._cplexsol.variables.get_names()

        return set([var_name_list[var_idx] for var_idx in self.getNonzeroFluxRxnIdxList(sol_idx, var_idx_limit)])


class MPASolutionManager():
    def __init__(self, sol, rxnlist=None):
        if sol.__class__ == CBSolution: #cobra.core.Solution:
            self._solution = MPACOBRASol(sol, rxnlist)
        elif sol.__class__ == GUROBISolution: #gurobipy.Model:
            self._solution = MPAGUROBISol(sol)
        elif sol.__class__ == cobra.solvers.cplex_solver.Problem:
            self._solution = MPACPLEXSol(sol)

    def getFluxByRxnID(self, rxn_id, sol_idx=0):
        return self._solution.getFluxByRxnID(rxn_id, sol_idx)

    def getFluxByIdx(self, var_idx, sol_idx=0):
        return self._solution.getFluxByIdx(var_idx, sol_idx)

    def getFluxesByRxnIDList(self, rxn_id_list, sol_idx=0):
        return self._solution.getFluxesByRxnIDList(rxn_id_list, sol_idx)

    def getZeroFluxRxnIdxList(self, var_idx_limit=0, sol_idx=0):
        return self._solution.getZeroFluxRxnIdxList(var_idx_limit, sol_idx)

    def getZeroFluxRxnIDSet(self, var_idx_limit=0, sol_idx=0):
        return self._solution.getZeroFluxRxnIDSet(var_idx_limit, sol_idx)

    def getNonzeroFluxRxnIdxList(self, var_idx_limit=0, sol_idx=0):
        return self._solution.getNonzeroFluxRxnIdxList(var_idx_limit, sol_idx)

    def getNonzeroFluxRxnIDSet(self, var_idx_limit=0, sol_idx=0):
        return self._solution.getNonzeroFluxRxnIDSet(var_idx_limit, sol_idx)




class BioCycOWLReference():
    def __init__(self, biocyc_ref_owl_filepath="data/biocyc/2.5/data/biopax-level3.owl"):
        self._rdf_graph = rdflib.Graph()
        self._rdf_graph.parse(biocyc_ref_owl_filepath)
        self._pathway_proteins_dict = {}
        self._pathway_name_dict = {}
        self._pathway_id_list = None

    # return all pathways in biocyc
    # return data format: list of pathway id strings
    def queryBioCycOWL_Pathways(self):

        if not self._pathway_id_list:
            pathway_list = list(self._rdf_graph.query("""select ?pathwayid ?pathwayname
                                where {?pathway a bp:Pathway. ?pathway bp:xref ?pathwayref.
                                ?pathwayref bp:id ?pathwayid. ?pathway bp:standardName ?pathwayname }"""))
            pathway_id_list = []
            for pathway in pathway_list:
                pathway_id = pathway[0].toPython()
                pathway_name = pathway[1].toPython()
                pathway_id_list.append(pathway_id)
                self._pathway_name_dict[pathway_id] = pathway_name

            self._pathway_id_list = pathway_id_list
        else:
            pathway_id_list = self._pathway_id_list

        return pathway_id_list

    # return all proteins that are part of a pathway
    # return data format: list of protein name strings
    def queryBioCycOWL_ProteinNamesByPathwayID(self, pathway_id):

        if pathway_id not in self._pathway_proteins_dict:
            protein_names = self._rdf_graph.query("""select distinct ?proteinName ?protein where {
                {?pathway a bp:Pathway. ?pathway bp:xref ?pathwayref. ?pathwayref bp:id ?pathwayid.
                ?pathway bp:pathwayComponent ?catalysis. ?catalysis a bp:Catalysis.
                ?catalysis bp:controller ?complex. ?complex a bp:Complex.
                ?complex bp:component ?protein. ?protein a bp:Protein. ?protein bp:displayName ?proteinName.
                FILTER (STR(?pathwayid)=""" + "\"" + pathway_id + "\"" + """)} UNION
                {?pathway a bp:Pathway. ?pathway bp:xref ?pathwayref. ?pathwayref bp:id ?pathwayid.
                ?pathway bp:pathwayComponent ?catalysis. ?catalysis a bp:Catalysis.
                ?catalysis bp:controller ?protein. ?protein a bp:Protein.
                ?protein bp:displayName ?proteinName.
                FILTER (STR(?pathwayid)=""" + "\"" + pathway_id + "\"" + """) }}""")

            protein_names_list = []
            for protein_name in protein_names:
                protein_names_list.append(protein_name[0].toPython())

            self._pathway_proteins_dict[pathway_id] = protein_names_list
        else:
            protein_names_list = self._pathway_proteins_dict[pathway_id]

        return protein_names_list

    def queryBioCycOWL_GeneNamesByPathwayID(self, pathway_id):
        protein_name_list = self.queryBioCycOWL_ProteinNamesByPathwayID(pathway_id)
        return self.convertProteinNamesToGeneNames(protein_name_list)

    # convert the first letter of each gene name to upper case
    def convertGeneNamesToProteinNames(self, gene_name_list):
        protein_name_list = []
        for gene_name in gene_name_list:
            protein_name_list.append(gene_name[0].upper() + gene_name[1:])

        return protein_name_list

    # convert the first letter of each protein name to lower case
    def convertProteinNamesToGeneNames(self, protein_name_list):
        gene_name_list = []
        for protein_name in protein_name_list:
            gene_name_list.append(protein_name[0].lower() + protein_name[1:])

        return gene_name_list

    #
    def getPathwayNameByID(self, pathway_id):
        if pathway_id in self._pathway_name_dict:
            return self._pathway_name_dict[pathway_id]
        else:
            return None


class MPAModelProfiler():
    def __init__(self, cb_model, gene_dict, var_idx_limit=0):
        try:
            self._cb_model = cb_model
            self._gene_dict = gene_dict
            self._var_idx_limit = var_idx_limit
            self.findMetSet()
            self.findRxnSet()
            self._boundary_rxn_set = None
            self._transport_rxn_set = None
            self._curated_currency_cmp_set =  set(
                ['cpd00001_c', 'cpd00001_e', 'cpd00002_c', 'cpd00003_c', 'cpd00004_c',
                 'cpd00005_c', 'cpd00006_c', 'cpd00007_c', 'cpd00007_e', 'cpd00008_c',
                 'cpd00009_c', 'cpd00009_e', 'cpd00010_c', 'cpd00011_c', 'cpd00011_e',
                 'cpd00012_c', 'cpd00012_e', 'cpd00067_c', 'cpd00067_e', 'cpd00971_e',
                 'cpd00971_c'])
            self._curated_mass_rxn_set = set(
                ['rxn11921', 'rxn10198', 'rxn10199', 'rxn10200', 'rxn10201', 'rxn05296', 'rxn05295', 'rxn05294', 'bio00127', 'rxn13568']
            )
            print 'Constructing BioCyc OWL reference data...'
            self._biocycref = BioCycOWLReference()
            print 'Finding the boundary rxn set...'
            self.findBoundaryRxnSet()
            print 'Finding the transport rxn set...'
            self.findTransportRxnSet()
            print 'Finding the catalyst compound set...'
            self.findCatalystMetSet()
            print 'Constructing a graph from the model...'
            self.constructGraphFromModel()
            print 'Calculating the betweenness centrality...'
            self.calculateBtwnessCentrality()
            print 'MPA Model profiler is ready.'

        except Exception as e:
            print e

    def getPathwayStatsBySolution(self, mpa_sol, sol_idx=0):
        nonzero_flux_rxn_set = mpa_sol.getNonzeroFluxRxnIDSet(self._var_idx_limit, sol_idx)
        nonzero_flux_gene_list = self._gene_dict.convertRxnIDsToGeneNames(nonzero_flux_rxn_set)
        pathway_list = self._biocycref.queryBioCycOWL_Pathways()
        for pathway in pathway_list:
            pathway_genes = self._biocycref.queryBioCycOWL_GeneNamesByPathwayID(pathway)
            pathway_rxn_set = set(self._gene_dict.convertGeneNamesToRxnIDs(pathway_genes))
            nonzero_flux_pathway_rxns = pathway_rxn_set.intersection(nonzero_flux_rxn_set)
            if len(nonzero_flux_pathway_rxns) > 0:
                nonzero_flux_pathway_genes = self._gene_dict.convertRxnIDsToGeneNames(nonzero_flux_pathway_rxns)
                #nonzero_flux_vals = mpa_sol.getFluxesByRxnIDList(nonzero_flux_pathway_rxns, sol_idx)
                gene_rxn_flux_list = [{gene: zip(self._gene_dict.convertGeneNamesToRxnIDs([gene]), mpa_sol.getFluxesByRxnIDList(self._gene_dict.convertGeneNamesToRxnIDs([gene])))} for gene in nonzero_flux_pathway_genes]
                pathway_name = self._biocycref.getPathwayNameByID(pathway)
                percent_coverage = "%.2f " % (len(nonzero_flux_pathway_rxns)*100.0/len(pathway_rxn_set))
                print pathway_name + ': ' + percent_coverage + '% ' + str(gene_rxn_flux_list)

    def calculateBtwnessCentrality(self):
        self._centrality_btwness_vp, btw_cent_ep = betweenness(self._graph)
        self._centrality_btwness_vp_arr = self._centrality_btwness_vp.get_array()
        self._centrality_btwness_arr_sorted_idx = sorted(range(len(self._centrality_btwness_vp_arr)), key=lambda k: self._centrality_btwness_vp_arr[k], reverse=True) # obtain the permutation indices for sorting vp_arr in a descending order

    def constructGraphFromModel(self):
        # We start with an empty, directed graph

        g = Graph(directed=True)

        g.vp['v_id'] = g.new_vertex_property('string')
        g.vp['v_type'] = g.new_vertex_property('string')
        #g.vp['v_color'] = g.new_vertex_property('vector<float>')
        g.vp['v_color'] = g.new_vertex_property('string')
        g.ep['is_forward_rxn'] = g.new_edge_property('bool')
        g.ep['is_enabled'] = g.new_edge_property('bool')
        g.ep['flux'] = g.new_edge_property('float')
        g.ep['cap'] = g.new_edge_property('float')

        numRxns = len(self._cb_model.reactions)
        numMets = len(self._cb_model.metabolites)

        v_lookup = {} #initialize
        v_idx_lookup = {} #initialize

        for i in range(0,numRxns):
            r = self._cb_model.reactions[i]
            if not v_lookup.has_key(r): # if hasn't been seen before
                v = g.add_vertex()
                g.vp['v_id'][v] = r
                g.vp['v_type'][v] = 'rxn'
                g.vp['v_color'][v] = "#00ff00" #0x00ff00 #[0.,1.0, 0., 1.0] #'g' #0x00ff00
                v_lookup[r] = v
                v_idx_lookup[r.id] = g.vertex_index[v]

            participant_met_list = self.getRxnParticipantSet(r.id)
            reactants_set = self.getReactantsSet(r.id)
            products_set = self.getProductsSet(r.id)

            # for each metabolite concerning the metabolic reaction of id 'r.id'
            for m_id in participant_met_list:
                m = self._cb_model.metabolites.get_by_id(m_id)
                if not v_lookup.has_key(m): # if hasn't been seen before
                    v_met = g.add_vertex()      # create a new vertex for the metabolite
                    g.vp['v_id'][v_met] = m
                    g.vp['v_type'][v_met] = 'met'
                    g.vp['v_color'][v_met] = "#ffff00" #0xffff00 #[0., 0.5, 0.5, 1.0] #'' #0x00ffff
                    v_lookup[m] = v_met
                    v_idx_lookup[m_id] = g.vertex_index[v_met]
                else:
                    v_met = v_lookup[m]


                if m_id in products_set:
                    e = g.add_edge(v, v_met) # add an edge from rxn to met
                    g.ep['is_forward_rxn'][e] = True
                    g.ep['is_enabled'][e] = True
                    if r.reversibility == True:
                        e = g.add_edge(v_met, v)
                        g.ep['is_forward_rxn'][e] = False
                        g.ep['is_enabled'][e] = False
                elif m_id in reactants_set:
                    e = g.add_edge(v_met, v) # add an edge from met to rxn
                    g.ep['is_forward_rxn'][e] = True
                    g.ep['is_enabled'][e] = True
                    if r.reversibility == True:
                        e = g.add_edge(v, v_met)
                        g.ep['is_forward_rxn'][e] = False
                        g.ep['is_enabled'][e] = False

                g.ep['flux'][e] = 0 # we don't know flux at this stage, so set it to a default value of 0

        self._graph = g
        self._vidx_lookup = v_idx_lookup
        self._v_lookup = v_lookup
        print 'graph construction done'


    def graph_disableAllEdges(self):
        for e in self._graph.edges():
            self._graph.ep['is_enabled'][e] = False
            self._graph.ep['flux'][e] = 0

    def graph_enableAllDefaultEdges(self):
        for e in [e for e in self._graph.edges() if g._graph.ep['is_forward_rxn'] == True]:
            self._graph.ep['is_enabled'][e] = True
            self._graph.ep['flux'][e] = 0

    def graph_getForwardRxnEdgeSet(self, r_id):
        r = self._cb_model.reactions.get_by_id(r_id)
        v_rxn = self._v_lookup[r]

        edge_set = set([e for e in v_rxn.in_edges() if self._graph.ep['is_forward_rxn'][e] == True])
        edge_set = edge_set.union([e for e in v_rxn.out_edges() if self._graph.ep['is_forward_rxn'][e] == True])

        return edge_set

    def graph_getReverseRxnEdgeSet(self, r_id):
        r = self._cb_model.reactions.get_by_id(r_id)

        edge_set = set() # initialize to null set
        if r.reversibility == True:
            v_rxn = self._v_lookup[r]
            edge_set = edge_set.union([e for e in v_rxn.in_edges() if self._graph.ep['is_forward_rxn'][e] == False])
            edge_set = edge_set.union([e for e in v_rxn.out_edges() if self._graph.ep['is_forward_rxn'][e] == False])

        return edge_set

    def graph_enableReverseRxnEdgesByRxnID(self, r_id, flux_val):
        edge_set = self.graph_getReverseRxnEdgeSet(r_id)
        if len(edge_set) == 0:
            edge_set = self.graph_getForwardRxnEdgeSet(r_id)

        for e in edge_set:
            self._graph.ep['is_enabled'][e] = True
            self._graph.ep['flux'][e] = flux_val

    def graph_enableForwardRxnEdgesByRxnID(self, r_id, flux_val):
        edge_set = self.graph_getForwardRxnEdgeSet(r_id)

        for e in edge_set:
            self._graph.ep['is_enabled'][e] = True
            self._graph.ep['flux'][e] = flux_val

    def applyFBASolution(self, mpa_sol_manager):
        flux_rxn_set = mpa_sol_manager.getNonzeroFluxRxnIDSet(self.getRxnSetSize())

        self.graph_disableAllEdges()
        for rxn_id in flux_rxn_set:
            flux_val = mpa_sol_manager.getFluxByRxnID(rxn_id)
            if flux_val > 0:
                self.graph_enableForwardRxnEdgesByRxnID(rxn_id, flux_val)
            elif flux_val < 0:
                self.graph_enableReverseRxnEdgesByRxnID(rxn_id, flux_val)

        sol_graphview = GraphView(self._graph, efilt=lambda e: self._graph.ep['is_enabled'][e] == True)

        return sol_graphview


    # predicates
    def isExtracellularMet(self, m_id):
        m = self._cb_model.metabolites.get_by_id(m_id)
        if m.compartment == 'e':
            return True
        else:
            return False

    def isCytosolicMet(self, m_id):
        m = self._cb_model.metabolites.get_by_id(m_id)
        if m.compartment == 'c':
            return True
        else:
            return False

    def hasExtracellularSpecies(self, met_set):
        extracellular_met_list = [m_id for m_id in met_set if self.isExtracellularMet(m_id)]
        if len(extracellular_met_list) > 0:
            return True
        else:
            return False

    def isCuratedCurrencyCmp(self, m_id):
        if m_id in self._curated_currency_cmp_set:
            return True
        else:
            return False

    def isCuratedMassRxn(self, r_id):
        if r_id in self._curated_mass_rxn_set:
            return True
        else:
            return False

    # a compound is a metabolite if there exists at least one reaction that either consumes or produces the compound
    # pls note that a reaction that has the compound on both the reactant and the product sides does not count
    def isMetabolite(self, m_id):
        m_id_compartmentless = self.removeMetIDCompartmentStr(m_id)
        m_list = self._cb_model.metabolites.query(m_id_compartmentless)
        m_rxn_set = set() # initialize
        # find the set of non-boundary rxns involving both the extracellular and cytosolic species of the met id'ed by m_id
        for m in m_list:
            m_rxn_set = m_rxn_set.union([r.id for r in m.get_reaction()])
        m_rxn_set = m_rxn_set.difference(self._boundary_rxn_set)
        m_rxn_set = m_rxn_set.difference(self._curated_mass_rxn_set)

        metabolic_rxn_set = set([r_id for r_id in m_rxn_set if (m_id_compartmentless in self.getReactantsCompartmentlessSet(r_id)) != (m_id_compartmentless in self.getProductsCompartmentlessSet(r_id))])
        if len(metabolic_rxn_set) > 0:
            return True
        else:
            return False

    # a reaction is a metabolic reaction if the reaction has at least two metabolic compounds
    def isMetabolicRxn(self, r_id):
        rxn_participants_set = self.getRxnParticipantSet(r_id)
        met_set = [m_id for m_id in rxn_participants_set if self.isMetabolite(m_id)]
        if len(met_set) > 1:
            return True
        else:
            return False

    def hasNonCurrencyMetabolite(self, r_id):
        rxn_participants_set = self.getRxnParticipantSet(r_id)
        met_set = [m_id for m_id in rxn_participants_set if self.isMetabolite(m_id) and not self.isCuratedCurrencyCmp(m_id)]
        if len(met_set) > 0:
            return True
        else:
            return False



    # a compound is a catalyst if there exists at least one reaction in which the compound is participating
    # and the compound is both a reactant and a product, and the compound itself is not a metabolite
    # pls note that some metabolites only have uptake reactions but do not have cytosolic metabolic reactions
    # , probably due to incompleteness of the model.
    # A check is necessary to verify if the given compound appears on both sides of a reaction
    # and has a metabolite as a co-participant in the reaction
    def isCatalyst(self, m_id):
        #m = self._cb_model.metabolites.get_by_id(m_id)
        if self.isMetabolite(m_id):
            return False
        m_id_compartmentless = self.removeMetIDCompartmentStr(m_id)
        m_list = self._cb_model.metabolites.query(m_id_compartmentless)
        m_rxn_set = set() # initialize
        # find the set of rxns involving both the extracellular and cytosolic species of the met id'ed by m_id
        for m in m_list:
            m_rxn_set = m_rxn_set.union([r.id for r in m.get_reaction()])

        # find metabolic rxns that involve the compound that is not itself metabolized as part of the reaction
        catalyzed_rxn_set = set([r_id for r_id in m_rxn_set \
                                if m_id_compartmentless in self.getReactantsCompartmentlessSet(r_id) and \
                                   m_id_compartmentless in self.getProductsCompartmentlessSet(r_id) and \
                                   self.hasNonCurrencyMetabolite(r_id)
                                ])

        # if there exists at least one metabolic rxn involving the compound where the compound itself is not metabolized -> the compound is a catalyst
        num_cat_rxn = len(catalyzed_rxn_set) #.difference(self._transport_rxn_set))
        #num_non_boundary_rxn = len(m_rxn_set.difference(self._boundary_rxn_set)) #.union(self._transport_rxn_set)))
        #if num_non_boundary_rxn == num_cat_rxn and num_cat_rxn > 1:
        if num_cat_rxn > 0:
            return True
        else:
            return False

    # regular expression op
    def removeMetIDCompartmentStr(self, met_id):
        re_m = re.search('(.*)_[c|e]', met_id)
        return re_m.group(1)

    # Met = {m | m is an element of V, m is a metabolite}
    def findMetSet(self):
        #met_vertices = [self._graph.vp['v_id'][v] for v in self._graph.vertices() if self._graph.vp['v_type'][v] == 'met']
        met_list = [m.id for m in self._cb_model.metabolites]

        self._met_set = set(met_list) # assign the met list into a set
        return self._met_set

    # Rxn = {r | r is an element of V, r is a metabolic reaction}
    def findRxnSet(self):
        #rxn_vertices = [self._graph.vp['v_id'][v] for v in self._graph.vertices() if self._graph.vp['v_type'][v] == 'rxn']
        rxn_list = [r.id for r in self._cb_model.reactions]

        self._rxn_set = set(rxn_list) # assign the rxn list into a set
        return self._rxn_set

    # BoundaryRxnSet = {r | r ∈ Rxn, Reactants(r) = ∅ ∨ Products(r) = ∅}
    def findBoundaryRxnSet(self):
        boundary_rxn_list = [r_id for r_id in self._rxn_set if len(self.getReactantsSet(r_id)) == 0 or len(self.getProductsSet(r_id)) == 0]
        self._boundary_rxn_set = set(boundary_rxn_list)
        return self._boundary_rxn_set

    # TransportRxnSet = {r | r ∈ Rxn, r ∉ BoundaryRxnSet,  ∃ m ∈ (Reactants(r) ∪ Products(r)) : m is an extracellular species}
    def findTransportRxnSet(self):
        if self._boundary_rxn_set is None:
            self.findBoundaryRxnSet()
        transport_rxn_list = [r_id for r_id in self._rxn_set if r_id not in self._boundary_rxn_set and self.hasExtracellularSpecies(self.getRxnParticipantSet(r_id))]
        self._transport_rxn_set = set(transport_rxn_list)
        return self._transport_rxn_set

     # CytosolicRxnSet = {r | r ∈ Rxn, ¬ ∃ m ∈ (Reactants(r) ∪ Products(r)) : m is an extracellular species}
    def findCytosolicRxnSet(self):
        cytosolic_rxn_list = [r_id for r_id in self._rxn_set if not self.hasExtracellularSpecies(self.getRxnParticipantSet(r_id))]
        self._cytosolic_rxn_set = set(cytosolic_rxn_list)
        return self._cytosolic_rxn_set

    # CatalystMetSet = {m | m ∈ Met, ∀ r ∈ Rxn(m) : m ∈ Reactants(r) ∧ m ∈ Products(r)}
    def findCatalystMetSet(self):
        catalyst_met_list = [m_id for m_id in self._met_set if self.isCatalyst(m_id)]
        self._catalyst_met_set = set(catalyst_met_list)
        return self._catalyst_met_set

    def getMetSet(self, exclusion_set=None):
        if exclusion_set:
            return self._met_set.difference(exclusion_set)
        else:
            return self._met_set

    def getRxnSet(self, exclusion_set=None):
        if exclusion_set:
            return self._rxn_set.difference(exclusion_set)
        else:
            return self._rxn_set

    def getRxnSetSize(self):
        return len(self._rxn_set)

    def getBoundaryRxnSet(self, exclusion_set=None):
        if exclusion_set:
            return self._boundary_rxn_set.difference(exclusion_set)
        else:
            return self._boundary_rxn_set

    def getTransportRxnSet(self, exclusion_set=None):
        if exclusion_set:
            return self._transport_rxn_set.difference(exclusion_set)
        else:
            return self._transport_rxn_set

    def getCytosolicRxnSet(self, exclusion_set=None):
        if exclusion_set:
            return self._cytosolic_rxn_set.difference(exclusion_set)
        else:
            return self._cytosolic_rxn_set

    def getCatalystMetSet(self, exclusion_set=None):
        if exclusion_set:
            return self._catalyst_met_set.difference(exclusion_set)
        else:
            return self._catalyst_met_set

            # Reactants(r) = {m | m ∈ Met is a reactant to r ∈ Rxn}
    def getReactantsSet(self, r_id):
        r = self._cb_model.reactions.get_by_id(r_id)
        reactants = [m.id for m in r.get_reactants()]
        reactant_set = set(reactants)
        return reactant_set

    # Reactants(r) = {m | m ∈ Met is a reactant to r ∈ Rxn}
    def getReactantsCompartmentlessSet(self, r_id):
        r = self._cb_model.reactions.get_by_id(r_id)
        reactants = [self.removeMetIDCompartmentStr(m.id) for m in r.get_reactants()]
        reactant_set = set(reactants)
        return reactant_set

    # Products(r) = {m | m ∈ Met is a product of r ∈ Rxn}
    def getProductsSet(self, r_id):
        r = self._cb_model.reactions.get_by_id(r_id)
        products = [m.id for m in r.get_products()]
        product_set = set(products)
        return product_set

    # Products(r) = {m | m ∈ Met is a product of r ∈ Rxn}
    def getProductsCompartmentlessSet(self, r_id):
        r = self._cb_model.reactions.get_by_id(r_id)
        products = [self.removeMetIDCompartmentStr(m.id) for m in r.get_products()]

        product_set = set(products)
        return product_set

    # Reactants(r) ∪ Products(r))
    def getRxnParticipantSet(self, r_id):
        participant_set = set()
        participant_set = participant_set.union(self.getReactantsSet(r_id))
        participant_set = participant_set.union(self.getProductsSet(r_id))
        return participant_set

    # Reactions_producing(m) = {r | r ∈ Rxn, m ∈ Products(r)}
    def getReactionSetProducingMet(self, m_id, exclusion_set = None):
        m = self._cb_model.metabolites.get_by_id(m_id)
        if exclusion_set:
            reactions = [r.id for r in m.get_reaction() if r not in exclusion_set and (m in r.get_products() or (m in r.get_reactants() and r.reversibility == 1))]
        else:
            reactions = [r.id for r in m.get_reaction() if m in r.get_products() or (m in r.get_reactants() and r.reversibility == 1)]
        rxn_set = set(reactions)
        return rxn_set

    # Reactions_consuming(m) = {r | r ∈ Rxn, m ∈ Reactants(r)}
    def getReactionSetConsumingMet(self, m_id, exclusion_set = None):
        m = self._cb_model.metabolites.get_by_id(m_id)
        if exclusion_set:
            reactions = [r.id for r in m.get_reaction() if r not in exclusion_set and (m in r.get_reactants() or (m in r.get_products() and r.reversibility == 1))]
        else:
            reactions = [r.id for r in m.get_reaction() if m in r.get_reactants() or (m in r.get_products() and r.reversibility == 1)]
        rxn_set = set(reactions)
        return rxn_set

    # Byproducts(m) = {x | x ∈ Products(r ∈ Reactions_producing(m ∈ Met)), x != m}
    def getByproductSet(self, m_id, exclusion_set = None):
        m = self._cb_model.metabolites.get_by_id(m_id)
        rxn_producing_met = self.getReactionSetProducingMet(m_id, exclusion_set)
        byproduct_set = set() # create an empty set
        for r_id in rxn_producing_met:
            byproduct_set = byproduct_set.union(set([x_id for x_id in self.getProductsSet(r_id) if x_id != m_id]))

        return byproduct_set


    # PrecursorMets(m) = {x | x ∈ Reactants(r ∈ Reactions_producing(m ∈ Met))}
    def getPrecursorSet(self, m_id, exclusion_set = None):
        rxn_producing_met = self.getReactionSetProducingMet(m_id, exclusion_set)
        precursor_set = set() # create an empty set
        for r_id in rxn_producing_met:
            precursor_set.union(set([x_id for x_id in self.getReactantsSet(r_id)]))

        return precursor_set





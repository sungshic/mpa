__author__ = 'spark'

import rdflib

g = rdflib.Graph()
g.parse("data/biocyc/2.5/data/biopax-level3.owl")

result = g.query("select * where {?x ?y ?z}")
print result

results = g.query("""select ?x ?y2 ?z ?proteinName
where {?x a bp:Pathway. ?x bp:xref ?y. ?x bp:standardName ?z.
?y bp:id ?y2. ?x bp:pathwayComponent ?catalysis.
?catalysis a bp:Catalysis.
?catalysis bp:controller ?protein.
?protein bp:displayName ?proteinName.  FILTER (STR(?proteinName)="MetE"). }""")

print list(results)

results = g.query("""select ?pathways where {?pathways a bp:Pathway.}""")

# return all pathways in biocyc
pathways = g.query("""select ?pathwayid
where {?pathway a bp:Pathway. ?pathway bp:xref ?pathwayref. ?pathwayref bp:id ?pathwayid }""")

# return all proteins that are part of a pathway
results = g.query("""select ?pathwayid ?pathwayDesc ?proteinName ?reaction
WHERE {?pathway a bp:Pathway. ?pathway bp:xref ?pathwayref. ?pathway bp:standardName ?pathwayDesc. ?pathwayref bp:id ?pathwayid.
?pathway bp:pathwayComponent ?catalysis.
?catalysis a bp:Catalysis.
FILTER (STR(?pathwayid)="TRPSYN-PWY").
{?catalysis bp:controller ?protein. ?protein a bp:Protein.} UNION
{?catalysis bp:controller ?complex. ?complex a bp:Complex.
?complex bp:component ?protein. ?protein a bp:Protein.}
?protein bp:displayName ?proteinName.
?catalysis bp:controlled ?reaction.
}""")

results = g.query("""select ?pathwayid ?pathwayDesc ?proteinName ?reaction
WHERE {?pathway a bp:Pathway. ?pathway bp:xref ?pathwayref. ?pathway bp:standardName ?pathwayDesc. ?pathwayref bp:id ?pathwayid.
?pathway bp:pathwayComponent ?catalysis.
?catalysis a bp:Catalysis.
?catalysis bp:controlled ?reaction.
{ SELECT ?catalysis ?proteinName {?catalysis bp:controller ?protein. ?protein a bp:Protein.
?protein bp:displayName ?proteinName. }} UNION
{ SELECT ?catalysis ?proteinName {?catalysis bp:controller ?complex. ?complex a bp:Complex.
?complex bp:component ?protein. ?protein a bp:Protein. ?protein bp:displayName ?proteinName.
}}
FILTER (STR(?pathwayid)="TRPSYN-PWY").
}""")
#

results = g.query("""select ?proteinName where {
{?pathway a bp:Pathway. ?pathway bp:xref ?pathwayref. ?pathwayref bp:id ?pathwayid.
?pathway bp:pathwayComponent ?catalysis. ?catalysis a bp:Catalysis.
?catalysis bp:controller ?complex. ?complex a bp:Complex. ?complex bp:component ?protein. ?protein a bp:Protein. ?protein bp:displayName ?proteinName.
FILTER (STR(?pathwayid)="TRPSYN-PWY")} UNION
{?pathway a bp:Pathway. ?pathway bp:xref ?pathwayref. ?pathway bp:standardName ?pathwayDesc. ?pathwayref bp:id ?pathwayid.
?pathway bp:pathwayComponent ?catalysis. ?catalysis a bp:Catalysis.
?catalysis bp:controller ?protein. ?protein a bp:Protein.
?protein bp:displayName ?proteinName.
FILTER (STR(?pathwayid)="TRPSYN-PWY") }}""")
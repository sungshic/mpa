# -*- coding: utf-8 -*-
"""
Created on Wed Oct 03 17:08:37 2012

@author: Sungshic Park (b0928115@ncl.ac.uk)
"""

def setLBmedia(model, lb):
    exchangeFuncList = model.reactions.query(search_function='EX_')



    rxnList =   ['EX_cpd00028_e',# new metabolite added to LB
                 'EX_cpd00046_e',# new metabolite added to LB
                 'EX_cpd00084_e',# new metabolite added to LB
                 'EX_cpd00126_e',# new metabolite added to LB
                 'EX_cpd00215_e',# new metabolite added to LB
                 'EX_cpd00383_e',# new metabolite added to LB
                 'EX_cpd00541_e',# new metabolite added to LB
                 'EX_cpd00793_e',# new metabolite added to LB
                 'EX_cpd10515_e',# new metabolite added to LB
                # 'EX_cpd00219_e',# new metabolite added to LB
               #  'EX_cpd00216_e',
	       	 'EX_cpd00117_e', #D-Ala temporary nutrient for murE knock-out
                 'EX_cpd00182_e',
                 'EX_cpd00035_e',
                 'EX_cpd00051_e',
                 'EX_cpd01048_e',
                 'EX_cpd00041_e',
                 'EX_cpd00063_e',
                 'EX_cpd01012_e',
                 'EX_cpd11595_e',
                 'EX_cpd00381_e',
                 'EX_cpd00438_e',
                 'EX_cpd00654_e',
                 'EX_cpd10516_e',
                 'EX_cpd00393_e',
                 'EX_cpd00027_e',
                 'EX_cpd00023_e',
                 'EX_cpd00033_e',
                 'EX_cpd00067_e',
                 'EX_cpd00001_e',
                 'EX_cpd00531_e',
                 'EX_cpd00119_e',
                 'EX_cpd00226_e',
                 'EX_cpd00322_e',
                 'EX_cpd00246_e',
                 'EX_cpd00205_e',
                 'EX_cpd00107_e',
                 'EX_cpd00039_e',
                 'EX_cpd00060_e',
                 'EX_cpd00254_e',
                 'EX_cpd00971_e',
                 'EX_cpd00218_e',
                 'EX_cpd00066_e',#it is presented in the media
                 'EX_cpd00009_e',
                 'EX_cpd00644_e',
                 'EX_cpd00129_e',
                 'EX_cpd00220_e',
                 'EX_cpd00054_e',
                 'EX_cpd00048_e',
                 'EX_cpd00161_e',
                 'EX_cpd00184_e',
                 'EX_cpd00065_e',
                 'EX_cpd00069_e',
                 'EX_cpd00092_e',
                 'EX_cpd00249_e',
                 'EX_cpd00156_e',
                 'EX_cpd00034_e',
                 'EX_cpd00007_e',
                  #'EX_cpd00099_e',
                 'EX_cpd00058_e',
                 'EX_cpd00149_e',
                 'EX_cpd00030_e']

    for rxn in exchangeFuncList:
        rxn.lower_bound = 0
        rxn.upper_bound = 1000
        
    for rxnID in rxnList:
        rxn = model.reactions.get_by_id(rxnID)
        rxn.lower_bound = lb
        rxn.upper_bound = 1000
	print rxn.name + ', '
        
    return model
    

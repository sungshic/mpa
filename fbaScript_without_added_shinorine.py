# -*- coding: utf-8 -*-
"""
Created on Wed Oct 03 18:53:34 2012

@author: Sungshic Park (b0928115@ncl.ac.uk)
"""

import cobra
import re 
from cobra.io.sbml import *

#sbml_file = "data/gb-2009-10-6-r69-s4-mod.xml"
sbml_file = "data/iBsu1103V2/nar-01731-m-2012-File013.xml"

sb_model = create_cobra_model_from_sbml_file(sbml_file)

from setLBmediaV2 import *
from setShinorineReactions import *

#setShinorineReactions(sb_model)
setLBmedia(sb_model, -9)




#biomassRxnIdx = sb_model.reactions.index('bio00006')
#targetRxnIdx = sb_model.reactions.index('rxn05146')

biomassRxnIdx = sb_model.reactions.index('bio00127')
#targetRxnIdx = sb_model.reactions.index('rxnShinorine_Transport')
#substrateRxnIdx = sb_model.reactions.index('EX_cpd00027_e')
#targetRxnIdx = sb_model.reactions.index('rxn01333')

sb_model.reactions.bio00127.objective_coefficient = 1

sb_model_mat = sb_model.to_array_based_model()

sb_model_mat.optimize()
print sb_model_mat.solution
print 'biomass flux 1: ' + str(sb_model.solution.x[biomassRxnIdx])
#print 'target flux 1: ' + str(sb_model.solution.x[targetRxnIdx])

# dual objective
sb_model.reactions.bio00127.objective_coefficient = 1
#sb_model.reactions.rxnShinorine_Transport.objective_coefficient = 1
#sb_model.reactions.rxn01333.objective_coefficient = 1

sb_model_mat.update()

sb_model_mat.optimize()
print sb_model_mat.solution
print 'biomass flux 2: ' + str(sb_model.solution.x[biomassRxnIdx])
#print 'target flux 2: ' + str(sb_model.solution.x[targetRxnIdx])


# dual objective with minimum biomass requirement

sb_model.reactions.bio00127.lower_bound = 0
sb_model_mat.update()
sb_model_mat.optimize()
print sb_model_mat.solution
print 'biomass flux 3: ' + str(sb_model.solution.x[biomassRxnIdx])
#print 'target flux 3: ' + str(sb_model.solution.x[targetRxnIdx])

#print 'knock-out analysis'

#sb_model_knockout = sb_model.copy()
#sb_model_ko_mat = sb_model_knockout.to_array_based_model()

'''
#setLBmedia(sb_model_knockout, -1*uptakeRate)
for uptakeRate in range(10, 15):
	print sb_model_ko_mat.reactions[substrateRxnIdx].name + ' is ' + str(uptakeRate)
	sb_model_ko_mat.reactions[substrateRxnIdx].lower_bound = -1*uptakeRate

	sb_model_ko_mat.update()
	sb_model_ko_mat.optimize()
	if sb_model_ko_mat.solution != None:
		print sb_model_ko_mat.solution
		print 'biomass flux: ' + str(sb_model_ko_mat.solution.x[biomassRxnIdx])
		print 'target flux: ' + str(sb_model_ko_mat.solution.x[targetRxnIdx])
	else:
		print 'fatal knock-out'
	

for rxnidx, rxn in enumerate(sb_model_knockout.reactions):
	m = re.search('(rxn|EX\_cpd)\w+', rxn.id)
	if m and rxn.id not in ['rxn05146', 'bio00006']:
		print '## ' + rxn.id
		#print sb_model_knockout.reactions[rxnidx].id
	
		rxn.lower_bound = 0
		rxn.upper_bound = 0
		sb_model_ko_mat.update()
		sb_model_ko_mat.optimize()
		if sb_model_ko_mat.solution != None:
			print sb_model_ko_mat.solution
			print 'biomass flux: ' + str(sb_model_ko_mat.solution.x[biomassRxnIdx])
			print 'target flux: ' + str(sb_model_ko_mat.solution.x[targetRxnIdx])
		else:
			print 'fatal knock-out'


		# restore
		rxn.lower_bound = sb_model.reactions[rxnidx].lower_bound
		rxn.upper_bound = sb_model.reactions[rxnidx].upper_bound



'''


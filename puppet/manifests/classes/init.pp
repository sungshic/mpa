#include apt

class init {

        group { "puppet":
                ensure => "present",
        }

        exec { "install hiera dependencies":
                command => "sudo gem install hiera hiera-puppet",
        }

	# add docker repo
	exec { "add docker repo for installing docker v1.2 on ubuntu 14.04":
		command => "sudo echo deb https://get.docker.io/ubuntu docker main > /etc/apt/sources.list.d/docker.list",
	}

	# add docker gpg key
	exec { "add the gpg key for docker v1.2":
		command => "sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 36A1D7869245C8950F966E92D8576A8BA88D21E9",
	}

        # update the apt-get repositories
        exec { "update-apt":
                command => "sudo apt-get update",
        }

        # install apt-get module dependencies
        package {
                ["lxc-docker", "rabbitmq-server", "openvpn", "easy-rsa", "python-pip", "python-dev"]:
                ensure => installed,
                install_options => ['--force-yes'],
                require => Exec['update-apt'], # the system update needs to run first
        }

        # install "pip install" dependencies
        package {
		["celery", "cmd2"]:
                provider => pip,
                ensure => installed,
                require => [Package['python-pip', 'python-dev']],
        }

}


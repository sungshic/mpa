__author__ = 'spark'
import numpy
import cobra
from addLoopLawConstraints import addLoopLawConstraints

def format_lp_problem(cobra_model, add_looplaw=False):
    class Bunch:
        def __init__(self, **kwds):
            self.__dict__.update(kwds)

    lp_problem = Bunch()

    if not hasattr(cobra_model, 'S'):
       cobra_model.to_array_based_model()

    [nMets, nRxns] = cobra_model.S.shape

    lp_problem.osense = -1 # maximize
    lp_problem.csense = numpy.chararray((nMets,1))
    lp_problem.csense[:] = 'E'

    if hasattr(cobra_model, 'b'):
        lp_problem.b = numpy.array([cobra_model.b]).T
    else:
        lp_problem.b = numpy.zeros((cobra_model.shape[0],1))

    # rest of the LP problem setup
    lp_problem.A = cobra_model.S
    lp_problem.c = numpy.array([cobra_model.objective_coefficients]).T
    lp_problem.lb = numpy.array([cobra_model.lower_bounds]).T
    lp_problem.ub = numpy.array([cobra_model.upper_bounds]).T
    lp_problem.colnames = numpy.array([[rxn.id for rxn in cobra_model.reactions]]).T
    lp_problem.rownames = numpy.array([[met.id for met in cobra_model.metabolites]]).T

    if add_looplaw:
        milp_problem = addLoopLawConstraints(lp_problem, cobra_model, range(0,nRxns))
        return milp_problem
    else:
        return lp_problem


def format_cobra_solution_from_CPLEXresult(cplexsol, pool_idx, rxn_list):
    if cplexsol.solution.pool.get_num() > pool_idx:
        lp_x = cplexsol.solution.pool.get_values(pool_idx)
        f_val = cplexsol.solution.pool.get_objective_value(pool_idx)
        cobra_sol = cobra.core.Solution(f_val)
        cobra_sol.status = 'optimal'

        cobra_sol.x = lp_x
        cobra_sol.x_dict = {}
        for idx, rxn in enumerate(rxn_list):
            cobra_sol.x_dict[rxn.id] = lp_x[idx]
    else:
        cobra_sol = cobra.core.Solution(0)

    return cobra_sol


__author__ = 'spark'

# set virtual env
#activate_env_file = '/Users/spark/pyvirtualenv/pycobra/bin/activate_this.py'
#execfile(activate_env_file, dict(__file__=activate_env_file))

sys.setdlopenflags(DLFCN.RTLD_NOW | DLFCN.RTLD_GLOBAL)

from cobra.io.sbml import create_cobra_model_from_sbml_file
from cobra.flux_analysis import *
#import cobra.solvers.gurobi_solver as gs

#from setLBmedia import *
from mpa_tool.setMedia import *

from pylab import *  # for plotting
from numpy.random import *  # for random sampling

seed(42)

# We need to import the graph_tool module itself
from graph_tool.all import *

from metpathdictionary import MetPathDict, GeneDict

from mpa_tool.mpa_model_manager import MPAModelManager


sbml_file = "data/iBsu1103V2/nar-01731-m-2012-File013.xml"
is_tgt_prod_enforced = True
is_shiki_on = True # shiki off
shiki_on = True # shiki on

#is_# initialize metabolic cmp and rxn lookup dictionary
# NOTE: a lil' ugly marriage to a single genome scale model, but works for the croda project in the mean time.
met_path_dict_cmplist = 'data/cpdlist2.csv'
met_path_dict_rxnlist_v1 = 'data/rxnlist2.csv'
met_path_dict_rxnlist_v2extra = 'data/rxnlist_nar.csv'
metdict = MetPathDict(met_path_dict_cmplist, met_path_dict_rxnlist_v1)
metdict2 = MetPathDict(met_path_dict_cmplist, met_path_dict_rxnlist_v2extra)
metdict.mergeRxnLookupTable(metdict2._rxnDict) # merge extra rxns from v2 to v1



graph_file = "graphvis/bsu_met_network_biomass_1_shikion.xml.gz"
graph_lookup_file = "graphvis/bsu_met_network_biomass_1_shikion_vid_vidx.json"

is_shiki_on = False
mediaIdx = 21

sb_model = create_cobra_model_from_sbml_file(sbml_file)

#g = load_graph(graph_file)
#jsonf = open(graph_lookup_file)
#vidx_lookup = json.loads(jsonf.read())

#model_profiler = MPAModelProfiler(sb_model)


setMedia(sb_model, -10, mediaIdx, is_calibrated=0)
sb_model.reactions.bio00127.objective_coefficient = 1
ll_model = sb_model.copy() #ArrayBasedModel(sb_model, deepcopy_model=True)
ll_model_arr = ll_model.to_array_based_model()


genedict_file = 'data/iBsu1103/gb-2009-10-6-r69-s1_genedata.csv'
genedict = GeneDict(genedict_file, sb_model.reactions)

ll_model_arr = ll_model.to_array_based_model() # update the S matrix with the newly populated 'reverse' reactions
model_manager = MPAModelManager(ll_model_arr, genedict, len(sb_model.reactions))
model_manager.convert_to_irreversible()
model_manager._cb_model.update()
# recalculate rxn sets
model_manager.findRxnSet()
model_manager.findMetSet()
model_manager.findBoundaryRxnSet()
model_manager.findTransportRxnSet()
sb_model = ll_model.guided_copy()

cbsol_set = set()

#sb_model_small = sb_model.copy()


print 'adding ll constraints to cobra model'
ll_model_arr, [int_rxns, constraint_rxns, constraint_mets] = model_manager.addLoopLawConstraintsCOBRA()
print 'cobra model updated'

cur_sol = None
cur_f = 0.0
old_sol = None
old_f = 0.0

ref_layout = None
pdflist = []
mpa_vis_ref = None

model_manager.setObjective(1,1) # biomass maximization
model_manager.setObjective(2,-1, False) # atp maintenance minimization as a second obj

filename_base = 'cplex_opt_result_bio_atpm'
print 'optimizing cobra model using cplex...'
old_sol = cur_sol
#if hasattr(ll_model, 'solution') and ll_model.solution.status == 'optimal': #ll_model.solution:
#    old_f = ll_model.solution.f
cbsol1 = model_manager._cb_model.optimize(solver='cplex', the_problem=cur_sol, reuse_basis=True, print_solver_time=True, CPX_PARAM_POPULATELIM=1000, CPX_PARAM_SOLNPOOLAGAP=0.5, CPX_PARAM_SOLNPOOLINTENSITY=4, CPX_PARAM_SOLNPOOLREPLACE=2)
print 'optimization done.'

cbsol1.solution.pool.write(filename_base)
cur_sol = cbsol1


#mpa_sol = MPASolutionManager(cbsol1)

#model_profiler = MPAModelProfiler(sb_model, genedict, var_idx_limit=0)
#model_profiler.getPathwayStatsBySolution(mpa_sol, 0)

sol1 = cbsol1.solution.pool.get_values(0) #[:1707]
biomass_sol = sol1[model_manager._cb_model.reactions.index(model_manager._cb_model.reactions.get_by_id('bio00127'))]
atpm_sol = sol1[model_manager._cb_model.reactions.index(model_manager._cb_model.reactions.get_by_id('rxn00062'))]
print 'biomass: ' + str(biomass_sol)
print 'atpm: ' + str(atpm_sol)

model_manager.setObjective(1,1) # biomass maximization

filename_base = 'cplex_opt_result_bio_only'
print 'optimizing cobra model using cplex...'
cur_sol = None
#if hasattr(ll_model, 'solution') and ll_model.solution.status == 'optimal': #ll_model.solution:
#    old_f = ll_model.solution.f
cbsol1 = model_manager._cb_model.optimize(solver='cplex', the_problem=cur_sol, reuse_basis=True, print_solver_time=True, CPX_PARAM_POPULATELIM=1000, CPX_PARAM_SOLNPOOLAGAP=0.5, CPX_PARAM_SOLNPOOLINTENSITY=4, CPX_PARAM_SOLNPOOLREPLACE=2)
print 'optimization done.'

cbsol1.solution.pool.write(filename_base)
cur_sol = cbsol1


#mpa_sol = MPASolutionManager(cbsol1)

#model_profiler = MPAModelProfiler(sb_model, genedict, var_idx_limit=0)
#model_profiler.getPathwayStatsBySolution(mpa_sol, 0)

sol1 = cbsol1.solution.pool.get_values(0) #[:1707]
biomass_sol = sol1[model_manager._cb_model.reactions.index(model_manager._cb_model.reactions.get_by_id('bio00127'))]
atpm_sol = sol1[model_manager._cb_model.reactions.index(model_manager._cb_model.reactions.get_by_id('rxn00062'))]

print 'biomass: ' + str(biomass_sol)
print 'atpm: ' + str(atpm_sol)

print 'hello'



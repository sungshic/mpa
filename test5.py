__author__ = 'spark'

from cobra.io.sbml import *
from cobra.flux_analysis import *

#from setLBmedia import *
from mpa_tool.setMedia import *
from setShinorineReactions import *

from pylab import *  # for plotting
from numpy.random import *  # for random sampling

seed(42)

# We need to import the graph_tool module itself
from graph_tool.all import *

from addLoopLawConstraints import addLoopLawConstraintsCOBRA

from metpathdictionary import MetPathDict
from graphvis.mpavisualizer import MPAVisualizer
from graphvis.pdftemplate import CrodaMPAReportTemplate

sbml_file = "data/iBsu1103V2/nar-01731-m-2012-File013.xml"
is_tgt_prod_enforced = True
is_shiki_on = True # shiki off
shiki_on = True # shiki on

#is_# initialize metabolic cmp and rxn lookup dictionary
# NOTE: a lil' ugly marriage to a single genome scale model, but works for the croda project in the mean time.
met_path_dict_cmplist = 'data/cpdlist2.csv'
met_path_dict_rxnlist_v1 = 'data/rxnlist2.csv'
met_path_dict_rxnlist_v2extra = 'data/rxnlist_nar.csv'
metdict = MetPathDict(met_path_dict_cmplist, met_path_dict_rxnlist_v1)
metdict2 = MetPathDict(met_path_dict_cmplist, met_path_dict_rxnlist_v2extra)
metdict.mergeRxnLookupTable(metdict2._rxnDict) # merge extra rxns from v2 to v1


for mediaIdx in [1]: #[15, 17, 18, 19, 20, 21, 22]: #[0, 1, 6, 10, 11, 12, 13, 15, 17, 18, 19, 20, 21, 22]: #14 infeasible #range(16,17):
    print 'processing media idx: ' + str(mediaIdx)
    sb_model = create_cobra_model_from_sbml_file(sbml_file)

    setShinorineReactions(sb_model, is_shiki_on)
    #setLBmedia(sb_model, -10)
    setMedia(sb_model, -10, mediaIdx, is_calibrated=2)

    biomassRxnIdx = sb_model.reactions.index('bio00127')
    #srcRxnIdx = sb_model.reactions.index('EX_cpd00027_e') #D-Glu uptake
    srcRxnIdx = sb_model.reactions.index(getMediaSrcRxn(mediaIdx)) #'EX_cpd00027_e') #D-Glu uptake
    tgtRxnIdx = sb_model.reactions.index('rxnS00150') #Shinorine secretion
    #tgtRxnIdx = sb_model.reactions.index('rxn01343') #sh7p
    srcCompoundIdx = sb_model.metabolites.index(getMediaSrcCmp(mediaIdx)) #'cpd00027_e')
    #tgtCompoundIdx = sb_model.metabolites.index('cpd00238_c')
    tgtCompoundIdx = sb_model.metabolites.index('cpdS00140_e') #'cpdADDED_Shinorine_e') shinorine
    #tgtCompoundIdx = sb_model.metabolites.index('cpd00238_c') #cytosolic sedoheptulose-7-phosphate
    #tgtCompoundIdx = sb_model.metabolites.index('cpdADDED_DDG_c') #cytosolic 2-demethyl-4-deoxygadusol
    #tgtCompoundIdx = sb_model.metabolites.index('cpd00699_c') #cytosolic 2-demethyl-4-deoxygadusol

    # setting a dual objective
    sb_model.reactions.bio00127.objective_coefficient = 1
    #sb_model.reactions.rxnS00150.objective_coefficient = 1
    #sb_model.reactions.bio00127.lower_bound = 0.5
    #sb_model.genes.

    #sb_model_mat = sb_model.to_array_based_model()
    #sb_model_mat.update()


    #sb_model.reactions[tgtRxnIdx].lower_bound = 0.2 #0.4983 #fva_sol['rxnS00150']['maximum']

    #lp_problem = format_lp_problem(sb_model, True)
    #lp_sol = solve_gurobi(lp_problem)
    #cobra_sol = format_cobra_solution_from_GUROBIresult(lp_sol)
    #
    #sb_model.solution = cobra_sol
    #mpa_wt_biomass_mintgt = MPAVisualizer()
    #mpa_wt_biomass_mintgt.initializeGraphDataFromCOBRAModel(sb_model, metdict, getMediaSrcCmp(mediaIdx), getMediaSrcRxn(mediaIdx))
    #pdffilename3, layout_coords3 = mpa_wt_biomass_mintgt.drawGraphvlist("graphvis/results/compare_mpa_wt_biomass_mintgt_media" + str(mediaIdx))

    #print 'formatting gurobi lp_struct...'
    #lp_problem = format_lp_problem(sb_model, True)
    #print 'formatting done.'

    ll_model = sb_model.copy() #ArrayBasedModel(sb_model, deepcopy_model=True)
    ll_model_arr = ll_model.to_array_based_model()

    print 'adding ll constraints to cobra model'
    ll_model_arr, [int_rxns, constraint_rxns, constraint_mets] = addLoopLawConstraintsCOBRA(ll_model_arr)
    print 'cobra model updated'
    print 'optimizing cobra model using cplex...'
    cbsol1 = ll_model.optimize(solver='cplex', the_problem=None, error_reporting=True)
    #gs.solve(ll_model, the_problem=None, error_reporting=True)
    #ll_model_gurobi =
    print 'optimization done.'

    print ll_model.solution.status

    # flux variability analysis to find out minimum tgt flux to solve for
    fva_rxn_list = [ll_model.reactions[tgtRxnIdx]]
    print 'running fva for the cobra model using cplex...'
    ll_model_fva = ll_model.copy()
    fva_sol = cobra.flux_analysis.flux_variability_analysis(ll_model_fva, fraction_of_optimum=0.8, the_reactions=fva_rxn_list, solver='cplex', copy_model=True, lp_method=0)


    true_rxn_num = len(sb_model.reactions)
    true_rxn_list = ll_model.reactions[0:true_rxn_num]

    true_rxn_dict = {}
    for rxn in true_rxn_list:
        true_rxn_dict[rxn.id] = ll_model.solution.x_dict[rxn.id]

    sb_model.solution = Solution(ll_model.solution.f, ll_model.solution.x[0:true_rxn_num], true_rxn_dict, status=ll_model.solution.status)

    #lp_problem = format_lp_problem(sb_model, True)
    #lp_sol = solve_gurobi(lp_problem)
    #cobra_sol = format_cobra_solution_from_GUROBIresult(lp_sol)

    #sb_model.solution = cobra_sol
    #sb_model_mat.optimize()

    print ll_model.solution

    #wt_biomass_only = MPALabelledGraph() #"graphvis/results")
    mpa_wt_biomass_only = MPAVisualizer()
    print 'initializing graph from cobra model...'
    wt_biomass_only_graph = mpa_wt_biomass_only.initializeGraphDataFromCOBRAModel(sb_model.to_array_based_model(), metdict, getMediaSrcCmp(mediaIdx), getMediaSrcRxn(mediaIdx))
    print 'drawing the graph...'
    pdffilename1, layout_coords1 = mpa_wt_biomass_only.drawGraphvlist("graphvis/results/mpa_wt_biomass_only_media" + str(mediaIdx), is_save_layout=True)
    wt_biomass_only_graph.saveLabelledGraph("graphvis/results/graph_mpa_wt_biomass_only_media" + str(mediaIdx))

    ll_model.reactions[tgtRxnIdx].lower_bound = fva_sol['rxnS00150']['maximum']
    print 'max flux for rxnS00150: ' + str(fva_sol['rxnS00150']['maximum'])
    print 'optimizing the cobra model with the min imposed for rxnS00150'
    cbsol_tgt1 = ll_model.optimize(solver='gurobi', the_problem=None)
    print ll_model.solution.status

    true_rxn_num = len(sb_model.reactions)
    true_rxn_list = ll_model.reactions[0:true_rxn_num]

    true_rxn_dict = {}
    for rxn in true_rxn_list:
        true_rxn_dict[rxn.id] = ll_model.solution.x_dict[rxn.id]

    sb_model.solution = Solution(ll_model.solution.f, ll_model.solution.x[0:true_rxn_num], true_rxn_dict, status=ll_model.solution.status)


    mpa_wt_biomass_mintgt = MPAVisualizer()
    print 'initializing graph from cobra model...'
    mintgt_graph = mpa_wt_biomass_mintgt.initializeGraphDataFromCOBRAModel(sb_model.to_array_based_model(), metdict, getMediaSrcCmp(mediaIdx), getMediaSrcRxn(mediaIdx))
    print 'drawing the graph...'
    pdffilename2, layout_coords2 = mpa_wt_biomass_mintgt.drawGraphvlist("graphvis/results/mpa_wt_biomass_mintgt_media" + str(mediaIdx))
    mintgt_graph.saveLabelledGraph("graphvis/results/graph_mpa_wt_biomass_mintgt_media" + str(mediaIdx))

    print 'drawing the graph...'
    if mpa_wt_biomass_only._graph.num_vertices() > mpa_wt_biomass_mintgt._graph.num_vertices():
        pdffilename3, layout_coords3 = mpa_wt_biomass_mintgt.drawGraphvlistByLayout("graphvis/results/mpa_wt_biomass_mintgt_media" + str(mediaIdx), layout_coords1)
    else:
        pdffilename3, layout_coords3 = mpa_wt_biomass_only.drawGraphvlistByLayout("graphvis/results/mpa_wt_biomass_only_media" + str(mediaIdx), layout_coords2)

    graph_pdf_files = [pdffilename1, pdffilename2, pdffilename3]
    croda_mpa_report = CrodaMPAReportTemplate("test.pdf")
    #fig_file = "stat_data/2014_02_06_wildtypes_oxy1.5.png"
    #fig_file = "graphvis/wt_oxy1.5_dual_shikioff/bsu_met_network_bio+tgt_shikioff_0_1785541245594011467.pdf"
    page_text = "Experimental conditions (all under aerobic growth): <br/>\
        #0: Glucose + Ammonium sulfate \
        <font color='red'>#1: Malate + Ammonium sulfate </font>\
        #6: Glucose + Malate + Ammonium sulfate \
        #10: Glucose + L-Glutamate + L-Glutamine + Ammonium sulfate \
        #11: Malate + L-Glutamate + L-Glutamine + Ammonium sulfate \
        #12: Glucose + L-Glutamate + Ammonium sulfate \
        #13: Glucose + L-Glutamine + Ammonium sulfate \
        #14: Malate + L-Glutamate + Ammonium sulfate \
        #15: Malate + L-Glutamine + Ammonium sulfate \
        #17: Glucose + L-arginine + ammonium sulfate \
        #18: Malate + L-arginine + ammonium sulfate \
        #19: Malate + L-arginine + L-glutamine + ammonium sulfate \
        #20: Malate + L-arginine + L-glutamate + ammonium sulfate \
        #21: Glucose + L-arginine + L-glutamine + ammonium sulfate \
        #22: Glucose + L-arginine + L-glutamate + ammonium sulfate"
    page_heading = "Experimental conditions"
    fig_heading = "Fluxes of shinorine vs biomass across experimental conditions"
    fig_caption = ""

    croda_mpa_report.addATitlePage("comparing the experimental conditions #0 shiki-on/off")
    #croda_mpa_report.addAFigPage(page_heading, page_text, fig_heading, fig_file, fig_caption)
    croda_mpa_report.addAPage(page_heading, page_text)
    croda_mpa_report.addAContentPage("Shiki-on biomass only", "test page 2",graph_pdf_files[0]) # page 2
    croda_mpa_report.addAContentPage("Shiki-on biomass+shinorine", "test page 3", graph_pdf_files[1]) # page 3
    croda_mpa_report.addAContentPage("Shiki-on overlap", "test page 4", graph_pdf_files[2]) # page 4
    croda_mpa_report.createATemplatePDF()
    croda_mpa_report.saveMPAGraphs("graphvis/results/test5_malate.pdf")



__author__ = 'Jurijs Meitalovs'

import cobra
import re
from cobra.io.sbml import *

def singleGeneDeletion(sb_model):

    print '#############################'


    from cobra.flux_analysis import single_deletion
    from cPickle import load
    from time import time

    sb_model_mat = sb_model.to_array_based_model()
    sb_model_mat.optimize()
    solution_of_original_model = sb_model_mat.solution

    biomassRxnIdx = sb_model.reactions.index('bio00127')
    targetRxnIdx = sb_model.reactions.index('rxn_S00150')

    start_time = time()  # start timer

    # Perform deletions for all genes in the list
    rates, statuses, problems = single_deletion(sb_model, element_list=None,
                        method='fba', the_problem='return',
                        element_type='gene', solver='glpk', error_reporting=None)

    total_time = time() - start_time  # stop timer

    for gene_locus, rate in rates.items():
        # get gene name from gene locus (i.e. STM4081 -> tpiA)
        name = sb_model.genes.get_by_id(gene_locus).name
        # test if the simulation not failed
        if statuses[gene_locus] == "optimal":
            if rate > solution_of_original_model.f:
                if rate - solution_of_original_model.f > 1:
                    print 'gene: ' + name + ' biomass flux: ' + str(rate) + ' change: ' + str(rate - solution_of_original_model.f)


    #print  (name, rate, expected_growth_rates[gene_locus])
    print 'single deletion time: %f seconds' % (total_time)

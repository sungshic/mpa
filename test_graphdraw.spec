# -*- mode: python -*-
a = Analysis(['test_graphdraw.py'],
             pathex=['/vagrant/Documents/workspace/CrodaMPA'],
             hiddenimports=[],
             hookspath=hooks,
             runtime_hooks=None)
pyz = PYZ(a.pure)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='test_graphdraw',
          debug=False,
          strip=None,
          upx=True,
          console=True )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=None,
               upx=True,
               name='test_graphdraw')

__author__ = 'spark'

# had to change graph_tool.draw.cairo_draw.py to make this script work with setting vertex color with alpha values
# _convert function in cairo_draw.py edited

# We probably will need some things from several places
#import cobra

import json
import pickle
import math
from cobra.io.sbml import *

import matplotlib

from pylab import *  # for plotting
from numpy.random import *  # for random sampling

from numpy import sqrt

from operator import itemgetter

#from itertools import islice
from itertools import tee, izip
#from metpathdictionary import MetPathDict

# We need to import the graph_tool module itself
from graph_tool.all import *
from .mpagraphreconstructor import MPALabelledGraph


class MPAVisualizer():
    _default_hub_met_list = {'cpd00001_c', 'cpd00001_e', 'cpd00002_c', 'cpd00003_c', 'cpd00004_c',
                        'cpd00005_c', 'cpd00006_c', 'cpd00007_c', 'cpd00007_e', 'cpd00008_c',
                        'cpd00009_c', 'cpd00009_e', 'cpd00010_c', 'cpd00011_c', 'cpd00011_e',
                        'cpd00012_c', 'cpd00012_e', 'cpd00067_c', 'cpd00067_e', 'cpd00971_e',
                        'cpd00971_c'}

    _default_fixed_met_list = {'cpd00699_c', 'cpd00020_c', 'cpd00051_e', 'cpdS00130_c', 'cpdS00110_c'}
    _default_otherpaths_list = [
                        ['cpd00130_e', 'cpdS00140_c'], # L-malate, Shinorine
                        ['cpd00130_e', 'cpd11416_c'], # L-malate, Biomass
                        ['cpd00027_e', 'cpd11416_c'], # D-glucose, Biomass
                        ['cpd00022_c','cpd00029_c'], # Acetyl-CoA, Acetate
                        ['cpd00020_c','cpd00022_c'], # Pyruvate, Acetyl-CoA
                        ['cpd00023_c','cpd00024_c'], # L-glutamate, 2-Oxoglutarate
                        ['cpd00053_c','cpd00024_c'], # L-glutamine, 2-Oxoglutarate
                        ['cpd00079_c','cpd00238_c'], # Glucose-6-phosphate, Sedoheptulose-7-phosphat
                        ['cpd00238_c','cpdS00110_c'], # Sedoheptulose-7-phosphate, 4-deoxygadusol
                        ['cpd00061_c','cpd00238_c'], # Phosphoenolpyruvate, sedoheptulose-7-p
                        ['cpd00061_c','cpd00699_c'], # Phosphoenolpyruvate, 3-dehydroquinate
                        ['cpd00079_c','cpd00699_c'], # Glucose-6-phosphate, 3-dehydroquinate
                        #['cpd00699_c','cpd00216_c'], # 3-dehydroquinatee, chorismate
                        ['cpd00020_c','cpd00699_c'], # Pyruvate, 3-dehydroquinate
                        ['cpd00238_c','cpdS00150_c'] # Sedoheptulose-7-phosphate, 2-epi-5-epi-valiolone
                      ]
    _default_critical_node_list = {'cpd00699_c'} # 3-dehydroquinate

    def __init__(self, hub_met_list = _default_hub_met_list):
        seed(42)
        # list of hub metabolites in high demand
        # For universal applications, a better refid system is essential for metabolites: possibly adopt CHEBI compound IDs.
        self._hub_met_list = hub_met_list

    def initializeGraphDataFromFile(self, graph_file, graph_lookup_file, met_dict, layout_pos_file, is_save_layoutfile):
        # load the graph data
        g = load_graph(graph_file)

        # reconstruct the vertex index lookup table
        jsonf = open(graph_lookup_file)
        vidx_lookup = json.loads(jsonf.read())

        self._layout_pos_file = layout_pos_file #= 'layoutcoord.json'
        self._is_save_layoutfile = is_save_layoutfile
        self._initializeGraphData(g, vidx_lookup, met_dict)

    def initializeGraphDataFromCOBRAModel(self, model, met_dict, srcCmpId, srcRxnId, tgtCmpId='cpdS00140_e', tgtRxnId='rxnS00150'):
        mpa_graph = MPALabelledGraph()
        graphed_model = mpa_graph.constructLabelledGraphFromCbModel(model, srcCmpId, srcRxnId, tgtCmpId, tgtRxnId)
        if graphed_model:
            self._initializeGraphData(mpa_graph._graph, mpa_graph._v_idx_lookup, met_dict)
        else:
            raise Exception("no solution", "no solution")

        return mpa_graph

    def initializeGraphDataFromCPLEXSolval(self, cplex_sol_vals, met_dict, srcCmpId, srcRxnId, tgtCmpId='cpdS00140_e', tgtRxnId='rxnS00150'):
        mpa_graph = None

    #graph_file = "bsu_met_network_biomass_1_shikioff.xml.gz"
    #graph_lookup_file = "bsu_met_network_biomass_1_shikioff_vid_vidx.json"
    #filenamebase = "bsu_met_network_biomass_ex1_shikioff"
    #is_save_layoutfile = True
    def _initializeGraphData(self, graph, vidx_lookup, met_dict):
        # link the graph data
        self._graph = graph

        # link the vertex index lookup table
        self._vidx_lookup = vidx_lookup

        print 'graph data reconstruction done'

        self._metdict = met_dict

        #cpd00020_c: pyruvate
        #cpd00051_e: L-arginine
        self._fixed_met_list = self._default_fixed_met_list
        self._otherpaths_list = self._default_otherpaths_list
        self._critical_node_list = self._default_critical_node_list

        self._graph_ep_flux = self._graph.ep['flux']
        srcv_idx = 0 #self._vidx_lookup['cpd00079_c']
        tgtv_idx = 1 #self._vidx_lookup['cpdS00140_c']
        #srcv, tgtv = g.vertex(0), g.vertex(1)
        srcv, tgtv = self._graph.vertex(srcv_idx), self._graph.vertex(tgtv_idx)
        print 'srcv: ' + self._graph.vp['v_id'][srcv]
        print 'tgtv: ' + self._graph.vp['v_id'][tgtv]
        self._graph_ep_cap = self._graph.ep['cap']

        #res = edmonds_karp_max_flow(g, srcv, tgtv, cap)
        #res.a = cap.a - res.a
        #max_flow_val = sum(res[e] for e in tgtv.in_edges())
        #print "max flow val: " + str(max_flow_val)

        outdeg = self._graph.degree_property_map("out")
        weight2 = self._graph_ep_cap.copy()
        #weight1.a = (weight1.a*1.0/weight1.a.max())**2.0
        weight2.a = weight2.a/weight2.a.max()
        #(eflux.a/(eflux.a.max()/1.0))**2.0
        #weight.a = 1/(1+abs(weight.a))
        weight2.a = 1/(1+weight2.a)
        #weight = prop_to_size(weight, mi=0, ma=weight.a.max(), power=1),

        weight1 = outdeg
        weight1.a = weight1.a/weight1.a.max()

        #weight1.a = weight1.a * 10 + weight2.a

        self._graph_ep_weight = self._graph.new_edge_property("float")
        self._graph_ep_edgewidth = self._graph.new_edge_property("float")

        #weight.a = (10.0 * weight1.a) + weight2.a
        #for e in g.edges():
        #    vt = e.target()
        #    weight[e] = weight1[vt] * 10 + weight2[e]

        for e in self._graph.edges():
            vt = e.target()
        #    vt_out = vt.out_edges()
            if self._graph.vp['v_id'][vt] in self._hub_met_list:
                hubfactor = 1.0
            else:
                hubfactor = 0.0

            #if g.vp['v_id'][vt] not in fixed_met_list:
            #    fixfactor = 1.0
            #else:
            #    fixfactor = 0.0

            #if g.vp['v_id'][vt] in fixed_met_list:
            #    fixedfactor = 0.0
            #else:
            #    fixedfactor = 1.0

            self._graph_ep_weight[e] = hubfactor * 1000 + weight1[vt] * 10 + weight2[e] #+ 1000 * fixfactor)*fixfactor
            self._graph_ep_edgewidth[e] = math.log(abs(self._graph_ep_flux[e])+5.0)#weight1[vt] * 10 + weight2[e]

            #weight[e] = fixedfactor * 10 + hubfactor * 100 + weight1[vt] * 10 + weight2[e]

        self._graph_vp_touch_v = self._graph.new_vertex_property("bool")
        self._graph_ep_touch_e = self._graph.new_edge_property("bool")

        #vcolor = g.new_vertex_property("string")
        self._graph_vp_vcolor = self._graph.new_vertex_property("string") # arbitrary object list #g.new_vertex_property("vector<float>")
        #vcolor.a = "#ffffff" #initialize
        self._graph_vp_vtext = self._graph.new_vertex_property("string")
        self._graph_vp_vsize = self._graph.new_vertex_property("float")
        self._graph_vp_vsize.a = 1
        self._graph.vp['vtext'] = self._graph_vp_vtext
        self._graph.vp['vcolor'] = self._graph_vp_vcolor
        self._graph.vp['vsize'] = self._graph_vp_vsize

        target = self._graph.vertex(1)

        #layout_pos = sfdp_layout(g, C=1000, p=10)
        #layout_pos = arf_layout(g, max_iter=1000)
        #layout_pos = fruchterman_reingold_layout(g, n_iter=1000)
        shape = [[1,6000], [1,6000], 4]
        layout_pos = random_layout(self._graph, shape=shape, dim=3)
        self._graph_ep_control = self._graph.new_edge_property("vector<double>")

        for e in self._graph.edges():
            d = sqrt(sum((layout_pos[e.source()].a - layout_pos[e.target()].a) ** 2)) / 5
            self._graph_ep_control[e] = [0.3, d, 0.7, d]

        #dist, pred = astar_search(g, g.vertex(0), weight,
        #                          VisitorExample(touch_v, touch_e, target),
        #                          heuristic=lambda v: h(v, target, layout_pos))

        #time = g.new_vertex_property("int")
        #dist, pred = dijkstra_search(g, g.vertex(0), weight,
        #                             VisitorExample(vtext, time))


        self._graph_ep_efluxstr = self._graph.new_edge_property("string")
        self._graph_ep_ecolor = self._graph.new_edge_property("string")
        self._graph_ep_ewidth = self._graph.new_edge_property("double")
        self._graph_ep_ewidth.a = self._graph_ep_edgewidth.a

        self._graph.ep['ecolor'] = self._graph_ep_ecolor
        self._graph.ep['ewidth'] = self._graph_ep_ewidth
        self._graph.ep['fluxstr'] = self._graph_ep_efluxstr

        #for v in g.vertices():
        #    vcolor[v] = "#000000, 1.0" if touch_v[v] else "#595959, 0.1"
            #vcolor[v] = "black" if touch_v[v] else "white"


        #for e in g.edges():
        #    ecolor[e] = "#0000ff, 0.1" if touch_e[e] else "#595959, 0.1"
            #ecolor[e] = "blue" if touch_e[e] else "black"


        kshortest = self.ksp_yen(self._graph, srcv, tgtv, weights=self._graph_ep_weight, max_k=6)
        print 'kshortest: ' + str(kshortest)

        vlists = []
        vlist1 = kshortest[0]['path']
        if vlist1:
            vlists.append(vlist1)
        for metid in self._fixed_met_list:
            cursrcv = self._graph.vertex(self._vidx_lookup[metid])
            kshortest = self.ksp_yen(self._graph, cursrcv, tgtv, weights=self._graph_ep_weight, max_k=1)
            vlist_ext = kshortest[0]['path']

            if vlist_ext:
                # append vlist_ext to vlists
                vlists.append(vlist_ext)
                # merge vlist_ext without duplicates into vlist
                #vlist = vlist + list(set(vlist_ext) - set(vlist))

        for metids in self._otherpaths_list:
            cursrcv = self._graph.vertex(self._vidx_lookup[metids[0]])
            curtgtv = self._graph.vertex(self._vidx_lookup[metids[1]])
            kshortest = self.ksp_yen(self._graph, cursrcv, curtgtv, weights=self._graph_ep_weight, max_k=1)
            vlist_ext = kshortest[0]['path']

            if vlist_ext:
                # append vlist_ext to vlists
                vlists.append(vlist_ext)

        #print vlist
        self._vlists = vlists

    # end of def


    ## Finds a paths from a source to a sink using a supplied previous node list.
    #
    # @param previous A list of node predecessors.
    # @param node_start The source node of the graph.
    # @param node_end The sink node of the graph.
    #
    # @retval [] Array of nodes if a path is found, an empty list if no path is
    # found from the source to sink.
    ##
    def path(self, graph, previous, node_start, node_end):
        route = []

        node_curr = node_end
        try:
            while True:
                route.append(node_curr)
                #print route
                if graph.vertex(previous[node_curr]) == node_start:
                    route.append(node_start)
                    break
                    #elif previous[node_curr] == DiGraph.UNDEFINDED:
                #    return []

                node_curr = graph.vertex(previous[node_curr])

            route.reverse()
            return route
        except Exception:
            print 'exception: '
            print node_curr
            return []


    def find_edge(self, g, node_from, node_to, cost=None):
        elist = node_to.in_edges()

        for e in elist:
            if node_from == e.source():
                return e

        return None

    ## @package YenKSP
    # Computes K-Shortest Paths using Yen's Algorithm.
    #
    # Yen's algorithm computes single-source K-shortest loopless paths for a graph
    # with non-negative edge cost. The algorithm was published by Jin Y. Yen in 1971
    # and implores any shortest path algorithm to find the best path, then proceeds
    # to find K-1 deviations of the best path.

    ## Computes K paths from a source to a sink in the supplied graph.
    #
    # @param graph A digraph of class Graph.
    # @param start The source node of the graph.
    # @param sink The sink node of the graph.
    # @param K The amount of paths being computed.
    #
    # @retval [] Array of paths, where [0] is the shortest, [1] is the next
    # shortest, and so on.
    #
    def ksp_yen(self, graph, node_start, node_end, weights=None, max_k=2):

        #graph = g #g.copy()

        #distances, previous = dijkstra_search(graph, node_start, weights)
        is_minimized, distances, previous = bellman_ford_search(graph, node_start, weights)

        if graph.vertex(previous[node_end]) == node_end: # no pathway
            A = [{'cost': distances[node_end],
                  'path': None}]
            return A

        A = [{'cost': distances[node_end],
              'path': self.path(graph, previous, node_start, node_end)}]
        B = []

        #print A
        if not A[0]['path']: return A

        for k in range(1, max_k):
            for i in range(0, len(A[-1]['path']) - 1):
                node_spur = A[-1]['path'][i] # going through nodes in the path in reverse order, node_end to node_start
                path_root = A[-1]['path'][:i+1] # path root: nodes list up to and including the node_spur, making up the current path (last path in A)

                edges_removed = []
                for path_k in A:
                    curr_path = path_k['path']
                    if len(curr_path) > i and path_root == curr_path[:i+1]:
                        #cost = graph.remove_edge(curr_path[i], curr_path[i+1])
                        cur_edge = self.find_edge(graph, curr_path[i], curr_path[i+1])
                        if cur_edge:
                            cost = weights[cur_edge]
                            graph.remove_edge(cur_edge)
                        else:
                            cost = -1
                            continue

                        edges_removed.append([curr_path[i], curr_path[i+1], cost])

                if weights != None:
                    path_spur_vl, path_spur_el = shortest_path(graph, node_spur, node_end, weights)
                else:
                    path_spur_vl, path_spur_el = shortest_path(graph, node_spur, node_end)



                if len(path_spur_vl) > 0:
                    path_spur = {'cost': sum(map(lambda e: weights[e], path_spur_el)),
                                 'path': path_spur_vl}
                    print 'path_root: '
                    print path_root[:-1]
                    print 'path_spur: '
                    print path_spur
                    path_total = path_root[:-1] + path_spur['path']
                    dist_total = distances[node_spur] + path_spur['cost']
                    potential_k = {'cost': dist_total, 'path': path_total}

                    if not (potential_k in B):
                        B.append(potential_k)

                for edge in edges_removed:
                    graph.add_edge(edge[0], edge[1])

            if len(B):
                B = sorted(B, key=itemgetter('cost'))
                A.append(B[0])
                B.pop(0)
            else:
                break

        return A


    #"Returns a sliding window (of width n) over data from the iterable"
    #"   s -> (s0,s1,...s[n-1]), (s1,s2,...,sn), ...                   "
    #def sliding_window(seq, n=2):
    #    it = iter(seq)
    #    result = tuple(islice(it, n))
    #    if len(result) == n:
    #        yield result
    #    for elem in it:
    #        result = result[1:] + (elem,)
    #        yield result
    def sliding_window(self, iterable, size):
        iters = tee(iterable, size)
        for i in xrange(1, size):
            for each in iters[i:]:
                next(each, None)
        return izip(*iters)


    ##
    # mark graph nodes and edges eligible for visualization as per vlists
    ##
    def preprocess_graph2(self, g, vlists, metDict, hl_color):

        #print 'source vertex is: ' + g.vp['v_id'][srcv]

        vcolor = g.vp['vcolor']
        vtext = g.vp['vtext']
        vsize = g.vp['vsize']
        vtype = g.vp['v_type']

        ecolor = g.ep['ecolor']
        ewidth = g.ep['ewidth']
        efluxstr = g.ep['fluxstr']
        cap = g.ep['cap']

        #vcolor[v] = "#ff0000"
        #vcolor[srcv] = "#55ff00, 1.0" #[1.0,0.,0.,1.0]
        # src vertex is most certainly a compound id not a rxn id
        #srcv_aliases, compartment_tag = metDict.lookupAliasesByID(g.vp['v_id'][srcv])

        #if compartment_tag:
        #    vtext[srcv] = srcv_aliases[0] + '_' + compartment_tag #g.vp['v_id'][curv]
        #else:
        #    vtext[srcv] = srcv_aliases[0]
        #vsize[srcv] = 5
        #p = v

        vlists.reverse() # primary srcv to tgtv last so the path stays colored in red all the way through
        for vlist in vlists:
            startv = vlist[0]
            # process in edges of the starting vertex
            edgelist = list(startv.in_edges())
            edgelist = [e for e in edgelist if cap[e]>0] # get non-zero flux edge list
            listlen = len(edgelist)
            edgelist_sorted = sorted(edgelist, key=lambda e: cap[e], reverse=True) #shortest_distance(g, e.source(), tgtv))

            for idx, e in enumerate(edgelist_sorted):
                curv = e.source()
                if idx < 5 and g.vp['v_id'][curv] not in self._hub_met_list: ####and vtype[startv] == 'met':
                    #if listlen < 5:
                    ecolor[e] = "#afaf00, 0.5"
                    efluxstr[e] = "{:.3e}".format(cap[e]) #"{0:.2f}".format(flux[e])
                    #ewidth[e] += 1
                    #ewidth[e] *=3
                    #else:
                    #    ecolor[e] = "#110000, 0.1"
                    valiases, compartment_tag = metDict.lookupAliasesByID(g.vp['v_id'][curv])
                    if compartment_tag:
                        vtext[curv] = valiases[0] + '_' + compartment_tag #g.vp['v_id'][curv]
                    else:
                        vtext[curv] = valiases[0]
                    vsize[curv] = 3
                    vcolor[curv] = "#555555, 0.2"

            for each in self.sliding_window(vlist, 2):
                v1 = each[0]
                v2 = each[1]
                #p = g.vertex(pred[v])


                #vcolor[p] = "#000000"
                #vcolor[p] = "#000000, 0.2" #[0.,0.,0.,0.5]v
                p = v1

                print 'current vertex is: ' + g.vp['v_id'][p]
                vcolor[p] = "#555555, 0.2"
                curedge = self.find_edge(g, v1, v2)
                print 'current edge between v1, v2: ' + str(curedge)

                # process out edges of the source vertex
                edgelist = list(v1.out_edges())
                edgelist = [e for e in edgelist if cap[e]>1e-9] # get non-zero flux edge list, zero cutoff threshold being 1e-9
                listlen = len(edgelist)
                edgelist_sorted = sorted(edgelist, key=lambda e: cap[e], reverse=True) #shortest_distance(g, e.source(), tgtv))

                for idx, e in enumerate(edgelist_sorted):
                    curv = e.target()
                    if idx < 5 and curv != v2 and g.vp['v_id'][curv] not in self._hub_met_list: ####and vtype[v1] == 'met':
                        #if listlen < 5:
                        ecolor[e] = "#00afaf, 0.5"
                        efluxstr[e] = "{:.3e}".format(cap[e]) #"{0:.2f}".format(flux[e])
                        #ewidth[e] += 1
                        #ewidth[e] *=3
                        #else:
                        #    ecolor[e] = "#110000, 0.1"
                        valiases, compartment_tag = metDict.lookupAliasesByID(g.vp['v_id'][curv])
                        if compartment_tag:
                            vtext[curv] = valiases[0] + '_' + compartment_tag #g.vp['v_id'][curv]
                        else:
                            vtext[curv] = valiases[0]
                        vsize[curv] = 3
                        vcolor[curv] = "#555555, 0.2"


                # process in edges of the target vertex
                edgelist = list(v2.in_edges())
                edgelist = [e for e in edgelist if cap[e]>1e-9] # get non-zero flux edge list
                listlen = len(edgelist)
                #edgelist_sorted = sorted(edgelist, key=lambda e: cap[e], reverse=True)
                #edgelist_sorted = sorted(edgelist, key=lambda e: cap[e])
                edgelist_sorted = sorted(edgelist, key=lambda e: cap[e], reverse=True) #shortest_distance(g, e.source(), tgtv))

                #p = edgelist_sorted[0].source()

                #print edgelist_sorted
                for idx, e in enumerate(edgelist_sorted):
                    curv = e.source()

                    if curv == v1:
                        print 'yeah! ' + "{:.3e}".format(cap[e])
                        print e
                        #ecolor[e] = "#a40000"
                        ecolor[e] = hl_color #"#af0000, 1.0" #[0.8,0.,0.,1.0]
                        #ewidth[e] += 1
                        #ewidth[e] *= 3
                        valiases, compartment_tag = metDict.lookupAliasesByID(g.vp['v_id'][curv])
                        if compartment_tag:
                            vtext[curv] = valiases[0] + '_' + compartment_tag #g.vp['v_id'][curv]
                        else:
                            vtext[curv] = valiases[0]
                        vsize[curv] = 5
                        efluxstr[e] = "{:.3e}".format(cap[e]) #"{0:.2f}".format(flux[e])
                    else:
                        if idx < 5 and g.vp['v_id'][curv] not in self._hub_met_list: ####and vtype[v2] == 'met':
                            #if listlen < 5:
                            ecolor[e] = "#afaf00, 0.5"
                            efluxstr[e] = "{:.3e}".format(cap[e]) #"{0:.2f}".format(flux[e])
                            #ewidth[e] += 1
                            #ewidth[e] *=3
                            #else:
                            #    ecolor[e] = "#110000, 0.1"
                            valiases, compartment_tag = metDict.lookupAliasesByID(g.vp['v_id'][curv])
                            if compartment_tag:
                                vtext[curv] = valiases[0] + '_' + compartment_tag #g.vp['v_id'][curv]
                            else:
                                vtext[curv] = valiases[0]
                            vsize[curv] = 3
                            vcolor[curv] = "#555555, 0.2"

            # deal with the last vertex in the list (the final target vertex)
            vcolor[v2] = "#ff0000, 1.0" #[0.8,1.0,0.,1.0]
            valiases, compartment_tag = metDict.lookupAliasesByID(g.vp['v_id'][v2])
            if compartment_tag:
                vtext[v2] = valiases[0] + '_' + compartment_tag #g.vp['v_id'][curv]
            else:
                vtext[v2] = valiases[0]
            vsize[v2] = 5

            # process out edges of the final target vertex
            edgelist = list(v2.out_edges())
            edgelist = [e for e in edgelist if cap[e]>1e-9] # get non-zero flux edge list
            listlen = len(edgelist)
            edgelist_sorted = sorted(edgelist, key=lambda e: cap[e], reverse=True) #shortest_distance(g, e.source(), tgtv))

            for idx, e in enumerate(edgelist_sorted):
                curv = e.target()
                if idx < 5 and g.vp['v_id'][curv] not in self._hub_met_list: ####and vtype[v2] == 'met':
                    #if listlen < 5:
                    ecolor[e] = "#00afaf, 0.5"
                    efluxstr[e] = "{:.3e}".format(cap[e]) #"{0:.2f}".format(flux[e])
                    #ewidth[e] += 1
                    #ewidth[e] *=3
                    #else:
                    #    ecolor[e] = "#110000, 0.1"
                    valiases, compartment_tag = metDict.lookupAliasesByID(g.vp['v_id'][curv])
                    if compartment_tag:
                        vtext[curv] = valiases[0] + '_' + compartment_tag #g.vp['v_id'][curv]
                    else:
                        vtext[curv] = valiases[0]
                    vsize[curv] = 3
                    vcolor[curv] = "#555555, 0.2"


        return g


    def loadGraphLayoutCoordinates(self, layout_pos, infilepath):
        jsonf = open(infilepath)
        jsondata = json.loads(jsonf.read())
        g = layout_pos.get_graph()
        vidx_lookup = self._vidx_lookup
        for key in jsondata.keys():
            try:
                curv = g.vertex(vidx_lookup[key])
                unpickled_value = pickle.loads(jsondata[key]) # this restores the value of python type 'list'
                layout_pos[curv] = array(unpickled_value, dtype=float64)
            except KeyError:
                print 'key: ' + str(key) + ' ignored.'

        jsonf.close()

        return layout_pos

    def copyGraphLayoutCoordinates(self, from_layout, to_layout):
        vidx_lookup = self._vidx_lookup
        from_g = from_layout.get_graph()
        from_vertices = from_g.vertices()
        for curv in from_vertices:
            try:
                coord = from_layout[curv]
                if coord:
                    to_layout[curv] = coord
                else:
                    print 'empty coord'
            except KeyError:
                print 'key: ' + str(curv) + ' ignored.'
        return to_layout

    def _saveGraphLayoutCoordinates(self, layout_pos, outfilepath):
        coordLookup = {}
        g = layout_pos.get_graph()
        vertices = g.vertices()
        vid_lookup = g.vp['v_id']
        for v in vertices: #g.vertices():
            coord = layout_pos[v]
            if coord:
                coordlist = [str(coord[0]),str(coord[1])] #list(array(coord.a,dtype=str))
                pickled_value = pickle.dumps(coordlist) # pickle the coord data in 'numpy.ndarray' type in python type 'list' of 'str'
                #coordLookup[g.vp['v_id'][v]] = pickled_value
                coordLookup[vid_lookup[v]] = pickled_value

        jsonf = open(outfilepath, 'w')
        jsondata = json.dumps(coordLookup)
        print >> jsonf, jsondata
        jsonf.close()

        return coordLookup

    def saveGraphLayout(self, layout_pos_filename, layout_coords): #is_precalculated=False):
        self._saveGraphLayoutCoordinates(layout_coords, layout_pos_filename)

    def drawGraphvlist(self, filename_base, is_save_layout=False):
        graph = self._graph
        vlists = self._vlists

        #g = graph#.copy()
        #srcv, tgtv = g.vertex(0), g.vertex(1)

        srcv_idx = 0 #self._vidx_lookup['cpd00079_c']
        tgtv_idx = 1 #self._vidx_lookup['cpdS00140_c']
        #srcv, tgtv = g.vertex(0), g.vertex(1)
        srcv, tgtv = graph.vertex(srcv_idx), graph.vertex(tgtv_idx)

        #graph = preprocess_graph(graph, srcv, tgtv, vlists, metDict, "#af0000, 1.0")
        graph = self.preprocess_graph2(graph, vlists, self._metdict, "#af0000, 1.0")

        graph.save(filename_base + "_" + str(hash(str(vlists))) + ".xml.gz")

        # graph view only consisting of vertices with non-empty vtext and edges with non-empty ecolor
        # as were pre-processed in the call to preprocess_graph2()
        filtered_graph = GraphView(graph, vfilt=lambda v: self._graph_vp_vtext[v] != "", efilt=lambda e: self._graph_ep_ecolor[e] != "")

        layout_pos = arf_layout(filtered_graph, max_iter=0)

        layout_saveas_name = None
        if is_save_layout:
            layout_saveas_name = filename_base+"_layoutcoord.json"
            self.saveGraphLayout(layout_saveas_name, layout_pos)
            #layout_pos = arf_layout(filtered_graph, max_iter=0)#sfdp_layout(u)
            #self.saveGraphLayoutCoordinates(graph, layout_pos, layout_pos_file)

        return self._drawGraphvlist(filtered_graph, vlists, filename_base, layout_pos), layout_pos #layout_saveas_name

    def drawGraphvlistByLayout(self, filename_base, ref_layout_coords, is_save_layout=False):
        graph = self._graph
        vlists = self._vlists

        srcv_idx = 0 #self._vidx_lookup['cpd00079_c']
        tgtv_idx = 1 #self._vidx_lookup['cpdS00140_c']
        #srcv, tgtv = g.vertex(0), g.vertex(1)
        srcv, tgtv = graph.vertex(srcv_idx), graph.vertex(tgtv_idx)

        #graph = preprocess_graph(graph, srcv, tgtv, vlists, metDict, "#af0000, 1.0")
        graph = self.preprocess_graph2(graph, vlists, self._metdict, "#af0000, 1.0")

        graph.save(filename_base + "_" + str(hash(str(vlists))) + ".xml.gz")

        # graph view only consisting of vertices with non-empty vtext and edges with non-empty ecolor
        # as were pre-processed in the call to preprocess_graph2()
        filtered_graph = GraphView(graph, vfilt=lambda v: self._graph_vp_vtext[v] != "", efilt=lambda e: self._graph_ep_ecolor[e] != "")

        layout_saveas_name = None
        if is_save_layout:
            layout_saveas_name = filename_base+"_layoutcoord.json"
            layout_pos = arf_layout(filtered_graph, max_iter=0)
            self.saveGraphLayout(layout_saveas_name, layout_pos)
            #layout_pos = arf_layout(filtered_graph, max_iter=0)#sfdp_layout(u)
            #self.saveGraphLayoutCoordinates(graph, layout_pos, layout_pos_file)
        else:
            to_layout = random_layout(filtered_graph)
            to_layout = self.copyGraphLayoutCoordinates(ref_layout_coords, to_layout)
            to_layout = arf_layout(filtered_graph, pos=to_layout, max_iter=0)#random_layout(u)
            layout_pos = self.copyGraphLayoutCoordinates(ref_layout_coords, to_layout)
            filename_base += "_overlayed"

        return self._drawGraphvlist(filtered_graph, vlists, filename_base, layout_pos), layout_pos

    def drawGraphvlistByLayoutFromFile(self, filename_base, ref_layout_file, is_save_layout=False):
        graph = self._graph
        vlists = self._vlists

        srcv_idx = 0 #self._vidx_lookup['cpd00079_c']
        tgtv_idx = 1 #self._vidx_lookup['cpdS00140_c']
        #srcv, tgtv = g.vertex(0), g.vertex(1)
        srcv, tgtv = graph.vertex(srcv_idx), graph.vertex(tgtv_idx)

        #graph = preprocess_graph(graph, srcv, tgtv, vlists, metDict, "#af0000, 1.0")
        graph = self.preprocess_graph2(graph, vlists, self._metdict, "#af0000, 1.0")

        graph.save(filename_base + "_" + str(hash(str(vlists))) + ".xml.gz")

        # graph view only consisting of vertices with non-empty vtext and edges with non-empty ecolor
        # as were pre-processed in the call to preprocess_graph2()
        filtered_graph = GraphView(graph, vfilt=lambda v: self._graph_vp_vtext[v] != "", efilt=lambda e: self._graph_ep_ecolor[e] != "")

        layout_saveas_name = None
        if is_save_layout:
            layout_saveas_name = filename_base+"_layoutcoord.json"
            layout_pos = arf_layout(filtered_graph, max_iter=0)
            self.saveGraphLayout(layout_saveas_name, layout_pos)
        else:
            to_layout = random_layout(filtered_graph)
            to_layout = self.loadGraphLayoutCoordinates(filtered_graph, self._vidx_lookup, to_layout, ref_layout_file) #self.copyGraphLayoutCoordinates(ref_layout_coords, to_layout)
            to_layout = arf_layout(filtered_graph, pos=to_layout)#random_layout(u)
            layout_pos = self.loadGraphLayoutCoordinates(filtered_graph, self._vidx_lookup, to_layout, ref_layout_file)
            layout_pos = self.copyGraphLayoutCoordinates(ref_layout_coords, to_layout)
            filename_base += "_overlayed"

        return self._drawGraphvlist(filtered_graph, vlists, filename_base, layout_pos), layout_saveas_name

    def _drawGraphvlist(self, graph, vlists, filename_base, layout_pos):

        #u.save("bsu_met_network++_filtered.xml.gz")

        #for e in u.edges():
        #    if ecolor[e] == "":
        #        ecolor[e] = "#ff0000, 0.2"
        #        efluxstr[e] = cap[e]
        #    ecolor[e] = "#0000ff, 0.1" if touch_e[e] else "#595959, 0.1"
        #ecolor[e] = "blue" if touch_e[e] else "black"



        for e in graph.edges():
            if self._graph_ep_ecolor[e] == "":
                print e
            d = sqrt(sum((layout_pos[e.source()].a - layout_pos[e.target()].a) ** 2)) / 5
            self._graph_ep_control[e] = [0.3, d, 0.7, d]


        deg = graph.degree_property_map("in")
        deg.a = 4 * (sqrt(deg.a) * 0.5 + 0.4) + 3

        print 'drawing graph...'

        outputfilename = filename_base+"_"+str(hash(str(vlists)))+".pdf"

        graph_draw(graph, pos=layout_pos, output_size=(1024, 768), vertex_size=deg,
                   vertex_font_size=6, vertex_text=graph.vp['vtext'], vertex_text_position=0,
                   vertex_fill_color=graph.vp['vcolor'], #touch_v,
                   vcmap=matplotlib.cm.binary, edge_color=graph.ep['ecolor'],
                   edge_control_points=self._graph_ep_control, # some curvy edges
                   edge_pen_width=graph.ep['ewidth'],
                   edge_text = graph.ep['fluxstr'],
                   text_distance=0,
                   edge_font_size=4,
                   output=outputfilename)
        print 'drawing graph done!'

        return outputfilename

__author__ = 'spark'

# had to change graph_tool.draw.cairo_draw.py to make this script work with setting vertex color with alpha values
# _convert function in cairo_draw.py edited

# We probably will need some things from several places
#import cobra
import re
import sys, os

from cobra.io.sbml import *

#from setLBmedia import *
from setShinorineReactions import *
import matplotlib

from pylab import *  # for plotting
from numpy.random import *  # for random sampling

from numpy import sqrt

seed(42)

# We need to import the graph_tool module itself
import graph_tool.draw
import graph_tool.all
from graph_tool.all import *

def h(v, target, pos):
    return sqrt(sum((pos[v].a - pos[target].a) ** 2))


class VisitorExample(graph_tool.search.AStarVisitor):
    def __init__(self, touched_v, touched_e, target):
        self.touched_v = touched_v
        self.touched_e = touched_e
        self.target = target

    def discover_vertex(self, u):
        print 'vt:' + g.vp['v_id'][u]
        self.touched_v[u] = True

    def examine_edge(self, e):
        #print 'et:' + g.ep['v_id'][u]
        self.touched_e[e] = True

    def edge_relaxed(self, e):
        if e.target() == self.target:
            print 'Ahoy!'
            self.examine_edge(e)
            self.discover_vertex(self.target)
            raise graph_tool.search.StopSearch()


graph_file = "graphvis/bsu_met_network_glu_dhquinate_0.xml.gz"

# We start with an empty, directed graph


g = load_graph(graph_file)

print 'graph reconstruction done'

flux = g.ep['flux']
srcv, tgtv = g.vertex(0), g.vertex(1)
print 'srcv: ' + g.vp['v_id'][srcv]
print 'tgtv: ' + g.vp['v_id'][tgtv]

weight = flux.copy()
weight.a = (weight.a*1.0/weight.a.max())**2.0
#(eflux.a/(eflux.a.max()/1.0))**2.0
weight.a = 1/(1+abs(weight.a))
#weight = prop_to_size(weight, mi=0, ma=weight.a.max(), power=1),

touch_v = g.new_vertex_property("bool")
touch_e = g.new_edge_property("bool")

#vcolor = g.new_vertex_property("string")
vcolor = g.new_vertex_property("string") # arbitrary object list #g.new_vertex_property("vector<float>")
#vcolor.a = "#ffffff" #initialize
vtext = g.new_vertex_property("string")
vsize = g.new_vertex_property("float")
vsize.a = 1
g.vp['vtext'] = vtext
g.vp['vcolor'] = vcolor
g.vp['vsize'] = vsize

target = g.vertex(1)

#layout_pos = sfdp_layout(g, C=1000, p=10)
#layout_pos = arf_layout(g, max_iter=1000)
#layout_pos = fruchterman_reingold_layout(g, n_iter=1000)
shape = [[1,6000], [1,6000], 4]
layout_pos = graph_tool.draw.random_layout(g, shape=shape, dim=3)
control = g.new_edge_property("vector<double>")

for e in g.edges():
    d = sqrt(sum((layout_pos[e.source()].a - layout_pos[e.target()].a) ** 2)) / 5
    control[e] = [0.3, d, 0.7, d]

dist, pred = astar_search(g, g.vertex(0), weight,
                          VisitorExample(touch_v, touch_e, target),
                          heuristic=lambda v: h(v, target, layout_pos))

efluxstr = g.new_edge_property("string")
ecolor = g.new_edge_property("string")
ewidth = g.new_edge_property("double")
ewidth.a = weight.a

g.ep['ecolor'] = ecolor
g.ep['ewidth'] = ewidth
g.ep['fluxstr'] = efluxstr

for v in g.vertices():
    vcolor[v] = "#000000, 1.0" if touch_v[v] else "#595959, 0.1"
    #vcolor[v] = "black" if touch_v[v] else "white"


for e in g.edges():
    ecolor[e] = "#0000ff, 0.1" if touch_e[e] else "#595959, 0.1"
    #ecolor[e] = "blue" if touch_e[e] else "black"

v = target
print 'target vertex is: ' + g.vp['v_id'][v]
#vcolor[v] = "#ff0000"
vcolor[v] = "#ff0000, 1.0" #[1.0,0.,0.,1.0]
vtext[v] = g.vp['v_id'][v]
vsize[v] = 5
p = v
while v != srcv:
    p = g.vertex(pred[v])

    #vcolor[p] = "#000000"
    #vcolor[p] = "#000000, 0.2" #[0.,0.,0.,0.5]v

    print 'current vertex is: ' + g.vp['v_id'][p]
    edgelist = list(v.in_edges())
    listlen = len(edgelist)
    edgelist_sorted = sorted(edgelist, key=lambda e: ewidth[e], reverse=True)

    '''
    print edgelist_sorted
    for e in edgelist_sorted:
        print ewidth[e]
    print '########'
    '''
    for idx, e in enumerate(edgelist_sorted):
        curv = e.source()
        if curv == p:
            print 'yeah!'
            #ecolor[e] = "#a40000"
            ecolor[e] = "#af0000, 1.0" #[0.8,0.,0.,1.0]
            ewidth[e] *= 3
            vtext[p] = g.vp['v_id'][p]
            vsize[p] = 5

            efluxstr[e] = "{:.3e}".format(flux[e]) #"{0:.2f}".format(flux[e])
        else:
            if idx < 5:
                #if listlen < 5:
                ecolor[e] = "#afaf00, 0.5"
                efluxstr[e] = "{:.3e}".format(flux[e]) #"{0:.2f}".format(flux[e])
                ewidth[e] *=3
                #else:
                #    ecolor[e] = "#110000, 0.1"
                vtext[curv] = g.vp['v_id'][curv]
                vsize[p] = 3

    v = p

#vcolor[g.vertex(0)] = "#55ff00"

vcolor[srcv] = "#55ff00, 1.0" #[0.8,1.0,0.,1.0]
vtext[srcv] = g.vp['v_id'][srcv]
vsize[srcv] = 5

print 'src vertex is: ' + g.vp['v_id'][g.vertex(0)]

g.save("bsu_met_network++.xml.gz")

u = GraphView(g, vfilt=lambda v: vtext[v] != "")
u.save("bsu_met_network++_filtered.xml.gz")

layout_pos = graph_tool.draw.sfdp_layout(u)

for e in u.edges():
    d = sqrt(sum((layout_pos[e.source()].a - layout_pos[e.target()].a) ** 2)) / 5
    control[e] = [0.3, d, 0.7, d]

deg = u.degree_property_map("in")
deg.a = 4 * (sqrt(deg.a) * 0.5 + 0.4)

graph_draw(u, pos=layout_pos, output_size=(600, 600), vertex_size=deg, vertex_font_size=6,
                           vertex_text=u.vp['vtext'], vertex_text_position=0, vertex_fill_color=u.vp['vcolor'],  #touch_v,
                           vcmap=matplotlib.cm.binary, edge_color=u.ep['ecolor'],
                           edge_control_points=control, # some curvy edges
                           edge_pen_width=u.ep['ewidth'],
                           edge_text = u.ep['fluxstr'],
                           edge_font_size=4,
                           output="astar-delaunay_filtered.pdf")

from subprocess import call
call(["open", "astar-delaunay_filtered.pdf"])

graph_tool.draw.interactive_window(u, pos=layout_pos, geometry=(500,500), vertex_size=deg, vertex_font_size=8, vertex_text=u.vp['vtext'], vertex_text_position=0, vertex_fill_color=u.vp['vcolor'], #touch_v
           edge_color=u.ep['ecolor'],
           edge_control_points=control, # some curvy edges
           edge_pen_width=u.ep['ewidth'],
           edge_text = u.ep['fluxstr'],
           edge_font_size=7,
           update_layout=False,
           vcmap=matplotlib.cm.binary)
#graph_draw(g, pos=layout_pos, output_size=(600, 600), vertex_size=vsize, vertex_font_size=6, vertex_text=vtext, vertex_fill_color=vcolor, #touch_v,
#           vcmap=matplotlib.cm.binary, edge_color=ecolor,
#           edge_control_points=control, # some curvy edges
#           edge_pen_width=ewidth, output="astar-delaunay.pdf")

'''
deg = g.degree_property_map("in")
#edge_flux = g.edge_properties['flux']

deg.a = 4 * (sqrt(deg.a) * 0.5 + 0.4)
#deg[g.vertex(0)] = deg.a.max()
#deg[g.vertex(1)] = deg.a.max()

u = GraphView(g, vfilt=lambda v: deg.a[v] <= deg.a.max()/4.0, efilt=lambda e: )

ebet = betweenness(u)[1]
eflux = u.edge_properties["flux"]
vid = u.vertex_properties["v_id"]
vtype = u.vertex_properties["v_type"]
vcolor = u.vertex_properties["v_color"]
ebet.a /= ebet.a.max()/10.0
eflux.a = (eflux.a/(eflux.a.max()/1.0))**2.0
#eflux.a *= eflux.a
eorder = ebet.copy()
#eorder = eflux.copy()
eorder.a *= 1
vorder = deg.copy()
vorder.a *= -1


#res = max_independent_vertex_set(g)

# draw src and tgt vertices last
vorder[srcv] = vorder.a.max()
vorder[tgtv] = vorder.a.max()





#s_dist_vlist = shortest_distance(g, srcv, tgtv, revflux, directed=True)
#g.set_vertex_filter(shortest_dist)
#s_path_vlist, s_path_elist = shortest_path(g, srcv, tgtv, revflux)
#u = GraphView(g, efilt=lambda e: e in s_path_elist)
#g.set_edge_filter(s_path_elist)




#res = push_relabel_max_flow(g, source=srcv, target=tgtv, capacity=flux)
#res = push_relabel_max_flow(g, source=srcv, target=tgtv, capacity=flux)
#res.a = flux.a - res.a # the actual flow
#max_flow = sum(res[e] for e in tgtv.in_edges())
#print 'max_flow: ' + str(max_flow)

#graph_draw(g, pos=layout_pos, vertex_size=deg, vertex_fill_color=deg, vorder=deg,
#           edge_color=ebet, eorder=eorder, edge_pen_width=ebet,
#           edge_control_points=control, # some curvy edges
#           output="graph-draw.pdf")

graph_draw(u, pos=layout_pos, vertex_size=deg, vertex_fill_color=vcolor, vorder=vorder,
           edge_color=ebet, eorder=eorder, edge_pen_width=prop_to_size(eflux, mi=0, ma=eflux.a.max(), power=1),
           edge_control_points=control, # some curvy edges
           output="graph-draw_filtered.pdf")

g.set_vertex_filter(None)
g.set_edge_filter(None)

graph_draw(u, pos=layout_pos, vertex_size=deg, vertex_fill_color=vcolor, vorder=vorder,
           edge_color=ebet, eorder=eorder, edge_pen_width=flux,
           edge_control_points=control, # some curvy edges
           output="graph-draw_nonfiltered.pdf")


#g.save("bsu_met_network.xml.gz")
#graph_draw(g, pos=layout_pos, output="bsu_met_network_arf.pdf")

'''




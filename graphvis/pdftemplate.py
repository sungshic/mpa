__author__ = 'spark'

from reportlab.lib.styles import ParagraphStyle as PS
from reportlab.platypus import PageBreak
from reportlab.platypus.paragraph import Paragraph
from reportlab.platypus.figures import ImageFigure, FlexFigure
from reportlab.platypus.doctemplate import PageTemplate, BaseDocTemplate
from reportlab.platypus.tableofcontents import TableOfContents
from reportlab.platypus.frames import Frame
from reportlab.lib.units import cm
from reportlab.lib.pagesizes import A4, landscape

from hashlib import sha1
from PyPDF2 import PdfFileWriter, PdfFileReader

from reportlab.platypus import Flowable
from reportlab.lib.enums import TA_LEFT, TA_CENTER, TA_RIGHT

from pdfrw import PdfReader
from pdfrw.buildxobj import pagexobj
from pdfrw.toreportlab import makerl

class PdfImage(Flowable):
#class PdfImage(FlexFigure):
    """PdfImage wraps the first page from a PDF file as a Flowable
which can be included into a ReportLab Platypus document.
Based on the vectorpdf extension in rst2pdf (http://code.google.com/p/rst2pdf/)"""

    def __init__(self, filename_or_object, width=None, height=None, kind='direct'):
        #from reportlab.lib.units import inch
        # If using StringIO buffer, set pointer to begining
        if hasattr(filename_or_object, 'read'):
            filename_or_object.seek(0)
        page = PdfReader(filename_or_object, decompress=False).pages[0]
        self.xobj = pagexobj(page)
        self.imageWidth = width
        self.imageHeight = height
        x1, y1, x2, y2 = self.xobj.BBox

        self._w, self._h = x2 - x1, y2 - y1
        if not self.imageWidth:
            self.imageWidth = self._w
        if not self.imageHeight:
            self.imageHeight = self._h
        self.__ratio = float(self.imageWidth)/self.imageHeight
        if kind in ['direct','absolute'] or width==None or height==None:
            self.drawWidth = width or self.imageWidth
            self.drawHeight = height or self.imageHeight
        elif kind in ['bound','proportional']:
            factor = min(float(width)/self._w,float(height)/self._h)
            self.drawWidth = self._w*factor
            self.drawHeight = self._h*factor
        #FlexFigure.__init__(self, self.drawWidth, self.drawHeight, "", None)

    def wrap(self, aW, aH):
        return self.drawWidth, self.drawHeight

    def drawOn(self, canv, x, y, _sW=0):
        if _sW > 0 and hasattr(self, 'hAlign'):
            a = self.hAlign
            if a in ('CENTER', 'CENTRE', TA_CENTER):
                x += 0.5*_sW
            elif a in ('RIGHT', TA_RIGHT):
                x += _sW
            elif a not in ('LEFT', TA_LEFT):
                raise ValueError("Bad hAlign value " + str(a))

        xobj = self.xobj
        xobj_name = makerl(canv._doc, xobj)

        xscale = self.drawWidth/self._w
        yscale = self.drawHeight/self._h

        x -= xobj.BBox[0] * xscale
        y -= xobj.BBox[1] * yscale

        canv.saveState()
        canv.translate(x, y)
        canv.scale(xscale, yscale)
        canv.doForm(xobj_name)
        canv.restoreState()


class CrodaMPAReportTemplate(BaseDocTemplate):
    centered = PS(name = 'centered',
        fontSize = 14,
        leading = 16,
        alignment = 1,
        spaceAfter = 20)

    h1 = PS(
        name = 'Heading1',
        fontSize = 14,
        leading = 16)


    h2 = PS(name = 'Heading2',
        fontSize = 12,
        leading = 14)

    def __init__(self, filename, **kw):
        self.allowSplitting = 0
        apply(BaseDocTemplate.__init__, (self, filename), kw)
        template = PageTemplate('normal', [Frame(1.5*cm, 4.5*cm, 25*cm, 15*cm, id='F1')], onPage=self.make_landscape)
        #template = PageTemplate('normal', id='F1', onPage=self.make_landscape)
        self.addPageTemplates([template])

        # Build story.
        self._story = []
        self._template_filename = filename
        self._pdf_writer = PdfFileWriter()
        self._pagecount = 0
        self._content_pdf_files = {} # initialize a lookup dictionary

    def make_portrait(self, canvas, doc):
        canvas.setPageSize(A4)

    def make_landscape(self, canvas, doc):
        canvas.setPageSize(landscape(A4))

    # Entries to the table of contents can be done either manually by
    # calling the addEntry method on the TableOfContents object or automatically
    # by sending a 'TOCEntry' notification in the afterFlowable method of
    # the DocTemplate you are using. The data to be passed to notify is a list
    # of three or four items countaining a level number, the entry text, the page
    # number and an optional destination key which the entry should point to.
    # This list will usually be created in a document template's method like
    # afterFlowable(), making notification calls using the notify() method
    # with appropriate data.

    def afterFlowable(self, flowable):
        "Registers TOC entries."
        if flowable.__class__.__name__ == 'Paragraph':
            text = flowable.getPlainText()
            style = flowable.style.name
            if style == 'Heading1':
                level = 0
            elif style == 'Heading2':
                level = 1
            else:
                return
            E = [level, text, self.page]
            #if we have a bookmark name append that to our notify data
            bn = getattr(flowable,'_bookmarkName',None)
            if bn is not None: E.append(bn)
            self.notify('TOCEntry', tuple(E))

    #this function makes our headings
    def doHeading(self, text,sty):
        #create bookmarkname
        bn=sha1(text+sty.name).hexdigest()
        #modify paragraph text to include an anchor point with name bn
        h=Paragraph(text+'<a name="%s"/>' % bn,sty)
        #store the bookmark name on the flowable so afterFlowable can see this
        h._bookmarkName=bn
        self._story.append(h)

    # Create an instance of TableOfContents. Override the level styles (optional)
    def addATitlePage(self, pdf_title_str):
        self._pagecount += 1
        self._toc = TableOfContents()
        self._toc.levelStyles = [
            #PS(fontName='Times-Bold', fontSize=20, name='TOCHeading1', leftIndent=20, firstLineIndent=-20, spaceBefore=10, leading=16),
            PS(fontName='Times-Bold', fontSize=12, name='TOCHeading1', leftIndent=20, firstLineIndent=-20, spaceBefore=10, leading=10),
            PS(fontSize=12, name='TOCHeading2', leftIndent=40, firstLineIndent=-20, spaceBefore=5, leading=12),
            ]

        self._story.append(Paragraph('<b>' + pdf_title_str + '</b>', self.centered))
        self._story.append(self._toc)

    def addAPage(self, page_heading, page_text): #fig_heading, title_fig_file, fig_caption):
        self._story.append(PageBreak()) # a new page
        self._pagecount += 1
        #title_fig = PdfImage(fig_file,width=696, height=413, kind='bound')
        #title_fig.wrap(300, 400)
        self.doHeading(page_heading, self.h1)
        self._story.append(Paragraph(page_text, PS('body')))

    def addAFigPage(self, page_heading, page_text, fig_heading, fig_file, fig_caption): #fig_heading, title_fig_file, fig_caption):
        self._story.append(PageBreak()) # a new page
        self._pagecount += 1
        #title_fig = PdfImage(fig_file,width=696, height=413, kind='bound')
        #title_fig.wrap(300, 400)
        self.doHeading(page_heading, self.h1)
        self._story.append(Paragraph(page_text, PS('body')))
        title_fig = ImageFigure(fig_file, fig_caption)
        self.doHeading(fig_heading, self.h2)
        self._story.append(title_fig)

    def addAPDFPage(self, page_heading, page_text, pdf_file):
        self._story.append(PageBreak()) # a new page
        self._pagecount += 1
        #title_fig = ImageFigure(fig_file, fig_caption)
        #pdfimg = PdfImage(pdf_file,width=696, height=413, kind='bound')
        print self.pagesize
        #pdfimg = PdfImage(pdf_file,width=self.pagesize[0], height=self.pagesize[1], kind='bound')
        landscape_A4_size = landscape(A4)
        template_frame = self.pageTemplates[0].frames[0]
        print 'frame dim: ' + str(template_frame._aW) + 'x' + str(template_frame._aH)
        pdfimg = PdfImage(pdf_file,width=template_frame.width, height=template_frame.height, kind='bound')
        #title_fig.wrap(300, 400)
        self.doHeading(page_heading, self.h1)
        self._story.append(Paragraph(page_text, PS('body')))
        self._story.append(pdfimg)

    def _getPDFFileByPageIdx(self, page_idx):
        try:
            return self._content_pdf_files[page_idx]
        except KeyError:
            return None

    def _addPDFFileByPageIdx(self, page_idx, filename):
        self._content_pdf_files[page_idx] = filename

    def addAContentPage(self, heading_str, text_str, pdf_file=None):
        self._story.append(PageBreak()) # a new page
        self._pagecount += 1
        self.doHeading(heading_str, self.h1)
        self._story.append(Paragraph(text_str, PS('body')))
        if pdf_file:
            self._addPDFFileByPageIdx(self._pagecount - 1, pdf_file)

    def createATemplatePDF(self):
        # and add the object to the story
        #self.doHeading('First heading', h1)
        #self._story.append(Paragraph('Text in first heading', PS('body')))
        #self._doHeading('First sub heading', h2)
        #self._story.append(Paragraph('Text in first sub heading', PS('body')))
        #self._story.append(PageBreak())
        #self._doHeading('Second sub heading', h2)
        #self._story.append(Paragraph('Text in second sub heading', PS('body')))
        #self._story.append(PageBreak())
        #self._doHeading('Last heading', h1)
        #self._story.append(Paragraph('Text in last heading', PS('body')))
        #self._doc = MyDocTemplate(pdf_filename)
        #self._doc.multiBuild(self._story)
        self.multiBuild(self._story)

    def saveMPAGraphs(self, saveas_filename):
        pdf_writer = PdfFileWriter()
        template_pdf_reader = PdfFileReader(file(self._template_filename, "rb"))

        #mergerpages = [pdffilename1, pdffilename2, pdffilename3]
        #print mergerpages

        title_page = template_pdf_reader.getPage(0) # get the title page of the template
        fig_page = template_pdf_reader.getPage(1) # get the fig page
        template_page_width = float(title_page.mediaBox.getWidth())
        template_page_height = float(title_page.mediaBox.getHeight())

        pdf_writer.addPage(title_page) # page 1
        pdf_writer.addPage(fig_page) # page 2

        #for idx, cur_file in enumerate(graph_pdf_list):
        #    graph_pdf_file = PdfFileReader(file(cur_file, "rb"))
        #    template_p = template_pdf_reader.getPage(idx+2)
        #    mp = graph_pdf_file.getPage(0) # get the first page of the graph pdf file
        #    mp.scaleTo(template_page_width, template_page_height) # scale the graph pdf file to fit the template
        #    template_p.mergePage(mp) # merge the graph into the template page
        #    pdf_writer.addPage(template_p)

        for idx, p in enumerate(template_pdf_reader.pages):
            cur_filename = self._getPDFFileByPageIdx(idx)
            if cur_filename:
                graph_pdf_file = PdfFileReader(file(cur_filename, "rb"))
                mp = graph_pdf_file.getPage(0)
                mp.scaleTo(template_page_width, template_page_height)
                p.mergePage(mp)
                pdf_writer.addPage(p)

        outputStream = file(saveas_filename, "wb")

        pdf_writer.write(outputStream)
        outputStream.close()





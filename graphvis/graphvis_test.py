__author__ = 'spark'

# We probably will need some things from several places
#import cobra

from cobra.io.sbml import *
import json

#from setLBmedia import *
from mpa_tool.setMedia import *
from setShinorineReactions import *

from pylab import *  # for plotting
from numpy.random import *  # for random sampling

seed(42)

# We need to import the graph_tool module itself
from graph_tool.all import *

from ll_cobra import *


sbml_file = "../data/iBsu1103V2/nar-01731-m-2012-File013.xml"
is_tgt_prod_enforced = True
is_shiki_on = True # shiki off
#is_shiki_on = True # shiki on


for mediaIdx in [6, 10, 11, 12, 13, 14, 15, 17, 18, 19, 20, 21, 22]: #[15, 17, 18, 19, 20, 21, 22]: #[0, 1, 6, 10, 11, 12, 13, 15, 17, 18, 19, 20, 21, 22]: #14 infeasible #range(16,17):
    print 'processing media idx: ' + str(mediaIdx)
    sb_model = create_cobra_model_from_sbml_file(sbml_file)

    setShinorineReactions(sb_model, is_shiki_on)
    #setLBmedia(sb_model, -10)
    setMedia(sb_model, -10, mediaIdx)

    biomassRxnIdx = sb_model.reactions.index('bio00127')
    #srcRxnIdx = sb_model.reactions.index('EX_cpd00027_e') #D-Glu uptake
    srcRxnIdx = sb_model.reactions.index(getMediaSrcRxn(mediaIdx)) #'EX_cpd00027_e') #D-Glu uptake
    tgtRxnIdx = sb_model.reactions.index('rxnS00150') #Shinorine secretion
    #tgtRxnIdx = sb_model.reactions.index('rxn01343') #sh7p
    srcCompoundIdx = sb_model.metabolites.index(getMediaSrcCmp(mediaIdx)) #'cpd00027_e')
    #tgtCompoundIdx = sb_model.metabolites.index('cpd00238_c')
    tgtCompoundIdx = sb_model.metabolites.index('cpdS00140_e') #'cpdADDED_Shinorine_e') shinorine
    #tgtCompoundIdx = sb_model.metabolites.index('cpd00238_c') #cytosolic sedoheptulose-7-phosphate
    #tgtCompoundIdx = sb_model.metabolites.index('cpdADDED_DDG_c') #cytosolic 2-demethyl-4-deoxygadusol
    #tgtCompoundIdx = sb_model.metabolites.index('cpd00699_c') #cytosolic 2-demethyl-4-deoxygadusol

    # setting a dual objective
    sb_model.reactions.bio00127.objective_coefficient = 1
    #sb_model.reactions.rxn_S00150.objective_coefficient = 1
    sb_model.reactions.bio00127.lower_bound = 0.5
    #sb_model.genes.
    sb_model_mat = sb_model.to_array_based_model()

    sb_model_mat.update()
    #sb_model_mat.optimize()

    lp_problem = format_lp_problem(sb_model, True)
    lp_sol = solve_gurobi(lp_problem)
    cobra_sol = format_cobra_solution_from_GUROBIresult(lp_sol)

    if is_tgt_prod_enforced:
        if cobra_sol.status == "optimal": #cobra_sol >= 0.05:
            print 'biomass theoretical maximum: ' + str(cobra_sol.f)
            min_biomass = cobra_sol.f/2.0
            sb_model.reactions.rxnS00150.objective_coefficient = 1
            sb_model.reactions.bio00127.lower_bound = min_biomass
            sb_model_mat.update()

            lp_problem = format_lp_problem(sb_model, True)
            lp_sol = solve_gurobi(lp_problem)
            cobra_sol = format_cobra_solution_from_GUROBIresult(lp_sol)

            sb_model.solution = cobra_sol
    else: # just use biomass objective, to see if tgt is coupled to the growth
        sb_model.solution = cobra_sol

    #sb_model_mat.optimize()
    #cobra_sol = sb_model.solution

    if cobra_sol.status != "optimal" or cobra_sol.f < 0.05:
        print 'Model is infeasible, skipping...'
    else:
        print 'fba solutions: ' + str(sb_model.solution)

        # We start with an empty, directed graph

        g = Graph(directed=True)

        g.vp['v_id'] = g.new_vertex_property('string')
        g.vp['v_type'] = g.new_vertex_property('string')
        #g.vp['v_color'] = g.new_vertex_property('vector<float>')
        g.vp['v_color'] = g.new_vertex_property('string')
        g.ep['flux'] = g.new_edge_property('float')
        g.ep['cap'] = g.new_edge_property('float')

        numMets, numRxns = sb_model.S.shape

        v_lookup = {} #initialize
        v_idx_lookup = {} #initialize

        # just so the src vertex has the index 0
        srcv = g.add_vertex()
        vid = sb_model.metabolites[srcCompoundIdx]
        g.vp['v_id'][srcv] = vid
        g.vp['v_type'][srcv] = 'met'
        g.vp['v_color'][srcv] = "#5f5f00" #0x0000ff #[0.,0.,1.0, 1.0] #'b' #0x0000FF
        v_lookup[vid] = srcv
        v_idx_lookup[vid.id] = g.vertex_index[srcv]

        # just so the tgt vertex has the index 1
        tgtv = g.add_vertex()
        vid = sb_model.metabolites[tgtCompoundIdx]
        g.vp['v_id'][tgtv] = vid
        g.vp['v_type'][tgtv] = 'met'
        g.vp['v_color'][tgtv] = "#ff0000" #0xff0000 #[1.0, 0., 0., 1.0] #'r' #0xff0000
        v_lookup[vid] = tgtv
        v_idx_lookup[vid.id] = g.vertex_index[tgtv]

        flux_val = sb_model.solution.x[srcRxnIdx]
        print 'flux_val of src: ' + str(flux_val)

        flux_val = sb_model.solution.x[tgtRxnIdx]
        print 'flux_val of tgt: ' + str(flux_val)

        flux_val = sb_model.solution.x[biomassRxnIdx]
        print 'flux_val of biomass: ' + str(flux_val)

        for i in range(0,numRxns):
            vid = sb_model.reactions[i]
            if not v_lookup.has_key(vid): # if hasn't been seen before
                v = g.add_vertex()
                g.vp['v_id'][v] = vid
                g.vp['v_type'][v] = 'rxn'
                g.vp['v_color'][v] = "#00ff00" #0x00ff00 #[0.,1.0, 0., 1.0] #'g' #0x00ff00
                v_lookup[vid] = v
                v_idx_lookup[vid.id] = g.vertex_index[v]


            met_list = sb_model.S[:,i]
            met_list_nonzero = met_list.nonzero()[0]
            flux_val = sb_model.solution.x[i] # get the flux value from the current FBA run

            # for each metabolite concerning the metabolic enzyme (or reaction) of id 'vid'
            for m in met_list_nonzero:
                metid = sb_model.metabolites[m]
                if not v_lookup.has_key(metid): # if hasn't been seen before
                    v_met = g.add_vertex()      # create a new vertex for the metabolite
                    g.vp['v_id'][v_met] = metid
                    g.vp['v_type'][v_met] = 'met'
                    g.vp['v_color'][v_met] = "#ffff00" #0xffff00 #[0., 0.5, 0.5, 1.0] #'' #0x00ffff
                    v_lookup[metid] = v_met
                    v_idx_lookup[metid.id] = g.vertex_index[v_met]
                else:
                    v_met = v_lookup[metid]

                if flux_val * sb_model.S[m,i] >= 0:
                    e = g.add_edge(v, v_met)
                    g.ep['cap'][e] = flux_val * sb_model.S[m,i]
                else:
                    e = g.add_edge(v_met, v)
                    g.ep['cap'][e] = - flux_val * sb_model.S[m,i]

                g.ep['flux'][e] = flux_val

                if metid == 'cpdADDED_Shinorine_e' or metid == 'cpd00027_e':
                    print 'flux in/out of rxn ' + str(vid) + ' of met ' + str(metid) + ' is: ' + str(flux_val)


        print 'graph reconstruction done'

        #layout_pos = graph_tool.draw.sfdp_layout(g)

        # save the vid vertex idx lookup table
        jsontxt = json.dumps(v_idx_lookup)
        if not is_tgt_prod_enforced:
            filename_prefix = "bsu_met_network_biomass_"
        else:
            filename_prefix = "bsu_met_network_bio+tgt_"

        if is_shiki_on:
            filename_prefix2 = "shikion_"
        else:
            filename_prefix2 = "shikioff_"

        jsonf = open(filename_prefix + filename_prefix2 + str(mediaIdx) + "_vid_vidx.json", "w")
        print >> jsonf, jsontxt
        jsonf.close()

        #g.save("bsu_met_network_glu_shinorine.xml.gz")
        g.save(filename_prefix + filename_prefix2 + str(mediaIdx) + ".xml.gz")
        #with open("v_lookup_dict.json", 'w') as f:
        #    json.dump(v_lookup, f)

        #graph_draw(g, pos=layout_pos, output="bsu_met_network.pdf")







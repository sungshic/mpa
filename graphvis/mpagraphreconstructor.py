__author__ = 'spark'

import csv
import json
# We need to import the graph_tool module itself
from graph_tool.all import Graph

class MPA_FBAStats():
    def __init__(self, datadir, is_new=False):
        if is_new:
            self._stat_outfile = open(datadir+'/stat.csv', "w")
            self._csv_writer = csv.writer(self._stat_outfile, delimiter=',', quotechar="'", quoting=csv.QUOTE_NONNUMERIC)
            # write a header row
            self._csv_writer.writerow(["strain", "experimental condition", "biomass flux", "shinorine flux (mmol/gDCW h)"]) # write the header row
        else:
            self._stat_outfile = open(datadir+'/stat.csv', "a")
            self._csv_writer = csv.writer(self._stat_outfile, delimiter=',', quotechar="'", quoting=csv.QUOTE_NONNUMERIC)

    def __del__(self):
        self._stat_outfile.close()

    def addFBAStat(self, strain_name, media_type, growth_rate, tgt_flux):
        csv_row = [strain_name, media_type, round(growth_rate, 2), round(tgt_flux, 2)]
        self._csv_writer.writerow(csv_row)


class MPALabelledGraph():
    def __init__(self, datadir=None):
        self._datadir = datadir

    def constructLabelledGraphFromCbModel(self, model, srcCmpId, srcRxnId, tgtCmpId='cpdS00140_e', tgtRxnId='rxnS00150'):
        if model.solution.status == "optimal": # and model.solution.f >= 0.05:
            print 'fba solutions: ' + str(model.solution)

            biomassRxnIdx = model.reactions.index('bio00127')
            srcRxnIdx = model.reactions.index(srcRxnId) #'EX_cpd00027_e') #D-Glu uptake
            tgtRxnIdx = model.reactions.index(tgtRxnId) #Shinorine secretion
            srcCmpIdx = model.metabolites.index(srcCmpId) #'cpd00027_e')
            tgtCmpIdx = model.metabolites.index(tgtCmpId)

            # We start with an empty, directed graph

            g = Graph(directed=True)

            self._graph = g
            self._cb_model = model

            g.vp['v_id'] = g.new_vertex_property('string')
            g.vp['v_type'] = g.new_vertex_property('string')
            #g.vp['v_color'] = g.new_vertex_property('vector<float>')
            g.vp['v_color'] = g.new_vertex_property('string')
            g.ep['flux'] = g.new_edge_property('float')
            g.ep['cap'] = g.new_edge_property('float')

            numMets, numRxns = model.S.shape

            v_lookup = {} #initialize
            v_idx_lookup = {} #initialize

            self._v_lookup = v_lookup
            self._v_idx_lookup = v_idx_lookup

            # just so the src vertex has the index 0
            srcv = g.add_vertex()
            vid = model.metabolites[srcCmpIdx]
            g.vp['v_id'][srcv] = vid
            g.vp['v_type'][srcv] = 'met'
            g.vp['v_color'][srcv] = "#5f5f00" #0x0000ff #[0.,0.,1.0, 1.0] #'b' #0x0000FF
            v_lookup[vid] = srcv
            v_idx_lookup[vid.id] = g.vertex_index[srcv]

            # just so the tgt vertex has the index 1
            tgtv = g.add_vertex()
            vid = model.metabolites[tgtCmpIdx]
            g.vp['v_id'][tgtv] = vid
            g.vp['v_type'][tgtv] = 'met'
            g.vp['v_color'][tgtv] = "#ff0000" #0xff0000 #[1.0, 0., 0., 1.0] #'r' #0xff0000
            v_lookup[vid] = tgtv
            v_idx_lookup[vid.id] = g.vertex_index[tgtv]

            src_flux = model.solution.x[srcRxnIdx]
            print 'flux_val of src: ' + str(src_flux)
            self._src_flux = src_flux

            tgt_flux = model.solution.x[tgtRxnIdx]
            print 'flux_val of tgt: ' + str(tgt_flux)
            self._tgt_flux = tgt_flux

            biomass_flux = model.solution.x[biomassRxnIdx]
            print 'flux_val of biomass: ' + str(biomass_flux)
            self._biomass_flux = biomass_flux

            #csv_row_str = [strain_name, mediaIdx, "{:.2f}".format(biomass_flux)  + ", " + "{:.2f}".format(tgt_flux)]
            #csv_row = [strain_name, mediaIdx, round(biomass_flux, 2), round(tgt_flux, 2)]
            #csv_writer.writerow(csv_row)

            for i in range(0,numRxns):
                vid = model.reactions[i]
                #if vid._genes:
                if not v_lookup.has_key(vid): # if hasn't been seen before
                    v = g.add_vertex()
                    g.vp['v_id'][v] = vid
                    g.vp['v_type'][v] = 'rxn'
                    g.vp['v_color'][v] = "#00ff00" #0x00ff00 #[0.,1.0, 0., 1.0] #'g' #0x00ff00
                    v_lookup[vid] = v
                    v_idx_lookup[vid.id] = g.vertex_index[v]


                met_list = model.S[:,i]
                met_list_nonzero = met_list.nonzero()[0]
                flux_val = model.solution.x[i] # get the flux value from the current FBA run

                # for each metabolite concerning the metabolic enzyme (or reaction) of id 'vid'
                for m in met_list_nonzero:
                    metid = model.metabolites[m]
                    if not v_lookup.has_key(metid): # if hasn't been seen before
                        v_met = g.add_vertex()      # create a new vertex for the metabolite
                        g.vp['v_id'][v_met] = metid
                        g.vp['v_type'][v_met] = 'met'
                        g.vp['v_color'][v_met] = "#ffff00" #0xffff00 #[0., 0.5, 0.5, 1.0] #'' #0x00ffff
                        v_lookup[metid] = v_met
                        v_idx_lookup[metid.id] = g.vertex_index[v_met]
                    else:
                        v_met = v_lookup[metid]

                    if flux_val * model.S[m,i] >= 0:
                        e = g.add_edge(v, v_met)
                        g.ep['cap'][e] = flux_val * model.S[m,i]
                    else:
                        e = g.add_edge(v_met, v)
                        g.ep['cap'][e] = - flux_val * model.S[m,i]

                    g.ep['flux'][e] = flux_val

                    #if metid == 'cpdADDED_Shinorine_e' or metid == 'cpd00027_e':
                    #    print 'flux in/out of rxn ' + str(vid) + ' of met ' + str(metid) + ' is: ' + str(flux_val)
                #else: # skipping house keeping reactions
                #    print 'skipping a house keeping rxn: ' + str(vid.id)

            print 'graph reconstruction done'

            return g
        else: # neither a feasible solution exists nor a solution is computed for the given cobra model
            return None


    def saveLabelledGraph(self, filename):
        if self._datadir:
            filename_prefix = self._datadir + filename
        else:
            filename_prefix = filename
        #layout_pos = graph_tool.draw.sfdp_layout(g)

        # save the vid vertex idx lookup table
        jsontxt = json.dumps(self._v_idx_lookup)

        jsonf = open(filename_prefix + "_vid_vidx.json", "w")
        print >> jsonf, jsontxt
        jsonf.close()

        #g.save("bsu_met_network_glu_shinorine.xml.gz")
        self._graph.save(filename_prefix + ".xml.gz")
        #with open("v_lookup_dict.json", 'w') as f:
        #    json.dump(v_lookup, f)

        #graph_draw(g, pos=layout_pos, output="bsu_met_network.pdf")
        #else:
        #    print "No directory is specified for saving the graph..."
        #    raise Exception('MPALabelledGraph.saveLabelledGraph', 'error: no directory specified')



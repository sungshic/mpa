__author__ = 'spark'

# had to change graph_tool.draw.cairo_draw.py to make this script work with setting vertex color with alpha values
# _convert function in cairo_draw.py edited

# We probably will need some things from several places
#import cobra

import json
import pickle
import math
from cobra.io.sbml import *

import matplotlib

from pylab import *  # for plotting
from numpy.random import *  # for random sampling

from numpy import sqrt

from operator import itemgetter

seed(42)

# We need to import the graph_tool module itself
from graph_tool.all import *

# list of hub metabolites in high demand
hub_met_list = {'cpd00001_c', 'cpd00001_e', 'cpd00002_c', 'cpd00003_c', 'cpd00004_c',
                'cpd00005_c', 'cpd00006_c', 'cpd00007_c', 'cpd00007_e', 'cpd00008_c',
                'cpd00009_c', 'cpd00009_e', 'cpd00010_c', 'cpd00011_c', 'cpd00011_e',
                'cpd00012_c', 'cpd00012_e', 'cpd00067_c', 'cpd00067_e', 'cpd00971_e',
                'cpd00971_c'}


## Finds a paths from a source to a sink using a supplied previous node list.
#
# @param previous A list of node predecessors.
# @param node_start The source node of the graph.
# @param node_end The sink node of the graph.
#
# @retval [] Array of nodes if a path is found, an empty list if no path is
# found from the source to sink.
#
def path(graph, previous, node_start, node_end):
    route = []

    node_curr = node_end
    try:
        while True:
            route.append(node_curr)
            #print route
            if graph.vertex(previous[node_curr]) == node_start:
                route.append(node_start)
                break
                #elif previous[node_curr] == DiGraph.UNDEFINDED:
            #    return []

            node_curr = graph.vertex(previous[node_curr])

        route.reverse()
        return route
    except Exception:
        print 'exception: '
        print node_curr
        return []

def find_edge(g, node_from, node_to, cost=None):
    elist = node_to.in_edges()

    for e in elist:
        if node_from == e.source():
            return e

    return None




## @package YenKSP
# Computes K-Shortest Paths using Yen's Algorithm.
#
# Yen's algorithm computes single-source K-shortest loopless paths for a graph
# with non-negative edge cost. The algorithm was published by Jin Y. Yen in 1971
# and implores any shortest path algorithm to find the best path, then proceeds
# to find K-1 deviations of the best path.

## Computes K paths from a source to a sink in the supplied graph.
#
# @param graph A digraph of class Graph.
# @param start The source node of the graph.
# @param sink The sink node of the graph.
# @param K The amount of paths being computed.
#
# @retval [] Array of paths, where [0] is the shortest, [1] is the next
# shortest, and so on.
#
def ksp_yen(g, node_start, node_end, weights=None, max_k=2):

    graph = g #g.copy()

    #distances, previous = dijkstra_search(graph, node_start, weights)
    is_minimized, distances, previous = bellman_ford_search(graph, node_start, weights)

    if graph.vertex(previous[node_end]) == node_end: # no pathway
        A = [{'cost': distances[node_end],
              'path': None}]
        return A

    A = [{'cost': distances[node_end],
          'path': path(graph, previous, node_start, node_end)}]
    B = []

    #print A
    if not A[0]['path']: return A

    for k in range(1, max_k):
        for i in range(0, len(A[-1]['path']) - 1):
            node_spur = A[-1]['path'][i] # going through nodes in the path in reverse order, node_end to node_start
            path_root = A[-1]['path'][:i+1] # path root: nodes list up to and including the node_spur, making up the current path (last path in A)

            edges_removed = []
            for path_k in A:
                curr_path = path_k['path']
                if len(curr_path) > i and path_root == curr_path[:i+1]:
                    #cost = graph.remove_edge(curr_path[i], curr_path[i+1])
                    cur_edge = find_edge(graph, curr_path[i], curr_path[i+1])
                    if cur_edge:
                        cost = weights[cur_edge]
                        graph.remove_edge(cur_edge)
                    else:
                        cost = -1
                        continue

                    edges_removed.append([curr_path[i], curr_path[i+1], cost])

            if weights != None:
                path_spur_vl, path_spur_el = shortest_path(graph, node_spur, node_end, weights)
            else:
                path_spur_vl, path_spur_el = shortest_path(graph, node_spur, node_end)



            if len(path_spur_vl) > 0:
                path_spur = {'cost': sum(map(lambda e: weights[e], path_spur_el)),
                             'path': path_spur_vl}
                print 'path_root: '
                print path_root[:-1]
                print 'path_spur: '
                print path_spur
                path_total = path_root[:-1] + path_spur['path']
                dist_total = distances[node_spur] + path_spur['cost']
                potential_k = {'cost': dist_total, 'path': path_total}

                if not (potential_k in B):
                    B.append(potential_k)

            for edge in edges_removed:
                graph.add_edge(edge[0], edge[1])

        if len(B):
            B = sorted(B, key=itemgetter('cost'))
            A.append(B[0])
            B.pop(0)
        else:
            break

    return A

#from itertools import islice

#"Returns a sliding window (of width n) over data from the iterable"
#"   s -> (s0,s1,...s[n-1]), (s1,s2,...,sn), ...                   "
#def sliding_window(seq, n=2):
#    it = iter(seq)
#    result = tuple(islice(it, n))
#    if len(result) == n:
#        yield result
#    for elem in it:
#        result = result[1:] + (elem,)
#        yield result

from itertools import tee, izip

def sliding_window(iterable, size):
    iters = tee(iterable, size)
    for i in xrange(1, size):
        for each in iters[i:]:
            next(each, None)
    return izip(*iters)

from metpathdictionary import MetPathDict

def preprocess_graph2(g, vlists, metDict, hl_color):

    #print 'source vertex is: ' + g.vp['v_id'][srcv]

    vcolor = g.vp['vcolor']
    vtext = g.vp['vtext']
    vsize = g.vp['vsize']
    vtype = g.vp['v_type']

    ecolor = g.ep['ecolor']
    ewidth = g.ep['ewidth']
    efluxstr = g.ep['fluxstr']
    cap = g.ep['cap']

    #vcolor[v] = "#ff0000"
    #vcolor[srcv] = "#55ff00, 1.0" #[1.0,0.,0.,1.0]
    # src vertex is most certainly a compound id not a rxn id
    #srcv_aliases, compartment_tag = metDict.lookupAliasesByID(g.vp['v_id'][srcv])

    #if compartment_tag:
    #    vtext[srcv] = srcv_aliases[0] + '_' + compartment_tag #g.vp['v_id'][curv]
    #else:
    #    vtext[srcv] = srcv_aliases[0]
    #vsize[srcv] = 5
    #p = v

    vlists.reverse() # primary srcv to tgtv last so the path stays colored in red all the way through
    for vlist in vlists:
        startv = vlist[0]
        # process in edges of the starting vertex
        edgelist = list(startv.in_edges())
        edgelist = [e for e in edgelist if cap[e]>0] # get non-zero flux edge list
        listlen = len(edgelist)
        edgelist_sorted = sorted(edgelist, key=lambda e: cap[e], reverse=True) #shortest_distance(g, e.source(), tgtv))

        for idx, e in enumerate(edgelist_sorted):
            curv = e.source()
            if idx < 5 and g.vp['v_id'][curv] not in hub_met_list: ####and vtype[startv] == 'met':
                #if listlen < 5:
                ecolor[e] = "#afaf00, 0.5"
                efluxstr[e] = "{:.3e}".format(cap[e]) #"{0:.2f}".format(flux[e])
                #ewidth[e] += 1
                #ewidth[e] *=3
                #else:
                #    ecolor[e] = "#110000, 0.1"
                valiases, compartment_tag = metDict.lookupAliasesByID(g.vp['v_id'][curv])
                if compartment_tag:
                    vtext[curv] = valiases[0] + '_' + compartment_tag #g.vp['v_id'][curv]
                else:
                    vtext[curv] = valiases[0]
                vsize[curv] = 3
                vcolor[curv] = "#555555, 0.2"

        for each in sliding_window(vlist, 2):
            v1 = each[0]
            v2 = each[1]
            #p = g.vertex(pred[v])


            #vcolor[p] = "#000000"
            #vcolor[p] = "#000000, 0.2" #[0.,0.,0.,0.5]v
            p = v1

            print 'current vertex is: ' + g.vp['v_id'][p]
            vcolor[p] = "#555555, 0.2"
            curedge = find_edge(g, v1, v2)
            print 'current edge between v1, v2: ' + str(curedge)

            # process out edges of the source vertex
            edgelist = list(v1.out_edges())
            edgelist = [e for e in edgelist if cap[e]>1e-9] # get non-zero flux edge list, zero cutoff threshold being 1e-9
            listlen = len(edgelist)
            edgelist_sorted = sorted(edgelist, key=lambda e: cap[e], reverse=True) #shortest_distance(g, e.source(), tgtv))

            for idx, e in enumerate(edgelist_sorted):
                curv = e.target()
                if idx < 5 and curv != v2 and g.vp['v_id'][curv] not in hub_met_list: ####and vtype[v1] == 'met':
                    #if listlen < 5:
                    ecolor[e] = "#00afaf, 0.5"
                    efluxstr[e] = "{:.3e}".format(cap[e]) #"{0:.2f}".format(flux[e])
                    #ewidth[e] += 1
                    #ewidth[e] *=3
                    #else:
                    #    ecolor[e] = "#110000, 0.1"
                    valiases, compartment_tag = metDict.lookupAliasesByID(g.vp['v_id'][curv])
                    if compartment_tag:
                        vtext[curv] = valiases[0] + '_' + compartment_tag #g.vp['v_id'][curv]
                    else:
                        vtext[curv] = valiases[0]
                    vsize[curv] = 3
                    vcolor[curv] = "#555555, 0.2"


            # process in edges of the target vertex
            edgelist = list(v2.in_edges())
            edgelist = [e for e in edgelist if cap[e]>1e-9] # get non-zero flux edge list
            listlen = len(edgelist)
            #edgelist_sorted = sorted(edgelist, key=lambda e: cap[e], reverse=True)
            #edgelist_sorted = sorted(edgelist, key=lambda e: cap[e])
            edgelist_sorted = sorted(edgelist, key=lambda e: cap[e], reverse=True) #shortest_distance(g, e.source(), tgtv))

            #p = edgelist_sorted[0].source()

            #print edgelist_sorted
            for idx, e in enumerate(edgelist_sorted):
                curv = e.source()

                if curv == v1:
                    print 'yeah! ' + "{:.3e}".format(cap[e])
                    print e
                    #ecolor[e] = "#a40000"
                    ecolor[e] = hl_color #"#af0000, 1.0" #[0.8,0.,0.,1.0]
                    #ewidth[e] += 1
                    #ewidth[e] *= 3
                    valiases, compartment_tag = metDict.lookupAliasesByID(g.vp['v_id'][curv])
                    if compartment_tag:
                        vtext[curv] = valiases[0] + '_' + compartment_tag #g.vp['v_id'][curv]
                    else:
                        vtext[curv] = valiases[0]
                    vsize[curv] = 5
                    efluxstr[e] = "{:.3e}".format(cap[e]) #"{0:.2f}".format(flux[e])
                else:
                    if idx < 5 and g.vp['v_id'][curv] not in hub_met_list: ####and vtype[v2] == 'met':
                        #if listlen < 5:
                        ecolor[e] = "#afaf00, 0.5"
                        efluxstr[e] = "{:.3e}".format(cap[e]) #"{0:.2f}".format(flux[e])
                        #ewidth[e] += 1
                        #ewidth[e] *=3
                        #else:
                        #    ecolor[e] = "#110000, 0.1"
                        valiases, compartment_tag = metDict.lookupAliasesByID(g.vp['v_id'][curv])
                        if compartment_tag:
                            vtext[curv] = valiases[0] + '_' + compartment_tag #g.vp['v_id'][curv]
                        else:
                            vtext[curv] = valiases[0]
                        vsize[curv] = 3
                        vcolor[curv] = "#555555, 0.2"

        # deal with the last vertex in the list (the final target vertex)
        vcolor[v2] = "#ff0000, 1.0" #[0.8,1.0,0.,1.0]
        valiases, compartment_tag = metDict.lookupAliasesByID(g.vp['v_id'][v2])
        if compartment_tag:
            vtext[v2] = valiases[0] + '_' + compartment_tag #g.vp['v_id'][curv]
        else:
            vtext[v2] = valiases[0]
        vsize[v2] = 5

        # process out edges of the final target vertex
        edgelist = list(v2.out_edges())
        edgelist = [e for e in edgelist if cap[e]>1e-9] # get non-zero flux edge list
        listlen = len(edgelist)
        edgelist_sorted = sorted(edgelist, key=lambda e: cap[e], reverse=True) #shortest_distance(g, e.source(), tgtv))

        for idx, e in enumerate(edgelist_sorted):
            curv = e.target()
            if idx < 5 and g.vp['v_id'][curv] not in hub_met_list: ####and vtype[v2] == 'met':
                #if listlen < 5:
                ecolor[e] = "#00afaf, 0.5"
                efluxstr[e] = "{:.3e}".format(cap[e]) #"{0:.2f}".format(flux[e])
                #ewidth[e] += 1
                #ewidth[e] *=3
                #else:
                #    ecolor[e] = "#110000, 0.1"
                valiases, compartment_tag = metDict.lookupAliasesByID(g.vp['v_id'][curv])
                if compartment_tag:
                    vtext[curv] = valiases[0] + '_' + compartment_tag #g.vp['v_id'][curv]
                else:
                    vtext[curv] = valiases[0]
                vsize[curv] = 3
                vcolor[curv] = "#555555, 0.2"


    return g

def saveGraphLayoutCoordinates(g, layout_pos, outfilepath):
    coordLookup = {}
    for v in g.vertices():
        coord = layout_pos[v]
        if coord:
            coordlist = [str(coord[0]),str(coord[1])] #list(array(coord.a,dtype=str))
            pickled_value = pickle.dumps(coordlist) # pickle the coord data in 'numpy.ndarray' type in python type 'list' of 'str'
            coordLookup[g.vp['v_id'][v]] = pickled_value

    jsonf = open(outfilepath, 'w')
    jsondata = json.dumps(coordLookup)
    print >> jsonf, jsondata
    jsonf.close()

    return coordLookup

def loadGraphLayoutCoordinates(g, vidx_lookup, layout_pos, infilepath):
    jsonf = open(infilepath)
    jsondata = json.loads(jsonf.read())
    for key in jsondata.keys():
        try:
            curv = g.vertex(vidx_lookup[key])
            unpickled_value = pickle.loads(jsondata[key]) # this restores the value of python type 'list'
            layout_pos[curv] = array(unpickled_value, dtype=float64)
        except KeyError:
            print 'key: ' + str(key) + ' ignored.'

    jsonf.close()

    return layout_pos

def preprocess_graph(g, srcv, tgtv, vlists, metDict, hl_color):

    print 'source vertex is: ' + g.vp['v_id'][srcv]

    vcolor = g.vp['vcolor']
    vtext = g.vp['vtext']
    vsize = g.vp['vsize']
    vtype = g.vp['v_type']

    ecolor = g.ep['ecolor']
    ewidth = g.ep['ewidth']
    efluxstr = g.ep['fluxstr']
    cap = g.ep['cap']

    #vcolor[v] = "#ff0000"
    vcolor[srcv] = "#55ff00, 1.0" #[1.0,0.,0.,1.0]
    # src vertex is most certainly a compound id not a rxn id
    srcv_aliases, compartment_tag = metDict.lookupAliasesByID(g.vp['v_id'][srcv])

    if compartment_tag:
        vtext[srcv] = srcv_aliases[0] + '_' + compartment_tag #g.vp['v_id'][curv]
    else:
        vtext[srcv] = srcv_aliases[0]
    vsize[srcv] = 5
    #p = v

    vlists.reverse() # primary srcv to tgtv last so the path stays colored in red all the way through
    for vlist in vlists:
        for each in sliding_window(vlist, 2):
            v1 = each[0]
            v2 = each[1]
            #p = g.vertex(pred[v])


            #vcolor[p] = "#000000"
            #vcolor[p] = "#000000, 0.2" #[0.,0.,0.,0.5]v
            p = v1

            print 'current vertex is: ' + g.vp['v_id'][p]
            vcolor[p] = "#555555, 0.2"
            curedge = find_edge(g, v1, v2)
            print 'current edge between v1, v2: ' + str(curedge)

            # process out edges of the source vertex
            edgelist = list(v1.out_edges())
            edgelist = [e for e in edgelist if cap[e]>0] # get non-zero flux edge list
            listlen = len(edgelist)
            edgelist_sorted = sorted(edgelist, key=lambda e: cap[e], reverse=True) #shortest_distance(g, e.source(), tgtv))

            for idx, e in enumerate(edgelist_sorted):
                curv = e.target()
                if idx < 5 and curv != v2 and vtype[v1] == 'met':
                    #if listlen < 5:
                    ecolor[e] = "#00afaf, 0.5"
                    efluxstr[e] = "{:.3e}".format(cap[e]) #"{0:.2f}".format(flux[e])
                    #ewidth[e] += 1
                    #ewidth[e] *=3
                    #else:
                    #    ecolor[e] = "#110000, 0.1"
                    valiases, compartment_tag = metDict.lookupAliasesByID(g.vp['v_id'][curv])
                    if compartment_tag:
                        vtext[curv] = valiases[0] + '_' + compartment_tag #g.vp['v_id'][curv]
                    else:
                        vtext[curv] = valiases[0]
                    vsize[curv] = 3
                    vcolor[curv] = "#555555, 0.2"


            # process in edges of the target vertex
            edgelist = list(v2.in_edges())
            edgelist = [e for e in edgelist if cap[e]>1e-9] # get non-zero flux edge list, zero cutoff threshold being 1e-9
            listlen = len(edgelist)
            #edgelist_sorted = sorted(edgelist, key=lambda e: cap[e], reverse=True)
            #edgelist_sorted = sorted(edgelist, key=lambda e: cap[e])
            edgelist_sorted = sorted(edgelist, key=lambda e: cap[e], reverse=True) #shortest_distance(g, e.source(), tgtv))

            #p = edgelist_sorted[0].source()

            #print edgelist_sorted
            for idx, e in enumerate(edgelist_sorted):
                curv = e.source()

                if curv == v1:
                    print 'yeah! ' + "{:.3e}".format(cap[e])
                    print e
                    #ecolor[e] = "#a40000"
                    ecolor[e] = hl_color #"#af0000, 1.0" #[0.8,0.,0.,1.0]
                    #ewidth[e] += 1
                    #ewidth[e] *= 3
                    valiases, compartment_tag = metDict.lookupAliasesByID(g.vp['v_id'][curv])
                    if compartment_tag:
                        vtext[curv] = valiases[0] + '_' + compartment_tag #g.vp['v_id'][curv]
                    else:
                        vtext[curv] = valiases[0]
                    vsize[curv] = 5
                    efluxstr[e] = "{:.3e}".format(cap[e]) #"{0:.2f}".format(flux[e])
                else:
                    if idx < 5 and vtype[v2] == 'met':
                        #if listlen < 5:
                        ecolor[e] = "#afaf00, 0.5"
                        efluxstr[e] = "{:.3e}".format(cap[e]) #"{0:.2f}".format(flux[e])
                        #ewidth[e] += 1
                        #ewidth[e] *=3
                        #else:
                        #    ecolor[e] = "#110000, 0.1"
                        valiases, compartment_tag = metDict.lookupAliasesByID(g.vp['v_id'][curv])
                        if compartment_tag:
                            vtext[curv] = valiases[0] + '_' + compartment_tag #g.vp['v_id'][curv]
                        else:
                            vtext[curv] = valiases[0]
                        vsize[curv] = 3
                        vcolor[curv] = "#555555, 0.2"
    #                else:
    #                    if e.source() == g.vertex(1603) and e.target() == g.vertex(104):
    #                        print 'got here, very strange 2!'
    #                    ecolor[e] = "#000000, 0.1"
    #                    vtext[curv] = g.vp['v_id'][curv]
    #                    vcolor[curv] = "#000000, 0.1"
    #                    efluxstr[e] = "{:.3e}".format(cap[e])


    #vcolor[g.vertex(0)] = "#55ff00"

    # deal with the last vertex in the list (the final target vertex)
    vcolor[tgtv] = "#ff0000, 1.0" #[0.8,1.0,0.,1.0]
    valiases, compartment_tag = metDict.lookupAliasesByID(g.vp['v_id'][tgtv])
    if compartment_tag:
        vtext[tgtv] = valiases[0] + '_' + compartment_tag #g.vp['v_id'][curv]
    else:
        vtext[tgtv] = valiases[0]
    vsize[tgtv] = 5

    # process out edges of the final target vertex
    edgelist = list(tgtv.out_edges())
    edgelist = [e for e in edgelist if cap[e]>0] # get non-zero flux edge list
    listlen = len(edgelist)
    edgelist_sorted = sorted(edgelist, key=lambda e: cap[e], reverse=True) #shortest_distance(g, e.source(), tgtv))

    for idx, e in enumerate(edgelist_sorted):
        curv = e.target()
        if idx < 5 and vtype[tgtv] == 'met':
            #if listlen < 5:
            ecolor[e] = "#00afaf, 0.5"
            efluxstr[e] = "{:.3e}".format(cap[e]) #"{0:.2f}".format(flux[e])
            #ewidth[e] += 1
            #ewidth[e] *=3
            #else:
            #    ecolor[e] = "#110000, 0.1"
            valiases, compartment_tag = metDict.lookupAliasesByID(g.vp['v_id'][curv])
            if compartment_tag:
                vtext[curv] = valiases[0] + '_' + compartment_tag #g.vp['v_id'][curv]
            else:
                vtext[curv] = valiases[0]
            vsize[curv] = 3
            vcolor[curv] = "#555555, 0.2"

    return g
# end of preprocess_graph def

def draw_graph_vlist(graph, vlists, filename_base, layout_pos_file=None):
    #g = graph#.copy()
    #srcv, tgtv = g.vertex(0), g.vertex(1)


    metDict = MetPathDict('../data/cpdlist2.csv', '../data/rxnlist2.csv')
    metDict2 = MetPathDict('../data/cpdlist2.csv', '../data/rxnlist_nar.csv')
    metDict.mergeRxnLookupTable(metDict2._rxnDict)

    srcv_idx = vidx_lookup['cpd00079_c']
    tgtv_idx = vidx_lookup['cpdS00140_c']
    #srcv, tgtv = g.vertex(0), g.vertex(1)
    srcv, tgtv = g.vertex(srcv_idx), g.vertex(tgtv_idx)

    #graph = preprocess_graph(graph, srcv, tgtv, vlists, metDict, "#af0000, 1.0")
    graph = preprocess_graph2(graph, vlists, metDict, "#af0000, 1.0")

    graph.save(filename_base + "_" + str(hash(str(vlists))) + ".xml.gz")
    u = GraphView(g, vfilt=lambda v: vtext[v] != "", efilt=lambda e: ecolor[e] != "")

    #u.save("bsu_met_network++_filtered.xml.gz")

    #for e in u.edges():
    #    if ecolor[e] == "":
    #        ecolor[e] = "#ff0000, 0.2"
    #        efluxstr[e] = cap[e]
    #    ecolor[e] = "#0000ff, 0.1" if touch_e[e] else "#595959, 0.1"
    #ecolor[e] = "blue" if touch_e[e] else "black"

    if is_save_layoutfile:
        layout_pos = arf_layout(u, max_iter=0)#sfdp_layout(u)
        saveGraphLayoutCoordinates(graph, layout_pos, layout_pos_file)
    else:
        if layout_pos_file:
            layout_pos = loadGraphLayoutCoordinates(graph, vidx_lookup, random_layout(u), layout_pos_file)
        layout_pos = arf_layout(u, pos=layout_pos)#random_layout(u)

        if layout_pos_file:
            layout_pos = loadGraphLayoutCoordinates(graph, vidx_lookup, layout_pos, layout_pos_file)


    for e in u.edges():
        if ecolor[e] == "":
            print e
        d = sqrt(sum((layout_pos[e.source()].a - layout_pos[e.target()].a) ** 2)) / 5
        control[e] = [0.3, d, 0.7, d]


    deg = u.degree_property_map("in")
    deg.a = 4 * (sqrt(deg.a) * 0.5 + 0.4) + 3

    print 'drawing graph...'
    graph_draw(u, pos=layout_pos, output_size=(1024, 768), vertex_size=deg, vertex_font_size=6, vertex_text=u.vp['vtext'], vertex_text_position=0, vertex_fill_color=u.vp['vcolor'], #touch_v,
               vcmap=matplotlib.cm.binary, edge_color=u.ep['ecolor'],
               edge_control_points=control, # some curvy edges
               edge_pen_width=u.ep['ewidth'],
               edge_text = u.ep['fluxstr'],
               text_distance=0,
               edge_font_size=4,
               output=filename_base+"_"+str(hash(str(vlists)))+".pdf")
    print 'drawing graph done!'

    return g

def h(v, target, pos):
    return sqrt(sum((pos[v].a - pos[target].a) ** 2))

'''
class VisitorExample(graph_tool.search.AStarVisitor):
    def __init__(self, touched_v, touched_e, target):
        self.touched_v = touched_v
        self.touched_e = touched_e
        self.target = target

    def discover_vertex(self, u):
        #print 'vt:' + g.vp['v_id'][u]
        self.touched_v[u] = True

    def examine_edge(self, e):
        #print 'et:' + g.ep['v_id'][u]
        self.touched_e[e] = True

    def edge_relaxed(self, e):
        if e.target() == self.target:
            #print 'Ahoy!'
            self.examine_edge(e)
            self.discover_vertex(self.target)
            raise graph_tool.search.StopSearch()
'''

class VisitorExample(graph_tool.search.DijkstraVisitor):

    def __init__(self, name, time):
        self.name = name
        self.time = time
        self.last_time = 0

    def discover_vertex(self, u):
        print("-->", self.name[u], "has been discovered!")
        self.time[u] = self.last_time
        self.last_time += 1

    def examine_edge(self, e):
        print("edge (%s, %s) has been examined..." % \
            (self.name[e.source()], self.name[e.target()]))

    def edge_relaxed(self, e):
        print("edge (%s, %s) has been relaxed..." % \
            (self.name[e.source()], self.name[e.target()]))


#graph_file = "bsu_met_network_glu_dhquinate.xml.gz"
#graph_file = "bsu_met_network_glu_ddg.xml.gz"
#graph_file = "shikimate_off/bsu_met_network_glu_dhquinate_1.xml.gz"
#graph_lookup_file = "shikimate_off/bsu_met_network_glu_dhquinate_1_vid_vidx.json"
#filenamebase = "bsu_met_network_ex1_shikioff"
graph_file = "bsu_met_network_biomass_1_shikioff.xml.gz"
graph_lookup_file = "bsu_met_network_biomass_1_shikioff_vid_vidx.json"
filenamebase = "bsu_met_network_biomass_ex1_shikioff"
is_save_layoutfile = True


graph_file = "wt_oxy1.5_dual_shikioff/bsu_met_network_bio+tgt_shikioff_0.xml.gz"
graph_lookup_file = "wt_oxy1.5_dual_shikioff/bsu_met_network_bio+tgt_shikioff_0_vid_vidx.json"
filenamebase = "wt_oxy1.5_dual_shikioff/bsu_met_network_bio+tgt_shikioff_0"
layout_pos_file = 'wt_oxy1.5_dual_shikioff/bsu_met_network_bio+tgt+shikioff_0_layoutcoord.json'
is_save_layoutfile = True


# We start with an empty, directed graph


g = load_graph(graph_file)
jsonf = open(graph_lookup_file)
vidx_lookup = json.loads(jsonf.read())


print 'graph reconstruction done'


#cpd00020_c: pyruvate
#cpd00051_e: L-arginine
fixed_met_list = {'cpd00699_c', 'cpd00020_c', 'cpd00051_e', 'cpdS00130_c', 'cpdS00110_c'}
otherpaths_list = [
                    ['cpd00130_e', 'cpdS00140_c'], # L-malate, Shinorine
                    ['cpd00022_c','cpd00029_c'], # Acetyl-CoA, Acetate
                    ['cpd00020_c','cpd00022_c'], # Pyruvate, Acetyl-CoA
                    ['cpd00023_c','cpd00024_c'], # L-glutamate, 2-Oxoglutarate
                    ['cpd00053_c','cpd00024_c'], # L-glutamine, 2-Oxoglutarate
                    ['cpd00079_c','cpd00238_c'], # Glucose-6-phosphate, Sedoheptulose-7-phosphat
                    ['cpd00238_c','cpdS00110_c'], # Sedoheptulose-7-phosphate, 4-deoxygadusol
                    ['cpd00061_c','cpd00238_c'], # Phosphoenolpyruvate, sedoheptulose-7-p
                    ['cpd00061_c','cpd00699_c'], # Phosphoenolpyruvate, 3-dehydroquinate
                    ['cpd00079_c','cpd00699_c'], # Glucose-6-phosphate, 3-dehydroquinate
                    #['cpd00699_c','cpd00216_c'], # 3-dehydroquinatee, chorismate
                    ['cpd00020_c','cpd00699_c'], # Pyruvate, 3-dehydroquinate
                    ['cpd00238_c','cpdS00150_c'] # Sedoheptulose-7-phosphate, 2-epi-5-epi-valiolone
                  ]
critical_node_list = {'cpd00699_c'} # 3-dehydroquinate

flux = g.ep['flux']
srcv_idx = vidx_lookup['cpd00079_c']
tgtv_idx = vidx_lookup['cpdS00140_c']
#srcv, tgtv = g.vertex(0), g.vertex(1)
srcv, tgtv = g.vertex(srcv_idx), g.vertex(tgtv_idx)
print 'srcv: ' + g.vp['v_id'][srcv]
print 'tgtv: ' + g.vp['v_id'][tgtv]
cap = g.ep['cap']

#res = edmonds_karp_max_flow(g, srcv, tgtv, cap)
#res.a = cap.a - res.a
#max_flow_val = sum(res[e] for e in tgtv.in_edges())
#print "max flow val: " + str(max_flow_val)

outdeg = g.degree_property_map("out")
weight2 = cap.copy()
#weight1.a = (weight1.a*1.0/weight1.a.max())**2.0
weight2.a = weight2.a/weight2.a.max()
#(eflux.a/(eflux.a.max()/1.0))**2.0
#weight.a = 1/(1+abs(weight.a))
weight2.a = 1/(1+weight2.a)
#weight = prop_to_size(weight, mi=0, ma=weight.a.max(), power=1),

weight1 = outdeg
weight1.a = weight1.a/weight1.a.max()

#weight1.a = weight1.a * 10 + weight2.a

weight = g.new_edge_property("float")
edgewidth = g.new_edge_property("float")

#weight.a = (10.0 * weight1.a) + weight2.a
#for e in g.edges():
#    vt = e.target()
#    weight[e] = weight1[vt] * 10 + weight2[e]

for e in g.edges():
    vt = e.target()
#    vt_out = vt.out_edges()
    if g.vp['v_id'][vt] in hub_met_list:
        hubfactor = 1.0
    else:
        hubfactor = 0.0

    #if g.vp['v_id'][vt] not in fixed_met_list:
    #    fixfactor = 1.0
    #else:
    #    fixfactor = 0.0

    #if g.vp['v_id'][vt] in fixed_met_list:
    #    fixedfactor = 0.0
    #else:
    #    fixedfactor = 1.0

    weight[e] = hubfactor * 1000 + weight1[vt] * 10 + weight2[e] #+ 1000 * fixfactor)*fixfactor
    edgewidth[e] = math.log(abs(flux[e])+5.0)#weight1[vt] * 10 + weight2[e]

    #weight[e] = fixedfactor * 10 + hubfactor * 100 + weight1[vt] * 10 + weight2[e]

touch_v = g.new_vertex_property("bool")
touch_e = g.new_edge_property("bool")

#vcolor = g.new_vertex_property("string")
vcolor = g.new_vertex_property("string") # arbitrary object list #g.new_vertex_property("vector<float>")
#vcolor.a = "#ffffff" #initialize
vtext = g.new_vertex_property("string")
vsize = g.new_vertex_property("float")
vsize.a = 1
g.vp['vtext'] = vtext
g.vp['vcolor'] = vcolor
g.vp['vsize'] = vsize

target = g.vertex(1)

#layout_pos = sfdp_layout(g, C=1000, p=10)
#layout_pos = arf_layout(g, max_iter=1000)
#layout_pos = fruchterman_reingold_layout(g, n_iter=1000)
shape = [[1,6000], [1,6000], 4]
layout_pos = random_layout(g, shape=shape, dim=3)
control = g.new_edge_property("vector<double>")

for e in g.edges():
    d = sqrt(sum((layout_pos[e.source()].a - layout_pos[e.target()].a) ** 2)) / 5
    control[e] = [0.3, d, 0.7, d]

#dist, pred = astar_search(g, g.vertex(0), weight,
#                          VisitorExample(touch_v, touch_e, target),
#                          heuristic=lambda v: h(v, target, layout_pos))

#time = g.new_vertex_property("int")
#dist, pred = dijkstra_search(g, g.vertex(0), weight,
#                             VisitorExample(vtext, time))


efluxstr = g.new_edge_property("string")
ecolor = g.new_edge_property("string")
ewidth = g.new_edge_property("double")
ewidth.a = edgewidth.a

g.ep['ecolor'] = ecolor
g.ep['ewidth'] = ewidth
g.ep['fluxstr'] = efluxstr

#for v in g.vertices():
#    vcolor[v] = "#000000, 1.0" if touch_v[v] else "#595959, 0.1"
    #vcolor[v] = "black" if touch_v[v] else "white"


#for e in g.edges():
#    ecolor[e] = "#0000ff, 0.1" if touch_e[e] else "#595959, 0.1"
    #ecolor[e] = "blue" if touch_e[e] else "black"


kshortest = ksp_yen(g, srcv, tgtv, weights=weight, max_k=6)
print 'kshortest: ' + str(kshortest)

vlists = []
vlist1 = kshortest[0]['path']
if vlist1:
    vlists.append(vlist1)
for metid in fixed_met_list:
    cursrcv = g.vertex(vidx_lookup[metid])
    kshortest = ksp_yen(g, cursrcv, tgtv, weights=weight, max_k=1)
    vlist_ext = kshortest[0]['path']

    if vlist_ext:
        # append vlist_ext to vlists
        vlists.append(vlist_ext)
        # merge vlist_ext without duplicates into vlist
        #vlist = vlist + list(set(vlist_ext) - set(vlist))

for metids in otherpaths_list:
    cursrcv = g.vertex(vidx_lookup[metids[0]])
    curtgtv = g.vertex(vidx_lookup[metids[1]])
    kshortest = ksp_yen(g, cursrcv, curtgtv, weights=weight, max_k=1)
    vlist_ext = kshortest[0]['path']

    if vlist_ext:
        # append vlist_ext to vlists
        vlists.append(vlist_ext)

#print vlist

layout_pos_file = 'layoutcoord.json'
draw_graph_vlist(g, vlists, filenamebase, layout_pos_file)
#from subprocess import call

#call(["open", "met_path_"+str(hash(str(vlist)))+".pdf"])

'''
print 'source vertex is: ' + g.vp['v_id'][srcv]
#vcolor[v] = "#ff0000"
vcolor[srcv] = "#55ff00, 1.0" #[1.0,0.,0.,1.0]
vtext[srcv] = g.vp['v_id'][srcv]
vsize[srcv] = 5
p = v

for each in sliding_window(vlist, 2):
    v1 = each[0]
    v2 = each[1]
    #p = g.vertex(pred[v])


    #vcolor[p] = "#000000"
    #vcolor[p] = "#000000, 0.2" #[0.,0.,0.,0.5]v
    p = v1

    print 'current vertex is: ' + g.vp['v_id'][p]
    curedge = find_edge(g, v1, v2)
    edgelist = list(v2.in_edges())
    edgelist = [e for e in edgelist if cap[e]>0] # get non-zero flux edge list
    listlen = len(edgelist)
    #edgelist_sorted = sorted(edgelist, key=lambda e: cap[e], reverse=True)
    #edgelist_sorted = sorted(edgelist, key=lambda e: cap[e])
    edgelist_sorted = sorted(edgelist, key=lambda e: shortest_distance(g, srcv, e.source()))

    #p = edgelist_sorted[0].source()

    print edgelist_sorted
    for idx, e in enumerate(edgelist_sorted):
        curv = e.source()

        if curv == v1:

            print 'yeah! ' + "{:.3e}".format(cap[e])
            print e
            #ecolor[e] = "#a40000"
            ecolor[e] = "#af0000, 1.0" #[0.8,0.,0.,1.0]
            ewidth[e] += 1
            ewidth[e] *= 3
            vtext[p] = g.vp['v_id'][p]
            vsize[p] = 5

            efluxstr[e] = "{:.3e}".format(cap[e]) #"{0:.2f}".format(flux[e])
        else:
            if idx < 5:
                #if listlen < 5:
                ecolor[e] = "#afaf00, 0.5"
                efluxstr[e] = "{:.3e}".format(cap[e]) #"{0:.2f}".format(flux[e])
                ewidth[e] += 1
                ewidth[e] *=3
                #else:
                #    ecolor[e] = "#110000, 0.1"
                vtext[curv] = g.vp['v_id'][curv]
                vsize[p] = 3
            else:
                ecolor[e] = "#000000, 0.1"


#vcolor[g.vertex(0)] = "#55ff00"

vcolor[tgtv] = "#ff0000, 1.0" #[0.8,1.0,0.,1.0]
vtext[tgtv] = g.vp['v_id'][tgtv]
vsize[tgtv] = 5

print 'target vertex is: ' + g.vp['v_id'][g.vertex(1)]

g.save("bsu_met_network++.xml.gz")

u = GraphView(g, vfilt=lambda v: vtext[v] != "")
u.save("bsu_met_network++_filtered.xml.gz")

layout_pos = sfdp_layout(u)

for e in u.edges():
    d = sqrt(sum((layout_pos[e.source()].a - layout_pos[e.target()].a) ** 2)) / 5
    control[e] = [0.3, d, 0.7, d]

deg = u.degree_property_map("in")
deg.a = 4 * (sqrt(deg.a) * 0.5 + 0.4)


graph_draw(u, pos=layout_pos, output_size=(600, 600), vertex_size=deg, vertex_font_size=6, vertex_text=u.vp['vtext'], vertex_text_position=0, vertex_fill_color=u.vp['vcolor'], #touch_v,
           vcmap=matplotlib.cm.binary, edge_color=u.ep['ecolor'],
           edge_control_points=control, # some curvy edges
           edge_pen_width=u.ep['ewidth'],
           edge_text = u.ep['fluxstr'],
           edge_font_size=4,
           output="astar-delaunay_filtered.pdf")

from subprocess import call
call(["open", "astar-delaunay_filtered.pdf"])

interactive_window(u, pos=layout_pos, geometry=(500,500), vertex_size=deg, vertex_font_size=8, vertex_text=u.vp['vtext'], vertex_text_position=0, vertex_fill_color=u.vp['vcolor'], #touch_v
                   edge_color=u.ep['ecolor'],
                   edge_control_points=control, # some curvy edges
                   edge_pen_width=u.ep['ewidth'],
                   edge_text = u.ep['fluxstr'],
                   edge_font_size=7,
                   update_layout=False,
                   vcmap=matplotlib.cm.binary)

'''
'''
v = target
print 'target vertex is: ' + g.vp['v_id'][v]
#vcolor[v] = "#ff0000"
vcolor[v] = "#ff0000, 1.0" #[1.0,0.,0.,1.0]
vtext[v] = g.vp['v_id'][v]
vsize[v] = 5
p = v
while v != srcv:
    #p = g.vertex(pred[v])


    #vcolor[p] = "#000000"
    #vcolor[p] = "#000000, 0.2" #[0.,0.,0.,0.5]v

    print 'current vertex is: ' + g.vp['v_id'][p]
    edgelist = list(v.in_edges())
    edgelist = [e for e in edgelist if cap[e]>0] # get non-zero flux edge list
    listlen = len(edgelist)
    #edgelist_sorted = sorted(edgelist, key=lambda e: cap[e], reverse=True)
    #edgelist_sorted = sorted(edgelist, key=lambda e: cap[e])
    edgelist_sorted = sorted(edgelist, key=lambda e: shortest_distance(g, srcv, e.source()))

    rankofchoice = 3
    if (listlen < rankofchoice):
        rankofchoice = listlen -1
    else:
        rankofchoice = rankofchoice -1

    p = edgelist_sorted[rankofchoice].source()

    for idx, e in enumerate(edgelist_sorted):
        curv = e.source()
        sd_graph = shortest_distance(g, srcv, curv)
        print 'current vertex is: ' + g.vp['v_id'][curv] + ' and sd to target: ' + str(sd_graph)
        #if idx == rankofchoice or curv == srcv: #curv == p:
        touch_e[e] = True

        if curv == p:

            touch_v[curv] = True
            print 'yeah!'
            #ecolor[e] = "#a40000"
            ecolor[e] = "#af0000, 1.0" #[0.8,0.,0.,1.0]
            ewidth[e] += 1
            ewidth[e] *= 3
            vtext[p] = g.vp['v_id'][p]
            vsize[p] = 5

            efluxstr[e] = "{:.3e}".format(cap[e]) #"{0:.2f}".format(flux[e])
        else:
            if idx < 5 and idx != rankofchoice:
                #if listlen < 5:
                ecolor[e] = "#afaf00, 0.5"
                efluxstr[e] = "{:.3e}".format(cap[e]) #"{0:.2f}".format(flux[e])
                ewidth[e] += 1
                ewidth[e] *=3
                #else:
                #    ecolor[e] = "#110000, 0.1"
                vtext[curv] = g.vp['v_id'][curv]
                vsize[p] = 3

    v = p

#vcolor[g.vertex(0)] = "#55ff00"

vcolor[srcv] = "#55ff00, 1.0" #[0.8,1.0,0.,1.0]
vtext[srcv] = g.vp['v_id'][srcv]
vsize[srcv] = 5

print 'src vertex is: ' + g.vp['v_id'][g.vertex(0)]

g.save("bsu_met_network++.xml.gz")

u = GraphView(g, vfilt=lambda v: vtext[v] != "")
u.save("bsu_met_network++_filtered.xml.gz")

layout_pos = sfdp_layout(u)

for e in u.edges():
    d = sqrt(sum((layout_pos[e.source()].a - layout_pos[e.target()].a) ** 2)) / 5
    control[e] = [0.3, d, 0.7, d]

deg = u.degree_property_map("in")
deg.a = 4 * (sqrt(deg.a) * 0.5 + 0.4)


graph_draw(u, pos=layout_pos, output_size=(600, 600), vertex_size=deg, vertex_font_size=6, vertex_text=u.vp['vtext'], vertex_text_position=0, vertex_fill_color=u.vp['vcolor'], #touch_v,
           vcmap=matplotlib.cm.binary, edge_color=u.ep['ecolor'],
           edge_control_points=control, # some curvy edges
           edge_pen_width=u.ep['ewidth'],
           edge_text = u.ep['fluxstr'],
           edge_font_size=4,
           output="astar-delaunay_filtered.pdf")

from subprocess import call
call(["open", "astar-delaunay_filtered.pdf"])

interactive_window(u, pos=layout_pos, geometry=(500,500), vertex_size=deg, vertex_font_size=8, vertex_text=u.vp['vtext'], vertex_text_position=0, vertex_fill_color=u.vp['vcolor'], #touch_v
           edge_color=u.ep['ecolor'],
           edge_control_points=control, # some curvy edges
           edge_pen_width=u.ep['ewidth'],
           edge_text = u.ep['fluxstr'],
           edge_font_size=7,
           update_layout=False,
           vcmap=matplotlib.cm.binary)
#graph_draw(g, pos=layout_pos, output_size=(600, 600), vertex_size=vsize, vertex_font_size=6, vertex_text=vtext, vertex_fill_color=vcolor, #touch_v,
#           vcmap=matplotlib.cm.binary, edge_color=ecolor,
#           edge_control_points=control, # some curvy edges
#           edge_pen_width=ewidth, output="astar-delaunay.pdf")

'''

'''
deg = g.degree_property_map("in")
#edge_flux = g.edge_properties['flux']

deg.a = 4 * (sqrt(deg.a) * 0.5 + 0.4)
#deg[g.vertex(0)] = deg.a.max()
#deg[g.vertex(1)] = deg.a.max()

u = GraphView(g, vfilt=lambda v: deg.a[v] <= deg.a.max()/4.0, efilt=lambda e: )

ebet = betweenness(u)[1]
eflux = u.edge_properties["flux"]
vid = u.vertex_properties["v_id"]
vtype = u.vertex_properties["v_type"]
vcolor = u.vertex_properties["v_color"]
ebet.a /= ebet.a.max()/10.0
eflux.a = (eflux.a/(eflux.a.max()/1.0))**2.0
#eflux.a *= eflux.a
eorder = ebet.copy()
#eorder = eflux.copy()
eorder.a *= 1
vorder = deg.copy()
vorder.a *= -1


#res = max_independent_vertex_set(g)

# draw src and tgt vertices last
vorder[srcv] = vorder.a.max()
vorder[tgtv] = vorder.a.max()





#s_dist_vlist = shortest_distance(g, srcv, tgtv, revflux, directed=True)
#g.set_vertex_filter(shortest_dist)
#s_path_vlist, s_path_elist = shortest_path(g, srcv, tgtv, revflux)
#u = GraphView(g, efilt=lambda e: e in s_path_elist)
#g.set_edge_filter(s_path_elist)




#res = push_relabel_max_flow(g, source=srcv, target=tgtv, capacity=flux)
#res = push_relabel_max_flow(g, source=srcv, target=tgtv, capacity=flux)
#res.a = flux.a - res.a # the actual flow
#max_flow = sum(res[e] for e in tgtv.in_edges())
#print 'max_flow: ' + str(max_flow)

#graph_draw(g, pos=layout_pos, vertex_size=deg, vertex_fill_color=deg, vorder=deg,
#           edge_color=ebet, eorder=eorder, edge_pen_width=ebet,
#           edge_control_points=control, # some curvy edges
#           output="graph-draw.pdf")

graph_draw(u, pos=layout_pos, vertex_size=deg, vertex_fill_color=vcolor, vorder=vorder,
           edge_color=ebet, eorder=eorder, edge_pen_width=prop_to_size(eflux, mi=0, ma=eflux.a.max(), power=1),
           edge_control_points=control, # some curvy edges
           output="graph-draw_filtered.pdf")

g.set_vertex_filter(None)
g.set_edge_filter(None)

graph_draw(u, pos=layout_pos, vertex_size=deg, vertex_fill_color=vcolor, vorder=vorder,
           edge_color=ebet, eorder=eorder, edge_pen_width=flux,
           edge_control_points=control, # some curvy edges
           output="graph-draw_nonfiltered.pdf")


#g.save("bsu_met_network.xml.gz")
#graph_draw(g, pos=layout_pos, output="bsu_met_network_arf.pdf")

'''




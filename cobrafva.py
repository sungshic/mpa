__author__ = 'spark'

from cobra.core import Metabolite
from math import floor, ceil

def flux_variability_analysis(cobra_model, fraction_of_optimum=1.,
                              objective_sense='maximize', the_reactions=None,
                              allow_loops=True, solver='glpk',
                              the_problem='return', tolerance_optimality=1e-6,
                              tolerance_feasibility=1e-6, tolerance_barrier=1e-8,
                              lp_method=1, lp_parallel=0, new_objective=None,
                              relax_b=None, error_reporting=None,
                              number_of_processes=1, copy_model=True):
    """Runs flux variability analysis on a cobra.Model object

    cobra_model: a Model object

    fraction_of_optimum: fraction of the optimal solution that must be realized

    the_reactions: list of reactions to run FVA on.  if None then run on all
    reactions in the Model

    allow_loops:  Not Implemented.  If false then run the simulations with the
    loop law method to remove loops.

    the_problem: If 'return' or an LP model object for the specified solver then
    the optimizations will be sped up by attempting to use a previous solution
    as a starting point to optimize the current problem.  Can reduce
    simulation time by over an order of magnitude.

    solver: 'glpk', 'gurobi', or 'cplex'.

    the_problem: a problem object for the corresponding solver, 'return', or
    a float representing the wt_solution

    number_of_processes: If greater than 1 then this function will attempt
    to parallelize the problem.  NOTE: Currently not functional


    returns a dictionary: {reaction.id: {'maximum': float, 'minimum': float}}

    TODO: update how Metabolite._bound is handled so we can set a range instead
    of just a single value.  This will be done in cobra.flux_analysis.solvers.

    """
     #Need to copy the model because we're updating reactions.  However,
    #we can always just remove them.
    if isinstance(the_problem, float):
        wt_solution = the_problem
        the_problem='return'
    else:
        wt_model = cobra_model
        wt_sol = wt_model.optimize(solver=solver,objective_sense='maximize',
                          tolerance_optimality=tolerance_optimality,
                          tolerance_feasibility=tolerance_feasibility,
                          tolerance_barrier=tolerance_barrier,
                          lp_method=lp_method, lp_parallel=lp_parallel,
                          the_problem=the_problem, new_objective=new_objective)
        wt_solution = wt_model.solution.f
    if copy_model:
        cobra_model = cobra_model.copy()
    if not the_reactions:
        the_reactions = cobra_model.reactions
    else:
        if hasattr(the_reactions[0], 'id'):
            #Because cobra_model = cobra_model.copy() any cobra.Reactions
            #from the input won't point to cobra_model
            the_reactions = [x.id for x in the_reactions]
        #
        the_reactions = map(cobra_model.reactions.get_by_id, the_reactions)


    #Basically, add a virtual metabolite that reflects the
    #objective coefficeints and the solution
    objective_metabolite = Metabolite('objective')
    if not copy_model:
        original_objectives = dict([(k, float(k.objective_coefficient))
                                    for k in cobra_model.reactions])

    [x.add_metabolites({objective_metabolite: x.objective_coefficient})
     for x in cobra_model.reactions if x.objective_coefficient != 0]

    #TODO: Kick back an error if cobra_model.solution.status is not optimal
    if objective_sense == 'maximize':
        objective_cutoff = floor(wt_solution/tolerance_optimality)*\
                           tolerance_optimality*fraction_of_optimum
        objective_metabolite._constraint_sense = 'G'
    else:
        objective_cutoff = ceil(wt_solution/tolerance_optimality)*\
                           tolerance_optimality*fraction_of_optimum
        objective_metabolite._constraint_sense = 'L'
    objective_metabolite._bound = objective_cutoff
    #If objective_metabolite._model is None then we should cycle through
    #each reaction as the initial objective.
    ##if the_problem:
    ##    the_problem = cobra_model.optimize(solver=solver,
    ##                                       objective_sense='maximize',
    ##                                       tolerance_optimality=tolerance_optimality,
    ##                                       tolerance_feasibility=tolerance_feasibility,
    ##                                       tolerance_barrier=tolerance_barrier,
    ##                                       lp_method=lp_method, lp_parallel=lp_parallel,
    ##                                       the_problem='return', relax_b=relax_b,
    ##                                       error_reporting=error_reporting)
    if the_problem:
        the_problem = wt_sol
    variability_dict = {}
    the_sense_dict = {'maximize': 'maximum',
                      'minimize': 'minimum'}
    basic_problem = the_problem
    #Adding in solver-specific code could improve the speed substantially.
    for the_reaction in the_reactions:
        tmp_dict = {}
        the_problem = basic_problem
        for the_sense, the_description in the_sense_dict.iteritems():
            the_problem = cobra_model.optimize(solver=solver,
                                               new_objective=the_reaction,
                                               objective_sense=the_sense,
                                               tolerance_optimality=tolerance_optimality,
                                               tolerance_feasibility=tolerance_feasibility,
                                               tolerance_barrier=tolerance_barrier,
                                               lp_method=lp_method,
                                               lp_parallel=lp_parallel,
                                               the_problem=the_problem,
                                               error_reporting=error_reporting,
                                               update_problem_reaction_bounds=False)
            tmp_dict[the_description] = cobra_model.solution.f
            print cobra_model.solution.f
        variability_dict[the_reaction.id] = tmp_dict
    if not copy_model:
        [setattr(k, 'objective_coefficient', v)
         for k, v in original_objectives.iteritems()]
        objective_metabolite.remove_from_model()

    return variability_dict



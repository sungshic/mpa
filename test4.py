__author__ = 'spark'

from metpathdictionary import MetPathDict
from graphvis.mpavisualizer import MPAVisualizer
from graphvis.pdftemplate import CrodaMPAReportTemplate



'''
# initialize metabolic cmp and rxn lookup dictionary
# NOTE: a lil' ugly marriage to a single genome scale model, but works for the croda project in the mean time.
met_path_dict_cmplist = 'data/cpdlist2.csv'
met_path_dict_rxnlist_v1 = 'data/rxnlist2.csv'
met_path_dict_rxnlist_v2extra = 'data/rxnlist_nar.csv'
metdict = MetPathDict(met_path_dict_cmplist, met_path_dict_rxnlist_v1)
metdict2 = MetPathDict(met_path_dict_cmplist, met_path_dict_rxnlist_v2extra)
metdict.mergeRxnLookupTable(metdict2._rxnDict) # merge extra rxns from v2 to v1

graph_file = "graphvis/wt_oxy1.5_dual_shikioff/bsu_met_network_bio+tgt_shikioff_0.xml.gz"
graph_lookup_file = "graphvis/wt_oxy1.5_dual_shikioff/bsu_met_network_bio+tgt_shikioff_0_vid_vidx.json"
filenamebase = "graphvis/wt_oxy1.5_dual_shikioff/bsu_met_network_bio+tgt_shikioff_0"
layout_pos_file = 'graphvis/wt_oxy1.5_dual_shikioff/bsu_met_network_bio+tgt+shikioff_0_layoutcoord.json'
is_save_layoutfile = True

mpa1 = MPAVisualizer()

mpa1._initializeGraphData(graph_file, graph_lookup_file, metdict, layout_pos_file, is_save_layoutfile)
pdffilename1 = mpa1.draw_graph_vlist(filenamebase, layout_pos_file, is_save_layoutfile)


graph_file = "graphvis/wt_oxy1.5_dual_shikion/bsu_met_network_bio+tgt_shikion_0.xml.gz"
graph_lookup_file = "graphvis/wt_oxy1.5_dual_shikion/bsu_met_network_bio+tgt_shikion_0_vid_vidx.json"
filenamebase = "graphvis/wt_oxy1.5_dual_shikion/bsu_met_network_bio+tgt_shikion_0"
layout_pos_file = 'graphvis/wt_oxy1.5_dual_shikioff/bsu_met_network_bio+tgt+shikioff_0_layoutcoord.json'
is_save_layoutfile = False

mpa2 = MPAVisualizer()

mpa2._initializeGraphData(graph_file, graph_lookup_file, metdict, layout_pos_file, is_save_layoutfile)
pdffilename2 = mpa2.draw_graph_vlist(filenamebase, layout_pos_file, is_save_layoutfile)


layout_pos_file = 'graphvis/wt_oxy1.5_dual_shikion/bsu_met_network_bio+tgt+shikion_0_layoutcoord.json'
is_save_layoutfile = True
pdffilename3 = mpa2.draw_graph_vlist(filenamebase, layout_pos_file, is_save_layoutfile)

graph_pdf_files = [pdffilename1, pdffilename2, pdffilename3]

#print mergerpages
'''

graph_pdf_files = ['graphvis/wt_oxy1.5_dual_shikioff/bsu_met_network_bio+tgt_shikioff_0_1785541245594011467.pdf',
                   'graphvis/wt_oxy1.5_dual_shikion/bsu_met_network_bio+tgt_shikion_0_9023311053591387160_overlayed.pdf',
                   'graphvis/wt_oxy1.5_dual_shikion/bsu_met_network_bio+tgt_shikion_0_-616815674993107792.pdf']

croda_mpa_report = CrodaMPAReportTemplate("test.pdf")
fig_file = "stat_data/2014_02_06_wildtypes_oxy1.5.png"
#fig_file = "graphvis/wt_oxy1.5_dual_shikioff/bsu_met_network_bio+tgt_shikioff_0_1785541245594011467.pdf"
page_text = "Experimental conditions (all under aerobic growth): <br/>\
    #0: Glucose + Ammonium sulfate \
    <font color='red'>#1: Malate + Ammonium sulfate</font> \
    #6: Glucose + Malate + Ammonium sulfate \
    #10: Glucose + L-Glutamate + L-Glutamine + Ammonium sulfate \
    #11: Malate + L-Glutamate + L-Glutamine + Ammonium sulfate \
    #12: Glucose + L-Glutamate + Ammonium sulfate \
    #13: Glucose + L-Glutamine + Ammonium sulfate \
    #14: Malate + L-Glutamate + Ammonium sulfate \
    #15: Malate + L-Glutamine + Ammonium sulfate \
    #17: Glucose + L-arginine + ammonium sulfate \
    #18: Malate + L-arginine + ammonium sulfate \
    #19: Malate + L-arginine + L-glutamine + ammonium sulfate \
    #20: Malate + L-arginine + L-glutamate + ammonium sulfate \
    #21: Glucose + L-arginine + L-glutamine + ammonium sulfate \
    #22: Glucose + L-arginine + L-glutamate + ammonium sulfate"
page_heading = "Experimental conditions"
fig_heading = "Fluxes of shinorine vs biomass across experimental conditions"
fig_caption = ""

croda_mpa_report.addATitlePage("comparing the experimental conditions #0 shiki-on/off")
croda_mpa_report.addAFigPage(page_heading, page_text, fig_heading, fig_file, fig_caption)
croda_mpa_report.addAContentPage("Shiki-off", "test page 2",graph_pdf_files[0]) # page 2
croda_mpa_report.addAContentPage("Shiki-on using the shiki-off layout", "test page 3", graph_pdf_files[1]) # page 3
croda_mpa_report.addAContentPage("Shiki-on", "test page 4", graph_pdf_files[2]) # page 4
croda_mpa_report.createATemplatePDF()
croda_mpa_report.saveMPAGraphs("test2.pdf")

'''
pdf_writer = PdfFileWriter()
pdftemplate1 = PdfFileReader(file("mintoc2.pdf", "rb"))

mergerpages = [pdffilename1, pdffilename2, pdffilename3]
print mergerpages
for idx, p in enumerate(pdftemplate1.pages):
    if idx > 0:
        inputfile = PdfFileReader(file(mergerpages[idx-1], "rb"))
        mp = inputfile.getPage(0)
        mp.scaleTo(float(p.mediaBox.getWidth()), float(p.mediaBox.getHeight()))
        p.mergePage(mp)
    pdf_writer.addPage(p)

outputStream = file("test.pdf", "wb")
pdf_writer.write(outputStream)
outputStream.close()

'''

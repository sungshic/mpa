# -*- mode: python -*-
a = Analysis(['test_installer.py'],
             pathex=['/vagrant/Documents/workspace/CrodaMPA',
		'/home/vagrant/.virtualenv/pycobra/lib/python2.7/site-packages/cobra/solvers/'],
             hiddenimports=[],
             hookspath=['hooks'],
             runtime_hooks=None)
pyz = PYZ(a.pure)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='test_installer',
          debug=False,
          strip=None,
          upx=True,
          console=True )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=None,
               upx=True,
               name='test_installer')

__author__ = 'Jurijs Meitalovs'

import cobra
import re

from libsbml import *
from cobra.io.sbml import *
from cobra.io import read_sbml_model, write_sbml_model

sbml_file = "data/iBsu1103V2/nar-01731-m-2012-File013.xml"
sb_model = create_cobra_model_from_sbml_file(sbml_file)

#AVA_3858
#C7H15O10P -> C7H10O5 + HPO4 + H2O + 2H+
#SHP -> DDG (2-demethyl-4-deoxygadusol)+ Pi + H2O + 2H+
rxn_S00100 = Reaction('rxn_S00100')
rxn_S00100.name = 'Sedoheptulose7-phosphate_2-demethyl-4-deoxygadusol'
rxn_S00100.subsystem = 'c'
rxn_S00100.lower_bound = -1000. #This is the default
rxn_S00100.upper_bound = 1000. #This is the default
rxn_S00100.reversibility = 1 #This is the default
rxn_S00100.objective_coefficient = 0 #this is the default

cpd00238_c = sb_model.metabolites.get_by_id('cpd00238_c') #Sedoheptulose7-phosphate
cpd00009_c = sb_model.metabolites.get_by_id('cpd00009_c') #Phosphate (Pi)
cpd00001_c = sb_model.metabolites.get_by_id('cpd00001_c') #H20
cpd00067_c = sb_model.metabolites.get_by_id('cpd00067_c') #H+
cpd_S00100_c = Metabolite('cpd_S00100_c', formula='C7H10O5', name='2-demethyl-4-deoxygadusol', compartment='c') #DDG

#add the metabolites to the reaction:
rxn_S00100.add_metabolites({cpd00238_c: -1.0,
                          cpd00009_c: 1.0,
                          cpd00001_c: 1.0,
                          cpd00067_c: 2.0,
                          cpd_S00100_c : 1.0})

sb_model.add_reaction(rxn_S00100)

#AVA_3857
#C7H10O5 + CO2 + 2H2O -> C8H12O5 + 2O2 +2H+
#DDG + CO2 + 2H2O -> 4-DG + 2O2 + 2H+
rxn_S00110 = Reaction('rxn_S00110')
rxn_S00110.name = '2-demethyl-4-deoxygadusol_4-deoxygadusol'
rxn_S00110.subsystem = 'c'
rxn_S00110.lower_bound = -1000. #This is the default
rxn_S00110.upper_bound = 1000. #This is the default
rxn_S00110.reversibility = 1 #This is the default
rxn_S00110.objective_coefficient = 0 #this is the default

#Create the metabolites
cpd_S00100_c = sb_model.metabolites.get_by_id('cpd_S00100_c') #DDG
cpd00001_c = sb_model.metabolites.get_by_id('cpd00001_c') #H20
cpd00011_c = sb_model.metabolites.get_by_id('cpd00011_c') #CO2
cpd00007_c = sb_model.metabolites.get_by_id('cpd00007_c') #O2
cpd00067_c = sb_model.metabolites.get_by_id('cpd00067_c') #H+
cpd_S00110_c = Metabolite('cpd_S00110_c', formula='C8H12O5', name='4-deoxygadusol', compartment='c') #4DG

#add the metabolites to the reaction:
rxn_S00110.add_metabolites({cpd_S00100_c : -1.0,
                          cpd00001_c: -1.0,
                          cpd00011_c: -1.0,
                          cpd00007_c: 2.0,
                          cpd00067_c: 2.0,
                          cpd_S00110_c: 1.0})

sb_model.add_reaction(rxn_S00110)


#AVA_3856
#C8H12O5 + C10H16N5O13P3 (C10H13N5O13P3 in the model)-> C8H12O8P + C10H15N5O10P2 (C10H13N5O10P2 in the model)
#4-DG + ATP -> 4-DG_ATP + ADP
rxn_S00120 = Reaction('rxn_S00120')
rxn_S00120.name = '4-deoxygadusol_4-deoxygadusol-ATP'
rxn_S00120.subsystem = 'c'
rxn_S00120.lower_bound = -1000. #This is the default
rxn_S00120.upper_bound = 1000. #This is the default
rxn_S00120.reversibility = 1 #This is the default
rxn_S00120.objective_coefficient = 0 #this is the default

#Create the metabolites
cpd_S00110_c = sb_model.metabolites.get_by_id('cpd_S00110_c') #4-DG
cpd00002_c = sb_model.metabolites.get_by_id('cpd00002_c') #ATP
cpd00008_c = sb_model.metabolites.get_by_id('cpd00008_c') #ADP
cpd_S00120_c = Metabolite('cpd_S00120_c', formula='C8H12O8P', name='4-deoxygadusol-phosphate', compartment='c') #4-DG-ATP

#add the metabolites to the reaction:
rxn_S00120.add_metabolites({cpd_S00110_c: -1.0,
                          cpd00002_c: -1.0,
                          cpd00008_c: 1.0,
                          cpd_S00120_c: 1.0})

sb_model.add_reaction(rxn_S00120)


#AVA_3856
#C8H12O8P + C2H5NO2 -> C10H15O6N + HPO4 + H+
#4-DG-ATP + Glycine -> Mycosporine-Gly + Pi + H+
rxn_S00130 = Reaction('rxn_S00130')
rxn_S00130.name = '4-deoxygadusol-ATP_Mycosporine-Gly'
rxn_S00130.subsystem = 'c'
rxn_S00130.lower_bound = -1000. #This is the default
rxn_S00130.upper_bound = 1000. #This is the default
rxn_S00130.reversibility = 1 #This is the default
rxn_S00130.objective_coefficient = 0 #this is the default

#Create the metabolites
cpd_S00120_c = sb_model.metabolites.get_by_id('cpd_S00120_c') #4-DG-ATP
cpd00033_c = sb_model.metabolites.get_by_id('cpd00033_c') #Glycine
cpd00009_c = sb_model.metabolites.get_by_id('cpd00009_c') #Phosphate (Pi)
cpd00067_c = sb_model.metabolites.get_by_id('cpd00067_c') #H+
cpd_S00130_c = Metabolite('cpd_S00130_c', formula='C10H15O6N', name='Mycosporine-Gly', compartment='c') #Mycosporine-Gly

#add the metabolites to the reaction:
rxn_S00130.add_metabolites({cpd_S00120_c: -1.0,
                          cpd00033_c: -1.0,
                          cpd00009_c: 1.0,
                          cpd00067_c: 1.0,
                          cpd_S00130_c: 1.0})

sb_model.add_reaction(rxn_S00130)

#AVA_3855
#C10H15O6N + C3H7NO3 ->C13O8H20N2 + H2O
#Mycosporine-Gly + Serine -> Shinorine + H2O
rxn_S00140 = Reaction('rxn_S00140')
rxn_S00140.name = 'Mycosporine-Gly_Shinorine'
rxn_S00140.subsystem = 'c'
rxn_S00140.lower_bound = -1000. #This is the default
rxn_S00140.upper_bound = 1000. #This is the default
rxn_S00140.reversibility = 1 #This is the default
rxn_S00140.objective_coefficient = 0 #this is the default

#Create the metabolites
cpd_S00130_c = sb_model.metabolites.get_by_id('cpd_S00130_c') #Mycosporine-Gly
cpd00054_c = sb_model.metabolites.get_by_id('cpd00054_c') #Serine
cpd00001_c = sb_model.metabolites.get_by_id('cpd00001_c') #H20
cpd_S00140_c = Metabolite('cpd_S00140_c', formula='C13O8H20N2', name='Shinorine', compartment='c') #Shinorine

#add the metabolites to the reaction:
rxn_S00140.add_metabolites({cpd_S00130_c: -1.0,
                          cpd00054_c: -1.0,
                          cpd00001_c: 1.0,
                          cpd_S00140_c: 1.0})

sb_model.add_reaction(rxn_S00140)


#Shinorine_C <=> Shinorine_E exchange reaction
rxn_S00150 = Reaction('rxn_S00150')
rxn_S00150.name = 'Shinorine_Transport'
rxn_S00150.subsystem = 'Extracellular'
rxn_S00150.lower_bound = -1000. #This is the default
rxn_S00150.upper_bound = 1000. #This is the default
rxn_S00150.reversibility = 1 #This is the default
rxn_S00150.objective_coefficient = 0 #this is the default

cpd_S00140_c = sb_model.metabolites.get_by_id('cpd_S00140_c') #Shinorine C
cpd_S00140_e = Metabolite('cpd_S00140_e', formula='C13O8H20N2', name='Shinorine', compartment='e') #Shinorine E

rxn_S00150.add_metabolites({cpd_S00140_c: -1.0, cpd_S00140_e: 1.0})
sb_model.add_reaction(rxn_S00150)


#Shinorine_E <=> Shinorine_B boundary reaction
rxn_S00150_EX = Reaction('EX_rxn_S00150_e')
rxn_S00150_EX.name = 'Shinorine_Transport_Boundary'
rxn_S00150_EX.subsystem = 'Extracellular'
rxn_S00150_EX.lower_bound = -1000. #This is the default
rxn_S00150_EX.upper_bound = 1000. #This is the default
rxn_S00150_EX.reversibility = 1 #This is the default
rxn_S00150_EX.objective_coefficient = 0 #this is the default

cpd_S00140_e = sb_model.metabolites.get_by_id('cpd_S00140_e') #Shinorine E

rxn_S00150_EX.add_metabolites({cpd_S00140_e: -1.0})
sb_model.add_reaction(rxn_S00150_EX)


#cpd00238_e = Metabolite('cpd00238_e', formula='C7H14O10P', name='acyl-carrier-protein', compartment='Extracellular')
#EX_reaction_to_boundary
#reaction2.add_metabolites({cpd00238_e: 1.0})


#sb_model.reactions.bio00127.objective_coefficient = 0

sb_model_mat = sb_model.to_array_based_model()
sb_model_mat.optimize()

print sb_model_mat.solution

write_cobra_model_to_sbml_file(sb_model, 'data/test_cobra_reactions.xml', 2,  1, print_time=True)
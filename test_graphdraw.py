__author__ = 'spark'

from cobra.io.sbml import *
from graphvis.mpavisualizer import MPAVisualizer
from graphvis.pdftemplate import CrodaMPAReportTemplate
from metpathdictionary import MetPathDict

met_path_dict_cmplist = 'data/cpdlist2.csv'
met_path_dict_rxnlist_v1 = 'data/rxnlist2.csv'
met_path_dict_rxnlist_v2extra = 'data/rxnlist_nar.csv'
metdict = MetPathDict(met_path_dict_cmplist, met_path_dict_rxnlist_v1)
metdict2 = MetPathDict(met_path_dict_cmplist, met_path_dict_rxnlist_v2extra)
metdict.mergeRxnLookupTable(metdict2._rxnDict) # merge extra rxns from v2 to v1


'''
sbml_file = "data/iBsu1103V2/nar-01731-m-2012-File013.xml"
is_tgt_prod_enforced = True
is_shiki_on = True # shiki off
shiki_on = True # shiki on


sb_model = create_cobra_model_from_sbml_file(sbml_file)

#g = load_graph(graph_file)
#jsonf = open(graph_lookup_file)
#vidx_lookup = json.loads(jsonf.read())

#model_profiler = MPAModelProfiler(sb_model)

mediaIdx = 21

setMedia(sb_model, -10, mediaIdx, is_calibrated=0)
sb_model.reactions.bio00127.objective_coefficient = 1
ll_model = sb_model.copy() #ArrayBasedModel(sb_model, deepcopy_model=True)
ll_model_arr = ll_model.to_array_based_model()


genedict_file = 'data/iBsu1103/gb-2009-10-6-r69-s1_genedata.csv'
genedict = GeneDict(genedict_file, sb_model.reactions)

ll_model_arr = ll_model.to_array_based_model() # update the S matrix with the newly populated 'reverse' reactions
model_manager = MPAModelManager(ll_model_arr, genedict, len(sb_model.reactions))
model_manager.convert_to_irreversible()
model_manager._cb_model.update()
# recalculate rxn sets
model_manager.findRxnSet()
model_manager.findMetSet()
model_manager.findBoundaryRxnSet()
model_manager.findTransportRxnSet()
sb_model = ll_model.guided_copy()
'''

graph_file = "/vagrant/Downloads/mpa_pos_only.xml.gz"
graph_lookup_file = "/vagrant/Downloads/mpa_pos_only_vid_vidx.json"


mpa_vis = MPAVisualizer()
print 'initializing graph from graph_tool files...'
#sb_model_copy = sb_model.to_array_based_model(deepcopy_model=True)
wt_biomass_only_graph = mpa_vis.initializeGraphDataFromFile(graph_file, graph_lookup_file, metdict, None, False) #sb_model_copy, metdict, getMediaSrcCmp(mediaIdx), getMediaSrcRxn(mediaIdx))
print 'drawing the graph...'

pdflist = []

pdffilename1, ref_layout = mpa_vis.drawGraphvlist("/vagrant/Downloads/CrodaMPA/results/mpa_pos_only", is_save_layout=True)
mpa_vis_ref = mpa_vis
#wt_biomass_only_graph.saveLabelledGraph("/vagrant/Downloads/CrodaMPA/results/mpa_pos_only")
pdflist.append(pdffilename1)

print 'combining the graph pdf files...'

graph_pdf_files = pdflist
croda_mpa_report = CrodaMPAReportTemplate("test.pdf")
#fig_file = "stat_data/2014_02_06_wildtypes_oxy1.5.png"
#fig_file = "graphvis/wt_oxy1.5_dual_shikioff/bsu_met_network_bio+tgt_shikioff_0_1785541245594011467.pdf"
page_text = "Experimental conditions (all under aerobic growth): <br/>\
    #0: Glucose + Ammonium sulfate \
    <font color='red'>#1: Malate + Ammonium sulfate </font>\
    #6: Glucose + Malate + Ammonium sulfate \
    #10: Glucose + L-Glutamate + L-Glutamine + Ammonium sulfate \
    #11: Malate + L-Glutamate + L-Glutamine + Ammonium sulfate \
    #12: Glucose + L-Glutamate + Ammonium sulfate \
    #13: Glucose + L-Glutamine + Ammonium sulfate \
    #14: Malate + L-Glutamate + Ammonium sulfate \
    #15: Malate + L-Glutamine + Ammonium sulfate \
    #17: Glucose + L-arginine + ammonium sulfate \
    #18: Malate + L-arginine + ammonium sulfate \
    #19: Malate + L-arginine + L-glutamine + ammonium sulfate \
    #20: Malate + L-arginine + L-glutamate + ammonium sulfate \
    #21: Glucose + L-arginine + L-glutamine + ammonium sulfate \
    #22: Glucose + L-arginine + L-glutamate + ammonium sulfate"
page_heading = "Experimental conditions"
fig_heading = "Fluxes of shinorine vs biomass across experimental conditions"
fig_caption = ""

croda_mpa_report.addATitlePage("comparing the experimental conditions #0 shiki-on/off")
#croda_mpa_report.addAFigPage(page_heading, page_text, fig_heading, fig_file, fig_caption)
croda_mpa_report.addAPage(page_heading, page_text)
for pdfidx, pdffile in enumerate(graph_pdf_files):
    croda_mpa_report.addAContentPage("Alt optima " + str(pdfidx), "graph view",pdffile)
croda_mpa_report.createATemplatePDF()
croda_mpa_report.saveMPAGraphs("/vagrant/Downloads/CrodaMPA/results/test_malate_altoptima.pdf")